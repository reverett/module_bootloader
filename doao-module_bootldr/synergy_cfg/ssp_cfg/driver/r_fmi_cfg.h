/* generated configuration header file - do not edit */
#ifndef R_FMI_CFG_H_
#define R_FMI_CFG_H_
#define FMI_CFG_PARAM_CHECKING_ENABLE (BSP_CFG_PARAM_CHECKING_ENABLE)
#define FMI_CFG_USE_CUSTOM_BASE_ADDRESS (0)
#define FMI_CFG_FACTORY_FLASH_BASE_ADDRESS (&g_fmi_data[0])

extern const uint32_t g_fmi_data[];
#endif /* R_FMI_CFG_H_ */
