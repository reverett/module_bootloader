/* generated configuration header file - do not edit */
#ifndef BSP_MCU_DEVICE_PN_CFG_H_
#define BSP_MCU_DEVICE_PN_CFG_H_
#define BSP_MCU_R7FS128783A01CFM
#define BSP_ROM_SIZE_BYTES (262144)
#define BSP_RAM_SIZE_BYTES (24576)
#define BSP_DATA_FLASH_SIZE_BYTES (4096)
#define BSP_PACKAGE_LQFP
#define BSP_PACKAGE_PINS (64)
#endif /* BSP_MCU_DEVICE_PN_CFG_H_ */
