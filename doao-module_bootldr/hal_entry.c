/* HAL-only entry function */
#include "int_prot_frame.h"
#include "hal_entry.h"
#include "hal_data.h"

extern sw_info_struct firmware_info;
// for test
unsigned char testBuffer[1024];
sw_info_struct firmware_info;

void initialize_bootloader_info(void)
{
    firmware_info.app.device_id = MY_ADDR; // load in default address
    firmware_info.app.fw_status = BL_ST_GET_NEW_FW;
    memcpy (&firmware_info.app.fw_info.ascii_info.productID[0], "sim app\0", 9);
    memcpy (&firmware_info.app.fw_info.ascii_info.skew[0], "jfish\0", 6);
    memcpy (&firmware_info.app.fw_info.ascii_info.version[0], "0000001\0", 8); // version 000.00.00

    firmware_info.btldr.device_id = MY_ADDR; // load in default address
    firmware_info.btldr.fw_status = BL_ST_FW_IN_FLASH;
    memcpy (&firmware_info.btldr.fw_info.ascii_info.productID[0], "sim boot\0", 9);
    memcpy (&firmware_info.btldr.fw_info.ascii_info.skew[0], "jfish\0", 6);
    memcpy (&firmware_info.btldr.fw_info.ascii_info.version[0], "0000000\0", 8); // version 000.00.00
}

record_status_t write_header_to_flash(uint8_t update_status)
{
    record_status_t rec_status = PRC_REC_RCVD_OK;

#if 1
    ssp_err_t err = SSP_SUCCESS;
    flash_result_t result;
    unsigned char *ptr_data_flash_buff = &firmware_info;
    // set our flag
    firmware_info.app.fw_status = update_status;

    //Erase application area
    err = g_flash0.p_api->erase(g_flash0.p_ctrl, DATAFLASH_ADDRESS, sizeof(fw_update_flash_struct));
    if (SSP_SUCCESS != err)
    {
        rec_status = PRC_REC_ERASE_FAIL;
    }
    else
    {
        // Blank check
        err = g_flash0.p_api->blankCheck(g_flash0.p_ctrl, DATAFLASH_ADDRESS, sizeof(fw_update_flash_struct), &result);
        if ((SSP_SUCCESS != err) || (FLASH_RESULT_BLANK != result))
        {
            rec_status = PRC_REC_BLANK_FAIL;
        }
        else
        {
            // Set destination address
            err = g_flash0.p_api->write(g_flash0.p_ctrl, (uint32_t)ptr_data_flash_buff, DATAFLASH_ADDRESS, sizeof(fw_update_flash_struct));
            if (SSP_SUCCESS != err)
            {
                rec_status = PRC_REC_FLASH_FAIL;
            }
            else // for test read the data in flash
            {
                err = g_flash0.p_api->read(g_flash0.p_ctrl, testBuffer, DATAFLASH_ADDRESS, sizeof(fw_update_flash_struct));
            }
        }
    }
#endif
    return(rec_status);
}

void hal_entry(void)
{
    flash_result_t err;
    // for test
    // load data flash struct for bootloader into RAM
    initialize_bootloader_info();

#if 0
    // read data flash struct for application into RAM
    // check to see if application requested new image be downloaded
    err = g_flash0.p_api->read(g_flash0.p_ctrl, &firmware_info, DATAFLASH_ADDRESS, sizeof(fw_update_flash_struct));
    if (SSP_SUCCESS != err)
    {
        while(1);
    }
#endif
}
