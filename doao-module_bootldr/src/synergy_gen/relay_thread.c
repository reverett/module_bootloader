/* generated thread source file - do not edit */
#include "relay_thread.h"

TX_THREAD relay_thread;
void relay_thread_create(void);
static void relay_thread_func(ULONG thread_input);
/** Alignment requires using pragma for IAR. GCC is done through attribute. */
#if defined(__ICCARM__)
#pragma data_alignment = BSP_STACK_ALIGNMENT
#endif
static uint8_t relay_thread_stack[1024] BSP_PLACE_IN_SECTION(".stack.relay_thread") BSP_ALIGN_VARIABLE(BSP_STACK_ALIGNMENT);
#if (2) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_adc0) && !defined(SSP_SUPPRESS_ISR_ADC0)
SSP_VECTOR_DEFINE_CHAN(adc_scan_end_isr, ADC, SCAN_END, 0);
#endif
#endif
#if (BSP_IRQ_DISABLED) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_adc0) && !defined(SSP_SUPPRESS_ISR_ADC0)
SSP_VECTOR_DEFINE_CHAN(adc_scan_end_b_isr, ADC, SCAN_END_B, 0);
#endif
#endif
#ifndef adc_callback
void adc_callback(adc_callback_args_t * p_args);
#endif
adc_instance_ctrl_t g_adc0_ctrl;
const adc_cfg_t g_adc0_cfg = { .unit = 0, .mode = ADC_MODE_SINGLE_SCAN,
		.resolution = ADC_RESOLUTION_12_BIT, .alignment = ADC_ALIGNMENT_RIGHT,
		.add_average_count = ADC_ADD_OFF, .clearing = ADC_CLEAR_AFTER_READ_ON,
		.trigger = ADC_TRIGGER_SOFTWARE,
		.trigger_group_b = ADC_TRIGGER_SYNC_ELC, .p_callback = adc_callback,
		.p_context = &g_adc0, .scan_end_ipl = (2), .scan_end_b_ipl =
				(BSP_IRQ_DISABLED), };
const adc_channel_cfg_t g_adc0_channel_cfg = { .scan_mask = (uint32_t)(
		((uint64_t) ADC_MASK_CHANNEL_0) | ((uint64_t) ADC_MASK_CHANNEL_1)
				| ((uint64_t) ADC_MASK_CHANNEL_2)
				| ((uint64_t) ADC_MASK_CHANNEL_3)
				| ((uint64_t) ADC_MASK_CHANNEL_4) | ((uint64_t) 0)
				| ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
				| ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
				| ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
				| ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
				| ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
				| ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
				| ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
				| ((uint64_t) 0) | ((uint64_t) 0) | (0)),
/** Group B channel mask is right shifted by 32 at the end to form the proper mask */
.scan_mask_group_b = (uint32_t)(
		(((uint64_t) ADC_MASK_CHANNEL_0) | ((uint64_t) ADC_MASK_CHANNEL_1)
				| ((uint64_t) ADC_MASK_CHANNEL_2)
				| ((uint64_t) ADC_MASK_CHANNEL_3)
				| ((uint64_t) ADC_MASK_CHANNEL_4) | ((uint64_t) 0)
				| ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
				| ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
				| ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
				| ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
				| ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
				| ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
				| ((uint64_t) 0) | ((uint64_t) 0) | ((uint64_t) 0)
				| ((uint64_t) 0) | ((uint64_t) 0) | (0)) >> 32),
		.priority_group_a = ADC_GROUP_A_PRIORITY_OFF, .add_mask = (uint32_t)(
				(0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0)
						| (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0)
						| (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0) | (0)
						| (0)), .sample_hold_mask = (uint32_t)((0) | (0) | (0)),
		.sample_hold_states = 24, };
/* Instance structure to use this module. */
const adc_instance_t g_adc0 = { .p_ctrl = &g_adc0_ctrl, .p_cfg = &g_adc0_cfg,
		.p_channel_cfg = &g_adc0_channel_cfg, .p_api = &g_adc_on_adc };
#if (1) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_timer4_pwm_1_2) && !defined(SSP_SUPPRESS_ISR_GPT4)
SSP_VECTOR_DEFINE_CHAN(gpt_counter_overflow_isr, GPT, COUNTER_OVERFLOW, 4);
#endif
#endif
static gpt_instance_ctrl_t g_timer4_pwm_1_2_ctrl;
static const timer_on_gpt_cfg_t g_timer4_pwm_1_2_extend =
		{ .gtioca = { .output_enabled = true, .stop_level =
				GPT_PIN_LEVEL_RETAINED }, .gtiocb = { .output_enabled = true,
				.stop_level = GPT_PIN_LEVEL_RETAINED } };
static const timer_cfg_t g_timer4_pwm_1_2_cfg = { .mode = TIMER_MODE_PWM,
		.period = 25, .unit = TIMER_UNIT_PERIOD_USEC, .duty_cycle = 50,
		.duty_cycle_unit = TIMER_PWM_UNIT_RAW_COUNTS, .channel = 4, .autostart =
				true, .p_callback = pwm4_callback, .p_context =
				&g_timer4_pwm_1_2, .p_extend = &g_timer4_pwm_1_2_extend,
		.irq_ipl = (1), };
/* Instance structure to use this module. */
const timer_instance_t g_timer4_pwm_1_2 = { .p_ctrl = &g_timer4_pwm_1_2_ctrl,
		.p_cfg = &g_timer4_pwm_1_2_cfg, .p_api = &g_timer_on_gpt };
#if (1) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_timer1_pwm_3_4) && !defined(SSP_SUPPRESS_ISR_GPT1)
SSP_VECTOR_DEFINE_CHAN(gpt_counter_overflow_isr, GPT, COUNTER_OVERFLOW, 1);
#endif
#endif
static gpt_instance_ctrl_t g_timer1_pwm_3_4_ctrl;
static const timer_on_gpt_cfg_t g_timer1_pwm_3_4_extend =
		{ .gtioca = { .output_enabled = true, .stop_level =
				GPT_PIN_LEVEL_RETAINED }, .gtiocb = { .output_enabled = true,
				.stop_level = GPT_PIN_LEVEL_RETAINED } };
static const timer_cfg_t g_timer1_pwm_3_4_cfg = { .mode = TIMER_MODE_PWM,
		.period = 25, .unit = TIMER_UNIT_PERIOD_USEC, .duty_cycle = 50,
		.duty_cycle_unit = TIMER_PWM_UNIT_RAW_COUNTS, .channel = 1, .autostart =
				true, .p_callback = pwm1_callback, .p_context =
				&g_timer1_pwm_3_4, .p_extend = &g_timer1_pwm_3_4_extend,
		.irq_ipl = (1), };
/* Instance structure to use this module. */
const timer_instance_t g_timer1_pwm_3_4 = { .p_ctrl = &g_timer1_pwm_3_4_ctrl,
		.p_cfg = &g_timer1_pwm_3_4_cfg, .p_api = &g_timer_on_gpt };
#if (2) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_timer0) && !defined(SSP_SUPPRESS_ISR_GPT0)
SSP_VECTOR_DEFINE_CHAN(gpt_counter_overflow_isr, GPT, COUNTER_OVERFLOW, 0);
#endif
#endif
static gpt_instance_ctrl_t g_timer0_ctrl;
static const timer_on_gpt_cfg_t g_timer0_extend = { .gtioca = {
		.output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW }, .gtiocb = {
		.output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW } };
static const timer_cfg_t g_timer0_cfg = { .mode = TIMER_MODE_PERIODIC, .period =
		0xffffffff, .unit = TIMER_UNIT_PERIOD_RAW_COUNTS, .duty_cycle = 50,
		.duty_cycle_unit = TIMER_PWM_UNIT_RAW_COUNTS, .channel = 0, .autostart =
				false, .p_callback = gp_timer0_callback, .p_context = &g_timer0,
		.p_extend = &g_timer0_extend, .irq_ipl = (2), };
/* Instance structure to use this module. */
const timer_instance_t g_timer0 = { .p_ctrl = &g_timer0_ctrl, .p_cfg =
		&g_timer0_cfg, .p_api = &g_timer_on_gpt };
#if (2) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_external_irq7) && !defined(SSP_SUPPRESS_ISR_ICU7)
SSP_VECTOR_DEFINE( icu_irq_isr, ICU, IRQ7);
#endif
#endif
static icu_instance_ctrl_t g_external_irq7_ctrl;
static const external_irq_cfg_t g_external_irq7_cfg = { .channel = 7, .trigger =
		EXTERNAL_IRQ_TRIG_FALLING, .filter_enable = false, .pclk_div =
		EXTERNAL_IRQ_PCLK_DIV_BY_64, .autostart = true, .p_callback =
		irq7_capture_zc4, .p_context = &g_external_irq7, .p_extend = NULL,
		.irq_ipl = (2), };
/* Instance structure to use this module. */
const external_irq_instance_t g_external_irq7 = { .p_ctrl =
		&g_external_irq7_ctrl, .p_cfg = &g_external_irq7_cfg, .p_api =
		&g_external_irq_on_icu };
#if (2) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_external_irq6) && !defined(SSP_SUPPRESS_ISR_ICU6)
SSP_VECTOR_DEFINE( icu_irq_isr, ICU, IRQ6);
#endif
#endif
static icu_instance_ctrl_t g_external_irq6_ctrl;
static const external_irq_cfg_t g_external_irq6_cfg = { .channel = 6, .trigger =
		EXTERNAL_IRQ_TRIG_FALLING, .filter_enable = false, .pclk_div =
		EXTERNAL_IRQ_PCLK_DIV_BY_64, .autostart = true, .p_callback =
		irq6_capture_zc3, .p_context = &g_external_irq6, .p_extend = NULL,
		.irq_ipl = (2), };
/* Instance structure to use this module. */
const external_irq_instance_t g_external_irq6 = { .p_ctrl =
		&g_external_irq6_ctrl, .p_cfg = &g_external_irq6_cfg, .p_api =
		&g_external_irq_on_icu };
#if (2) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_external_irq5) && !defined(SSP_SUPPRESS_ISR_ICU5)
SSP_VECTOR_DEFINE( icu_irq_isr, ICU, IRQ5);
#endif
#endif
static icu_instance_ctrl_t g_external_irq5_ctrl;
static const external_irq_cfg_t g_external_irq5_cfg = { .channel = 5, .trigger =
		EXTERNAL_IRQ_TRIG_FALLING, .filter_enable = false, .pclk_div =
		EXTERNAL_IRQ_PCLK_DIV_BY_64, .autostart = false, .p_callback =
		irq5_capture_zc2, .p_context = &g_external_irq5, .p_extend = NULL,
		.irq_ipl = (2), };
/* Instance structure to use this module. */
const external_irq_instance_t g_external_irq5 = { .p_ctrl =
		&g_external_irq5_ctrl, .p_cfg = &g_external_irq5_cfg, .p_api =
		&g_external_irq_on_icu };
#if (2) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_external_irq4) && !defined(SSP_SUPPRESS_ISR_ICU4)
SSP_VECTOR_DEFINE( icu_irq_isr, ICU, IRQ4);
#endif
#endif
static icu_instance_ctrl_t g_external_irq4_ctrl;
static const external_irq_cfg_t g_external_irq4_cfg = { .channel = 4, .trigger =
		EXTERNAL_IRQ_TRIG_FALLING, .filter_enable = false, .pclk_div =
		EXTERNAL_IRQ_PCLK_DIV_BY_64, .autostart = false, .p_callback =
		irq4_capture_zc1, .p_context = &g_external_irq4, .p_extend = NULL,
		.irq_ipl = (2), };
/* Instance structure to use this module. */
const external_irq_instance_t g_external_irq4 = { .p_ctrl =
		&g_external_irq4_ctrl, .p_cfg = &g_external_irq4_cfg, .p_api =
		&g_external_irq_on_icu };
#if (2) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_timer3) && !defined(SSP_SUPPRESS_ISR_GPT3)
SSP_VECTOR_DEFINE_CHAN(gpt_counter_overflow_isr, GPT, COUNTER_OVERFLOW, 3);
#endif
#endif
static gpt_instance_ctrl_t g_timer3_ctrl;
static const timer_on_gpt_cfg_t g_timer3_extend = { .gtioca = {
		.output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW }, .gtiocb = {
		.output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW } };
static const timer_cfg_t g_timer3_cfg = { .mode = TIMER_MODE_PERIODIC, .period =
		10, .unit = TIMER_UNIT_PERIOD_USEC, .duty_cycle = 50, .duty_cycle_unit =
		TIMER_PWM_UNIT_RAW_COUNTS, .channel = 3, .autostart = false,
		.p_callback = relay_pulse_callback, .p_context = &g_timer3, .p_extend =
				&g_timer3_extend, .irq_ipl = (2), };
/* Instance structure to use this module. */
const timer_instance_t g_timer3 = { .p_ctrl = &g_timer3_ctrl, .p_cfg =
		&g_timer3_cfg, .p_api = &g_timer_on_gpt };
#if (2) != BSP_IRQ_DISABLED
#if !defined(SSP_SUPPRESS_ISR_g_timer2) && !defined(SSP_SUPPRESS_ISR_GPT2)
SSP_VECTOR_DEFINE_CHAN(gpt_counter_overflow_isr, GPT, COUNTER_OVERFLOW, 2);
#endif
#endif
static gpt_instance_ctrl_t g_timer2_ctrl;
static const timer_on_gpt_cfg_t g_timer2_extend = { .gtioca = {
		.output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW }, .gtiocb = {
		.output_enabled = false, .stop_level = GPT_PIN_LEVEL_LOW } };
static const timer_cfg_t g_timer2_cfg = { .mode = TIMER_MODE_PERIODIC, .period =
		5, .unit = TIMER_UNIT_PERIOD_MSEC, .duty_cycle = 50, .duty_cycle_unit =
		TIMER_PWM_UNIT_RAW_COUNTS, .channel = 2, .autostart = true,
		.p_callback = switch_callback, .p_context = &g_timer2, .p_extend =
				&g_timer2_extend, .irq_ipl = (2), };
/* Instance structure to use this module. */
const timer_instance_t g_timer2 = { .p_ctrl = &g_timer2_ctrl, .p_cfg =
		&g_timer2_cfg, .p_api = &g_timer_on_gpt };
extern bool g_ssp_common_initialized;
extern uint32_t g_ssp_common_thread_count;
extern TX_SEMAPHORE g_ssp_common_initialized_semaphore;
void g_hal_init(void);

void relay_thread_create(void) {
	/* Increment count so we will know the number of ISDE created threads. */
	g_ssp_common_thread_count++;

	/* Initialize each kernel object. */

	tx_thread_create(&relay_thread, (CHAR *) "Relay Thread", relay_thread_func,
			(ULONG) NULL, &relay_thread_stack, 1024, 1, 1, 1, TX_AUTO_START);
}

static void relay_thread_func(ULONG thread_input) {
	/* Not currently using thread_input. */
	SSP_PARAMETER_NOT_USED(thread_input);

	/* First thread will take care of common initialization. */
	UINT err;
	err = tx_semaphore_get(&g_ssp_common_initialized_semaphore,
			TX_WAIT_FOREVER);

	while (TX_SUCCESS != err) {
		/* Check err, problem occurred. */
		BSP_CFG_HANDLE_UNRECOVERABLE_ERROR(0);
	}

	/* Only perform common initialization if this is the first thread to execute. */
	if (false == g_ssp_common_initialized) {
		/* Later threads will not run this code. */
		g_ssp_common_initialized = true;

		/* Perform common module initialization. */
		g_hal_init();

		/* Now that common initialization is done, let other threads through. */
		/* First decrement by 1 since 1 thread has already come through. */
		g_ssp_common_thread_count--;
		while (g_ssp_common_thread_count > 0) {
			err = tx_semaphore_put(&g_ssp_common_initialized_semaphore);

			while (TX_SUCCESS != err) {
				/* Check err, problem occurred. */
				BSP_CFG_HANDLE_UNRECOVERABLE_ERROR(0);
			}

			g_ssp_common_thread_count--;
		}
	}

	/* Initialize each module instance. */

	/* Enter user code for this thread. */
	relay_thread_entry();
}
