/* generated thread header file - do not edit */
#ifndef RS485_COMM_THREAD_H_
#define RS485_COMM_THREAD_H_
#include "bsp_api.h"
#include "tx_api.h"
#include "hal_data.h"
#ifdef __cplusplus 
extern "C" void rs485_comm_thread_entry(void);
#else 
extern void rs485_comm_thread_entry(void);
#endif
#ifdef __cplusplus
extern "C" {
#endif
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* RS485_COMM_THREAD_H_ */
