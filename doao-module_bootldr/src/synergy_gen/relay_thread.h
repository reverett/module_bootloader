/* generated thread header file - do not edit */
#ifndef RELAY_THREAD_H_
#define RELAY_THREAD_H_
#include "bsp_api.h"
#include "tx_api.h"
#include "hal_data.h"
#ifdef __cplusplus 
extern "C" void relay_thread_entry(void);
#else 
extern void relay_thread_entry(void);
#endif
#include "r_adc.h"
#include "r_adc_api.h"
#include "r_gpt.h"
#include "r_timer_api.h"
#include "r_icu.h"
#include "r_external_irq_api.h"
#ifdef __cplusplus
extern "C" {
#endif
/** ADC on ADC Instance. */
extern const adc_instance_t g_adc0;
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer4_pwm_1_2;
#ifndef pwm4_callback
void pwm4_callback(timer_callback_args_t * p_args);
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer1_pwm_3_4;
#ifndef pwm1_callback
void pwm1_callback(timer_callback_args_t * p_args);
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer0;
#ifndef gp_timer0_callback
void gp_timer0_callback(timer_callback_args_t * p_args);
#endif
/* External IRQ on ICU Instance. */
extern const external_irq_instance_t g_external_irq7;
#ifndef irq7_capture_zc4
void irq7_capture_zc4(external_irq_callback_args_t * p_args);
#endif
/* External IRQ on ICU Instance. */
extern const external_irq_instance_t g_external_irq6;
#ifndef irq6_capture_zc3
void irq6_capture_zc3(external_irq_callback_args_t * p_args);
#endif
/* External IRQ on ICU Instance. */
extern const external_irq_instance_t g_external_irq5;
#ifndef irq5_capture_zc2
void irq5_capture_zc2(external_irq_callback_args_t * p_args);
#endif
/* External IRQ on ICU Instance. */
extern const external_irq_instance_t g_external_irq4;
#ifndef irq4_capture_zc1
void irq4_capture_zc1(external_irq_callback_args_t * p_args);
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer3;
#ifndef relay_pulse_callback
void relay_pulse_callback(timer_callback_args_t * p_args);
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer2;
#ifndef switch_callback
void switch_callback(timer_callback_args_t * p_args);
#endif
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* RELAY_THREAD_H_ */
