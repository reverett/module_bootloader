//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  relay_state.c
//
//  Description: Contains the relay state machines
//
//-----------------------------------------------------------------------------


#include "jellyfish_common.h"
#include "common_def.h"
#include "relay_state.h"
#include "relay_thread.h"
#include "relay_module.h"
#include "hw_io_api.h"
#include "critical_section.h"
#include "db_api.h"
#include "control_thread.h"
#include "relay_thread_entry.h"
#include <stdio.h>

#define RLY_DELAY 100 // 100 msec
#define RLY_MAKE_DELAY 500 //
#define RLY_BREAK_DELAY 500 //

#define RLY_MAKE_COUNT RLY_MAKE_DELAY/RLY_DELAY
#define RLY_BREAK_COUNT RLY_BREAK_DELAY/RLY_DELAY


static RLY_COMMAND_E new_relay1_command = RLY_CMD_NONE;
static RLY_COMMAND_E new_relay2_command = RLY_CMD_NONE;
static RLY_COMMAND_E new_relay3_command = RLY_CMD_NONE;
static RLY_COMMAND_E new_relay4_command = RLY_CMD_NONE;

static relay_context_t relay_1 = {0, 0, RLY_STATE_BREAK};
static relay_context_t relay_2 = {0, 0, RLY_STATE_BREAK};
static relay_context_t relay_3 = {0, 0, RLY_STATE_BREAK};
static relay_context_t relay_4 = {0, 0, RLY_STATE_BREAK};

static BOOL relay_timeout = FALSE;
extern TX_TIMER line_meas_timer;

BOOL get_timer_expired()
{
    BOOL meas_timeout;

    // needs to be atomic operation
    enter_critical_section();
    meas_timeout = relay_timeout;
    exit_critical_section();

    return(meas_timeout);
}
void set_timer_expired(BOOL new_value)
{
    relay_timeout = new_value;
}

RLY_STATE_E getRelayState(RELAY_ID_E relay_id)
{
    RLY_STATE_E curr_relay_state = RLY_STATE_BREAK;
    relay_context_t *relay_state_context = NULL;

    switch (relay_id)
    {
    case RELAY_1:
        relay_state_context = &relay_1;
        break;
    case RELAY_2:
        relay_state_context = &relay_2;
        break;
    case RELAY_3:
        relay_state_context = &relay_3;
        break;
    case RELAY_4:
        relay_state_context = &relay_4;
        break;
    default:
        // no such relay
        // todo add error case
        break;
    }
    if (relay_state_context != NULL)
    {
      // atomic operation - need exclusive access here while copying the state OK
      // fn getRelayState is called within interrupt routine
      // critical section is not needed
      curr_relay_state = relay_state_context->rly_old_state;
    }
    return(curr_relay_state);
}

// toggle relay state
// BREAK -> MAKE, or MAKE -> BREAK
//
RLY_STATE_E toggleRelay(RELAY_ID_E relay_id)
{
    RLY_STATE_E current_relay_state;
    // toggle the current relay state
    // if relay is in BREAK or MAKE states
    current_relay_state = getRelayState(relay_id);
    //printf("switch toggle\n\r");

    if (current_relay_state == RLY_STATE_BREAK)
    {
        // current relay state BREAK toggle to MAKE;
        //printf("current relay state BREAK toggle to MAKE\r\n");
        set_relay_state(relay_id, ON, MANUAL_SW);
    }
    else if (current_relay_state == RLY_STATE_MAKE)
    {
        //printf("current relay state MAKE toggle to BREAK\n\r");
        set_relay_state(relay_id, OFF, MANUAL_SW);
    }
    else
    {
        //printf("ignore manual switch override command\n\r");
    }
    return(current_relay_state);
}

// this is our api function
STATUS_E set_relay_state(RELAY_ID_E relay_id, SWITCH_STATE_E relay_state, SWITCH_TYPE_E switch_type)
{
    RLY_COMMAND_E rly_command;


    if (switch_type == MANUAL_SW)
    {
		if (relay_state == ON)
			rly_command = RLY_CMD_MAKE_MAN;
		else if (relay_state == OFF)
			rly_command = RLY_CMD_BREAK_MAN;
		else
			rly_command = RLY_CMD_NONE;
    }
    else
    {
		if (relay_state == ON)
			rly_command = RLY_CMD_MAKE;
		else if (relay_state == OFF)
			rly_command = RLY_CMD_BREAK;
		else
			rly_command = RLY_CMD_NONE;
    }
    switch (relay_id)
    {
    case RELAY_1:
        new_relay1_command = rly_command; // we store this command until a new command comes in
        break;
    case RELAY_2:
        new_relay2_command = rly_command;
        break;
    case RELAY_3:
        new_relay3_command = rly_command;
        break;
    case RELAY_4:
        new_relay4_command = rly_command;
        break;
    default:
        // no such relay
        // todo add error case
        break;
    }
    return(SUCCESS);
}


// switches state according to command received
void relayState(RELAY_ID_E relay_id)
{
    RLY_STATE_E rly_new_state;
    relay_context_t *relay_state_context = NULL;
    RLY_COMMAND_E new_rly_command;

    switch (relay_id)
    {
        case RELAY_1:
            relay_state_context = &relay_1;
            new_rly_command = new_relay1_command;
            break;
        case RELAY_2:
            relay_state_context = &relay_2;
            new_rly_command = new_relay2_command;
            break;
        case RELAY_3:
            relay_state_context = &relay_3;
            new_rly_command = new_relay3_command;
            break;
        case RELAY_4:
            relay_state_context = &relay_4;
            new_rly_command = new_relay4_command;
            break;
        default:
            // no such relay
            // todo add error case
            break;
    }

    if (relay_state_context != NULL)
    {
        rly_new_state = relay_state_context->rly_old_state;

        switch (relay_state_context->rly_old_state)
        {
            case RLY_STATE_BREAK:
            {
              switch (new_rly_command)
              {
                case RLY_CMD_MAKE:
                case RLY_CMD_MAKE_MAN:
                {
                     stop_period_meas(relay_id);
                    // clear our mean values
                    clr_relay_line_period(relay_id);
                    start_period_meas(relay_id);
                    set_timer_expired(FALSE); // clear timer expired flag before starting timer
                    tx_timer_activate(&line_meas_timer);
                    rly_new_state = RLY_STATE_TR_MAKE_1;

                }
                break;
                case RLY_CMD_BREAK: // command ignored
                {
                	new_rly_command = RLY_CMD_NONE;
                	// call callback fn for message command sent
					if (control_relay_instr_complete_cb_func != NULL)
					{
						(*control_relay_instr_complete_cb_func)();
					}
                }
                break;
                case RLY_CMD_BREAK_MAN: // command ignored
                case RLY_CMD_NONE:
                 default:
                 new_rly_command = RLY_CMD_NONE;
                 break; // do nothing for these cases
              }
            } // end of switch
             break;
            case RLY_STATE_TR_MAKE_1:
            {
                E_LINE_PERIOD_TYPE meas_status;
                uint32_t avg_period_value;

                meas_status = line_period_meas(relay_id, &avg_period_value);

               if (meas_status == PERIOD_OK)
               {
                   //printf("PERIOD_OK prd = %d\r\n", (unsigned int)avg_period_value/48);
                   tx_timer_deactivate(&line_meas_timer);
                   db_vt_set_line_period(relay_id, avg_period_value); // save current meas to database
                   tx_timer_change(&line_meas_timer, RELAY_TIMEOUT, ONE_SHOT); // setup one shot timer
                   // set up pulse timer for relay pulse with synchronization
                   setup_pulse(ZC_SYNC, avg_period_value, relay_id, TRUE);
                   // OK to switch relay state
                   rly_new_state = RLY_STATE_TR_MAKE_2;
               }
               if (get_timer_expired() == TRUE)
               {
                   //printf("timer exp prd = %d\r\n", (unsigned int)avg_period_value/48);
                   db_vt_set_line_period(relay_id, avg_period_value); // save current meas to database
                   // set up pulse timer for relay pulse without synchronization
                   setup_pulse(NO_ZC_SYNC, avg_period_value, relay_id, TRUE);
                   {
                       // OK to switch relay state
                       rly_new_state = RLY_STATE_TR_MAKE_2;
                   }
                   tx_timer_deactivate(&line_meas_timer);
                   tx_timer_change(&line_meas_timer, RELAY_TIMEOUT, ONE_SHOT); // setup one shot timer
               }
            }
             break;
            case RLY_STATE_TR_MAKE_2:
            {
                //
                // when status is PULSE_COMPLETE relay has transitioned to the MAKE state
                // update state
                if (get_pulse_state() == PULSE_TIMER_DONE)
                {
                    rly_new_state = RLY_STATE_MAKE;
                    relay_state_context->make_relay_count = relay_state_context->make_relay_count + 1;
                    db_vt_set_relay_state(relay_id, ON); // update database
                    switch (new_rly_command)
                    {
						case RLY_CMD_MAKE:
						{
							// Relay state change completed, call the control thread's
							// call-back function to proceed with the next instruction,
							// if any, or mark the completion of all relay instrucitons.
							if (control_relay_instr_complete_cb_func != NULL)
							{
								(*control_relay_instr_complete_cb_func)();
							}
						}
						break;
						case RLY_CMD_MAKE_MAN:
						default:
						break;
                    }
                    new_rly_command = RLY_CMD_NONE; // command completed
                }
            }
             break;
            case RLY_STATE_MAKE:
            {
              switch (new_rly_command)
              {
                case RLY_CMD_BREAK:
                case RLY_CMD_BREAK_MAN:
                {
                    stop_period_meas(relay_id);
                   // OK to switch relay state
                   rly_new_state = RLY_STATE_TR_BREAK;
                   // setup the relay pulse
                   // no need to sync with zero cross signal
                   setup_pulse(NO_ZC_SYNC, 0, relay_id, FALSE);
                }
                break;
                case RLY_CMD_MAKE: // command ignored
                {
                	new_rly_command = RLY_CMD_NONE;
                	// call callback fn for message command sent
					if (control_relay_instr_complete_cb_func != NULL)
					{
						(*control_relay_instr_complete_cb_func)();
					}
                }
                break;
                case RLY_CMD_MAKE_MAN: // command ignored
                case RLY_CMD_NONE:
                default:
                	new_rly_command = RLY_CMD_NONE;
                 break;
             } // end of switch
            }
             break;
            case RLY_STATE_TR_BREAK:
            {
                //
                // when pulse status is PULSE_COMPLETE relay has transitioned to the BREAK state
                // update state
                if (get_pulse_state() == PULSE_TIMER_DONE)
                {
                    rly_new_state = RLY_STATE_BREAK;
                    relay_state_context->break_relay_count = relay_state_context->break_relay_count + 1;
                    db_vt_set_relay_state(relay_id, OFF); // update database
                    switch (new_rly_command)
                    {
						case RLY_CMD_BREAK:
						{
							// Relay state change completed, call the control thread's
							// call-back function to proceed with the next instruction,
							// if any, or mark the completion of all relay instrucitons.
							if (control_relay_instr_complete_cb_func != NULL)
							{
								(*control_relay_instr_complete_cb_func)();
							}
						}
						break;
						case RLY_CMD_BREAK_MAN:
						default:
						break;
                    }
                    new_rly_command = RLY_CMD_NONE; // command completed
                }
            }
             break;
            default:
             break;
        } // end of switch
        // for debug print the transition
#if 0
        if (relay_state_context->rly_old_state != rly_new_state)
        {
            switch (rly_new_state)
            {
                case RLY_STATE_BREAK: printf("rly %d RLY_STATE_BREAK\r\n", relay_id); break;
                case RLY_STATE_TR_MAKE_1: printf("rly %d RLY_STATE_TR_MAKE_1\r\n", relay_id); break;
                case RLY_STATE_TR_MAKE_2: printf("rly %d RLY_STATE_TR_MAKE_2\r\n", relay_id); break;
                case RLY_STATE_MAKE: printf("rly %d RLY_STATE_MAKE\r\n", relay_id); break;
                case RLY_STATE_TR_BREAK: printf("rly %d RLY_STATE_TR_BREAK\r\n", relay_id); break;
                default: printf("no such relay state\r\n"); break;
            }
        }
#endif
        relay_state_context->rly_old_state = rly_new_state;


    }
}
