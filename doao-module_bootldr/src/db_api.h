//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  db_api.h
//
//  Description: API functions used by the non-database implementation module
//               to call (and call these functions only).
//-----------------------------------------------------------------------------

#ifndef __DB_API_H__
#define __DB_API_H__

#include "jellyfish_common.h"
#include "common_def.h"
#include "hw_io_api.h"
#include "db_def.h"


//------------------------------------------------------------------
// Database API functions that access the "Virtual Table" portion of
// the database.
//------------------------------------------------------------------

STATUS_E db_vt_get_table(doao_module_db_v_t *db_v_tbl_p);

STATUS_E db_vt_set_provsion_switch_state(SWITCH_STATE_E state);
STATUS_E db_vt_get_provsion_switch_state(SWITCH_STATE_E *state);

STATUS_E db_vt_set_relay_state(RELAY_ID_E id, SWITCH_STATE_E state);
STATUS_E db_vt_get_relay_state(RELAY_ID_E id, SWITCH_STATE_E *state);

STATUS_E db_vt_set_override_state(MANUAL_SWITCH_ID_E switch_id, SWITCH_STATE_E state);
STATUS_E db_vt_get_override_state(MANUAL_SWITCH_ID_E switch_id, SWITCH_STATE_E *state);

STATUS_E db_vt_set_line_period(RELAY_ID_E id, unsigned period);
STATUS_E db_vt_get_line_period(RELAY_ID_E id, unsigned *period);

STATUS_E db_vt_set_pwm_output(PWM_ID_E id, unsigned pwm);
STATUS_E db_vt_get_pwm_output(PWM_ID_E id, unsigned *pwm);

//STATUS_E db_vt_set_output_monitor(void);
STATUS_E db_vt_set_output_monitor(RELAY_ID_E relay_id, unsigned output_monitor);
STATUS_E db_vt_get_output_monitor(RELAY_ID_E relay_id, unsigned *output_monitor);

STATUS_E db_vt_set_v24_input_monitor(unsigned input_monitor);
STATUS_E db_vt_get_v24_input_monitor(unsigned *input_monitor);

STATUS_E db_vt_set_rs485_bus_eol(BOOL eol_flg);
STATUS_E db_vt_get_rs485_bus_eol(BOOL *eol_flg);



//------------------------------------------------------------------
// Database API functions that access the "Non-Virtual Table" portion
// of the database.
//------------------------------------------------------------------

STATUS_E   db_nvt_get_everything(void);    // this is just a dummy placeholder
STATUS_E   db_nvt_get_table(doao_module_db_nv_t *db_nv_tbl_p);
STATUS_E   db_nvt_get_firmware_version(version_tag_t *fmw_ver_p);

#endif // __DB_API_H__



