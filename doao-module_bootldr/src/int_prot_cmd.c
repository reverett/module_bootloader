/*
 * =====================================================================================
 *
 *       Filename:  rhacmd.c
 *
 *    Description:  this file includes rha serial protocol commands support
 *
 *        Version:  1.0
 *        Created:  09/01/2015 02:05:42 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Liang Huang (software developer), lhuang@intermatic.com
 *   Organization:  Intermatic Inc.
 *
 * =====================================================================================
 */

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>  //Sleep
#include <fcntl.h>
#include <errno.h>
#include <int_prot_cmd.h>
#include <int_prot_frame.h>
//#include <termios.h>
#include <inttypes.h>
#include <stdbool.h>
//#include "jnode.h"
#include <rs485_driver.h>
#include "jellyfish_common.h"

#if 0
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  rhaDeviceTypeRequest
 *  Description:  host send rhaDeviceTypeRequest cmd to check local module device type
 * =====================================================================================
 */
void rhaDeviceTypeRequest() {

	uint8_t payload_length = 0;
	uint8_t *buffer;

	buffer = construct_rha_frame(MASTER_ADDR, PH_ZIGBEE_SUPPORT_CONFIG, SH_ZIGBEE_SUPPORT_CONFIG_DEVICE_TYPE_REQUEST, NULL, 0);
	write(zb_port, buffer, payload_length+8);
	free(buffer);
	printf("\r\nDeviceTypeReques...\n");
}

//rha_endpoint_t localEndpoint;
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  rhaNetworkStatusRequest
 *  Description:  host send rhaNetworkStatusRequest cmd
 * =====================================================================================
 */
void rhaNetworkStatusRequest() {

	uint8_t payload_length = 0;
	uint8_t *buffer;

	buffer = construct_rha_frame(PH_NETWORK_COMISSIONING, SH_NETWORK_COMISSIONING_NETWORK_STATUS_REQUEST, NULL, 0);
	//write(zb_port, buffer, payload_length+7);
	//free(buffer);
	rs485_tx_write_nbyte(buffer, payload_length+7);
}
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  rhaFormNetwork
 *  Description:  host send FormNetwork cmd to module
 * =====================================================================================
 */
void rhaFormNetwork() {

	uint8_t payload[127];
	uint8_t payload_length = 0;
	uint8_t *buffer;

	uint32_t channelMask = 0x00000800;//only allow ch11. 	//0x03FFF800; HA channel mask ch11-25;
	uint8_t autoOptions = 0x03;
	uint16_t shortPANId = 0x0000;
	uint32_t extendedPANId_low = 0x00000000;
	uint32_t extendedPANId_high = 0x00000000;

	memcpy(payload,&channelMask,4);
	payload[4] = autoOptions;
	payload[5] = shortPANId;
	payload[6] = shortPANId>>8;

	memcpy(payload+7,&extendedPANId_low,4 );
	memcpy(payload+11,&extendedPANId_high,4 );

	payload_length = 15;



	buffer = construct_rha_frame(PH_NETWORK_COMISSIONING, SH_NETWORK_COMISSIONING_FORM_NETWORK, payload, payload_length);
	write(zb_port, buffer, payload_length+7);
	free(buffer);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  rhaJoinNetwork
 *  Description:  host send FormNetwork cmd to module
 * =====================================================================================
 */
void rhaJoinNetwork() {

	uint8_t payload[127];
	uint8_t payload_length = 0;
	uint8_t *buffer;

	uint32_t channelMask = 0x03FFF800;	//HA channel mask ch11-25;
	uint8_t autoOptions = 0x03;
	uint16_t shortPANId = 0x0000;
	uint32_t extendedPANId_low = 0x00000000;
	uint32_t extendedPANId_high = 0x00000000;

	printf("\nIn rhaJoinNetwork()...1");

	memcpy(payload,&channelMask,4);
	payload[4] = autoOptions;
	payload[5] = shortPANId;
	payload[6] = shortPANId>>8;

	memcpy(payload+7,&extendedPANId_low,4 );
	memcpy(payload+11,&extendedPANId_high,4 );

	payload_length = 15;
	printf("\nIn rhaJoinNetwork()...2");

	buffer = construct_rha_frame(PH_NETWORK_COMISSIONING, SH_NETWORK_COMISSIONING_JOIN_NETWORK, payload, payload_length);
	write(zb_port, buffer, payload_length+7);
	free(buffer);
}


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  rhaStartupSyncComplete
 *  Description:  host send rhaStartupSyncComplete cmd
 * =====================================================================================
 */
void rhaStartupSyncComplete() {

	uint8_t payload_length = 0;
	uint8_t *buffer;

	buffer = construct_rha_frame(PH_UTILITY, SH_UTILITY_STARTUP_SYNC_COMPLETE, NULL, 0);
	write(zb_port, buffer, payload_length+7);
	free(buffer);
}


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  createEndpoint
 *  Description:  create an local endpoint
 * =====================================================================================
 */
/*
void createEndpoint() {

//	if (NULL == localEndpoint)
//		localEndpoint = calloc(sizeof(rha_endpoint_t), 1);	//allocate memory for localEndpoint

//	if (localEndpoint == NULL){
//		printf("\n Failed allocate buffer for localEndpoint...");
//		return NULL;
//	}

	localEndpoint.endpointId = 0x01;
	localEndpoint.profileId = HA_PROFILE;
	localEndpoint.deviceId = HVAC;
	localEndpoint.deviceVersion = 0x01;
	localEndpoint.number_svr_cluster = 0x05;
	localEndpoint.svr_cluster->basic = 0x0000;
	localEndpoint.svr_cluster->identify = 0x0003;
	localEndpoint.svr_cluster->groups = 0x0004;
	localEndpoint.svr_cluster->scenes = 0x0005;
	localEndpoint.svr_cluster->fan_control = 0x0202;
	localEndpoint.number_client_cluster = 0x02;
	localEndpoint.client_cluster->Time = 0x0A;
	localEndpoint.client_cluster->OTA = 0x0019;

//	return localEndpoint;
}

*/


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  rhaAddEndpoint
 *  Description:  host add endpoint cmd
 * =====================================================================================
 */
void rhaAddEndpoint() {

	uint8_t payload[127];
	uint8_t payload_length = 0;
	uint8_t *buffer;

//	if( NULL == createEndpoint()){
//		printf("\n No localEndpoint created!...");
//		return;
//	}

//	printf("\nIn rhaAddEndpoint()...1");

//	rha_endpoint_t * localEndpoint = NULL;
	rha_endpoint_t	localEndpoint;
	rha_client_cluster_t  clientCluster;
	rha_svr_cluster_t 	svrCluster;

//	localEndpoint = calloc(sizeof(rha_endpoint_t), 1);	//allocate memory for localEndpoint
//	clientCluster = calloc(sizeof(rha_client_cluster_t),1);
//	svrCluster = calloc(sizeof(rha_svr_cluster_t),1);


//	if (localEndpoint == NULL){
//			printf("\n Failed allocate buffer for localEndpoint...");
//		}

	svrCluster.basic = 0x0000;
	svrCluster.identify = 0x0003;
	svrCluster.groups = 0x0004;
	svrCluster.scenes = 0x0005;
	svrCluster.fan_control = 0x0202;

	clientCluster.Time = 0x000A;
	clientCluster.OTA = 0x0019;


	localEndpoint.endpointId = 0x01;
	localEndpoint.profileId = HA_PROFILE;
	localEndpoint.deviceId = HVAC;
	localEndpoint.deviceVersion = 0x01;
	localEndpoint.number_svr_cluster = 0x05;
	localEndpoint.number_client_cluster = 0x02;
	localEndpoint.client_cluster = clientCluster;
	localEndpoint.svr_cluster = svrCluster;



	payload_length =0x16;

//	memcpy (payload, &localEndpoint, payload_length);
/*
	int i=0;
	uint8_t *idx = localEndpoint;
	for (i=0;i<payload_length;i++){

		payload[i] = (uint8_t)*idx;
		idx ++;

	}
*/


	payload[0] = localEndpoint.endpointId;
	payload[1] = localEndpoint.profileId;
	payload[2] = localEndpoint.profileId>>8;

	payload[3] = localEndpoint.deviceId;
	payload[4] = localEndpoint.deviceId>>8;

	payload[5] = localEndpoint.deviceVersion;
	payload[6] = localEndpoint.number_svr_cluster;

	payload[7] = svrCluster.basic;
	payload[8] = svrCluster.basic>>8;

	payload[9] = svrCluster.basic;
	payload[10] = svrCluster.basic>>8;

	payload[9] = svrCluster.identify;
	payload[10] = svrCluster.identify>>8;

	payload[11] = svrCluster.groups;
	payload[12] = svrCluster.groups>>8;

	payload[13] = svrCluster.scenes;
	payload[14] = svrCluster.scenes>>8;

	payload[15] = svrCluster.fan_control;
	payload[16] = svrCluster.fan_control>>8;

	payload[17] = localEndpoint.number_client_cluster;

	payload[18] = clientCluster.Time;
	payload[19] = clientCluster.Time>>8;

	payload[20] = clientCluster.OTA;
	payload[21] = clientCluster.OTA>>8;

	printf("\rIn rhaAddEndpoint()...");

	buffer = construct_rha_frame(PH_ZIGBEE_SUPPORT_CONFIG, SH_ZIGBEE_SUPPORT_CONFIG_ADD_ENDPOINT, payload, payload_length);
	write(zb_port, buffer, payload_length+7);
	free(buffer);


}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  RHAModuleInfoRequest
 *  Description:  host request module info
 * =====================================================================================
 */
void rhaModuleInfoRequest() {

	uint8_t payload[127];
	uint8_t payload_length = 0;
	uint8_t *buffer;

	buffer = construct_rha_frame(PH_UTILITY, SH_UTILITY_MODULE_INFO_REQUEST, payload, 0);
	write(zb_port, buffer, payload_length+7);
	free(buffer);
}
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  rhaRestoreDefaults
 *  Description:  host restore module to factory defaults
 * =====================================================================================
 */
void rhaRestoreDefaults() {

	uint8_t payload_length = 0;
	uint8_t *buffer;

	buffer = construct_rha_frame(PH_UTILITY, SH_UTILITY_RESTORE_DEFAULTS, NULL, 0);
	write(zb_port, buffer, payload_length+7);
	free(buffer);
	printf("\r\nrhaRestoreDefaults...");

}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  rhaSerialACKConfigWrite
 *  Description:  set serial ACK off 0x00, ON 0x01
 * =====================================================================================
 */
void rhaSerialACKConfigWrite(uint8_t ack) {             //serial ACK off 0x00, ON 0x01
        uint8_t *buffer;
	uint8_t payload[1];
	payload[0] = ack;
       	uint8_t payload_length = 1;

	buffer = construct_rha_frame(PH_UTILITY, SH_UTILITY_SERIAL_ACK_CONFIG_WRITE, payload, 1);
	write(zb_port, buffer, payload_length+7);
	free(buffer);
    printf("\r\nrhaSerialACKConfigWrite...");

}


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  rhaHostStartupReady
 *  Description:  Host send SH_UTILITY_HOST_STARTUP_READY
 * =====================================================================================
 */
void rhaHostStartupReady() {

	uint8_t payload[127];
	uint8_t payload_length = 0;
	uint8_t *buffer;

	buffer = construct_rha_frame(PH_UTILITY, SH_UTILITY_HOST_STARTUP_READY, payload, payload_length);
	write(zb_port, buffer, payload_length+7);
	free(buffer);
	printf("\rrhaHostStartupReady...");

}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  RHADeviceTypeWrite
 *  Description:  set module to FFD, non-sleepy device type
 * =====================================================================================
 */
void rhaDeviceTypeWrite(uint8_t device_type, uint8_t sleepy_type) {

	uint8_t payload[2];
	payload[0] = device_type;	//FFD
	payload[1] = sleepy_type;	//non-sleepy
	uint8_t payload_length = 2;
	uint8_t *buffer;

	buffer = construct_rha_frame(PH_ZIGBEE_SUPPORT_CONFIG, SH_ZIGBEE_SUPPORT_CONFIG_DEVICE_TYPE_WRITE, payload, 2);
	write(zb_port, buffer, payload_length+7);
	free(buffer);
	printf("\nrhaDeviceTypeWrite...");

}


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  rhaManufacturerIdWrite
 *  Description:  set module Manufacturer Id
 * =====================================================================================
 */
void rhaManufacturerIdWrite(uint16_t manuId) {

	uint8_t payload[2];
	payload[0] = (uint8_t)(manuId&0x00FF);
	payload[1] = (uint8_t)((manuId&0xFF00)>>8);
	uint8_t payload_length = 0;
	uint8_t *buffer;

	buffer = construct_rha_frame(PH_UTILITY, SH_UTILITY_MANUFACTURER_ID_WRITE, payload, 2);
	write(zb_port, buffer, payload_length+7);
	free(buffer);
	printf("\nrhaManufacturerIdWrite...");

}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  RHASerialACKConfigWrite
 *  Description:  set serial ACK off
 * =====================================================================================
 */
uint8_t * RHASerialACKConfigWrite() {

	uint8_t payload[1];
	payload[0] = 0x00;	//serial ACK off
	return construct_rha_frame(PH_UTILITY, SH_UTILITY_SERIAL_ACK_CONFIG_WRITE, payload, 1);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  RHAModuleInfoRequest
 *  Description:  host request module info
 * =====================================================================================
 */
uint8_t * RHAModuleInfoRequest() {

	return construct_rha_frame(PH_UTILITY, SH_UTILITY_MODULE_INFO_REQUEST, NULL, 0);
}




/*
 * ===  FUNCTION  ======================================================================
 *         Name:  RHAClearEndpointConfig
 *  Description:  set module to FFD, non-sleepy device type
 * =====================================================================================
 */
uint8_t * rhaClearEndpointConfig() {

	return construct_rha_frame(PH_ZIGBEE_SUPPORT_CONFIG, SH_ZIGBEE_SUPPORT_CONFIG_CLEAR_ENDPOINT_CONFIG, NULL, 0);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  RHAClearEndpointListRequest
 *  Description:  host request endpoint list
 * =====================================================================================
 */
uint8_t * rhaClearEndpointListRequest() {

	return construct_rha_frame(PH_ZIGBEE_SUPPORT_CONFIG, SH_ZIGBEE_SUPPORT_CONFIG_ENDPOINT_LIST_REQUEST, NULL, 0);
}


////////////////////SLEEPY CONTROL COMMANDS://///////////////////////////////////////////////////////////////////

/*SH_UTILITY_SLEEPY_HIBERNATE_DURATION_WRITE
 * ===  FUNCTION  ======================================================================
 *         Name:  rhaSleepyHibernateDurationWrite
 *  Description:  set sleepy Hibernation duration in qs(quarter-seconds)
 * =====================================================================================
 */
void rhaSleepyHibernateDurationWrite(uint32_t duration) {
        uint8_t *buffer;
    uint8_t payload[4];
    payload[0] = duration;
    payload[1] = duration>>8;
    payload[2] = duration>>16;
    payload[3] = duration>>24;

           uint8_t payload_length = 4;

    buffer = construct_rha_frame(PH_UTILITY, SH_UTILITY_SLEEPY_HIBERNATE_DURATION_WRITE, payload, payload_length);
	write(zb_port, buffer, payload_length+7);
	free(buffer);
	printf("\r\nrhaSleepyHibernateDurationWrite...");
}

/*SH_UTILITY_SLEEPY_CONTROL_WAKEUP
 * ===  FUNCTION  ======================================================================
 *         Name:  rhaSleepyControlAwake
 *  Description:  awake sleepy device
 * =====================================================================================
 */
void rhaSleepyControlAwake() {
        uint8_t *buffer;
        uint8_t payload[19];
    	uint8_t i;

        for (i=0;i<19;i++)
            payload[i] = 0xFF;          //stuff 19 0xFF    to payload;

        uint8_t payload_length = 19;

    buffer = construct_rha_frame(PH_UTILITY, SH_UTILITY_SLEEPY_CONTROL_WAKEUP, payload, payload_length );
	write(zb_port, buffer, payload_length+7);
	free(buffer);
	printf("\r\nrhaSleepyControlAwake...");

}

/*SH_UTILITY_SLEEPY_CONTROL_STAY_AWAKE_DURATION_WRITE
 * ===  FUNCTION  ======================================================================
 *         Name:  rhaSleepyControlStayAwakeDurationWrite
 *  Description:  set Stay Awake Duration after wakeup in ms(milliseconds)
 * =====================================================================================
 */
void rhaSleepyControlStayAwakeDurationWrite(uint32_t duration) {
        uint8_t *buffer;
    uint8_t payload[4];
    payload[0] = duration;
    payload[1] = duration>>8;
    payload[2] = duration>>16;
    payload[3] = duration>>24;

           uint8_t payload_length = 4;

    buffer = construct_rha_frame(PH_UTILITY, SH_UTILITY_SLEEPY_CONTROL_STAY_AWAKE_DURATION_WRITE, payload, payload_length);
	write(zb_port, buffer, payload_length+7);
	free(buffer);
	printf("\r\nrhaSleepyControlStayAwakeDurationWrite...");
}
/*SH_UTILITY_SLEEPY_CONTROL_STAY_AWAKE_DURATION_READ
 * ===  FUNCTION  ======================================================================
 *         Name:  rhaSleepyControlStayAwakeDurationRead
 *  Description:  set serial ACK off 0x00, ON 0x01
 * =====================================================================================
 */
void rhaSleepyControlStayAwakeDurationRead() {             //serial ACK off 0x00, ON 0x01
        uint8_t *buffer;
           uint8_t payload_length = 0;

    buffer = construct_rha_frame(PH_UTILITY, SH_UTILITY_SLEEPY_CONTROL_STAY_AWAKE_DURATION_READ, NULL, 0);
	write(zb_port, buffer, payload_length+7);
	free(buffer);
	printf("\r\nrhaSleepyControlStayAwakeDurationRead...");
}
/////////////////////////END OF SLEEPY CONTROL COMMANDS///////////////////////////////////////////

#endif
