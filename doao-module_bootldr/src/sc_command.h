//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  sc_cmd.h
//
//  Description: Defines command structure that the System Controller uses
//               to communicate with the DO_AO Module.
//
//-----------------------------------------------------------------------------

#ifndef __SC_COMMAND_H__
#define __SC_COMMAND_H__


#include "jellyfish_common.h"


#define SC_CMD_SET_INSTR_MAX_NUM            5

//------------------------------------------------------------------------
// Command instructions and parameter value definitions
typedef enum {
	SC_CMD_PRV    = 1,
	SC_CMD_SET    = 2,
    SC_CMD_GET    = 3,
    SC_CMD_IMG    = 4
} SC_CMD_TAG_T;


typedef enum {
	SC_CMD_HW_RELAY         = 1,
    SC_CMD_HW_ZC_DISABLE    = 2,  // the default is always have ZC enabled
	SC_CMD_HW_ANALOG_PWM    = 3,
	SC_CMD_HW_LED           = 4,
	SC_CMD_HW_RS485_EOL     = 5
} SC_CMD_HW_T;


typedef enum {
	OP_TBL      = 1,
	PRV_TBL     = 2,
	V23_MONITOR = 3       // just a demo of adding additional data field id
} SC_CMD_GET_DB_CMD_E;


//------------------------------------------------------------------------
// The common header for all commands

typedef struct sc_cmd_set_cmd_param_s {
	unsigned char       num_of_instrs;
	unsigned char       unused[2];
} sc_cmd_set_cmd_param_t;

typedef struct sc_cmd_field_s {
	unsigned char       tag;
	union {
		sc_cmd_set_cmd_param_t params;
		unsigned char          unused[3];
	}                   u;
} sc_cmd_field_t;


//------------------------------------------------------------------------
// The PRV command's instructions

typedef struct sc_cmd_prv_instr_s {
	unsigned char       _TBD;
} sc_cmd_prv_instr_t;


//------------------------------------------------------------------------
// The SET command's instructions

typedef struct sc_cmd_set_instr_s {
	unsigned char       hw_name;
	unsigned char       hw_id;
	unsigned char       param;
	unsigned char       unused;
} sc_cmd_set_instr_t;



//------------------------------------------------------------------------
// The GET command's instructions

typedef struct sc_cmd_get_instr_s {
    SC_CMD_GET_DB_CMD_E    db_field_id;
} sc_cmd_get_instr_t;


//------------------------------------------------------------------------
// The IMG command's instructions

typedef struct sc_cmd_img_instr_s {
	unsigned char       _TBD;
} sc_cmd_img_instr_t;


//------------------------------------------------------------------------

typedef struct sc_cmd_s {
	sc_cmd_field_t       cmd;
    union sc_cmd_u {
       sc_cmd_prv_instr_t   prv;
       sc_cmd_set_instr_t   set[SC_CMD_SET_INSTR_MAX_NUM];
       sc_cmd_get_instr_t   get;
       sc_cmd_img_instr_t   img;
    }                    instrs;
} sc_cmd_t;


BOOL        sc_cmd_is_set_cmd_max_instr_reached(void);
sc_cmd_t   *sc_cmd_init_set_cmd(void);
void        sc_cmd_add_set_instr_cnt(unsigned char instr_cnt);
STATUS_E    sc_cmd_add_set_cmd_instr(unsigned char hw_name, unsigned char hw_id , unsigned char param);


#endif // __SC_COMMAND_H__

