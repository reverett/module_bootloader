//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  test_driver.h
//
//  Description: The DO-AO Module unit test.
//
//-----------------------------------------------------------------------------
#ifndef __UNIT_TEST_H__
#define __UNIT_TEST_H__

STATUS_E    ut_dispatch_set_cmd_to_controller(void);
STATUS_E    ut_get_firmware_version_string(void);
STATUS_E    ut_get_rs485_eol_status(void);
STATUS_E    ut_get_24v_input_measure(void);
STATUS_E    ut_get_ao_adc_count(void);
STATUS_E    ut_get_relay_state(void);

#endif // __UNIT_TEST_H__
