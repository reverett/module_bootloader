//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  relay_state.h
//
//  Description:
//
//-----------------------------------------------------------------------------
#ifndef __RELAY_STATE_H__
#define __RELAY_STATE_H__

#include "common_def.h"

#define TIMER_VALUE            1 // value passed as arg to callback
#define RELAY_TIMEOUT         20 // 20 * 10 msec timeout timer ticks = 200 msec timeout value
#define ADC_TIMEOUT           50 // 50 * 10 msec timeout timer ticks = 500 msec timeout value
#define ONE_SHOT               0 // one shot timer

typedef enum {
    RLY_STATE_BREAK = 0,
    RLY_STATE_TR_MAKE_1 = 1,
    RLY_STATE_TR_MAKE_2 = 2,
    RLY_STATE_MAKE = 3,
    RLY_STATE_TR_BREAK = 4
}RLY_STATE_E;

typedef enum {
    RLY_CMD_NONE = 0,
    RLY_CMD_BREAK = 1,
    RLY_CMD_MAKE = 2,
	RLY_CMD_BREAK_MAN = 3,
	RLY_CMD_MAKE_MAN = 4
}RLY_COMMAND_E;

typedef struct relay_context
{
    int break_relay_count;
    int make_relay_count;
    RLY_STATE_E rly_old_state;
}relay_context_t;


//------------------------------------------------------------
//
//------------------------------------------------------------


//void         tRelay(RELAY_ID_E relay_id);
RLY_STATE_E  toggleRelay(RELAY_ID_E relay_id);
void         toggleRelayState(BOOL curr_switch_state, RELAY_ID_E relay_id);
STATUS_E set_relay_state(RELAY_ID_E relay_id, SWITCH_STATE_E relay_state, SWITCH_TYPE_E switch_type);
RLY_STATE_E  getRelayState(RELAY_ID_E relay_id);
void         relayState(RELAY_ID_E relay_id);
void         set_timer_expired(BOOL new_value);


#endif // __RELAY_STATE_H__
