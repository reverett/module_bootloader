//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  switch_state.h
//
//  Description:
//
//-----------------------------------------------------------------------------
#ifndef __SWITCH_STATE_H__
#define __SWITCH_STATE_H__

typedef enum {
    STATE_BREAK = 0,
    STATE_MAKE = 1
}SW_DB_STATE_E;

typedef struct pb_switch_context
{
    int sw_count;
    SW_DB_STATE_E old_sw_state;
    SW_DB_STATE_E prev_sw_state;
}pb_switch_context_t;

SW_DB_STATE_E    getSwitchState(MANUAL_SWITCH_ID_E switch_id);
void             stateSwitch(MANUAL_SWITCH_ID_E switch_id);

#endif // __SWITCH_STATE_H__
