//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  relay_module.h
//
//  Description:
//
//-----------------------------------------------------------------------------
#ifndef __RELAY_MODULE_H__
#define __RELAY_MODULE_H__

typedef enum {
    WPULSE_NONE = 0,
    WPULSE_WAIT = 1,
    WPULSE_IN_PROGRESS = 2,
    WPULSE_COMPLETE = 3,
    WPULSE_ERROR = 4
}E_WPULSE_STATE;

typedef enum {
    PULSE_TIMER_INIT        = 0,
    PULSE_TIMER_IN_PROGRESS = 1,
    PULSE_TIMER_DONE        = 2
}E_PULSE_STATE;

typedef enum {
    NO_ZC_SYNC = 0, // pulse not synchronized with zc signal
    ZC_SYNC    = 1  // pulse is synchronized with zc signal
}E_PULSE_TYPE;

typedef enum {
    PERIOD_OK = 0,
    PERIOD_OUT_OF_RANGE = 1,
    MEAS_IN_PROGRESS = 2,
    NO_WAVEFORM = 3
}E_LINE_PERIOD_TYPE;
void start_period_meas(RELAY_ID_E relay_id);
void stop_period_meas(RELAY_ID_E relay_id);
void clr_relay_line_period(RELAY_ID_E relay_id);
uint32_t get_relay_line_period(RELAY_ID_E relay_id);
void start_pulse_timer(RELAY_ID_E relay_num);
// E_WPULSE_STATE get_wait_pulse_status(void);


void set_wait_delay(void);
E_WPULSE_STATE setup_pulse(E_PULSE_TYPE pulse_type, uint32_t avg_line_period, RELAY_ID_E relay_num, BOOL relay_type);
void set_relay_port(RELAY_ID_E relay_num, BOOL port_type, BOOL port_level);
void set_timer_expired(BOOL);
BOOL get_timer_expired(void);
E_LINE_PERIOD_TYPE line_period_meas(RELAY_ID_E relay_id, uint32_t * avg_period_value);
void timer0_reset_start(void);
E_PULSE_STATE get_pulse_state(void);

#endif // __RELAY_MODULE_H__
