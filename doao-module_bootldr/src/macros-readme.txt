//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016-2017
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  macros-readme.txt
//
//  This file document the purpose of each compiler pre-processor's
//  macro.
//
//  These macros can be define in the e2studio by adding it to the
//  menu in 
//      Project
//          -> Renesas Tools Setting 
//               -> C/C++ General
//                  -> Paths and Symbols
// in the "Symbols" tab.
//
// Macros are listed in alphabetic order.
//
//-----------------------------------------------------------------------------
//
// BOARD_VERSION_  prefix to defines the board version, used for board-specific
//                 contents
//   BOARD_VERSION_PRE_ALPHA : for pre-alpha only only
//   BOARD_VERSION_ALPHA:      for alpha board, curently default if not defined
//
// DOAO_UNIT_TEST  Define this macro to perform the module's independent unit 
//                 test. In unit test, the module will not listen to the RS485
//                 port for SC command. It will run test case pre-determined.
//
// DOAO_CRITICAL_TEST
//                 Define this macro if the build is for Hardware Critical Test.
//                 It will change the default module address from 0xfa to 0x01
//                 which is an assigned address. This will allow the module to
//                 by-pass the address assignment steps.
// 
// NEED_ADC_THREADX_TIMER
//                 Define this macro only if need to create a ThreadX timer
//                 for ADC scanning.
//            
//---------------------------------------------------------------------

