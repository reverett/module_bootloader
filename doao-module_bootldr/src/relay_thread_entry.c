//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  relay_thread_entry.c
//
//  Description: Main thread that calls relay state machines.
//
//-----------------------------------------------------------------------------

#include "common_def.h"
#include "relay_thread.h"
#include "switch_state.h"
#include "relay_state.h"
#include "hw_io_api.h"
#include "db_api.h"
#include "critical_section.h"
#include "control_thread_entry.h"
#include "relay_thread_entry.h"
#include "relay_module.h"
#include "sys_config.h"
#include "adc_scan.h"


TX_TIMER          line_meas_timer; // times out if no zero cross signal detected
static uint16_t   data0;
static uint16_t   data1;
static uint16_t   data2;
static uint16_t   mon_24v;
static uint16_t   data4;

// call back function when relay set command is accomplished.
// (state change completed)
control_cb_func_t   control_relay_instr_complete_cb_func = NULL;



void         relay_thread_entry(void);
static void _relay_timeout_callback(unsigned long int);





void relay_set_control_relay_instr_complete_cb(control_cb_func_t cb_func)
{
	control_relay_instr_complete_cb_func = cb_func;
}


static void _relay_timeout_callback(unsigned long int expire_input)
{

  SSP_PARAMETER_NOT_USED(expire_input);
  // timer expired no signal measured
  set_timer_expired(TRUE);

}

//------------------------------------------------------------------------------
// Name:        relay_create_relay_thx_timer
// Abstract:    Create a ThreadX timer which will timeout when no pulses are
//              measured by the input capture hardware.
// Arguments:   (none)
// Return:
//   STATUS_E:  SUCCESS, if successful;
//              ERR_HW_RLY_TIMER_INIT, if error occurred.
//------------------------------------------------------------------------------
STATUS_E relay_create_relay_thx_timer(void)
{

    if (tx_timer_create(&line_meas_timer,
    	                "Relay_Timeout",
                        _relay_timeout_callback,
						TIMER_VALUE,
                        RELAY_TIMEOUT,
						ONE_SHOT,
						TX_NO_ACTIVATE) != TX_SUCCESS)
    {
    	return ERR_HW_RLY_TIMER_INIT;
    }

    return SUCCESS;
}


//------------------------------------------------------------------------------
// Name:        relay_thread_entry
// Abstract:    This function implements the thread that maintains the relay state
//              machine and does ADC scanning.
//              Only one instance of the thread will be created at run-time.
// Arguments:   (none)
// Return:      (none)
//------------------------------------------------------------------------------
void relay_thread_entry(void)
{
    ssp_err_t err;

    err = g_timer0.p_api->open(g_timer0.p_ctrl, g_timer0.p_cfg);

    if (err != SSP_SUCCESS)
    {
        while(1)
        {
            /** Check err. */
        }
    }

    // gpt switch input
    g_timer2.p_api->open(g_timer2.p_ctrl, g_timer2.p_cfg);
    // gpt pulse
    g_timer3.p_api->open(g_timer3.p_ctrl, g_timer3.p_cfg);

    // scan a/d
    g_adc0.p_api->scanStart(g_adc0.p_ctrl);


    // Main thread loop
    // First part of the loop walk-thru each of the relay state
    // machine, updates relays' status in the database and
    // execute any commands waiting in the command queue (not the
    // message queue.
    // Second part of the loop scans the analog and digital ports,
    // updates the database for any changes.
    //
    while (1)
    {
        // call each relay state machine
        relayState(RELAY_1);
        relayState(RELAY_2);
        relayState(RELAY_3);
        relayState(RELAY_4);


        if (g_adc0.p_api->scanStatusGet(g_adc0.p_ctrl) == SSP_SUCCESS)
        {
            //tx_timer_deactivate(&adc_timer);

        	// setup one shot timer
            //tx_timer_change(&adc_timer, ADC_TIMEOUT, ONE_SHOT);
                                                                              // module#             schematic
            g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_0, &data0);     // P000        AN000   AN1_FB
            g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_1, &data1);     // P001        AN001   AN2_FB
            g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_2, &data2);     // P002        AN002   AN3_FB
            g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_3, &mon_24v);   // P003        AN003   24V_MON
            g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_4, &data4);     // P004        AN004   AN4_FB

            db_vt_set_output_monitor(RELAY_1, data0);
            db_vt_set_output_monitor(RELAY_2, data1);
            db_vt_set_output_monitor(RELAY_3, data2);
            db_vt_set_v24_input_monitor(mon_24v);
            db_vt_set_output_monitor(RELAY_4, data4);

            // check end of line and store in database
            if (adc_test_rs485bus_eol() == TRUE)
            {
                db_vt_set_rs485_bus_eol(TRUE);
            }
            else
            {
                db_vt_set_rs485_bus_eol(FALSE);
            }
            //set_adc_timer_expired(FALSE); // clear timer expired flag before starting timer
            //timer_status = tx_timer_activate(&adc_timer); // start the adc timer again

            // scan a/d
            g_adc0.p_api->scanStart(g_adc0.p_ctrl);
        }

        // Thread delay for common duration of all threads
        tx_thread_sleep(THREAD_COMMON_DELAY);

    }

}

