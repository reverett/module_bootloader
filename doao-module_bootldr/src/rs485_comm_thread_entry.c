//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  rx485_comm_thread_entry.c
//
//  Description: The DO-AO Module RS485 Communication Thread defintion
//               and related functions.
//
//-----------------------------------------------------------------------------

#include <tx_api.h>

#include "jellyfish_common.h"
#include "rs485_comm_thread.h"
#include "common_def.h"
#include "sc_command.h"
#include "db_def.h"
#include "db_api.h"
#include "rs485_driver.h"
#include "unit_test.h"

#include "stdlib.h"
#include "time.h"
#include <int_prot_frame.h>
#include <int_prot_cmd.h>
#include "rs485_driver.h"
#include "hal_data.h"
#include "flash_data.h"

//------------------------------------------------------------------------------
// Function Declarations
//------------------------------------------------------------------------------

void             rx485_comm_thread_entry(void);


//------------------------------------------------------------------------------
// Data Definitions
//------------------------------------------------------------------------------

unsigned short   attempts = 0;

//init values of module info
module_info_t module_info = {
		{0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07,
				0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F},
		DEFAULT_ADDR,
		DO,
		discover
};

extern sw_info_struct firmware_info;

//------------------------------------------------------------------------------
// Name:        rs485_comm_thread_entry
// Abstract:    This function implements the thread that handles the
//              communication between SC and the DO-AO module on the RS485
//              bus.
//              It will dispatched commands received from the SC to the
//              module's control thread.
//              Only one instance of the thread will be created at run-time.
// Arguments:   None.
// Return:      None.
//------------------------------------------------------------------------------
void rs485_comm_thread_entry(void)
{
	tx_thread_sleep((uint32_t)RX485_THREAD_DELAY*3);

	// Initialize the RS485 port
	// rs485_initialize();

	// Initialize Interrupt controller
	//R_ICU_ExternalIrqOpen(g_external_irq6.p_ctrl, g_external_irq6.p_cfg);

	fmi_product_info_t      *p_fmi_info;
	g_fmi0.p_api->productInfoGet(&p_fmi_info);
	memcpy(module_info.uid,p_fmi_info->unique_id,sizeof(module_info.uid));

	//read version information
    uint8_t length = VER_LENGTH; //sizeof(firmware_info.app.fw_info.ascii_info.version);
    uint8_t *ptr_to_data = &firmware_info.app.fw_info.ascii_info.version[0];
    memcpy(module_info.version, ptr_to_data, length);

    unsigned int seed = 0;
    for(int i=0;i<16;i++) seed^=module_info.uid[i];
	srand(seed);

	while (1) {
   		while (rs485_rx_data_available()==TRUE)
		{
   			unsigned char rs485_data = rs485_rx_read_byte();
            rha_bytes(&rs485_data,1);
        }
		//tx_thread_sleep((uint32_t)RX485_THREAD_DELAY);
        //R_BSP_SoftwareDelay(100, BSP_DELAY_UNITS_MILLISECONDS);

	}

/*
	// Loop forever

	attempts = 1;

    while (attempts <= 2)
    {
        tx_thread_sleep((uint32_t)RX485_THREAD_DELAY);

//-----------------------------------------------------------
//
// The following pseudo code are to demonstrate the logic
// of the thread.
//
//-----------------------------------------------------------

        //              |
        //              V
        // +-------------------------------------+
        // | wait_for_incoming_rs485_packet()    |
        // +-------------------------------------+
        //              |
        //              V
        // +--------------------------------------------+
        // | if (command_in_packet_payload == get_data) |
        // |    then                                    |
        // |      - get_data_from = database;           |
        // |      - pack_data_into_packet_payload;      |
        // |      - send_packet_to_sc_via_rs485;        |
        // +--------------------------------------------+
        //              |
        //              V
        // +----------------------------------------------+
        // | if (command_in_packet_payload == set_HW)     |
        // |    then                                      |
        // |      - get_cmd_from_packet_payload;          |
        // |      - call_api_to_create_msg_header;        |
        // |      - parse_above_cmd_for_each_set_instr;   |
        // |      - call_api_to_add_all_set_instr_to_msg; |
        // |      - call_api_to_queue_up_msg;             |
        // +----------------------------------------------+
        //              |
        //              V
        // +----------------------+
        // | resume_waiting       |
        // +----------------------+


//----------------------------------------------------
// Test of the "SET" commands
//----------------------------------------------------

        ut_dispatch_set_cmd_to_controller();

//----------------------------------------------------
// Test of getting the state of each relay
//----------------------------------------------------

        ut_get_relay_state();

//----------------------------------------------------
// Test of getting "Firmware String"
//----------------------------------------------------

//      ut_get_firmware_version_string();

//----------------------------------------------------
// Test of getting the EOL status of NODE in RS485 Bus
//----------------------------------------------------

        ut_get_rs485_eol_status();

//----------------------------------------------------
// Test of getting the EOL status of NODE in RS485 Bus
//----------------------------------------------------

//       ut_get_24v_input_measure();

//----------------------------------------------------
// Test of getting the ADC Count of AO (1, 2, 3, 4)
//----------------------------------------------------

//       ut_get_ao_adc_count();

//----------------------------------------------------
// repeat above tests
//----------------------------------------------------
        ++attempts;


    } // while (1)
*/
}
