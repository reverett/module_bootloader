//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  sc_command.c
//
//  Description: Implementation of the functions for creating System Controller's
//               various commands. These commands are used by the SC to instruct
//               the DO-AO module the actions to be taken by the moduole.
//
//               The commands will be placed as message in the module's message
//               queue and be parsed by the module's control function.
//
//-----------------------------------------------------------------------------


#include <string.h>
#include "common_def.h"
#include "sc_command.h"


 sc_cmd_t       sc_cmd;


//--------------------------------------------------------------------------------
// NAME      : sc_cmd_is_set_cmd_max_instr_reached
// ABSTRACT  : This function check if a SET command has already been added
//             with max number of instructions. If this function returns
//             TRUE, the caller need append the command to message queue
//             and then reset the command to start a new command.
// ARGUMENTS : (none)
// RETURN    :
//    BOOL  - TRUE if the SET command already has max number of instructions
//            FALSE otherwise.
//--------------------------------------------------------------------------------
BOOL sc_cmd_is_set_cmd_max_instr_reached(void)
{
	if (sc_cmd.cmd.u.params.num_of_instrs == SC_CMD_SET_INSTR_MAX_NUM)
	{
		return TRUE;
	}
	return FALSE;
}


//--------------------------------------------------------------------------------
// NAME      : sc_cmd_add_set_cmd_instr
// ABSTRACT  : This function add instructions (the "SET" actions) to the
//             SET command.
// ARGUMENTS :
//    unsigned char hw_name  - hardware name
//    unsigned char hw_id - hardware ID
//    unsigned char param - parameter to set on the hardware
//
// RETURN    :
//   STATUS_E - SUCCESS, if successful,
//              ERR_SC_CMD_INSTR_NUM, max number of instructions per
//				             command reached
//--------------------------------------------------------------------------------
STATUS_E sc_cmd_add_set_cmd_instr(unsigned char hw_name, unsigned char hw_id, unsigned char param)
{
	sc_cmd_set_instr_t *instr_p;

	if (sc_cmd_is_set_cmd_max_instr_reached() == TRUE)
	{
		return ERR_SC_CMD_INSTR_NUM;
	}

	instr_p = &sc_cmd.instrs.set[sc_cmd.cmd.u.params.num_of_instrs];

	instr_p->hw_name = hw_name;
	instr_p->hw_id = hw_id;
	instr_p->param = param;

	++sc_cmd.cmd.u.params.num_of_instrs;

	return SUCCESS;
}


//--------------------------------------------------------------------------------
// NAME      : sc_cmd_add_set_instr_cnt(void)
// ABSTRACT  : This function back-fill the total instruction count field
//             in the SET command header, which was left as 0 when first
//             initialized.
// ARGUMENTS :
//     unsigned char instr_cnt - number of instruction in this SET command
// RETURN    : (none)
//--------------------------------------------------------------------------------
void sc_cmd_add_set_instr_cnt(unsigned char instr_cnt)
{
	sc_cmd.cmd.u.params.num_of_instrs = instr_cnt;
}


//--------------------------------------------------------------------------------
// NAME      : sc_cmd_init_set_cmd
// ABSTRACT  : This function creates the System Controller's "SET" command
//             header part, resets the instruction counter, and clears the
//             memory for the instructions.
// ARGUMENTS : None.
// RETURN    :
//     sc_cmd_t *: pointer to the data of of the command holder.
//--------------------------------------------------------------------------------
sc_cmd_t *sc_cmd_init_set_cmd(void)
{

	// Clear the memory used to hold the instructions of "SET" command
	memset(&sc_cmd,0, sizeof(sc_cmd_t));

    // Set the SC command "SET" as the command tag
	sc_cmd.cmd.tag = (unsigned char)SC_CMD_SET;

	// Initialize the instruction count to 0
	// If any instruction is added to the SET command, this counter
	// will be incremented. If it remains as 0 when sending the
	// the message, there is  not instructions for the module to
	// perform.
	sc_cmd.cmd.u.params.num_of_instrs = 0;

	return (&sc_cmd);
}


