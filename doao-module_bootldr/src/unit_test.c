//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  unit_test.c
//
//  Description: The DO-AO Module unit test for various functions and capabilities.
//
//-----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include <control_thread_entry.h>
#include <tx_api.h>


#include "jellyfish_common.h"
#include "rs485_comm_thread.h"
#include "common_def.h"
#include "sc_command.h"
#include "sys_manager.h"
#include "rs485_driver.h"
#include "db_def.h"
#include "db_api.h"
#include "unit_test.h"

static STATUS_E       _ut_create_sc_cmd_msg(sc_cmd_t **sc_cmd_created_p);

version_tag_t         fmwr_version;

#define _BUF_SIZE     30
char                  buf[_BUF_SIZE];   // this is a generic buffer


//static char        *test_msg="1. sending cmd\n\r";


//------------------------------------------------------------------------------
// Name:        _ut_create_sc_cmd_msg
// Abstract:    This function create a test message which the SC sends
//              to the DO-AO module.
// Arguments:   None.
//     sc_cmd_t **sc_cmd_p: pointer to location to return the address of
//                          the created SC command;
// Return:
//     STATUS_E: status code:
//                SUCCESS - if successful;
//                ERR_SC_CMD_INSTR_NUM - max number instructions reach
//                        (failed to add this one)
//------------------------------------------------------------------------------
static STATUS_E _ut_create_sc_cmd_msg(sc_cmd_t **sc_cmd_created_p)
{
	STATUS_E status = SUCCESS;

    // Initialize the "SET" command structure and fill the header
    *sc_cmd_created_p = sc_cmd_init_set_cmd();

  	// add instructions to the SET command:
    //
 	// Note, the command structure can host predetermined number of
   	//       instructions. There are two ways to deal with the limit.
   	//       1. Call API sc_cmd_is_set_cmd_max_instr_reached() before
   	//          calling sc_cmd_add_set_cmd_instr() to determine if
   	//          the limit is reached; or
   	//       2. Call the sc_cmd_add_set_cmd_instr() function and check
   	//          the result code.
   	// In this example, method 2 is used.

    // Note, the counter inside the set command structure is incremented
   	// when each instruction is added.

//-------------------------------------------------------------------
// LED Tests
//-------------------------------------------------------------------

#if 1
    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_LED,
       		                               (unsigned char)LED_RELAY3,
 									       (unsigned char)LED_ON))
         		!= SUCCESS)
    {
        return status;
    }
#endif
#if 1
    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_LED,
       		                               (unsigned char)LED_RELAY2,
										   (unsigned char)LED_ON))
          		!= SUCCESS)
    {
   	    return status;
    }
#endif
#if 0
    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_LED,
       		                               (unsigned char)LED_RELAY3,
										   (unsigned char)LED_REVERSE))
           		!= SUCCESS)
    {
   	    return status;
    }
#endif
#if 0
    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_LED,
       		                               (unsigned char)LED_RELAY4,
										   (unsigned char)LED_REVERSE))
           		!= SUCCESS)
    {
        return status;
    }

#endif

//-------------------------------------------------------------------
// PWM Tests (Analog Output)
//-------------------------------------------------------------------

#if 0
    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_ANALOG_PWM,
       		                               (unsigned char)PWM_1,
 									       (unsigned char)50))
         		!= SUCCESS)
    {
        return status;
    }
#endif
#if 0
    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_LED,
      		                               (unsigned char)LED_RELAY3,
 									       (unsigned char)LED_ON))
         		!= SUCCESS)
    {
   	    return status;
    }
#endif
#if 0
    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_ANALOG_PWM,
      		                               (unsigned char)PWM_4,
										   (unsigned char)40))
          		!= SUCCESS)
    {
       	return status;
    }
#endif
#if 0
    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_ANALOG_PWM,
       		                               (unsigned char)PWM_1,
   	 							           (unsigned char)10))
          		!= SUCCESS)
    {
        return status;
    }
#endif

#if 0
    if ((status=sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_RS485_EOL,
      		                             (unsigned char)0,  // this parameter is ignored
 										 (unsigned char)RS485_EOL_DISABLE))
         	    != SUCCESS)
    {
        return status;
    }
#endif

//-------------------------------------------------------------------
// Relay Test
//-------------------------------------------------------------------

#if 0
    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_RELAY,
      		                               (unsigned char)RELAY_1,
										   (unsigned char)ON))
        		!= SUCCESS)
    {
      	return status;
    }
#endif
#if 0
    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_RELAY,
      		                               (unsigned char)RELAY_1,
 	     								   (unsigned char)OFF))
         		!= SUCCESS)
    {
        return status;
    }
#endif
#if 0
    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_RELAY,
       		                               (unsigned char)RELAY_4,
										   (unsigned char)ON))
        		!= SUCCESS)
    {
 	    return status;
    }
#endif
#if 0
    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_RELAY,
      		                               (unsigned char)RELAY_1,
										   (unsigned char)OFF))
        		!= SUCCESS)
    {
  	    return status;
    }
#endif
#if 0
    if ((status=sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_RELAY,
       		                             (unsigned char)RELAY_1,
										 (unsigned char)ON))
        	    != SUCCESS)
    {
        return status;
    }
#endif

//-------------------------------------------------------------------
// ZC Test
//-------------------------------------------------------------------

#if 0
    if ((status=sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_ZC_DISABLE,
      		                             (unsigned char)ZC_RELAY_1,
 										 (unsigned char)ZC_DISABLE))
         	    != SUCCESS)
    {
        return status;
    }
#endif

#if 0
    if ((status=sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_ZC_DISABLE,
      		                             (unsigned char)ZC_RELAY_2,
 										 (unsigned char)ZC_ENABLE))
         	    != SUCCESS)
    {
        return status;
    }
#endif

#if 0
    if ((status=sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_ZC_DISABLE,
      		                             (unsigned char)ZC_RELAY_1,
 										 (unsigned char)ZC_DISABLE))
         	    != SUCCESS)
    {
        return status;
    }
#endif

#if 0
    if ((status=sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_ZC_DISABLE,
      		                             (unsigned char)ZC_RELAY_2,
 										 (unsigned char)ZC_ENABLE))
         	    != SUCCESS)
    {
        return status;
    }
#endif

//-------------------------------------------------------------------
// EOL Test
//-------------------------------------------------------------------
#if 0
    if ((status=sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_RS485_EOL,
      		                             (unsigned char)0,  // this parameter is ignored
 										 (unsigned char)RS485_EOL_ENABLE))
         	    != SUCCESS)
    {
        return status;
    }
#endif
#if 0
    if ((status=sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_RS485_EOL,
      		                             (unsigned char)0,  // this parameter is ignored
 										 (unsigned char)RS485_EOL_DISABLE))
         	    != SUCCESS)
    {
        return status;
    }
#endif

//-------------------------------------------------------------------
// End of Test Sample Instructions
//-------------------------------------------------------------------
    // settle the total instruction count in the command header
    // sc_cmd_add_set_instr_cnt( instr_cnt );

    return (SUCCESS);

}


//------------------------------------------------------------------------------
// Name:        ut_dispatch_set_cmd_to_controller
// Abstract:    This function demonstrate how to dispatch SET command
//              to the DO-AO's controller thread.
//              It can be replaced with actual code which parsed the
//              RS485 packet pay-load and dispatch the SET command to
//              the controller thread.
// Arguments:   None.
// Return:
//     STATUS_E - SUCCESS if successful,
//                FAILURE otherwise
//-----------------------------------------------------------------------------
STATUS_E ut_dispatch_set_cmd_to_controller(void)
{

  	sc_cmd_t      *cmd_p;
  	STATUS_E       status = SUCCESS;


     R_BSP_SoftwareDelay(100, BSP_DELAY_UNITS_MILLISECONDS);

 	// Create the test message (with SET command)
    status = _ut_create_sc_cmd_msg(&cmd_p);

    switch (status)
    {
      case SUCCESS:
      case ERR_SC_CMD_INSTR_NUM: // consider it is not fatal
        {
            if (cmd_p != NULL)
            {
            	control_send_msg_to_controller(cmd_p);
            }
        	return SUCCESS;
        }
      case FAILURE:
      default:
        {
    	    return FAILURE;
        }
    }

}

//------------------------------------------------------------------------------
// Name:        ut_get_relay_state
// Abstract:    This function demonstrates how to call database API to get the
//              state of each relay.
//              Note, such information is in the database and periodically
//              refreshed by the relay thread.
// Arguments:   (none)
// Return:
//     STATUS_E - SUCCESS if successful;
//     FAILURE - Owtherwise
//------------------------------------------------------------------------------
STATUS_E ut_get_relay_state()
{
	SWITCH_STATE_E state;
    unsigned char relay_id;

	memset(buf, '\0', _BUF_SIZE);

	for (relay_id=1; relay_id<=4; relay_id++)
	{
	    // Call DB API here
	    if (db_vt_get_relay_state(relay_id, &state) != SUCCESS)
	    {
		    strcpy(buf, "unknown output adc count\r\n\0");
	    }
		strcpy(buf, "relay ");

		buf[5]=(char)(relay_id+0x30);
		buf[6]='=';
		buf[7]=(char)(state+0x30);
		buf[8]='\n';
		buf[9]='\r';
		buf[10]='\0';

	    // print the message on the RS485 bus
		rs485_tx_write_nbyte(buf, 11);
	}

	return SUCCESS;
}

//------------------------------------------------------------------------------
// Name:        ut_get_firmware_verion_string
// Abstract:    This function demonstrates how to call database API to get the
//              firmware version info structured data and convert it to a
//              string that can be transmitted on the RS485 bus.
//              The firmware version is a 3-byte numerical value:
//                  byte0: major, e.g., 02
//                  byte1: minor, e.g., 13
//                  byte2: delta, e.g., 04
// Arguments:   (none)
// Return:
//     STATUS_E - SUCCESS if successful;
//     FAILURE - Owtherwise
//------------------------------------------------------------------------------
STATUS_E ut_get_firmware_version_string(void)
{

	// Call DB API here
	if (db_nvt_get_firmware_version(&fmwr_version) != SUCCESS)
	{
		return FAILURE;
	}

	memset(buf, '\0', _BUF_SIZE);

    // Convert the first 2-byte (version number) from numerical
    // value to ASCII characters
    strncpy(&buf[0], (char *)&fmwr_version, 7);
    buf[7] = '\n';
    buf[8] = '\r';
    buf[9] = '\0';

    // Now 'buf' contains the string converted from the version structure.

    rs485_tx_write_nbyte(buf, _BUF_SIZE);

    return SUCCESS;
}

//------------------------------------------------------------------------------
// Name:        ut_get_rs485_eol_status
// Abstract:    This function demonstrates how to call database API to get the
//              End-of-Line status of the DO-AO Module in RS485 bus.
//              Note, such information is in the database and periodically
//              refreshed by the relay thread.
// Arguments:   (none)
// Return:
//     STATUS_E - SUCCESS if successful;
//     FAILURE - Owtherwise
//------------------------------------------------------------------------------
STATUS_E ut_get_rs485_eol_status(void)
{
	BOOL eol_status;

	memset(buf, '\0', _BUF_SIZE);

	// Call DB API here
	if (db_vt_get_rs485_bus_eol(&eol_status) != SUCCESS)
	{
		strcpy(buf, "unknown EOL status\r\n\0");
	}
	else
	{
        if (eol_status == TRUE)
        {
            strcpy(buf, "EOL=1\n\r\0");
        }
        else
        {
        	strcpy(buf, "EOL=0\n\r\0");
        }
	}

    // print the message on the RS485 bus
	rs485_tx_write_nbyte(buf, _BUF_SIZE);


    return SUCCESS;
}

//------------------------------------------------------------------------------
// Name:        ut_get_24v_input_measure
// Abstract:    This function demonstrates how to call database API to get the
//              24V input monitor measurement.
//              Note, such information is in the database and periodically
//              refreshed by the relay thread.
// Arguments:   (none)
// Return:
//     STATUS_E - SUCCESS if successful;
//     FAILURE - Owtherwise
//------------------------------------------------------------------------------
STATUS_E ut_get_24v_input_measure(void)
{
	unsigned v24_input;

	memset(buf, '\0', _BUF_SIZE);

	// Call DB API here
	if ( db_vt_get_v24_input_monitor(&v24_input) != SUCCESS)
	{
		strcpy(buf, "unknown v24 measurement\r\n\0");
	}

	strcpy(buf, "24v input=");

	buf[12]=(char)((v24_input/10)+0x30);
	buf[13]=(char)((v24_input%10)+0x30);
	buf[14]='\n';
	buf[15]='\r';
	buf[16]='\0';

    // print the message on the RS485 bus
	rs485_tx_write_nbyte(buf, _BUF_SIZE);

    return SUCCESS;
}


//------------------------------------------------------------------------------
// Name:        ut_get_ao_adc_count
// Abstract:    This function demonstrates how to call database API to get the
//              ADC count of Analog Output.
//              Note, such information is in the database and periodically
//              refreshed by the relay thread.
// Arguments:   (none)
// Return:
//     STATUS_E - SUCCESS if successful;
//     FAILURE - Owtherwise
//------------------------------------------------------------------------------
STATUS_E ut_get_ao_adc_count()
{
	unsigned ao_adc_count;
    unsigned char relay_id;

	memset(buf, '\0', _BUF_SIZE);

	for (relay_id=1; relay_id<=4; relay_id++)
	{
	    // Call DB API here
	    if (db_vt_get_output_monitor((RELAY_ID_E)relay_id, &ao_adc_count)
	    		!= SUCCESS)
	    {
		    strcpy(buf, "unknown output adc count\r\n\0");
	    }
		strcpy(buf, "adc count ");

		buf[10]=(char)(relay_id+0x30);
		buf[11]='=';
		buf[12]=(char)((ao_adc_count/10)+0x30);
		buf[13]=(char)((ao_adc_count%10)+0x30);
		buf[14]='\n';
		buf[15]='\r';
		buf[16]='\0';

	    // print the message on the RS485 bus
		rs485_tx_write_nbyte(buf, 17);
	}

	return SUCCESS;

}


