//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  sys_manager.h
//
//  Description: Declaration of system management functions.
//
//-----------------------------------------------------------------------------

#ifndef __SYS_MANAGER_H__
#define __SYS_MANAGER_H__

// Include files of standard C language

#include <string.h>

#include "tx_api.h"

// Include files of SSP and ThreadX


// Include files of Jellyfish DO-AO Module


// Total space to allocate
#define SYS_TX_BYTEPOOL_SIZE            512

//---------------------------------------------------------------------
// Function Declarations
//---------------------------------------------------------------------
void            tx_application_define_user(void *first_unused_memory);
TX_BYTE_POOL   *sys_get_tx_byte_pool(void);

#endif // __SYS_MANAGER_H__
