//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  sys_manager.c
//
//  Description: Implementation of system management functions, such as thread
//               memory allocation pool creation, timer, event message queue
//               creation, system initialization and shutdown, etc.
//
//-----------------------------------------------------------------------------

// Include files of standard C language

#include <control_thread_entry.h>
#include <string.h>

// Include files of SSP and ThreadX

#include <tx_api.h>


// Include files of Jellyfish DO-AO Module

#include "common_def.h"
#include "control_thread.h"
#include "relay_thread_entry.h"
#include "sys_manager.h"
#include "relay_state.h"
#include "hw_io_api.h"
#include "sc_command.h"
#include "db.h"
#include "rs485_driver.h"
#include "adc_scan.h"


//---------------------------------------------------------------------
// Local Data Type Declarations
//---------------------------------------------------------------------

//---------------------------------------------------------------------
// Local Function Declarations
//---------------------------------------------------------------------
static void      _sys_shutdown_system(BOOL reset_to_btldr_flg);
static void      _sys_reset_to_bootloader(void);
static void      _sys_halt_system(void);
static STATUS_E  _sys_create_memory_pool(void *memory_p);

// static void   _sys_create_relay_timer(void);
// static void   _relay_timeout_callback(unsigned long int);



//---------------------------------------------------------------------
// Static Data Definitions
//---------------------------------------------------------------------

static TX_BYTE_POOL       tx_byte_pool;
static BOOL               tx_byte_pool_created_flg = FALSE;
TX_QUEUE                  iom_msg_queue;
unsigned long             rs485_message;


#ifdef DOAO_UNIT_TEST
// char        clear_screen[7] = {0x1B, 0x5B, '2', 'J', 0x1B, 0x5B, 'H',};
// char       *prompt_msg      = "Jellyfish DOAO Module Initialized, now start Threads ....\r\n";
// char       *prompt_msg      = "Jellyfish\r\n";
#endif


//--------------------------------------------------------------------------------
// NAME      : sys_getcreate_msg_queue
// ABSTRACT  : This function returns the ThreadX's "byte pool" created when
//             the system starts.
// Arguments:  None.
// RETURN    :
//   TX_BYTE_POOL *: pointer to the location of the ThreadX byte pool if the
//                   has been created successfully;
//                   NULL, otherwise.
//--------------------------------------------------------------------------------
TX_BYTE_POOL *sys_get_tx_byte_pool(void)
{

    if (tx_byte_pool_created_flg == TRUE)
    {
    	return (&tx_byte_pool);
    }
    else
    {
    	return ((TX_BYTE_POOL *)NULL);
    }
}


//--------------------------------------------------------------------------------
// NAME      : _sys_create_memory_pool
// ABSTRACT  : This function creates a ThreadX's "byte-pool" which will be used
//             for allocating memory for different entities, such as threads,
//             message queue, event, etc.
//             There will be only one such pool being created and used for the
//             whole system. It will be accessed by other modules via API.
// Arguments:
//   void *memory_p - location where memory allocation start
//                    (This is passed on down from the ThreadX system)
// RETURN    :
//   STATUS_E : SUCCESS           - successful;
//              ERR_THRD_MEM_POOL - otherwise
//--------------------------------------------------------------------------------
STATUS_E _sys_create_memory_pool(void *memory_p)
{

    if (tx_byte_pool_create(&tx_byte_pool, "tx_byte_pool", memory_p, SYS_TX_BYTEPOOL_SIZE)
    		!= TX_SUCCESS)
    {
    	return (ERR_THRD_MEM_POOL);
    }
    else
    {
    	tx_byte_pool_created_flg = TRUE;
    	return SUCCESS;
    }
}


//------------------------------------------------------------------------------
// Name:        sys_bootloader_reset
// Abstract:    This function, when called, will cause the system reset.
//              Upon reset, the system will jump to routines determined by
//              the reset vector which will lead to the Bootloader.
// Arguments:   None.
// Return:      None.
//------------------------------------------------------------------------------
static void _sys_reset_to_bootloader(void)
{
	while (1)
	{
		// Reset the system to bootloader
		// NVIC_SystemReset();
		;
	}
}


//------------------------------------------------------------------------------
// Name:        _sys_halt_system
// Abstract:    This function, when called, halt the system by entering an
//              infinite loop.
//              *****TBD*****Jianbai
//              Is there a better wait to do the system halt??
// Arguments:   None.
// Return:      None.
//------------------------------------------------------------------------------
static void _sys_halt_system(void)
{
	while (1)
	{
		;
	}
}


//------------------------------------------------------------------------------
// Name:        _sys_shutdown_system
// Abstract:    This function implements the system shutdown sequence, which
//              include disabling interrupt, flush the message queue, clear
//              event, disabling the UART, write out daily logging tp flash
//              memory. Upon completion of the house-keep chores, it will
//              reset the system.
// Arguments:
//     BOOL reset_to_btldr_flg - a flag to indicate if whether to halt
//              the system or reset to bootloader.
// Return:
//     None.
//------------------------------------------------------------------------------
static void _sys_shutdown_system(BOOL reset_to_bootldr_flg)
{

	//---------------------------------------------------------------
	// Disable IRQ
	//---------------------------------------------------------------

	//---------------------------------------------------------------
	// Disable UART
	//---------------------------------------------------------------

	//---------------------------------------------------------------
	// Disable RS485 traffic
	//---------------------------------------------------------------

	//---------------------------------------------------------------
	// Disable Timer
	//---------------------------------------------------------------

	//---------------------------------------------------------------
	// Flash write database
	//---------------------------------------------------------------

	//---------------------------------------------------------------
	// Clear message Queue
	//---------------------------------------------------------------
	// Clear IRQ
	// for (thread thread_p)
	// {
	//   ***** Do not terminate myself
	//     tx_thread_terminate(p);
	// }
    // if (BOOTLDR_RESET == TRUE)
	// {
	//	   hw_bootloader_reset();
	// }
	// else
	// {
	// 	  th_thread_terminate((thread_t myself);
	// }

    // All jobs in the shutdown sequence has completed.
	// Now, enter the final stage of the shutdown process.
	if (reset_to_bootldr_flg == TRUE)
	{
	    _sys_reset_to_bootloader();
	}
	else
	{
		_sys_halt_system();
	}

}


//------------------------------------------------------------------------------
// Name:        _sys_pre_thread_init
// Abstract:    This function implements the system startup sequence prior to
//              entering the individual threads. The jobs during the startup
//              includes three major steps:
//                    Step 1: disable IRQ, UART, and other service;
//                    Step 2: create memory pool, message queue, event, timer, etc.;
//                            and enabling hardware functions;
//                    Step 3: enable IRQ, UART, and the services.
// Arguments:
//     void *first_unused_memory - pointer to start allocating memory.
//                         This is passed on down from the ThreadX kernal.
// Return:
//     STATUS_E - SUCCESS, if successful;
//                ERR_SYS_INIT, otherwise.
//------------------------------------------------------------------------------
static STATUS_E _sys_pre_thread_init(void *first_unused_memory)
{

	//---------------------------------------------------------------
	// Disable IRQ
	//---------------------------------------------------------------

	//---------------------------------------------------------------
    // Disable Timer
	//---------------------------------------------------------------

//---------------------------------------------------------------
// Disable/Turn-off Hardware as necessary
//---------------------------------------------------------------

	// Disable RS485 traffic - system not ready to handle packet yet
	rs485_disable_rw();

	// Turn off all LEDs
	hw_set_all_leds(OFF);

    // Set all relays off (for test only)
    hw_set_all_relay_off_immediately();

//---------------------------------------------------------------
// Create ThreadX components
//---------------------------------------------------------------

    // Create ThreadX Memory Pool ("byte" pool)
	if (_sys_create_memory_pool(first_unused_memory) != SUCCESS)
	{
		return ERR_SYS_INIT;
	}

	// Create ThreadX Message Queue for RS485 thread to communicate
	// the Relay's main control thread
	if (control_create_msg_queue() != SUCCESS)
	{
	    return ERR_SYS_INIT;
	}

	// Create ThreadX MUTEX for the Database
	if (db_create_mutex() != SUCCESS)
	{
	    return ERR_SYS_INIT;
	}

	// Create ThreadX timer for relay state machine
	if (relay_create_relay_thx_timer() != SUCCESS)
	{
		return ERR_SYS_INIT;
	}

	// Create a ThreadX timer for ADC scanning
	//
	// Note this timer is not needed if thread goes to sleep for
	// "delay" time

// #define NEED_ADC_THREADX_TIMER 1
#ifdef NEED_ADC_THREADX_TIMER
    if (adc_create_adc_thx_timer() != SUCCESS)
    {
    	return ERR_SYS_INIT;
    }
#endif


//---------------------------------------------------------------
// Create/Initialize Database
//---------------------------------------------------------------

    db_initialization();

//---------------------------------------------------------------
// Enable/Turn-on Hardware as necessary
//---------------------------------------------------------------

    // Enable the Zero-Cross function of all relays
    hw_set_enable_all_zc();

    // set eol termination
    hw_init_eol_circuit();

    // Initialize timers for ADC scanning
    if (adc_init_hw_timer() != SUCCESS)
    {
        return ERR_SYS_INIT;
    }

    // Initialize ADC scanning channels
    if (adc_init_adc_channel() != SUCCESS)
    {
    	return ERR_SYS_INIT;
    }

    // Set the Analog output initial level
    adc_set_pwm_init_level();

	// Initialize the RS485 Ports, enable RS485 traffic
    rs485_initialize();

    init_data_memory();
//---------------------------------------------------------------
// Enable IRQ
//---------------------------------------------------------------

    // To be added

	return SUCCESS;
}


//------------------------------------------------------------------------------
// Name:        tx_application_define_user
// Abstract:    This function implements performs the system initialization
//              prior to entering the thread by calling the following function
//                            _sys_pre_thread_init();
//
//              This function itself is called by the ThreadX Kernel from
//              ThreadX's-defined function
//                            tx_application_define()
//              as user-defined application initializer prior to entering
//              the threads.
//	            The procedure of system startup is as the following:
//                        main()                 // SSP auto-generated
//                          |
//                          V
//                        tx_kernal_enter()
//                          |
//                          V
//                       tx_application_define_user() // this function
//                       then
//                       create_each_thread()
//
//              Upon successfully completion of each of the system initialization,
//              this function returns execution to the Kernel which will start
//              the threads. If the initialization failed, this function will
//              not relinquish execution, but rather goes into shutdown process
//              which may eventually lead to system reset.
//
// Arguments:
//     void *first_unused_memory - pointer to start allocating memory.
//                         This is passed on down from the ThreadX kernal.
// Return:
//     None.
//------------------------------------------------------------------------------
void tx_application_define_user(void *first_unused_memory)
{
	// We use a user-defined entry point to collect all system's
	// initialization

	if (_sys_pre_thread_init(first_unused_memory) != SUCCESS)
	{
		BOOL reset_to_bootldr_flg = TRUE;

		_sys_shutdown_system(reset_to_bootldr_flg);
	}

	// Print an ESC code to clear the screen
//	rs485_tx_write_nbyte(clear_screen, (unsigned short)7);

	// Print a prompt to the RS485 line
//	rs485_tx_write_nbyte(prompt_msg, (unsigned short)strlen(prompt_msg));

    // Upon return of this function, the thread kernel will start
    // all the individual user-defined threads.
	//init_data_memory();
}

