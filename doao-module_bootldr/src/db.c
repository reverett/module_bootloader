//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  db.c
//
//  Description: Functions for setup and initializing the Database.
//
//               Note, all accessing of the database, read or write, must
//               lock the database first. This makes things simple.
//               If it is necessary to allow read-read, the code can be
//               improved for that purpose.
//
//               All database assessing APIs are supposed to check and lock
//               the database prior to accessing it.
//-----------------------------------------------------------------------------

#include <string.h>


#include "jellyfish_common.h"
#include "common_def.h"
#include "tx_api.h"
#include "db_def.h"
#include "db.h"
#include "sys_config.h"
#include "version.h"



//----------------------------------------------------------------------------
// Database Definition
//----------------------------------------------------------------------------

// The Non-Volatile Table
doao_module_db_nv_t     db_nv_tbl;

// The Volatile Table
doao_module_db_v_t      db_v_tbl;

// Flag to indicate that the database has been initialized
static BOOL            _db_initialized = FALSE;

// Mutex for locking database for exclusive access
// We have two tables in the database and the access of
// the two tables are for different reasons.
// But we still just create one mutex for the sake
// of saving code size.
static TX_MUTEX        _db_mutex;

// Counter for total database's Non-volatile Table (Flash) write count
unsigned long          _db_nv_tbl_write_cnt;

// Flag to indicate that the database has been changed since
// last whole-database query. It will to be check if a query
// is specifically asking for a field in the database.
unsigned               _db_changed = TRUE;


//--------------------------------------------------------------------------------
// NAME      : db_create_mutex
// ABSTRACT  : This function creates the MUTEX used by the database for read/write
//             synchronization.
// ARGUMENTS : (none)
// RETURN    :
//     STATUS_E : SUCCESS         - successful;
//              ERR_THRD_MEM_POOL - if the memory pool is not created
//              ERR_MSGQ_CREATE   - otherwise
//--------------------------------------------------------------------------------
STATUS_E db_create_mutex(void)
{

 	if (tx_mutex_create(&_db_mutex,       // created mutex
			           "database_mutex",  // name of the mutex
						TX_INHERIT)       // raise mutex owner's priority when
			!= TX_SUCCESS)                // another thread is competing
    {
    	return (ERR_MUTEX_CREATE);
    }

   	return (SUCCESS);

}

//--------------------------------------------------------------------------------
// NAME      : db_lock
// ABSTRACT  : This locks the database for exclusive access.
//             Note, this will lock both the volatile and non-volatile
//             portion of the database at the same, although the two
//             two tables accessed separate. The purpose is to save
//             code size.
// ARGUMENTS : None.
// RETURN    :
//   STATUS_E : SUCCESS - successful;
//              ERR_DB_LOCK - failed
//--------------------------------------------------------------------------------
STATUS_E db_lock(void)
{

    // This needs to be non-waiting
	//
    if (tx_mutex_get(&_db_mutex, TX_NO_WAIT) == TX_SUCCESS)
    {
    	return SUCCESS;
    }
    else
    {
    	return ERR_DB_LOCK;
    }

}


//--------------------------------------------------------------------------------
// NAME      : db_wait_lock
// ABSTRACT  : This locks the database for exclusive access.
//             Note, this will lock both the volatile and non-volatile
//             portion of the database at the same, although the two
//             tables are accessed separately. in order to save code
//             size.
// ARGUMENTS : None.
// RETURN    :
//   STATUS_E : SUCCESS - successful;
//              ERR_DB_LOCK - failed
//--------------------------------------------------------------------------------
#if 0
STATUS_E db_wait_lock(void)
{

    if (tx_mutex_get(&_db_mutex, TX_WAIT_FOREVER) == TX_SUCCESS)
    {
    	return SUCCESS;
    }
    else
    {
    	return ERR_DB_LOCK;
    }

}
#endif

//--------------------------------------------------------------------------------
// NAME      : db_unlock
// ABSTRACT  : This unlocks the database after exclusive access.
// ARGUMENTS : None.
// RETURN    :
//   STATUS_E : SUCCESS - successful;
//              ERR_DB_LOCK - failed
//--------------------------------------------------------------------------------
STATUS_E db_unlock(void)
{

//	return SUCCESS;

     if (tx_mutex_put(&_db_mutex) == TX_SUCCESS)
    {
    	return SUCCESS;
    }
    else
    {
    	return ERR_DB_LOCK;
    }

}


//--------------------------------------------------------------------------------
// NAME      : db_initialization
// ABSTRACT  : This initializes the database.
// ARGUMENTS : None.
// RETURN    :
//   STATUS_E : SUCCESS - successful;
//              ERR_DB_INIT - failed
//--------------------------------------------------------------------------------
STATUS_E db_initialization(void)
{

    // *****TBD***** (Jianbai)
	// The during the life time of a module the NV table
	// of the database should only be initialized once.
	// But the volatile table should be initialized each
	// power-on and be updated with HW status immediately.

	// First check to see if the DB is already initialized
	if (_db_initialized == TRUE)
	{
		return SUCCESS;
	}

	// The database has not been initialized yet, initialize
	// now. Do not attempt to get/put MUTEX at this time since
	// the execution, although in ThreadX kernel, but individual
	// threads has not been started yet.






    // Set the flag
    _db_initialized = TRUE;

    // database has been initialized OK, regardless whether it is
    // unlocked OK.

    return SUCCESS;

}


//--------------------------------------------------------------------------------
// NAME      : db_is_initialized
// ABSTRACT  : This function checks to see if the database has been intialized.
// ARGUMENTS : None.
// RETURN    :
//   BOOL : TRUE  - the database is initialized;
//          FALSE - otherwise.
//--------------------------------------------------------------------------------
BOOL db_is_initialized(void)
{

	return _db_initialized;

}


//--------------------------------------------------------------------------------
// NAME      : db_has_changed
// ABSTRACT  : This function check to see if any part of the database has been
//             updated since last query of the entire database (the volatile
//             table for sure, but not sure about the non-volatile table yet).
//
//             The rationale is that not only the transmission of the entire
//             database will takes time and bandwidth, but the SC while have
//             to rake through each data and update it's knowledge-base. So it
//             is beneficial for not sending the entire database if there has
//             not been any changes since last such inquiry.
//
//             However, if the SC needs a specific field of the database, we
//             will send the data back regardless, since the benefit of time
//             saving is little and there must be a reason the SC needs it.
//
//             *****TBD***** (Jianbai)
//             The Volatile and the Non-Volatile portion of the database need
//             be treated separately. This is currently yet to be implemented.
//
// ARGUMENTS : None.
// RETURN    :
//   STATUS_E : SUCCESS - successful;
//              ERR_DB_INIT - failed
//--------------------------------------------------------------------------------
BOOL db_is_changed()
{
	if (_db_changed == TRUE)
	{
		return TRUE;
	}
	else
		return FALSE;
}



//--------------------------------------------------------------------------------
// NAME      : db_set_changed_flg
// ABSTRACT  : This function set/clear the database "changed" flag.
// ARGUMENTS : BOOL - changed_flg
// RETURN    : None.
//-------------------------------------------------------------------------------
void db_set_changed_flg(BOOL changed_flg)
{
	_db_changed = changed_flg;
}


