//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  relay_module.c
//
//  Description: Contains functions associated with:
//               - measuring line period
//               - controlling the ports to switch relays
//-----------------------------------------------------------------------------
#include "hal_data.h"
#include "common_def.h"
#include "relay_thread.h"
#include "relay_module.h"
#include "relay_state.h"
#include "critical_section.h"
#include "sys_config.h"

#define NUM_MEASUREMENTS (4)

typedef struct st_relay_pulse
{
    RELAY_ID_E             relay_num;
    BOOL                   relay_type;
} relay_pulse_t;

typedef struct
{
    uint32_t counter;
    uint32_t overflows;
}capture_time_t;


static relay_pulse_t relay_data;
static uint32_t g_arg_event;

// gpt timer 0
static capture_time_t capture = {0, 0};

static uint32_t line_period_relay1[NUM_MEASUREMENTS];
static uint8_t capture_tail_relay1 = 0;
static uint8_t capture_count_relay1 = 0;
static uint32_t line_measure_avg_relay1 = 0;
static uint32_t line_measure_sum_relay1 = 0;

static uint32_t line_period_relay2[NUM_MEASUREMENTS];
static uint8_t capture_tail_relay2 = 0;
static uint8_t capture_count_relay2 = 0;
static uint32_t line_measure_avg_relay2 = 0;
static uint32_t line_measure_sum_relay2 = 0;

static uint32_t line_period_relay3[NUM_MEASUREMENTS];
static uint8_t capture_tail_relay3 = 0;
static uint8_t capture_count_relay3 = 0;
static uint32_t line_measure_avg_relay3 = 0;
static uint32_t line_measure_sum_relay3 = 0;

static uint32_t line_period_relay4[NUM_MEASUREMENTS];
static uint8_t capture_tail_relay4 = 0;
static uint8_t capture_count_relay4 = 0;
static uint32_t line_measure_avg_relay4 = 0;
static uint32_t line_measure_sum_relay4 = 0;

static E_WPULSE_STATE wait_pulse_status = WPULSE_NONE;
static int pulse_state = PULSE_TIMER_INIT;

static BOOL first_pulse = TRUE;

#define MAX_COUNTS_16 0xFFFF // max counts for 16 bit counters capture channels 1-7

#define HI_PERIOD_LIM  1010526 // 47.5 Hz @ 48 MHz // 31579  // 47.5 Hz 1.5 MHz // 505263  // @ 47.5 Hz
#define LO_PERIOD_LIM  761905  // 63 Hz @ 48 MHz // 23810  // 63 Hz   1.5 MHz // 380952   // @ 63   Hz

#define RELAY_PULSE   15000  // 15 msec @ 1 MHz pulse to open close relay
#define NEXT_ZC       8400 // 8.4 msec // 9200  // 9.2 msec 1 MHz // 220800 // 9.2 msec with 24MHz clock
#define CLOSE_TIME    500   // 0.5 msec         // 12000 // 0.5 msec with 24MHz clock
#define OPEN_TIME     500   // 0.5 msec        // 12000 // 0.5 msec with 24MHz clock

void set_first_pulse_flag(void);
BOOL get_first_pulse_flag(void);

// set the relay pulse according to
void set_relay_port(RELAY_ID_E relay_num, BOOL port_type, BOOL port_level)
{
    switch (relay_num)
    {
        case RELAY_1:
        {
            if (port_level)
            {
                if (port_type)
                {
                   g_ioport.p_api->pinWrite(RELAY1_MAKE_PIN, IOPORT_LEVEL_HIGH); // Relay_1
                }
                else
                {
                   g_ioport.p_api->pinWrite(RELAY1_BREAK_PIN, IOPORT_LEVEL_HIGH); // !Relay_1
                }
            }
            else
            {
                if (port_type)
                {
                   g_ioport.p_api->pinWrite(RELAY1_MAKE_PIN, IOPORT_LEVEL_LOW); // Relay_1
                }
                else
                {
                   g_ioport.p_api->pinWrite(RELAY1_BREAK_PIN, IOPORT_LEVEL_LOW); // !Relay_1
                }
            }
        }
        break;
        case RELAY_2:
        {
            if (port_level)
            {
                if (port_type)
                {
                   g_ioport.p_api->pinWrite(RELAY2_MAKE_PIN, IOPORT_LEVEL_HIGH); // Relay_2
                }
                else
                {
                   g_ioport.p_api->pinWrite(RELAY2_BREAK_PIN, IOPORT_LEVEL_HIGH); // !Relay_2
                }
            }
            else
            {
                if (port_type)
                {
                   g_ioport.p_api->pinWrite(RELAY2_MAKE_PIN, IOPORT_LEVEL_LOW); // Relay_2
                }
                else
                {
                   g_ioport.p_api->pinWrite(RELAY2_BREAK_PIN, IOPORT_LEVEL_LOW); // !Relay_2
                }
            }
        }
        break;
        case RELAY_3:
        {
            if (port_level)
            {
                if (port_type)
                {
                   g_ioport.p_api->pinWrite(RELAY3_MAKE_PIN, IOPORT_LEVEL_HIGH); // Relay_3
                }
                else
                {
                   g_ioport.p_api->pinWrite(RELAY3_BREAK_PIN, IOPORT_LEVEL_HIGH); // !Relay_3
                }
            }
            else
            {
                if (port_type)
                {
                   g_ioport.p_api->pinWrite(RELAY3_MAKE_PIN, IOPORT_LEVEL_LOW); // Relay_3
                }
                else
                {
                   g_ioport.p_api->pinWrite(RELAY3_BREAK_PIN, IOPORT_LEVEL_LOW); // !Relay_3
                }

            }
        }
        break;
        case RELAY_4:
        {
            if (port_level)
            {
                if (port_type)
                {
                   g_ioport.p_api->pinWrite(RELAY4_MAKE_PIN, IOPORT_LEVEL_HIGH); // Relay_4
                }
                else
                {
                   g_ioport.p_api->pinWrite(RELAY4_BREAK_PIN, IOPORT_LEVEL_HIGH); // !Relay_4
                }
            }
            else
            {
                if (port_type)
                {
                   g_ioport.p_api->pinWrite(RELAY4_MAKE_PIN, IOPORT_LEVEL_LOW); // Relay_4
                }
                else
                {
                   g_ioport.p_api->pinWrite(RELAY4_BREAK_PIN, IOPORT_LEVEL_LOW); // !Relay_4
                }
            }
        }
        break;
        default:
            break;
    }
}
#if 0
// get the status of the pulse completion
E_WPULSE_STATE get_wait_pulse_status(void)
{
    E_WPULSE_STATE pulse;

    // needs to be atomic operation
    enter_critical_section();
    pulse = wait_pulse_status;
    exit_critical_section();

    return (pulse);
}
#endif

// setup  timer pulse
// wait_delay is the time pulse is OFF
// callback function will turn pulse ON
E_WPULSE_STATE setup_pulse(E_PULSE_TYPE pulse_type, uint32_t avg_line_period, RELAY_ID_E relay_num, BOOL relay_type)
{
    uint32_t wait_delay;

    if (pulse_type == ZC_SYNC)
    {
        //
        // wait time (approx 7 msec) = average period count (approx 16.66 msec @60Hz) - 9.2 msec - 0.5 msec
        // gpt timer 3 is a 16bit clock
        //
        {
            // convert from 48 MHz to 1.0 MHz clock
            wait_delay = avg_line_period/48 - NEXT_ZC - CLOSE_TIME;

            g_timer3.p_api->stop(g_timer3.p_ctrl);
            // setup the wait timer then start timer on next capture
            relay_data.relay_num = relay_num; // atomic operation - timer is disabled OK to write data
            relay_data.relay_type = relay_type; // On or Off type

            wait_pulse_status = WPULSE_WAIT;
            pulse_state = PULSE_TIMER_INIT;

            g_timer3.p_api->reset(g_timer3.p_ctrl);
            g_timer3.p_api->periodSet(g_timer3.p_ctrl, wait_delay, TIMER_UNIT_PERIOD_USEC);
            // timer is started when next pulse is detected
        }
    }
    else // no synch here go ahead and start pulse
    {
        wait_delay = OPEN_TIME;
        g_timer3.p_api->stop(g_timer3.p_ctrl);
        // start the wait timer
        relay_data.relay_num = relay_num; // atomic operation - timer is disabled OK to write data
        relay_data.relay_type = relay_type; // On or Off type

        wait_pulse_status = WPULSE_WAIT;
        pulse_state = PULSE_TIMER_INIT;

        g_timer3.p_api->reset(g_timer3.p_ctrl);
        g_timer3.p_api->periodSet(g_timer3.p_ctrl, wait_delay, TIMER_UNIT_PERIOD_USEC);
        g_timer3.p_api->start(g_timer3.p_ctrl); // no sync required here OK to start timer here
    }
    return (wait_pulse_status);
}


E_PULSE_STATE get_pulse_state(void)
{
    E_PULSE_STATE curr_pulse_state;

    // this operation needs to be atomic
    curr_pulse_state = pulse_state;

    return curr_pulse_state;
}


// turns pulse ON and OFF
void relay_pulse_callback(timer_callback_args_t * p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
    ssp_err_t ssp_err;

    wait_pulse_status = WPULSE_COMPLETE; // wait pulse is complete
    switch(pulse_state)
    {
        case PULSE_TIMER_INIT:
        {
            set_relay_port(relay_data.relay_num, relay_data.relay_type, TRUE);

            ssp_err = g_timer3.p_api->stop(g_timer3.p_ctrl);
            ssp_err = g_timer3.p_api->reset(g_timer3.p_ctrl);
            ssp_err = g_timer3.p_api->periodSet(g_timer3.p_ctrl, RELAY_PULSE, TIMER_UNIT_PERIOD_USEC);
            ssp_err = g_timer3.p_api->start(g_timer3.p_ctrl);
            pulse_state = PULSE_TIMER_IN_PROGRESS;
        }
        break;
        case PULSE_TIMER_IN_PROGRESS:
        {
            set_relay_port(relay_data.relay_num, relay_data.relay_type, FALSE);

            ssp_err = g_timer3.p_api->stop(g_timer3.p_ctrl);
            pulse_state = PULSE_TIMER_DONE;
            //disable_zero_cross(relay_data.relay_num); // disable zero cross signal
            // todo check period after closing relay to see if relay closed
        }
        break;
        default:
         break;
    }
    SSP_PARAMETER_NOT_USED(ssp_err);
}


void start_period_meas(RELAY_ID_E relay_id)
{
    // Commented to allow test sw to enable disable zc hardware.
	// The default is set to be enabled.
	//
    // enable_zero_cross(relay_id);

    // Timers are done in this way since we can only have one
	// input capture pin A, B. The input pin A, B is an enable
    switch(relay_id)
    {
        case RELAY_1:
            set_first_pulse_flag();
            g_external_irq4.p_api->open(g_external_irq4.p_ctrl, g_external_irq4.p_cfg);
            g_external_irq4.p_api->enable(g_external_irq4.p_ctrl);
            break;
        case RELAY_2:
            set_first_pulse_flag();
            g_external_irq5.p_api->open(g_external_irq5.p_ctrl, g_external_irq5.p_cfg);
            g_external_irq5.p_api->enable(g_external_irq5.p_ctrl);
            break;
        case RELAY_3:
            set_first_pulse_flag();
            g_external_irq6.p_api->open(g_external_irq6.p_ctrl, g_external_irq6.p_cfg);
            g_external_irq6.p_api->enable(g_external_irq6.p_ctrl);
            break;
        case RELAY_4:
            set_first_pulse_flag();
            g_external_irq7.p_api->open(g_external_irq7.p_ctrl, g_external_irq7.p_cfg);
            g_external_irq7.p_api->enable(g_external_irq7.p_ctrl);
            break;
        default:
            break;
    }
}


void stop_period_meas(RELAY_ID_E relay_id)
{
    // Timers are done in this way since we can only have one
	// input capture pin A, B. The input pin A, B is an enable
    switch(relay_id)
    {
        case RELAY_1:
            g_external_irq4.p_api->disable(g_external_irq4.p_ctrl);
            g_external_irq4.p_api->close(g_external_irq4.p_ctrl);
            break;
        case RELAY_2:
            g_external_irq5.p_api->disable(g_external_irq5.p_ctrl);
            g_external_irq5.p_api->close(g_external_irq5.p_ctrl);
            break;
        case RELAY_3:
            g_external_irq6.p_api->disable(g_external_irq6.p_ctrl);
            g_external_irq6.p_api->close(g_external_irq6.p_ctrl);

            break;
        case RELAY_4:
            g_external_irq7.p_api->disable(g_external_irq7.p_ctrl);
            g_external_irq7.p_api->close(g_external_irq7.p_ctrl);
            break;
        default:
            break;
    }

    // Commented to allow test sw to enable disable zc hardware.
	// The default is set to be enabled.
	//
    //disable_zero_cross(relay_id);

}

E_LINE_PERIOD_TYPE line_period_meas(RELAY_ID_E relay_id, uint32_t * avg_period_value)
{
    uint32_t period_value;
    E_LINE_PERIOD_TYPE status = MEAS_IN_PROGRESS;


       period_value = get_relay_line_period(relay_id);

       if (period_value == 0)
       {
           status = NO_WAVEFORM; // no pulses detected
       }
       else
       {
           // 63 Hz to 47.5 Hz
           // todo break down into limits for 60 Hz +/- 5 % 50 Hz +/- 5 %
           if ((period_value > LO_PERIOD_LIM)&&(period_value < HI_PERIOD_LIM))
           {
              status = PERIOD_OK;
           }
           else
           {
              status = PERIOD_OUT_OF_RANGE;
              period_value = 0xFFFF;
           }
       }
     *avg_period_value = period_value;

    return(status);
}

void clr_relay_line_period(RELAY_ID_E relay_id)
{
	// needs to be atomic operation if timer is enabled when this fn is called
    switch (relay_id)
    {
        case RELAY_1:
        {
            enter_critical_section();
            capture_tail_relay1 = 0;
            capture_count_relay1 = 0;
            line_measure_sum_relay1 = 0;
            line_measure_avg_relay1 = 0;
            exit_critical_section();
        }
         break;
        case RELAY_2:
        {
            enter_critical_section();
            capture_tail_relay2 = 0;
            capture_count_relay2 = 0;
            line_measure_sum_relay2 = 0;
            line_measure_avg_relay2 = 0;
            exit_critical_section();
        }
         break;
        case RELAY_3:
        {
            enter_critical_section();
            capture_tail_relay3 = 0;
            capture_count_relay3 = 0;
            line_measure_sum_relay3 = 0;
            line_measure_avg_relay3 = 0;
            exit_critical_section();
        }
         break;
        case RELAY_4:
        {
            enter_critical_section();
            capture_tail_relay4 = 0;
            capture_count_relay4 = 0;
            line_measure_sum_relay4 = 0;
            line_measure_avg_relay4 = 0;
            exit_critical_section();
        }
         break;
        default:
         break;
    }
}
uint32_t get_relay_line_period(RELAY_ID_E relay_id)
{
    uint32_t line_measure_avg = 0;

    switch (relay_id)
    {
        case RELAY_1:
        {
            enter_critical_section();
            line_measure_avg = line_measure_avg_relay1;
            exit_critical_section();
        }
         break;
        case RELAY_2:
        {
            enter_critical_section();
            line_measure_avg = line_measure_avg_relay2;
            exit_critical_section();
        }
         break;
        case RELAY_3:
        {
            enter_critical_section();
            line_measure_avg = line_measure_avg_relay3;
            exit_critical_section();

        }
         break;
        case RELAY_4:
        {
            enter_critical_section();
            line_measure_avg = line_measure_avg_relay4;
            exit_critical_section();
        }
         break;
        default:
         break;
    }
    return(line_measure_avg);
}

// sync pulse with zero cross signal
// start timer3 which controls the pulse to the relay
void start_pulse_timer(RELAY_ID_E relay_num)
{
    static int first_time = 0;
    timer_info_t info;

    if ((getRelayState(relay_num) == RLY_STATE_TR_MAKE_2) &&
            (first_time == 0))
    {
      g_timer3.p_api->start(g_timer3.p_ctrl);
      first_time = 1;
    }
    else if (first_time)
    {
        g_timer3.p_api->infoGet(g_timer3.p_ctrl, &info);
        if (info.status == TIMER_STATUS_STOPPED)
            first_time = 0;
    }
}

// timer 0 is used to measure line period
// callback occurs if timer0 overflow occurs
void gp_timer0_callback(timer_callback_args_t * p_args)
{
    g_arg_event = p_args->event;

    if (g_arg_event == TIMER_EVENT_EXPIRED)
    {
        // count the overflow event
        capture.overflows = capture.overflows + 1;
    }
}

void set_first_pulse_flag(void)
    {
    first_pulse = TRUE;
    }

BOOL get_first_pulse_flag(void)
{
    return(first_pulse);
}

// zero cross relay 1
// interrupt occurs every rising edge
void irq4_capture_zc1(external_irq_callback_args_t * p_args)
{
    uint32_t capture_value = 0;
    ssp_err_t err;

    SSP_PARAMETER_NOT_USED(p_args);

    // skip the first pulse measurement
    // restart the timer 0
    if (get_first_pulse_flag() == TRUE)
    {
        err = g_timer0.p_api->reset(g_timer0.p_ctrl);
        while (err)
        {
        }

        capture.counter = 0;
        capture.overflows = 0;

        err = g_timer0.p_api->start(g_timer0.p_ctrl);
        while (err)
        {
        }

        first_pulse = FALSE;
        return;
    }


    err = g_timer0.p_api->stop(g_timer0.p_ctrl);
    while (err)
    {
    }

    err = g_timer0.p_api->counterGet(g_timer0.p_ctrl, &capture.counter);

    while (err)
    {
    }

    if (capture.overflows == 0)
    	capture_value = capture.counter;
    else
    	capture_value = 0;

    // remove oldest sample
    if (capture_count_relay1 >= NUM_MEASUREMENTS)
    {
        line_measure_sum_relay1 = line_measure_sum_relay1 - line_period_relay1[capture_tail_relay1];
    }

    // add the latest sample
    line_period_relay1[capture_tail_relay1] = capture_value;
    line_measure_sum_relay1 = line_measure_sum_relay1 + capture_value;

    if (capture_count_relay1 >= NUM_MEASUREMENTS)
    {
        line_measure_avg_relay1 = line_measure_sum_relay1 / NUM_MEASUREMENTS;
    }

    capture_tail_relay1 = (uint8_t)(capture_tail_relay1 + 1);
    /* check the tail */
    if (capture_tail_relay1 == NUM_MEASUREMENTS)
    {
        capture_tail_relay1 = 0;
    }

    if (capture_count_relay1 < NUM_MEASUREMENTS)
        capture_count_relay1 = (uint8_t)(capture_count_relay1 + 1);

    err = g_timer0.p_api->reset(g_timer0.p_ctrl);
    while (err)
    {
    }

    capture.counter = 0;
    capture.overflows = 0;

    err = g_timer0.p_api->start(g_timer0.p_ctrl);
    while (err)
    {
    }
    // sync pulse with zero cross signal
    start_pulse_timer(RELAY_1);
}

// zero cross relay 2
// interrupt occurs every rising edge
void irq5_capture_zc2(external_irq_callback_args_t * p_args)
{
    uint32_t capture_value = 0;
    ssp_err_t err;

    SSP_PARAMETER_NOT_USED(p_args);

    // skip the first pulse measurement
    // restart the timer 0
    if (get_first_pulse_flag() == TRUE)
    {
        err = g_timer0.p_api->reset(g_timer0.p_ctrl);
        while (err)
        {
        }

        capture.counter = 0;
        capture.overflows = 0;

        err = g_timer0.p_api->start(g_timer0.p_ctrl);
        while (err)
        {
        }

        first_pulse = FALSE;
        return;
    }


    err = g_timer0.p_api->stop(g_timer0.p_ctrl);
    while (err)
    {
    }

    err = g_timer0.p_api->counterGet(g_timer0.p_ctrl, &capture.counter);

    while (err)
    {
    }

    if (capture.overflows == 0)
    	capture_value = capture.counter;
    else
    	capture_value = 0;

    // remove oldest sample
    if (capture_count_relay2 >= NUM_MEASUREMENTS)
    {
        line_measure_sum_relay2 = line_measure_sum_relay2 - line_period_relay2[capture_tail_relay2];
    }

    // add the latest sample
    line_period_relay2[capture_tail_relay2] = capture_value;
    line_measure_sum_relay2 = line_measure_sum_relay2 + capture_value;

    if (capture_count_relay2 >= NUM_MEASUREMENTS)
    {
        line_measure_avg_relay2 = line_measure_sum_relay2 / NUM_MEASUREMENTS;
    }

    capture_tail_relay2 = (uint8_t)(capture_tail_relay2 + 1);
    /* check the tail */
    if (capture_tail_relay2 == NUM_MEASUREMENTS)
    {
        capture_tail_relay2 = 0;
    }

    if (capture_count_relay2 < NUM_MEASUREMENTS)
        capture_count_relay2 = (uint8_t)(capture_count_relay2 + 1);

    err = g_timer0.p_api->reset(g_timer0.p_ctrl);
    while (err)
    {
    }

    capture.counter = 0;
    capture.overflows = 0;

    err = g_timer0.p_api->start(g_timer0.p_ctrl);
    while (err)
    {
    }
    // sync pulse with zero cross signal
    start_pulse_timer(RELAY_2);
}

// zero cross relay 3
// interrupt occurs every rising edge
void irq6_capture_zc3(external_irq_callback_args_t * p_args)
{
    uint32_t capture_value = 0;
    ssp_err_t err;

    SSP_PARAMETER_NOT_USED(p_args);

    // skip the first pulse measurement
    // restart the timer 0
    if (get_first_pulse_flag() == TRUE)
    {
        err = g_timer0.p_api->reset(g_timer0.p_ctrl);
        while (err)
        {
        }

        capture.counter = 0;
        capture.overflows = 0;

        err = g_timer0.p_api->start(g_timer0.p_ctrl);
        while (err)
        {
        }

        first_pulse = FALSE;
        return;
    }


    err = g_timer0.p_api->stop(g_timer0.p_ctrl);
    while (err)
    {
    }

    err = g_timer0.p_api->counterGet(g_timer0.p_ctrl, &capture.counter);

    while (err)
    {
    }

    if (capture.overflows == 0)
    	capture_value = capture.counter;
    else
    	capture_value = 0;

    // remove oldest sample
    if (capture_count_relay3 >= NUM_MEASUREMENTS)
    {
        line_measure_sum_relay3 = line_measure_sum_relay3 - line_period_relay3[capture_tail_relay3];
    }

    // add the latest sample
    line_period_relay3[capture_tail_relay3] = capture_value;
    line_measure_sum_relay3 = line_measure_sum_relay3 + capture_value;

    if (capture_count_relay3 >= NUM_MEASUREMENTS)
    {
        line_measure_avg_relay3 = line_measure_sum_relay3 / NUM_MEASUREMENTS;
    }

    capture_tail_relay3 = (uint8_t)(capture_tail_relay3 + 1);
    /* check the tail */
    if (capture_tail_relay3 == NUM_MEASUREMENTS)
    {
        capture_tail_relay3 = 0;
    }

    if (capture_count_relay3 < NUM_MEASUREMENTS)
        capture_count_relay3 = (uint8_t)(capture_count_relay3 + 1);

    err = g_timer0.p_api->reset(g_timer0.p_ctrl);
    while (err)
    {
    }

    capture.counter = 0;
    capture.overflows = 0;

    err = g_timer0.p_api->start(g_timer0.p_ctrl);
    while (err)
    {
    }
    // sync pulse with zero cross signal
    start_pulse_timer(RELAY_3);
}

// zero cross relay 4
// interrupt occurs every rising edge
void irq7_capture_zc4(external_irq_callback_args_t * p_args)
{
    uint32_t capture_value = 0;
    ssp_err_t err;

    SSP_PARAMETER_NOT_USED(p_args);

    // skip the first pulse measurement
    // restart the timer 0
    if (get_first_pulse_flag() == TRUE)
    {
        err = g_timer0.p_api->reset(g_timer0.p_ctrl);
        while (err)
        {
        }

        capture.counter = 0;
        capture.overflows = 0;

        err = g_timer0.p_api->start(g_timer0.p_ctrl);
        while (err)
        {
        }

        first_pulse = FALSE;
        return;
    }


    err = g_timer0.p_api->stop(g_timer0.p_ctrl);
    while (err)
    {
    }

    err = g_timer0.p_api->counterGet(g_timer0.p_ctrl, &capture.counter);

    while (err)
    {
    }

    if (capture.overflows == 0)
    	capture_value = capture.counter;
    else
    	capture_value = 0;

    // remove oldest sample
    if (capture_count_relay4 >= NUM_MEASUREMENTS)
    {
        line_measure_sum_relay4 = line_measure_sum_relay4 - line_period_relay4[capture_tail_relay4];
    }

    // add the latest sample
    line_period_relay4[capture_tail_relay4] = capture_value;
    line_measure_sum_relay4 = line_measure_sum_relay4 + capture_value;

    if (capture_count_relay4 >= NUM_MEASUREMENTS)
    {
        line_measure_avg_relay4 = line_measure_sum_relay4 / NUM_MEASUREMENTS;
    }

    capture_tail_relay4 = (uint8_t)(capture_tail_relay4 + 1);
    /* check the tail */
    if (capture_tail_relay4 == NUM_MEASUREMENTS)
    {
        capture_tail_relay4 = 0;
    }

    if (capture_count_relay4 < NUM_MEASUREMENTS)
        capture_count_relay4 = (uint8_t)(capture_count_relay4 + 1);

    err = g_timer0.p_api->reset(g_timer0.p_ctrl);
    while (err)
    {
    }

    capture.counter = 0;
    capture.overflows = 0;

    err = g_timer0.p_api->start(g_timer0.p_ctrl);
    while (err)
    {
    }
    // sync pulse with zero cross signal
    start_pulse_timer(RELAY_4);
}
