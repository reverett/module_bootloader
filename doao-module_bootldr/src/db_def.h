//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  db_def.h
//
//  Description: Definition of the DO_AO module database.
//               These definitions are to be shared among various software
//               modules, such as, the database implementation module, the
//               database API module, the RS485 rx/tx module, etc.
//-----------------------------------------------------------------------------
#ifndef __DB_DEF_H__
#define __DB_DEF_H__


#include "jellyfish_common.h"
#include "common_def.h"
#include "flash_data.h"

typedef enum reset_stat_s {
	NO_ERR          = 0,
	WATCHDOG_TIMER  = 1,
	EXCEPTION       = 2,
	CRC_FAILURE     = 3,
	OTHER_ERR       = 4
} reset_stat_t;

//----------------------------------------------------------------
// The Non-Volatile Table of the DO-AO Database
//----------------------------------------------------------------
// 1. Most of the fields in this table WILL NOT be updated periodically,
//    except a few items.
//
// 2. The table will be written to Flash once a day.
//
// 3. Information from this table will also be used in rebooting
//    and initializing the hardware.
//----------------------------------------------------------------
#if 0
typedef struct doao_module_db_nv_s {
    hardware_tag_t       hardware;
	firmware_tag_t       bootloader;
    firmware_tag_t       firmware_info;
    unsigned short       firmware_crc;
	reset_stat_t         firmware_stat;
	BOOL                 has_new_firmware;
	reset_stat_t         firmware_upgrate_stat;
	OPERATION_MODE_E     mode;
	unsigned             relay_stat[DOAO_MODULE_RELAY_NUM];
//	unsigned char        encryption_key[ENCRYPTION_KEY_SIZE];	//reduce table size, comment out for now;
} doao_module_db_nv_t;
#else

typedef struct doao_module_db_nv_s {

	// first info used in bootloader process
	sw_info_struct       firmware_info;

	// data that does not change
	hardware_tag_t       hardware;

	// configuration info
	unsigned             relay_stat[DOAO_MODULE_RELAY_NUM];

	// debug info how did module reset?
	reset_stat_t         firmware_upgrate_stat;
} doao_module_db_nv_t;
#endif
//----------------------------------------------------------------
// The Volatile Table of the DO-AO Database
//----------------------------------------------------------------
// 1. Most of the fields in this table WILL be updated periodically,
//    as a reflection of the running status of the hardware.
//
// 2. The table will stay in the RAM and will not be written to
//    the Flash at all, but a few items will be copied over to the
//    Non-Volatile table and written to the Flash with the NV table.
//
// 3. The Module driver code will conduct certain pulling of the
//    hardware components and refreshes this table periodically.
//
// 4. When the System Controller is requesting run-time data of the
//    module, the status data will be read out from the database,
//    as quick response, instead of pulling each individual hardware.
//----------------------------------------------------------------
typedef struct doao_m_db_s {
	SWITCH_STATE_E      provision_switch_state;
	SWITCH_STATE_E      relay_state[DOAO_MODULE_RELAY_NUM];
	SWITCH_STATE_E      override_state[DOAO_MODULE_RELAY_NUM]; // ON means pressed
	BOOL                zc_disabled[DOAO_MODULE_RELAY_NUM];
	unsigned            line_period[DOAO_MODULE_RELAY_NUM];
	unsigned            pwm_output[DOAO_MODULE_RELAY_NUM];
	unsigned            output_monitor[DOAO_MODULE_RELAY_NUM];
	unsigned            v24_input_monitor;
	BOOL                rs485_bus_eol;
} doao_module_db_v_t;



#endif // __DB_DEF_H__

