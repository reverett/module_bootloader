//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  relay_thread_entry.h
//
//  Description: To export function prototypes defined in control_thread_entry
//               module.
//
//-----------------------------------------------------------------------------
#ifndef __RELAY_THREAD_ENTRY_H__
#define __RELAY_THREAD_ENTRY_H__

#include "common_def.h"
#include "control_thread_entry.h"


void                      relay_set_control_relay_instr_complete_cb(control_cb_func_t cb_func);
STATUS_E                  relay_create_relay_thx_timer(void);


extern control_cb_func_t  control_relay_instr_complete_cb_func;



#endif // __RELAY_THREAD_ENTRY_H__
