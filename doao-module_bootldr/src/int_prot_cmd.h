/*
 * =====================================================================================
 *
 *       Filename:  rhacmd.h
 *
 *    Description:  head file for rhacmd, cmd function declarartion
 *
 *        Version:  1.0
 *        Created:  09/01/2015 02:07:58 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Liang Huang (software developer), lhuang@intermatic.com
 *   Organization:  Intermatic Inc.
 *
 * =====================================================================================
 */
#include <stdio.h>

void rhaHostStartupReady();
void rhaModuleInfoRequest();
void rhaDeviceTypeWrite(uint8_t device_type, uint8_t sleepy_type);
void rhaDeviceTypeRequest();
void rhaManufacturerIdWrite(uint16_t manuId);
void rhaAddEndpoint();
void rhaStartupSyncComplete();
void rhaFormNetwork();
void rhaNetworkStatusRequest();
void rhaRestoreDefaults();
void rhaSerialACKConfigWrite(uint8_t ack);

void rhaSleepyHibernateDurationWrite(uint32_t duration);
void rhaSleepyControlAwake();
void rhaSleepyControlStayAwakeDurationWrite(uint32_t duration);
void rhaSleepyControlStayAwakeDurationRead();

//primary headers
#define HA_PROFILE 	0x0104
#define HVAC     0x0300

/*-----------------------------------------------------------------------------
 *  rha_server cluster structure
 *-----------------------------------------------------------------------------*/
typedef struct {
	uint16_t basic;				//0x0000
	uint16_t identify;			//0x0003
	uint16_t groups;			//0x0004
	uint16_t scenes;			//0x0005
	uint16_t fan_control;		//0x0202
} rha_svr_cluster_t;

/*-----------------------------------------------------------------------------
 *  rha_client cluster structure
 *-----------------------------------------------------------------------------*/
typedef struct {
	uint16_t Time;				//0x000A
	uint16_t OTA;				//0x0019
} rha_client_cluster_t;

/*-----------------------------------------------------------------------------
 *  rha_endpoint structure
 *-----------------------------------------------------------------------------*/
typedef struct {
	uint8_t endpointId;							//0x01
	uint16_t profileId;							//0x0104
	uint16_t deviceId;							//0x0300
	uint8_t deviceVersion;						//0x01
	uint8_t number_svr_cluster;					//0x01
	rha_svr_cluster_t svr_cluster;
	uint8_t number_client_cluster;				//0x02
	rha_client_cluster_t client_cluster;
} rha_endpoint_t;
