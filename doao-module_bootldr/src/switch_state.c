//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  switch_state.c
//
//  Description: Contains the switch state machines.
//
//-----------------------------------------------------------------------------

//#include "hal_data.h"
#include "jellyfish_common.h"
#include "common_def.h"
#include "switch_state.h"
#include "relay_thread.h"
#include "relay_state.h"
#include "critical_section.h"
#include "db_api.h"
#include "sys_config.h"

#define GPT_DELAY 5 // milliseconds
#define DEBOUNCE_MAKE_DELAY 200 //10 // 10 milliseconds
#define DEBOUNCE_BREAK_DELAY 200 //20 // 20 milliseconds

#define MAKE_COUNT DEBOUNCE_MAKE_DELAY/GPT_DELAY
#define BREAK_COUNT DEBOUNCE_BREAK_DELAY/GPT_DELAY

static pb_switch_context_t switch_1 = {0, STATE_BREAK, STATE_BREAK};
static pb_switch_context_t switch_2 = {0, STATE_BREAK, STATE_BREAK};
static pb_switch_context_t switch_3 = {0, STATE_BREAK, STATE_BREAK};
static pb_switch_context_t switch_4 = {0, STATE_BREAK, STATE_BREAK};
static pb_switch_context_t switch_5 = {0, STATE_BREAK, STATE_BREAK};



void switch_callback(timer_callback_args_t * p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);

    stateSwitch(SWITCH_RELAY1);
    stateSwitch(SWITCH_RELAY2);
    stateSwitch(SWITCH_RELAY3);
    stateSwitch(SWITCH_RELAY4);
    stateSwitch(SWITCH_PB);
}



// run this routine every 5 msec for each switch from GPT interrupt
void stateSwitch(MANUAL_SWITCH_ID_E switch_id)
{
    SW_DB_STATE_E switch_new;
    ioport_level_t sw_status = 0;
    pb_switch_context_t *switch_context = NULL;

    switch (switch_id)
    {
        case SWITCH_RELAY1:
        {
            g_ioport.p_api->pinRead(SWITCH_RELAY1_PIN, &sw_status);
            switch_context = &switch_1;
        }
            break;
        case SWITCH_RELAY2:
        {
            g_ioport.p_api->pinRead(SWITCH_RELAY2_PIN, &sw_status);
            switch_context = &switch_2;
        }
            break;
        case SWITCH_RELAY3:
        {
            g_ioport.p_api->pinRead(SWITCH_RELAY3_PIN, &sw_status);
            switch_context = &switch_3;
        }
            break;
        case SWITCH_RELAY4:
        {
            g_ioport.p_api->pinRead(SWITCH_RELAY4_PIN, &sw_status);
            switch_context = &switch_4;
        }
            break;
        case SWITCH_PB:
        {
            g_ioport.p_api->pinRead(SWTICH_PB_PIN, &sw_status);
            switch_context = &switch_5;
        }
            break;
        default:
            // todo add error condition
            break;
    }
    if (sw_status)
        switch_new = STATE_BREAK; // pullup on input 1 = switch open, input 0 = switch closed
    else
        switch_new = STATE_MAKE;

    if (switch_context != NULL)
    {
        switch (switch_context->old_sw_state)
        {
            case STATE_MAKE:
            {
                if (switch_new == switch_context->old_sw_state)
                {
                    switch_context->sw_count = BREAK_COUNT; // reset the counter
                }
                else
                {
                    switch_context->sw_count = switch_context->sw_count - 1;
                    if (switch_context->sw_count <= 0) // switch is stable now
                    {
                        switch_context->old_sw_state = STATE_BREAK;
                        switch_context->sw_count = MAKE_COUNT;
                    }
                }
            }
            break;
            case STATE_BREAK:
            {
                if (switch_new == switch_context->old_sw_state)
                {
                    switch_context->sw_count = MAKE_COUNT;
                }
                else
                {
                    switch_context->sw_count = switch_context->sw_count - 1;
                    if (switch_context->sw_count <= 0)
                    {
                        switch_context->old_sw_state = STATE_MAKE;
                        switch_context->sw_count = BREAK_COUNT;
                    }
                }
            }
            break;
            default:
            {
                switch_context->sw_count = MAKE_COUNT;
                switch_context->old_sw_state = STATE_BREAK;
            }
            break;
        }

        // detect rising edge from BREAK to MAKE state
        {
            SW_DB_STATE_E curr_switch_state;

            curr_switch_state = switch_context->old_sw_state;

            // detect change in state rising edge (BREAK to MAKE) and store into data base
            if ((switch_context->prev_sw_state == STATE_BREAK)&&(curr_switch_state != switch_context->prev_sw_state))
            {
                switch (switch_id)
                {
                    case SWITCH_RELAY1:
                    case SWITCH_RELAY2:
                    case SWITCH_RELAY3:
                    case SWITCH_RELAY4:
                    {
                    	toggleRelay((RELAY_ID_E)switch_id);
                        db_vt_set_override_state(switch_id, ON);
                    }
                    break;
                    case SWITCH_PB:
                    {
                        // set event in data base
                        db_vt_set_provsion_switch_state(ON);
                    }
                    break;
                    default:
                    break;
                 }

             }
            switch_context->prev_sw_state = curr_switch_state;
        }
    }

}
