//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  common_def.h
//
//  Description: Defines common deta types and constants used by the
//               DO_AO Module.
//
//               Note, all accessing of the database, read or write, must
//               lock the database first. This makes things simple.
//               If it is necessary to allow read-read, the code can be
//               improved for that purpose.
//
//               All database assessing APIs are supposed to check and lock
//               the database prior to accessing it.
//
//-----------------------------------------------------------------------------
#ifndef __COMMON_DEF_H__
#define __COMMON_DEF_H__


#include "jellyfish_common.h"


#define DOAO_MODULE_RELAY_NUM             4
#define DOAO_MODULE_ANALOG_CH_NUM         4
#define RENESAS_PROCESSOR_ID_SIZE        16
#define INTERMATIC_MODEL_NUM_SIZE        10
#define PRODUCT_SERIAL_NUM_SIZE          10
#define ENCRYPTION_KEY_SIZE             128


typedef enum {
	NORMAL        = 0,
	FM_UPDATE     = 1 //,
//  CRITICAL_TEST = 2,
//  UNIT_TEST     = 3
} OPERATION_MODE_E;

// IDs for the 4 latch-on Relays
typedef enum {
	RELAY_1   = 1,
	RELAY_2   = 2,
	RELAY_3   = 3,
	RELAY_4   = 4
} RELAY_ID_E;

// IDs for the 4 Zero-Cross Circuits of the Relays
typedef enum {
	ZC_RELAY_1   = RELAY_1,
	ZC_RELAY_2   = RELAY_2,
	ZC_RELAY_3   = RELAY_3,
	ZC_RELAY_4   = RELAY_4
} ZC_RELAY_ID_E;


// IDs for the PWM outputs of the Relays
typedef enum {
    PWM_RELAY1   = RELAY_1,
    PWM_RELAY2   = RELAY_2,
    PWM_RELAY3   = RELAY_3,
    PWM_RELAY4   = RELAY_4
} PWM_ID_E;


// IDs for the 5 press-down switches, 4 switches are for Relays
typedef enum {
    SWITCH_RELAY1  = RELAY_1,
    SWITCH_RELAY2  = RELAY_2,
    SWITCH_RELAY3  = RELAY_3,
    SWITCH_RELAY4  = RELAY_4,
    SWITCH_PB      = SWITCH_RELAY4+1
} MANUAL_SWITCH_ID_E;


// IDs for the 6 LEDs, 4 LEDs are for the Relays
typedef enum {
	LED_RELAY1  = RELAY_1,
	LED_RELAY2  = RELAY_2,
	LED_RELAY3  = RELAY_3,
	LED_RELAY4  = RELAY_4,
	LED_RS485   = LED_RELAY4+1,   // LED for RS485 communication
	LED_UNUSED  = LED_RELAY4+2    // LED not commissioned
} LED_ID_E;


#ifdef SUCCESS
#undef SUCCESS
#endif

#ifdef FAILURE
#undef FAILURE
#endif


// IO Module software's status code. All status code are
// collectively defined in this set. Different software
// modules use corresponding number sections.
typedef enum {

	SUCCESS               =    0x0000,
	FAILURE               =    0x0001,          // General Failure, catch-all

	// Error code common for all software modules
	ERR_PARAM             =    0x0002,          // wrong parameter value for function call
 	ERR_MEM_ALLOC         =    0x0003,          // unable to allocate memory
	ERR_SYS_INIT          =    0x0004,          // problem occurred during system initialization

	// Thread and message queue errors
    ERR_THRD_CREATE       =    0x0101,          // Error unable to create thread
	ERR_THRD_TERMINATE    =    0x0102,          // Error unable to terminate thread
	ERR_THRD_MEM_POOL     =    0x0103,          // Error unable to create thread byte pool
	ERR_MSGQ_CREATE       =    0x0201,          // Error unable to create message queue
	ERR_MSGQ_FULL         =    0x0202,          // Error message queue is full
    ERR_MSGQ_POST         =    0x0203,          // Error unable to post a message to the queue
    ERR_MSGQ_RECIEVE      =    0x0204,          // Error unable to receive a message
	ERR_MSGQ_BUF_ALLOC    =    0x0205,          // Error unable to allocate memory for message buffer
	ERR_MSGQ_BUF_FREE     =    0x0206,          // Error unable to release memory allocated for message booting
	ERR_MUTEX_CREATE      =    0x0207,          // Error unable to create mutex
	
	// RS-485 communication/protocol errors
	ERR_COMM              =    0x1001,          // RS485 communication error - catch all
	ERR_COMM_SEND         =    0x1002,          // Error sending data
	ERR_COMM_RECEIVE      =    0x1003,          // Error receiving data
	ERR_COMM_CRC          =    0x1004,          // Error checking data CRC
    ERR_COMM_SRA          =    0x1005,          // Error message segmentation/Re-Assembling

	// SC command errors
	ERR_SC_CMD            =    0x2001,          // SC Command error - catch-all
	ERR_SC_CMD_EXEC       =    0x2002,          // Error execution of instruction failed
	ERR_SC_CMD_NO_INSTR   =    0x2003,          // Error (not really) no instruction in command
	ERR_SC_CMD_INVALID    =    0x2004,          // Error invalid SC command
	ERR_SC_CMD_PARAM      =    0x2005,          // Error SC command parameter value

	ERR_SC_CMD_INSTR_NUM  =    0x2101,          // Error SC command instruction number reach max
	ERR_SC_INSTR_Q_FULL   =    0x2102,          // Error the instruction queue for SET command full

	// Hardware specific errors
	ERR_HW                =    0x3001,          // Hardware failure - catch-all
    ERR_HW_ID             =    0x3002,          // Error Hardware Non-existing ID for Relay, etc.
    ERR_HW_TIMEOUT        =    0x3003,          // Error Hardware I/O timeout
	ERR_HW_RLY_TIMER_INIT =    0x3004,          // Error initializing timer for Relay state machine
	ERR_HW_ADC_CHNNL_INIT =    0x3004,          // Error initializing ADC Channel
	ERR_HW_ADC_TIMER_INIT =    0x3005,          // Error initializing timer for ADC scanning
	ERR_HW_SSP            =    0x3006,          // Error Hardware call to SSP function failed

	// DB specific errors
	ERR_DB                =    0x4001,          // Database - catch all
    ERR_DB_LOCK           =    0x4002,          // Error locking database
	ERR_DB_UNLOCK         =    0x4003,          // Error unlocking database
	ERR_DB_INIT           =    0x4004,          // Error database uninitialized or failed to initialize
	ERR_DB_FLASH          =    0x4005,          // Error writing non-volatile database table to FLASH
	ERR_DB_CRC            =    0x4006,          // Error database page CRC check
	ERR_DB_UNCHANGED      =    0x4007,          // Error databae has not been changed since last whole table retrieval

	// Firmware download/bootload errors
	ERR_BTLDR             =    0x5001,          // Firmware download & bootloading error - catch all
	ERR_BTLDR_FW_CRC      =    0x5002,          // Error firmware CRC checking
	ERR_BTLDR_FW_FLASH    =    0x5003,          // Error firmware writing to flash
	ERR_BTLDR_FW_SIZE     =    0x5004,          // Error firmware size error (too large)
	ERR_BTLDR_TIMEOUT     =    0x5005,          // Error firmware booting timed out

	// End-of-list
	ERR_LAST_MARK         =    0x9999           // This is just to make end of the list

} STATUS_E;



typedef struct version_tag_s {
	char    major[2];
	char    minor[2];
	char    build[3];
} version_tag_t;

typedef struct timestamp_str_s {
	char       year[4];
	char       month[2];
	char       day[2];
	char       hour[2];
	char       minute[2];
	char       second[2];
} timestamp_str_t;


typedef struct firmware_tag_s {
	version_tag_t        version;
	timestamp_str_t      timestamp;
} firmware_tag_t;


typedef struct hardware_tag_s {
	unsigned char        processor_id[RENESAS_PROCESSOR_ID_SIZE];
	unsigned char        model[INTERMATIC_MODEL_NUM_SIZE];
	unsigned             version;
	unsigned char        serial_num[PRODUCT_SERIAL_NUM_SIZE];
} hardware_tag_t;



// Define the units to be used with the ThreadX sleep function
#define THREADX_TICK_RATE_HZ     100

// Set the blink frequency (must be <= threadx_tick_rate_Hz)
#define FREQ_IN_HZ               2

// Calculate the delay in terms of the ThreadX tick rate
#define RX485_THREAD_DELAY             (THREADX_TICK_RATE_HZ/FREQ_IN_HZ)

#define CONTROL_THREAD_DELAY           RX485_THREAD_DELAY


// Call back functions
typedef STATUS_E (*iom_manual_override_cbfunc_t)(MANUAL_SWITCH_ID_E swtich_id, BOOL override);
typedef STATUS_E (*iom_provision_swtich_cbfunc_t)(BOOL override);

#endif // __COMMON_DEF_H__

