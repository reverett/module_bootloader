/*
 * flash_data.c
 *
 *  Created on: Mar 28, 2017
 *      Author: reverett
 */
/* HAL-only entry function */
#include "int_prot_frame.h"
#include "flash_data.h"
#include "hal_data.h"
#include <ssp_common_api.h>
#include <string.h>


//unsigned char testBuffer[1024];
sw_info_struct firmware_info;

// bootloader data
// DOAO Board @ 0x4010 0100
fw_update_app_flash_struct app_data BSP_PLACE_IN_SECTION(".data_flash") =
{.crc_loc[0] = 0, .crc_loc[1] = 0, .fw_status = BL_ST_FW_IN_FLASH,

 .mdle_info.cur_address = DEFAULT_ADDR, .mdle_info.type = AODO_TYPE, .mdle_info.state = disc,
 .mdle_info.uid[0] = 0x00, .mdle_info.uid[1] = 0x01, .mdle_info.uid[2] = 0x02, .mdle_info.uid[3] = 0x03,
 .mdle_info.uid[4] = 0x04, .mdle_info.uid[5] = 0x05, .mdle_info.uid[6] = 0x06, .mdle_info.uid[7] = 0x07,
 .mdle_info.uid[8] = 0x08, .mdle_info.uid[9] = 0x09, .mdle_info.uid[10] = 0x0A, .mdle_info.uid[11] = 0x0B,
 .mdle_info.uid[12] = 0x0C, .mdle_info.uid[13] = 0x0D, .mdle_info.uid[14] = 0x0E, .mdle_info.uid[15] = 0x0F,


 .fw_info.ascii_info.productID[0] = 'd',    .fw_info.ascii_info.productID[1] = 'o', .fw_info.ascii_info.productID[2] = 'a', .fw_info.ascii_info.productID[3] = 'o',
 .fw_info.ascii_info.productID[4] = 'b',    .fw_info.ascii_info.productID[5] = 'd', .fw_info.ascii_info.productID[6] = 'a', .fw_info.ascii_info.productID[7] = 'p',
 .fw_info.ascii_info.productID[8] = 'p',    .fw_info.ascii_info.productID[9] = '\0',

 .fw_info.ascii_info.skew[0] = 'j', .fw_info.ascii_info.skew[1] = 'f', .fw_info.ascii_info.skew[2] = 'i', .fw_info.ascii_info.skew[3] = 's',
 .fw_info.ascii_info.skew[4] = 'h', .fw_info.ascii_info.skew[5] = ' ', .fw_info.ascii_info.skew[6] = ' ', .fw_info.ascii_info.skew[7] = ' ',
 .fw_info.ascii_info.skew[8] = ' ', .fw_info.ascii_info.skew[9] = '\0',

 .fw_info.ascii_info.version[0] = '0', .fw_info.ascii_info.version[1] = '0', .fw_info.ascii_info.version[2] = '0', .fw_info.ascii_info.version[3] = '0',
 .fw_info.ascii_info.version[4] = '0', .fw_info.ascii_info.version[5] = '0', .fw_info.ascii_info.version[6] = '1',

 .fw_info.ascii_info.date[0] = '0', .fw_info.ascii_info.date[1] = '4', .fw_info.ascii_info.date[2] = '1', .fw_info.ascii_info.date[3] = '1', .fw_info.ascii_info.date[4] = '1', .fw_info.ascii_info.date[5] = '7',
 .fw_info.ascii_info.time[0] = '0', .fw_info.ascii_info.time[1] = '4', .fw_info.ascii_info.time[2] = '4', .fw_info.ascii_info.time[3] = '8', .fw_info.ascii_info.time[4] = '0', .fw_info.ascii_info.time[5] = '1',
 .fw_info.ascii_info.unused_arr[0] = '\0',

 .fw_info.num_info.start_address[0] = 0x00, .fw_info.num_info.start_address[1] = 0x01, .fw_info.num_info.start_address[2] = 0x80, .fw_info.num_info.start_address[3] = 0x00,
 .fw_info.num_info.end_address[0] = 0x00, .fw_info.num_info.end_address[1] = 0x03, .fw_info.num_info.end_address[2] = 0x00, .fw_info.num_info.end_address[3] = 0x00,
 .fw_info.num_info.record_count[0] = 0x00, .fw_info.num_info.record_count[1] = 0x00, .fw_info.num_info.record_count[2] = 0x00, .fw_info.num_info.record_count[3] = 0x00,

};

#if 0
void initialize_bootloader_info(void)
{
    firmware_info.app.device_id = MY_ADDR; // load in default address
    firmware_info.app.fw_status = BL_ST_GET_NEW_FW;
    memcpy (&firmware_info.app.fw_info.ascii_info.productID[0], "sim app", 8);
    memcpy (&firmware_info.app.fw_info.ascii_info.skew[0], "jfish", 5);
    memcpy (&firmware_info.app.fw_info.ascii_info.version[0], "0000001", 7); // version 000.00.00

    firmware_info.btldr.device_id = MY_ADDR; // load in default address
    firmware_info.btldr.fw_status = BL_ST_FW_IN_FLASH;
    memcpy (&firmware_info.btldr.fw_info.ascii_info.productID[0], "sim boot", 8);
    memcpy (&firmware_info.btldr.fw_info.ascii_info.skew[0], "jfish", 5);
    memcpy (&firmware_info.btldr.fw_info.ascii_info.version[0], "0000000", 7); // version 000.00.00
}
#endif
#define FLASH_DF_BLOCK_0     0x40100000 // Data Flash area
#define FLASH_DATA_BLOCK_SZ  0x400      // size of one block

BOOL write_info_to_flash(sw_info_struct *ptr_to_sw_struct)
{

	BOOL write_status = TRUE;
	ssp_err_t err = SSP_SUCCESS;
	flash_result_t result;

    // Disable interrupts
    //__disable_irq();

	//Erase application area
	err = g_flash0.p_api->erase(g_flash0.p_ctrl, FLASH_DF_BLOCK_0, 1); // erase 1 block
	if (SSP_SUCCESS == err)
	{
		// Blank check
		err = g_flash0.p_api->blankCheck(g_flash0.p_ctrl, FLASH_DF_BLOCK_0, FLASH_DATA_BLOCK_SZ, &result);
		if ((SSP_SUCCESS == err) && (FLASH_RESULT_BLANK == result))
		{
			// Set destination address
			err = g_flash0.p_api->write(g_flash0.p_ctrl, (uint32_t)ptr_to_sw_struct, FLASH_DF_BLOCK_0, sizeof(sw_info_struct));
		}
		else
			err = SSP_ERR_WRITE_FAILED;
	}
    // Enable interrupts
    //__enable_irq();

	if (err == SSP_SUCCESS)
		write_status = TRUE;
	else
		write_status = FALSE;

	return(write_status);
}


record_status_t write_header_to_flash(uint8_t update_status)
{
	record_status_t rec_status = PRC_REC_RCVD_OK;

#if 1
	ssp_err_t err = SSP_SUCCESS;
	flash_result_t result;
	unsigned char *ptr_data_flash_buff = &firmware_info;
	// set our flag
	firmware_info.app.fw_status = update_status;

    // Disable interrupts
    //__disable_irq();

	//Erase application area
	err = g_flash0.p_api->erase(g_flash0.p_ctrl, FLASH_DF_BLOCK_0, 1); // erase 1 block
	if (SSP_SUCCESS != err)
	{
		rec_status = PRC_REC_ERASE_FAIL;
	}
	else
	{
		// Blank check
		err = g_flash0.p_api->blankCheck(g_flash0.p_ctrl, FLASH_DF_BLOCK_0, FLASH_DATA_BLOCK_SZ, &result);
		if ((SSP_SUCCESS != err) || (FLASH_RESULT_BLANK != result))
		{
			rec_status = PRC_REC_BLANK_FAIL;
		}
		else
		{
			// Set destination address
			err = g_flash0.p_api->write(g_flash0.p_ctrl, (uint32_t)ptr_data_flash_buff, FLASH_DF_BLOCK_0, sizeof(sw_info_struct));
			if (SSP_SUCCESS != err)
			{
				rec_status = PRC_REC_FLASH_FAIL;
			}
			//else
			//{
				//err = g_flash0.p_api->read(g_flash0.p_ctrl, testBuffer, FLASH_DF_BLOCK_0, sizeof(sw_info_struct));
			//}
		}
	}
    // Enable interrupts
    //__enable_irq();
#endif


	return(rec_status);
}

void init_data_memory(void)
{
	ssp_err_t err;
    // for test
    // load data flash struct for bootloader into RAM
    //initialize_bootloader_info();

#if 1
    // Open flash driver
     err = g_flash0.p_api->open(g_flash0.p_ctrl, g_flash0.p_cfg);
     if (SSP_SUCCESS != err)
     {
         while(1);
     }

    // read data flash struct for application into RAM
    // check to see if application requested new image be downloaded
    err = g_flash0.p_api->read(g_flash0.p_ctrl, &firmware_info, DATAFLASH_ADDRESS, sizeof(sw_info_struct));
    if (SSP_SUCCESS != err)
    {
        while(1);
    }

    // copy into existing structure used in code
    module_info.cur_address = firmware_info.app.mdle_info.cur_address;
    module_info.state = firmware_info.app.mdle_info.state;
    module_info.type = firmware_info.app.mdle_info.type;
    memcpy(module_info.uid, &firmware_info.app.mdle_info.uid,sizeof(firmware_info.app.mdle_info.uid));
#endif
}


