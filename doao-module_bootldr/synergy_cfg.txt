Synergy Configuration
  Board "Custom User Board (Any Device)"
  R7FS128783A01CFM
    part_number: R7FS128783A01CFM
    rom_size_bytes: 262144
    ram_size_bytes: 24576
    data_flash_size_bytes: 4096
    package_style: LQFP
    package_pins: 64
    
  S128
    series: 1
    
  S128 Family
    OFS0 register settings: Select fields below
         IWDT Start Mode: IWDT is Disabled
         IWDT Timeout Period: 2048 cycles
         IWDT Dedicated Clock Frequency Divisor: 128
         IWDT Window End Position:  0% (no window end position)
         IWDT Window Start Position: 100% (no window start position)
         IWDT Reset Interrupt Request Select: Reset is enabled
         IWDT Stop Control: Stop counting when in Sleep, Snooze mode, Software Standby, or Deep Software Standby mode
         WDT Start Mode Select: Stop WDT after a reset (register-start mode)
         WDT Timeout Period: 16384 cycles
         WDT Clock Frequency Division Ratio: 128
         WDT Window End Position:  0% (no window end position)
         WDT Window Start Position: 100% (no window start position)
         WDT Reset Interrupt Request: Reset
         WDT Stop Control: Stop counting when entering Sleep mode
    OFS1 register settings: Select fields below
         Voltage Detection 0 Circuit Start: Voltage monitor 0 reset is disabled after reset
         Voltage Detection 0 Level: 1.90 V
         HOCO OScillation Enable: HOCO oscillation is disabled after reset
    
  Synergy Common
    Main stack size (bytes): 0x800
    Process stack size (bytes): 0
    Heap size (bytes) - A minimum of 4K (0x1000) is required if standard library functions are to be used.: 0x1200
    MCU Vcc (mV): 3300
    Parameter checking: Enabled
    Assert Failures: Return SSP_ERR_ASSERTION
    Error Log: No Error Log
    ID code 1: 0xFFFFFFFF
    ID code 2: 0xFFFFFFFF
    ID code 3: 0xFFFFFFFF
    ID code 4: 0xFFFFFFFF
    
  Clocks
    XTAL 16000000Hz
    HOCO 48MHz
    Clock Src: HOCO
    ICLK Div /2
    PCLKB Div /2
    PCLKD Div /1
    
  Pin Configurations
    R7FS128783A01CFM.pincfg -> g_bsp_pin_cfg
      AVCC0 56 ADC_AVCC0 - - - - - - - 
      AVSS0 57 ADC_AVSS0 - - - - - - - 
      P000 64 ADC0_AN00 AN1_FB - - "Analog mode" - - "ADC0: AN00; CMP0: IVCMP2; CTSU0: TS21; IRQ0: IRQ6; OPAMP0: AMP+" 
      P001 63 ADC0_AN01 AN2_FB - - "Analog mode" - - "ADC0: AN01; CMP0: IVREF2; CTSU0: TS22; IRQ0: IRQ7; OPAMP0: AMP-" 
      P002 62 ADC0_AN02 AN3_FB - - "Analog mode" - - "ADC0: AN02; IRQ0: IRQ2; OPAMP0: AMPO" 
      P003 61 ADC0_AN03 MON_24V - - "Analog mode" - - "ADC0: AN03; OPAMP3: AMPO" 
      P004 60 ADC0_AN04 AN4_FB - - "Analog mode" - - "ADC0: AN04; CTSU0: TS25; DAC122: DA; IRQ0: IRQ3" 
      P010 59 - - - - Disabled - - "ADC: VREFH0; ADC0: AN05; OPAMP1: AMPO" 
      P011 58 - - - - Disabled - - "ADC: VREFL0; ADC0: AN06; DAC122: DA; OPAMP2: AMPO" 
      P012 55 GPIO EN_R4_ZC Medium - "Output mode (Initial Low)" - None "ADC0: AN07; CMP0: IVREF0; OPAMP1: AMP-" 
      P013 54 GPIO EN_R3_ZC Medium - "Output mode (Initial Low)" - None "ADC0: AN08; CMP0: IVCMP0; OPAMP1: AMP+" 
      P014 53 GPIO EN_R2_AC Medium - "Output mode (Initial Low)" - None "ADC0: AN09; CMP0: IVREF1; CTSU0: TS29; DAC120: DA; OPAMP2: AMP-" 
      P015 52 GPIO EN_R1_ZC Medium None "Output mode (Initial Low)" - None "ADC0: AN10; CMP0: IVCMP1; CTSU0: TS28; DAC121: DA; IRQ0: IRQ7; OPAMP2: AMP+" 
      P100 48 GPIO RELAY_3 Medium None "Output mode (Initial Low)" CMOS None "ADC0: AN22; AGT0: AGTIO; CMP0: CMPIN0; CTSU0: TS26; DALI0: DRX; GPT5: GTIOCB; IIC1: SCL; IRQ0: IRQ2; KINT0: KRM0; POEG0: GTETRG; SCI0: RXD_MISO; SCI0: SCL; SCI1: SCK; SPI0: MISO" 
      P101 47 - - - - Disabled - - "ADC0: AN21; AGT0: AGTEE; CMP0: CMPREF0; CTSU0: TS16; DALI0: DTX; GPT5: GTIOCA; IIC1: SDA; IRQ0: IRQ1; KINT0: KRM1; POEG1: GTETRG; SCI0: SDA; SCI0: TXD_MOSI; SCI1: CTS_RTS_SS; SPI0: MOSI" 
      P102 46 - - - - Disabled - - "ADC0: ADTRG; ADC0: AN20; AGT0: AGTO; CAN0: CRX; CMP0: CMPIN1; CTSU0: TS15; GPT2: GTIOCB; KINT0: KRM2; OPS0: GTOWLO; SCI0: SCK; SPI0: RSPCK" 
      P103 45 GPIO PB_SW - - "Input mode" - None "ADC0: AN19; CAN0: CTX; CMP0: CMPREF1; CTSU0: TS14; GPT2: GTIOCA; KINT0: KRM3; OPS0: GTOWUP; SCI0: CTS_RTS_SS; SPI0: SSL0" 
      P104 44 SCI0_RXD_MISO RS485_RXD Low None "Peripheral mode" CMOS "input pull-up" "ADC0: AN18; CTSU0: TS13; GPT1: GTIOCB; IRQ0: IRQ1; KINT0: KRM4; POEG1: GTETRG; SCI0: RXD_MISO; SCI0: SCL; SPI0: SSL1" 
      P105 43 GPIO RS485_DE Medium None "Output mode (Initial Low)" - None "ADC0: AN17; GPT1: GTIOCA; IRQ0: IRQ0; KINT0: KRM5; POEG0: GTETRG; SPI0: SSL2" 
      P106 42 GPIO RS485_RE Medium - "Output mode (Initial Low)" - None "ADC0: AN16; GPT0: GTIOCB; KINT0: KRM6; SPI0: SSL3" 
      P107 41 - - - - Disabled - - "GPT0: GTIOCA; KINT0: KRM7" 
      P108 33 DEBUG0_SWDIO SWDIO Low - "Peripheral mode" - None "DEBUG0: SWDIO; GPT0: GTIOCB; OPS0: GTOULO; SCI9: CTS_RTS_SS; SPI1: SSL0" 
      P109 34 GPT1_GTIOCA PWM3 Low - "Peripheral mode" CMOS None "CAN0: CTX; CGC0: CLKOUT; CTSU0: TS10; GPT1: GTIOCA; OPS0: GTOVUP; SCI1: SCK; SCI9: SDA; SCI9: TXD_MOSI; SPI1: MOSI" 
      P110 35 GPT1_GTIOCB PWM4 Low None "Peripheral mode" CMOS None "CAN0: CRX; CMP0: VCOUT; CTSU0: TS11; GPT1: GTIOCB; IRQ0: IRQ3; OPS0: GTOVLO; SCI0: CTS_RTS_SS; SCI9: RXD_MISO; SCI9: SCL; SPI1: MISO" 
      P111 36 GPIO DS2_LED Medium None "Output mode (Initial Low)" - None "AGT0: AGTOA; CTSU0: TS12; GPT3: GTIOCA; IRQ0: IRQ4; SCI0: SCK; SCI9: SCK; SPI1: RSPCK" 
      P112 37 SCI0_TXD_MOSI RS485_TXD Low - "Peripheral mode" CMOS None "AGT0: AGTOB; CTSU0: TSCAP; GPT3: GTIOCB; SCI0: SDA; SCI0: TXD_MOSI; SCI1: SCK; SPI1: SSL0" 
      P113 38 GPIO DS1_LED Medium - "Output mode (Initial Low)" - None "GPT2: GTIOCA" 
      P200 27 GPIO RS485_TERM - None "Input mode" - - "IRQ0: NMI" 
      P201 26 - - - - Disabled - - - 
      P204 24 GPT4_GTIOCB PWM2 Low - "Peripheral mode" CMOS None "AGT1: AGTIO; CAC0: CACREF; CTSU0: TS00; GPT4: GTIOCB; IIC0: SCL; OPS0: GTIW; SCI0: SCK; SCI9: SCK; SPI1: RSPCK" 
      P205 23 GPT4_GTIOCA PWM1 Low None "Peripheral mode" CMOS None "AGT1: AGTO; CGC0: CLKOUT; CTSU0: TSCAP; GPT4: GTIOCA; IIC1: SCL; IRQ0: IRQ1; OPS0: GTIV; SCI0: SDA; SCI0: TXD_MOSI; SCI9: CTS_RTS_SS; SPI1: SSL0" 
      P206 22 GPIO EOL Medium None "Output mode (Initial Low)" CMOS None "CTSU0: TS01; IIC1: SDA; IRQ0: IRQ0; OPS0: GTIU; SCI0: RXD_MISO; SCI0: SCL; SPI1: SSL1" 
      P212 10 GPIO R1_OR - None "Input mode" - None "AGT1: AGTEE; CGC0: EXTAL; GPT0: GTIOCB; IRQ0: IRQ3; POEG1: GTETRG; SCI1: RXD_MISO; SCI1: SCL" 
      P213 9 GPIO R2_OR - None "Input mode" - None "CGC0: XTAL; GPT0: GTIOCA; IRQ0: IRQ2; POEG0: GTETRG; SCI1: SDA; SCI1: TXD_MOSI" 
      P214 7 GPIO R3_OR - - "Input mode" - - "CGC0: XCOUT" 
      P215 6 GPIO R4_OR - - "Input mode" - - "CGC0: XCIN" 
      P300 32 DEBUG0_SWCLK SWCLK Low - "Peripheral mode" - None "DEBUG0: SWCLK; GPT0: GTIOCA; OPS0: GTOUUP; SPI1: SSL1" 
      P301 31 GPIO R1_LED Medium None "Output mode (Initial Low)" - None "AGT0: AGTIO; CTSU0: TS09; GPT4: GTIOCB; IRQ0: IRQ6; OPS0: GTOULO; SCI9: CTS_RTS_SS; SPI1: SSL2" 
      P302 30 GPIO R2_LED Medium None "Output mode (Initial Low)" - None "CTSU0: TS08; GPT4: GTIOCA; IRQ0: IRQ5; OPS0: GTOUUP; SPI1: SSL3" 
      P303 29 GPIO R3_LED Medium - "Output mode (Initial Low)" - None "CTSU0: TS02; GPT1: GTIOCB" 
      P304 28 GPIO R4_LED Medium - "Output mode (Initial Low)" - None "GPT1: GTIOCA" 
      P400 1 GPIO RELAY_1 Medium None "Output mode (Initial Low)" CMOS None "AGT1: AGTIO; CAC0: CACREF; CTSU0: TS20; GPT6: GTIOCA; IIC0: SCL; IRQ0: IRQ0; SCI0: SCK; SCI1: SCK" 
      P401 2 GPIO RELAY1_NOT Medium None "Output mode (Initial Low)" CMOS None "CAN0: CTX; CTSU0: TS19; GPT6: GTIOCB; IIC0: SDA; IRQ0: IRQ5; POEG0: GTETRG; SCI0: CTS_RTS_SS; SCI1: SDA; SCI1: TXD_MOSI" 
      P402 3 GPIO RELAY_2 Medium None "Output mode (Initial Low)" CMOS None "CAN0: CRX; CTSU0: TS18; GPT3: GTIOCB; IRQ0: IRQ4; SCI1: RXD_MISO; SCI1: SCL" 
      P403 4 GPIO RELAY2_NOT Medium - "Output mode (Initial Low)" - None "CTSU0: TS17; GPT3: GTIOCA; SCI1: CTS_RTS_SS" 
      P407 16 - - - - Disabled - - "ADC0: ADTRG; AGT0: AGTIO; CTSU0: TS03; GPT0: GTIOCA; IIC0: SDA; RTC0: RTCOUT; SCI0: CTS_RTS_SS; SPI1: SSL3; USBFS0: VBUS" 
      P408 15 GPIO R4_ZC - IRQ7 "Input mode" - None "CTSU0: TS04; GPT5: GTIOCB; IIC0: SCL; IRQ0: IRQ7; OPS0: GTOWLO; SCI9: RXD_MISO; SCI9: SCL" 
      P409 14 GPIO - - IRQ6 "Input mode" - None "CTSU0: TS05; GPT5: GTIOCA; IRQ0: IRQ6; OPS0: GTOWUP; SCI0: SDA; SCI0: TXD_MOSI; SCI9: SDA; SCI9: TXD_MOSI" 
      P410 13 GPIO R2_ZC - IRQ5 "Input mode" - None "AGT1: AGTOB; CTSU0: TS06; GPT6: GTIOCB; IRQ0: IRQ5; OPS0: GTOVLO; SCI0: RXD_MISO; SCI0: SCL; SPI0: MISO" 
      P411 12 GPIO - - IRQ4 "Input mode" - None "AGT1: AGTOA; CTSU0: TS07; GPT6: GTIOCA; IRQ0: IRQ4; OPS0: GTOVUP; SCI0: SDA; SCI0: TXD_MOSI; SPI0: MOSI" 
      P500 49 GPIO RELAY_3_NOT Medium - "Output mode (Initial Low)" - None "ADC0: AN13; CTSU0: TS27; DAC121: DA" 
      P501 50 GPIO RELAY_4 Medium - "Output mode (Initial Low)" - None "ADC0: AN12; OPAMP3: AMP+" 
      P502 51 GPIO RELAY4_NOT Medium - "Output mode (Initial Low)" - None "ADC0: AN11; OPAMP3: AMP-" 
      P914 19 - - - - Disabled - - "USBFS0: USBDP" 
      P915 18 - - - - Disabled - - "USBFS0: USBDM" 
      RES 25 - - - - - - - - 
      VCC 11 - - - - - - - - 
      VCC 39 - - - - - - - - 
      VCCUSB 20 USBFS0_VCCUSB - - - - - - - 
      VCCUSBLDO 21 USBFS0_VCCUSBLDO - - - - - - - 
      VCL 5 - - - - - - - - 
      VSS 8 - - - - - - - - 
      VSS 40 - - - - - - - - 
      VSSUSB 17 USBFS0_VSSUSB - - - - - - - 
      
  Module "ELC Driver on r_elc"
    Parameter Checking: Default (BSP)
    
  Module "CGC Driver on r_cgc"
    Parameter Checking: Default (BSP)
    Main Oscillator Wait Time: 8163 cycles
    Main Oscillator Clock Source: Crystal or Resonator
    Oscillator Stop Detect: Enabled
    Subclock Drive: Standard (12.5pf)
    
  Module "I/O Port Driver on r_ioport"
    Parameter Checking: Default (BSP)
    
  Module "UART Driver on r_sci_uart"
    External RTS Operation: Disable
    Reception: Enable
    Transmission: Enable
    Parameter Checking: Default (BSP)
    
  Module "FMI Driver on r_fmi"
    Parameter Checking: Default (BSP)
    Use Custom Factory Flash: Disabled
    Custom Factory Flash Symbol Name: g_fmi_data
    
  Module "Flash Driver on r_flash_lp"
    Parameter Checking: Default (BSP)
    Code Flash Programming Enable: Disabled
    
  Module "Timer Driver on r_gpt"
    Parameter Checking: Default (BSP)
    
  Module "External IRQ Driver on r_icu"
    Parameter Checking: Default (BSP)
    
  Module "ADC Driver on r_adc"
    Parameter Checking: Enabled
    
  HAL
    Instance "g_elc ELC Driver on r_elc"
      Name [Fixed]: g_elc
      
    Instance "g_cgc CGC Driver on r_cgc"
      Name [Fixed]: g_cgc
      
    Instance "g_ioport I/O Port Driver on r_ioport"
      Name [Fixed]: g_ioport
      
    Instance "g_uart0 UART Driver on r_sci_uart"
      Name: g_uart0
      Channel: 0
      Baud Rate: 115200
      Data Bits: 8bits
      Parity: None
      Stop Bits: 1bit
      CTS/RTS Selection: RTS (CTS is disabled)
      Name of UART callback function to be defined by user: user_uart_callback
      Name of UART callback function for the RTS external pin control to be defined by user: NULL
      Clock Source: Internal Clock
      Baudrate Clock Output from SCK pin: Disable
      Start bit detection: Falling Edge
      Noise Cancel: Disable
      Bit Rate Modulation Enable: Enable
      Receive Interrupt Priority: Priority 2
      Transmit Interrupt Priority: Priority 2
      Transmit End Interrupt Priority: Priority 2
      Error Interrupt Priority: Priority 2
      
      Instance "g_transfer0 Transfer Driver on r_dtc Event SCI0 TXI"
        Name: g_transfer0
        Mode: Normal
        Transfer Size: 1 Byte
        Destination Address Mode: Fixed
        Source Address Mode: Incremented
        Repeat Area (Unused in Normal Mode): Source
        Interrupt Frequency: After all transfers have completed
        Destination Pointer: NULL
        Source Pointer: NULL
        Number of Transfers: 0
        Number of Blocks (Valid only in Block Mode): 0
        Activation Source (Must enable IRQ): Event SCI0 TXI
        Auto Enable: False
        Callback (Only valid with Software start): NULL
        ELC Software Event Interrupt Priority: Disabled
        
      Instance "g_transfer1 Transfer Driver on r_dtc Event SCI0 RXI"
        Name: g_transfer1
        Mode: Normal
        Transfer Size: 1 Byte
        Destination Address Mode: Incremented
        Source Address Mode: Fixed
        Repeat Area (Unused in Normal Mode): Destination
        Interrupt Frequency: After all transfers have completed
        Destination Pointer: NULL
        Source Pointer: NULL
        Number of Transfers: 0
        Number of Blocks (Valid only in Block Mode): 0
        Activation Source (Must enable IRQ): Event SCI0 RXI
        Auto Enable: False
        Callback (Only valid with Software start): NULL
        ELC Software Event Interrupt Priority: Disabled
        
    Instance "g_fmi0 FMI Driver on r_fmi"
      Name: g_fmi0
      
    Instance "g_flash0 Flash Driver on r_flash_lp"
      Name: g_flash0
      Data Flash Background Operation: Disabled
      Callback: NULL
      Flash Ready Interrupt Priority: Disabled
      
  Thread "Relay Thread"
    Symbol: relay_thread
    Name: Relay Thread
    Stack size (bytes): 1024
    Priority: 1
    Auto start: Enabled
    Time slicing interval (ticks): 1
    
    Instance "g_timer2 Timer Driver on r_gpt"
      Name: g_timer2
      Channel: 2
      Mode: Periodic
      Period Value: 5
      Period Unit: Milliseconds
      Duty Cycle Value: 50
      Duty Cycle Unit: Unit Raw Counts
      Auto Start: True
      GTIOCA Output Enabled: False
      GTIOCA Stop Level: Pin Level Low
      GTIOCB Output Enabled: False
      GTIOCB Stop Level: Pin Level Low
      Callback: switch_callback
      Interrupt Priority: Priority 2
      
    Instance "g_timer3 Timer Driver on r_gpt"
      Name: g_timer3
      Channel: 3
      Mode: Periodic
      Period Value: 10
      Period Unit: Microseconds
      Duty Cycle Value: 50
      Duty Cycle Unit: Unit Raw Counts
      Auto Start: False
      GTIOCA Output Enabled: False
      GTIOCA Stop Level: Pin Level Low
      GTIOCB Output Enabled: False
      GTIOCB Stop Level: Pin Level Low
      Callback: relay_pulse_callback
      Interrupt Priority: Priority 2
      
    Instance "g_external_irq4 External IRQ Driver on r_icu"
      Name: g_external_irq4
      Channel: 4
      Trigger: Falling
      Digital Filtering: Disabled
      Digital Filtering Sample Clock (Only valid when Digital Filtering is Enabled): PCLK / 64
      Interrupt enabled after initialization: False
      Callback: irq4_capture_zc1
      Interrupt Priority: Priority 2
      
    Instance "g_external_irq5 External IRQ Driver on r_icu"
      Name: g_external_irq5
      Channel: 5
      Trigger: Falling
      Digital Filtering: Disabled
      Digital Filtering Sample Clock (Only valid when Digital Filtering is Enabled): PCLK / 64
      Interrupt enabled after initialization: False
      Callback: irq5_capture_zc2
      Interrupt Priority: Priority 2
      
    Instance "g_external_irq6 External IRQ Driver on r_icu"
      Name: g_external_irq6
      Channel: 6
      Trigger: Falling
      Digital Filtering: Disabled
      Digital Filtering Sample Clock (Only valid when Digital Filtering is Enabled): PCLK / 64
      Interrupt enabled after initialization: True
      Callback: irq6_capture_zc3
      Interrupt Priority: Priority 2
      
    Instance "g_external_irq7 External IRQ Driver on r_icu"
      Name: g_external_irq7
      Channel: 7
      Trigger: Falling
      Digital Filtering: Disabled
      Digital Filtering Sample Clock (Only valid when Digital Filtering is Enabled): PCLK / 64
      Interrupt enabled after initialization: True
      Callback: irq7_capture_zc4
      Interrupt Priority: Priority 2
      
    Instance "g_timer0 Timer Driver on r_gpt"
      Name: g_timer0
      Channel: 0
      Mode: Periodic
      Period Value: 0xffffffff
      Period Unit: Raw Counts
      Duty Cycle Value: 50
      Duty Cycle Unit: Unit Raw Counts
      Auto Start: False
      GTIOCA Output Enabled: False
      GTIOCA Stop Level: Pin Level Low
      GTIOCB Output Enabled: False
      GTIOCB Stop Level: Pin Level Low
      Callback: gp_timer0_callback
      Interrupt Priority: Priority 2
      
    Instance "g_timer1_pwm_3_4 Timer Driver on r_gpt"
      Name: g_timer1_pwm_3_4
      Channel: 1
      Mode: PWM
      Period Value: 25
      Period Unit: Microseconds
      Duty Cycle Value: 50
      Duty Cycle Unit: Unit Raw Counts
      Auto Start: True
      GTIOCA Output Enabled: True
      GTIOCA Stop Level: Pin Level Retained
      GTIOCB Output Enabled: True
      GTIOCB Stop Level: Pin Level Retained
      Callback: pwm1_callback
      Interrupt Priority: Priority 1
      
    Instance "g_timer4_pwm_1_2 Timer Driver on r_gpt"
      Name: g_timer4_pwm_1_2
      Channel: 4
      Mode: PWM
      Period Value: 25
      Period Unit: Microseconds
      Duty Cycle Value: 50
      Duty Cycle Unit: Unit Raw Counts
      Auto Start: True
      GTIOCA Output Enabled: True
      GTIOCA Stop Level: Pin Level Retained
      GTIOCB Output Enabled: True
      GTIOCB Stop Level: Pin Level Retained
      Callback: pwm4_callback
      Interrupt Priority: Priority 1
      
    Instance "g_adc0 ADC Driver on r_adc"
      Name: g_adc0
      Unit: 0
      Resolution: 12-Bit
      Alignment: Right
      Clear after read: On
      Mode: Single Scan
      Channel Scan Mask: Select channels below
          Channel 0: Use in Normal/Group A
          Channel 1: Use in Normal/Group A
          Channel 2: Use in Normal/Group A
          Channel 3: Use in Normal/Group A
          Channel 4: Use in Normal/Group A
          Channel 5: Unused
          Channel 6: Unused
          Channel 7 (S3A7/S124 Only): Unused
          Channel 8 (S3A7/S124 Only): Unused
          Channel 9 (S3A7/S124 Only): Unused
          Channel 10 (S3A7/S124 Only): Unused
          Channel 11 (S3A7 Only): Unused
          Channel 12 (S3A7 Only): Unused
          Channel 13 (S3A7 Only): Unused
          Channel 14 (S3A7 Only): Unused
          Channel 15 (S3A7 Only): Unused
          Channel 16: Unused
          Channel 17: Unused
          Channel 18: Unused
          Channel 19: Unused
          Channel 20: Unused
          Channel 21 (Unit 0 Only): Unused
          Channel 22 (S3A7/S124 Only): Unused
          Channel 23 (S3A7 Only): Unused
          Channel 24 (S3A7 Only): Unused
          Channel 25 (S3A7 Only): Unused
          Channel 26 (S3A7 Only): Unused
          Channel 27 (S3A7 Only): Unused
          Temperature Sensor: Unused
          Voltage Sensor: Unused
      Normal/Group A Trigger: Software
      Group B Trigger (Valid only in Group Scan Mode): ELC Event (The only valid trigger for either group in Group Scan Mode)
      Group Priority (Valid only in Group Scan Mode): Group A cannot interrupt Group B
      Add/Average Count: Disabled
      Addition/Averaging Mask: Select channels to perform addition/averaging below
          Channel 0: Disabled
          Channel 1: Disabled
          Channel 2: Disabled
          Channel 3: Disabled
          Channel 4: Disabled
          Channel 5: Disabled
          Channel 6: Disabled
          Channel 7: Disabled
          Channel 8: Disabled
          Channel 9: Disabled
          Channel 10: Disabled
          Channel 11: Disabled
          Channel 12: Disabled
          Channel 13: Disabled
          Channel 14: Disabled
          Channel 15: Disabled
          Channel 16: Disabled
          Channel 17: Disabled
          Channel 18: Disabled
          Channel 19: Disabled
          Channel 20: Disabled
          Channel 21: Disabled
          Channel 22: Disabled
          Channel 23: Disabled
          Channel 24: Disabled
          Channel 25: Disabled
          Channel 26: Disabled
          Channel 27: Disabled
          Temperature Sensor: Disabled
          Voltage Sensor: Disabled
      Sample and Hold Mask: Select channels for which individual sample and hold circuit is to be enabled
          Channel 0: Disabled
          Channel 1: Disabled
          Channel 2: Disabled
      Sample Hold States (Applies only to the 3 channels selected above): 24
      Callback: adc_callback
      Scan End Interrupt Priority: Priority 2
      Scan End Group B Interrupt Priority: Disabled
      
  Thread "control_thread"
    Symbol: control_thread
    Name: control_thread
    Stack size (bytes): 1024
    Priority: 1
    Auto start: Enabled
    Time slicing interval (ticks): 1
    
  Thread "rs485_comm_thread"
    Symbol: rs485_comm_thread
    Name: rs485_comm_thread
    Stack size (bytes): 1024
    Priority: 1
    Auto start: Enabled
    Time slicing interval (ticks): 1
    
  Messaging
