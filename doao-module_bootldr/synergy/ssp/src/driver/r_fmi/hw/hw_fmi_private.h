/***********************************************************************************************************************
 * Copyright [2015] Renesas Electronics Corporation and/or its licensors. All Rights Reserved.
 * 
 * This file is part of Renesas SynergyTM Software Package (SSP)
 *
 * The contents of this file (the "contents") are proprietary and confidential to Renesas Electronics Corporation
 * and/or its licensors ("Renesas") and subject to statutory and contractual protections.
 *
 * This file is subject to a Renesas SSP license agreement. Unless otherwise agreed in an SSP license agreement with
 * Renesas: 1) you may not use, copy, modify, distribute, display, or perform the contents; 2) you may not use any name
 * or mark of Renesas for advertising or publicity purposes or in connection with your use of the contents; 3) RENESAS
 * MAKES NO WARRANTY OR REPRESENTATIONS ABOUT THE SUITABILITY OF THE CONTENTS FOR ANY PURPOSE; THE CONTENTS ARE PROVIDED
 * "AS IS" WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT; AND 4) RENESAS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, OR
 * CONSEQUENTIAL DAMAGES, INCLUDING DAMAGES RESULTING FROM LOSS OF USE, DATA, OR PROJECTS, WHETHER IN AN ACTION OF
 * CONTRACT OR TORT, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THE CONTENTS. Third-party contents
 * included in this file may be subject to different terms.
 **********************************************************************************************************************/

/**********************************************************************************************************************
 * File Name    : hw_fmi_private.h
 * Description  : FMI
 **********************************************************************************************************************/


/*******************************************************************************************************************//**
 * @addtogroup FMI
 * @{
 **********************************************************************************************************************/

#ifndef HW_FMI_PRIVATE_H
#define HW_FMI_PRIVATE_H

/**********************************************************************************************************************
 * Includes
 **********************************************************************************************************************/
#include "bsp_api.h"
#include "r_fmi.h"

/** Common macro for SSP header files. There is also a corresponding SSP_FOOTER macro at the end of this file. */
SSP_HEADER

/**********************************************************************************************************************
 * Macro definitions
 **********************************************************************************************************************/
#define FMI_TABLE_LENGTH_WORDS        (256) ///< FMI table is 256 4-byte words
#define FMI_PRIV_MAX_PERIPHERAL_MAPS  (16)  ///< Maximum number peripheral maps per record

/**********************************************************************************************************************
 * Typedef definitions
 **********************************************************************************************************************/
typedef enum
{
    FMI_PRODUCT_INFORMATION,//!< FMI_PRODUCT_INFORMATION
    SSP_IP_INFORMATION,     //!< SSP_IP_INFORMATION
    FMI_SW_PROVISIONING,    //!< FMI_SW_PROVISIONING
    FMI_TYPE_COUNT          //!< FMI_TYPE_COUNT
} fmi_priv_record_enum_t;

typedef union
{
    struct
    {
        fmi_header_t header;
        uint32_t offset[FMI_TYPE_COUNT];
    };
    uint32_t data[FMI_TABLE_LENGTH_WORDS];
}fmi_priv_table_t;

typedef struct
{
    uint32_t mask :16; // [0:15] ip mask
    uint32_t prior :8; // [16:23] count of bits in all prior mask records
    uint32_t lower :4; // [24:27] number of bits in lower byte of mask
    uint32_t reserved :4;
} fmi_priv_peripheral_map_t;

typedef struct
{
    fmi_header_t header;
    uint32_t base;               // base address of IP for compact format

    /* likely only partially populated based on bit count in header */
    fmi_priv_peripheral_map_t map[FMI_PRIV_MAX_PERIPHERAL_MAPS];
} fmi_priv_peripheral_record_t;

typedef struct
{
    uint16_t offset :10;          //[0:9] index from ff base to IP record (as uint32_t index...pFF[offset])
    uint16_t reserved :5;
    uint16_t verbose :1;          // [15:15] flag to indicate verbose formats
} fmi_priv_peripheral_locator_t;

typedef enum{
    FMI_PRIV_FORMAT_VERBOSE_MULTICHANNEL_COMMON_VARIANT = 0,
    FMI_PRIV_FORMAT_VERBOSE_MULTICHANNEL_MULTI_VARIANT = 1,
    FMI_PRIV_FORMAT_VERBOSE_MULTICHANNEL_MULTI_VARIANT_MASK = 2,
    FMI_PRIV_FORMAT_VERBOSE_FLASH = 3,
    FMI_PRIV_FORMAT_VERBOSE_NON_CHANNEL = 4,
    FMI_PRIV_FORMAT_VERBOSE_RAM = 5,
    FMI_PRIV_FORMAT_COMPACT = 0XFF
} fmi_priv_format_enum_t;

typedef struct
{
    union
    {
        struct
        {
            uint32_t addr_16:16;
            uint32_t variant_data:8;
            uint32_t version_minor:4;
            uint32_t version_major:4;
        }compact;
        struct
        {
            uint32_t record_continue:1;
            uint32_t format:7;
            uint32_t variant_data:8;
            uint32_t contents:8;
            uint32_t version_minor:4;
            uint32_t version_major:4;
        }verbose;
    };
    union
    {
        struct
        {
            uint32_t channel_count:8;
            uint32_t :8;
            uint32_t channel_size:16;
            uint32_t base_address;
            union
            {
                uint8_t variant_data[128];
                uint32_t mask[32];
            };
        }format_verbose;
        struct
        {
            uint32_t start;
            uint32_t end;
            uint32_t erase;
            uint32_t write;
        }format_verbose_flash[32];
        struct
        {
            uint8_t variant[4];
            struct
            {
                uint32_t start;
                uint32_t end;
            }region[4];
        }format_verbose_ram[4];
        uint32_t format_verbose_non_channel_address;
     };
}fmi_priv_ip_t;



/** Common macro for SSP header files. There is also a corresponding SSP_HEADER macro at the top of this file. */
SSP_FOOTER

#endif /* HW_FMI_PRIVATE_H */

/*******************************************************************************************************************//**
 * @} (end addtogroup FMI)
 **********************************************************************************************************************/
