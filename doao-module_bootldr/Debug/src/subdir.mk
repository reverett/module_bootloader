################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/adc_scan.c \
../src/control_thread_entry.c \
../src/critical_section.c \
../src/db.c \
../src/db_api.c \
../src/flash_data.c \
../src/hal_entry.c \
../src/hw_io_api.c \
../src/int_prot_cmd.c \
../src/int_prot_frame.c \
../src/pwm_callbacks.c \
../src/relay_module.c \
../src/relay_state.c \
../src/relay_thread_entry.c \
../src/rs485_comm_thread_entry.c \
../src/rs485_driver.c \
../src/sc_command.c \
../src/switch_state.c \
../src/sys_manager.c \
../src/unit_test.c 

OBJS += \
./src/adc_scan.o \
./src/control_thread_entry.o \
./src/critical_section.o \
./src/db.o \
./src/db_api.o \
./src/flash_data.o \
./src/hal_entry.o \
./src/hw_io_api.o \
./src/int_prot_cmd.o \
./src/int_prot_frame.o \
./src/pwm_callbacks.o \
./src/relay_module.o \
./src/relay_state.o \
./src/relay_thread_entry.o \
./src/rs485_comm_thread_entry.o \
./src/rs485_driver.o \
./src/sc_command.o \
./src/switch_state.o \
./src/sys_manager.o \
./src/unit_test.o 

C_DEPS += \
./src/adc_scan.d \
./src/control_thread_entry.d \
./src/critical_section.d \
./src/db.d \
./src/db_api.d \
./src/flash_data.d \
./src/hal_entry.d \
./src/hw_io_api.d \
./src/int_prot_cmd.d \
./src/int_prot_frame.d \
./src/pwm_callbacks.d \
./src/relay_module.d \
./src/relay_state.d \
./src/relay_thread_entry.d \
./src/rs485_comm_thread_entry.d \
./src/rs485_driver.d \
./src/sc_command.d \
./src/switch_state.d \
./src/sys_manager.d \
./src/unit_test.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -O2 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g3 -D_RENESAS_SYNERGY_ -I"C:\git\module_bootloader\doao-module_bootldr\synergy_cfg\ssp_cfg\bsp" -I"C:\git\module_bootloader\doao-module_bootldr\synergy_cfg\ssp_cfg\driver" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc\bsp" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc\bsp\cmsis\Include" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc\driver\api" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc\driver\instances" -I"C:\git\module_bootloader\doao-module_bootldr\src" -I"C:\git\module_bootloader\doao-module_bootldr\src\synergy_gen" -I"C:\git\module_bootloader\doao-module_bootldr\synergy_cfg\ssp_cfg\framework\el" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc\framework\el" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\src\framework\el\tx" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" -x c "$<"
	@echo 'Finished building: $<'
	@echo ' '


