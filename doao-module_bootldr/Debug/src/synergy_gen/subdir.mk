################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/synergy_gen/common_data.c \
../src/synergy_gen/control_thread.c \
../src/synergy_gen/hal_data.c \
../src/synergy_gen/main.c \
../src/synergy_gen/pin_data.c \
../src/synergy_gen/relay_thread.c \
../src/synergy_gen/rs485_comm_thread.c 

OBJS += \
./src/synergy_gen/common_data.o \
./src/synergy_gen/control_thread.o \
./src/synergy_gen/hal_data.o \
./src/synergy_gen/main.o \
./src/synergy_gen/pin_data.o \
./src/synergy_gen/relay_thread.o \
./src/synergy_gen/rs485_comm_thread.o 

C_DEPS += \
./src/synergy_gen/common_data.d \
./src/synergy_gen/control_thread.d \
./src/synergy_gen/hal_data.d \
./src/synergy_gen/main.d \
./src/synergy_gen/pin_data.d \
./src/synergy_gen/relay_thread.d \
./src/synergy_gen/rs485_comm_thread.d 


# Each subdirectory must supply rules for building sources it contributes
src/synergy_gen/%.o: ../src/synergy_gen/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -O2 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g3 -D_RENESAS_SYNERGY_ -I"C:\git\module_bootloader\doao-module_bootldr\synergy_cfg\ssp_cfg\bsp" -I"C:\git\module_bootloader\doao-module_bootldr\synergy_cfg\ssp_cfg\driver" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc\bsp" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc\bsp\cmsis\Include" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc\driver\api" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc\driver\instances" -I"C:\git\module_bootloader\doao-module_bootldr\src" -I"C:\git\module_bootloader\doao-module_bootldr\src\synergy_gen" -I"C:\git\module_bootloader\doao-module_bootldr\synergy_cfg\ssp_cfg\framework\el" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc\framework\el" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\src\framework\el\tx" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" -x c "$<"
	@echo 'Finished building: $<'
	@echo ' '


