################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../synergy/ssp/src/driver/r_ioport/r_ioport.c 

OBJS += \
./synergy/ssp/src/driver/r_ioport/r_ioport.o 

C_DEPS += \
./synergy/ssp/src/driver/r_ioport/r_ioport.d 


# Each subdirectory must supply rules for building sources it contributes
synergy/ssp/src/driver/r_ioport/%.o: ../synergy/ssp/src/driver/r_ioport/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -O2 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g3 -D_RENESAS_SYNERGY_ -I"C:\git\module_bootloader\doao-module_bootldr\synergy_cfg\ssp_cfg\bsp" -I"C:\git\module_bootloader\doao-module_bootldr\synergy_cfg\ssp_cfg\driver" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc\bsp" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc\bsp\cmsis\Include" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc\driver\api" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc\driver\instances" -I"C:\git\module_bootloader\doao-module_bootldr\src" -I"C:\git\module_bootloader\doao-module_bootldr\src\synergy_gen" -I"C:\git\module_bootloader\doao-module_bootldr\synergy_cfg\ssp_cfg\framework\el" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\inc\framework\el" -I"C:\git\module_bootloader\doao-module_bootldr\synergy\ssp\src\framework\el\tx" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" -x c "$<"
	@echo 'Finished building: $<'
	@echo ' '


