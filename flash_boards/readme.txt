directories:

flash_boards/doao_crc

 contains batch file create_data.bat that will combine data from bootloader and application projects

 contains batch file flash_board.bat that will flash data, program space for a doao controller

flash_boards/simulator_crc

 contains batch file create_data.bat that will combine data from bootloader and application projects

 conatains batch file flash_board.bat that will flash data, program space for a simulator

flash_boards/doao_upgrade

 contains binary files used by the windows PC Upgrade application and the SC application to upgrade the
 doao application code

  - out.bin used by SC controller

  - packet_broad.bin used by windows PC 

flash_boards/sim_upgrade

 contains binary files used by the windows PC Upgrade application and the SC application to upgrade the
 sim application code

  - out.bin used by SC controller

  - packet_broad.bin used by windows PC 

tools directory contains:

 CreateBinFile		converts srec to a bin file format used by the sc

 CreateSCPacket 	converts bin file to packet bin file to be used by the Windows PC upgrade application

 srec_cat		srec program

files:

 create_btldr_hex.bat 

  is executed from Debug directory of the bootloader e2studio project

 create_doao_app_hex.bat

  is executed from Debug directory of the doao e2studio project

  results are put into flash_boards/doao_crc, flash_boards/doao_upgrade directories

 create_sim_app_hex.bat

  is executed from Debug directory of the simulation e2studio project

  results are put into flash_boards/sim_crc, flash_boards/sim_upgrade directories


Note: JLink will be need to be installed and windows path will need to modified to JLink executable