rx 100

// Forces J-Link to program, even if flash contents already match data to be programmed

exec SetSkipProgOnCRCMatch=0

h

// erase the device
erase

// log to file
log logfile.txt

// load btldr program into flash
loadfile btld_prog_complete.srec

// load app program into flash
//loadfile sim_app_prog_complete.srec

// load data into flash
//loadfile btld_data_complete.srec

exit