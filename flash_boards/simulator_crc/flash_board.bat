@echo off
@echo:
@echo -------------------------------------------------------------------------
@echo  This script will flash the Renesas microcontroller into program flash
@echo:   
@echo: 
@echo -------------------------------------------------------------------------
@echo:
@echo: 
pause
::
:: flash device using generic CORTEX-M0+ device
::JLink.exe -speed 4000 -if SWD -device CORTEX-M0+ -CommanderScript load_sim_bd.txt
::JLink.exe -speed 4000 -if SWD -device R7FS12477 -CommanderScript load_sim_bd.txt
JLink.exe -speed 4000 -if SWD -device R7FS12878 -CommanderScript load.txt
pause



