:: ----------------------------------------------------------------------------
:: 
:: get a bootloader hex file ready for flashing device
::
:: create_btldr_hex.bat "project folder name" 
:: 
:: example: create_btldr_hex.bat JellyfishBootloader_loc 
::
:: in this case batch file will look for create_doao_app_hex.bat JellyfishBootloader_loc.srec file 
::
:: 0x00000000 0x00015000 is the location of the bootloader
:: 0x40100000 0x401000FF is the location of the bootloader flash
:: 0x40100000 0x40100001 is the location of the bootloader CRC
::
:: 0x00018000 0x00030000 is the location of the application 
:: 0x40100100 0x40101000 is the location of the application flash
:: 0x40100100, 0x40100101 is location of the application CRC
set projName=%1

:: make a copy of the original hex file
::
copy %projName% %projName%_org.srec

:: create a data only file btldr_data.hex
::
:: crop out the data flash area for bootloader only and store in btldr_data.hex file
:: exclude the bootloader crc location
::
..\..\flash_boards\tools\srec_cat.exe %projName% -Motorola -crop 0x40100002 0x40100100 --address-length=4 -o btldr_data.hex -Motorola -Line_Length 143

:: create a program only file btldr_prog.hex
::
:: crop program only
..\..\flash_boards\tools\srec_cat.exe %projName% -Motorola -crop 0x00000000 0x00015000 --address-length=4 -o btld_prog.hex -Motorola -Line_Length 143

:: fill blank areas 
::
:: fill blank areas of program space
:: address-length=4 uses S3 records 
:: Line_Length 143 gives records of byte count 64
:: store new file in btld_fill.hex
::
..\..\flash_boards\tools\srec_cat.exe btld_prog.hex -Motorola --fill 0xff 0x00000000 0x00015000 --address-length=4 -o btld_fill.hex -Motorola -Line_Length 143

:: calc CRC
::
:: calc crc of hex file over the range 0x00000000 0x00015000 with fill 
:: then put the crc at location 0x40100000 in data space
:: store result in btld_fill_crc.hex
..\..\flash_boards\tools\srec_cat.exe btld_fill.hex -Motorola -crop 0x00000000 0x00015000 -Little_Endian_CRC16 0x40100000 -Cyclic_Redundancy_Check_16_XMODEM --address-length=4 -o btld_fill_crc.hex -Motorola -Line_Length 143

:: crop CRC
..\..\flash_boards\tools\srec_cat.exe btld_fill_crc.hex -Motorola -crop 0x40100000 0x40100100 -o btld_data_crc.hex -Motorola -Line_Length 143

:: crop program only
..\..\flash_boards\tools\srec_cat.exe btld_fill_crc.hex -Motorola -crop 0x00000000 0x00015000 --address-length=4 -o btld_prog_complete.hex -Motorola -Line_Length 143


:: add CRC to data
::srec_cat.exe btld_data_crc.hex btldr_data.hex app_data_complete.hex -o btld_data_complete.srec -Motorola -Line_Length 143
..\..\flash_boards\tools\srec_cat.exe btld_data_crc.hex btldr_data.hex -o btld_data_complete.hex -Motorola -Line_Length 143

:: copy bootloader data
::
copy btld_data_complete.hex ..\..\flash_boards\doao_crc\btld_data_complete.hex
:: copy bootloader prog
::
copy btld_prog_complete.hex ..\..\flash_boards\doao_crc\btld_prog_complete.srec

:: copy bootloader data
::
copy btld_data_complete.hex ..\..\flash_boards\simulator_crc\btld_data_complete.hex
:: copy bootloader prog
::
copy btld_prog_complete.hex ..\..\flash_boards\simulator_crc\btld_prog_complete.srec