################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/frame_processing.c \
../src/hal_entry.c \
../src/int_prot_frame.c \
../src/payload_processing.c \
../src/rs485_driver.c \
../src/time_out_handlers.c 

OBJS += \
./src/frame_processing.o \
./src/hal_entry.o \
./src/int_prot_frame.o \
./src/payload_processing.o \
./src/rs485_driver.o \
./src/time_out_handlers.o 

C_DEPS += \
./src/frame_processing.d \
./src/hal_entry.d \
./src/int_prot_frame.d \
./src/payload_processing.d \
./src/rs485_driver.d \
./src/time_out_handlers.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	C:\Renesas\e2_studio\eclipse\../Utilities/isdebuild arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g3 -D_RENESAS_SYNERGY_ -D__VTOR_PRESENT -I"C:\git\module_bootloader\Module_Bootloader\synergy_cfg\ssp_cfg\bsp" -I"C:\git\module_bootloader\Module_Bootloader\synergy_cfg\ssp_cfg\driver" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc\bsp" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc\bsp\cmsis\Include" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc\driver\api" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc\driver\instances" -I"C:\git\module_bootloader\Module_Bootloader\src" -I"C:\git\module_bootloader\Module_Bootloader\src\synergy_gen" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" -x c "$<"
	@echo 'Finished building: $<'
	@echo ' '


