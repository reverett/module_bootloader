################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../synergy/ssp/src/bsp/mcu/s128/bsp_cache.c \
../synergy/ssp/src/bsp/mcu/s128/bsp_clocks.c \
../synergy/ssp/src/bsp/mcu/s128/bsp_feature.c \
../synergy/ssp/src/bsp/mcu/s128/bsp_fmi_R7FS128783A01CFM.c \
../synergy/ssp/src/bsp/mcu/s128/bsp_group_irq.c \
../synergy/ssp/src/bsp/mcu/s128/bsp_hw_locks.c \
../synergy/ssp/src/bsp/mcu/s128/bsp_module_stop.c \
../synergy/ssp/src/bsp/mcu/s128/bsp_rom_registers.c 

OBJS += \
./synergy/ssp/src/bsp/mcu/s128/bsp_cache.o \
./synergy/ssp/src/bsp/mcu/s128/bsp_clocks.o \
./synergy/ssp/src/bsp/mcu/s128/bsp_feature.o \
./synergy/ssp/src/bsp/mcu/s128/bsp_fmi_R7FS128783A01CFM.o \
./synergy/ssp/src/bsp/mcu/s128/bsp_group_irq.o \
./synergy/ssp/src/bsp/mcu/s128/bsp_hw_locks.o \
./synergy/ssp/src/bsp/mcu/s128/bsp_module_stop.o \
./synergy/ssp/src/bsp/mcu/s128/bsp_rom_registers.o 

C_DEPS += \
./synergy/ssp/src/bsp/mcu/s128/bsp_cache.d \
./synergy/ssp/src/bsp/mcu/s128/bsp_clocks.d \
./synergy/ssp/src/bsp/mcu/s128/bsp_feature.d \
./synergy/ssp/src/bsp/mcu/s128/bsp_fmi_R7FS128783A01CFM.d \
./synergy/ssp/src/bsp/mcu/s128/bsp_group_irq.d \
./synergy/ssp/src/bsp/mcu/s128/bsp_hw_locks.d \
./synergy/ssp/src/bsp/mcu/s128/bsp_module_stop.d \
./synergy/ssp/src/bsp/mcu/s128/bsp_rom_registers.d 


# Each subdirectory must supply rules for building sources it contributes
synergy/ssp/src/bsp/mcu/s128/%.o: ../synergy/ssp/src/bsp/mcu/s128/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	C:\Renesas\e2_studio\eclipse\../Utilities/isdebuild arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g3 -D_RENESAS_SYNERGY_ -D__VTOR_PRESENT -I"C:\git\module_bootloader\Module_Bootloader\synergy_cfg\ssp_cfg\bsp" -I"C:\git\module_bootloader\Module_Bootloader\synergy_cfg\ssp_cfg\driver" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc\bsp" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc\bsp\cmsis\Include" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc\driver\api" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc\driver\instances" -I"C:\git\module_bootloader\Module_Bootloader\src" -I"C:\git\module_bootloader\Module_Bootloader\src\synergy_gen" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" -x c "$<"
	@echo 'Finished building: $<'
	@echo ' '


