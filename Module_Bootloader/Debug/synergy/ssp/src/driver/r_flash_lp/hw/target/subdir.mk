################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../synergy/ssp/src/driver/r_flash_lp/hw/target/hw_flash_lp.c 

OBJS += \
./synergy/ssp/src/driver/r_flash_lp/hw/target/hw_flash_lp.o 

C_DEPS += \
./synergy/ssp/src/driver/r_flash_lp/hw/target/hw_flash_lp.d 


# Each subdirectory must supply rules for building sources it contributes
synergy/ssp/src/driver/r_flash_lp/hw/target/%.o: ../synergy/ssp/src/driver/r_flash_lp/hw/target/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	C:\Renesas\e2_studio\eclipse\../Utilities/isdebuild arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g3 -D_RENESAS_SYNERGY_ -D__VTOR_PRESENT -I"C:\git\module_bootloader\Module_Bootloader\synergy_cfg\ssp_cfg\bsp" -I"C:\git\module_bootloader\Module_Bootloader\synergy_cfg\ssp_cfg\driver" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc\bsp" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc\bsp\cmsis\Include" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc\driver\api" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc\driver\instances" -I"C:\git\module_bootloader\Module_Bootloader\src" -I"C:\git\module_bootloader\Module_Bootloader\src\synergy_gen" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" -x c "$<"
	@echo 'Finished building: $<'
	@echo ' '


