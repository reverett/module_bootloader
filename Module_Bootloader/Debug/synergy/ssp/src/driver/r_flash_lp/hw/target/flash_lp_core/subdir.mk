################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../synergy/ssp/src/driver/r_flash_lp/hw/target/flash_lp_core/hw_codeflash.c \
../synergy/ssp/src/driver/r_flash_lp/hw/target/flash_lp_core/hw_codeflash_extra.c \
../synergy/ssp/src/driver/r_flash_lp/hw/target/flash_lp_core/hw_dataflash.c \
../synergy/ssp/src/driver/r_flash_lp/hw/target/flash_lp_core/hw_flash_common.c 

OBJS += \
./synergy/ssp/src/driver/r_flash_lp/hw/target/flash_lp_core/hw_codeflash.o \
./synergy/ssp/src/driver/r_flash_lp/hw/target/flash_lp_core/hw_codeflash_extra.o \
./synergy/ssp/src/driver/r_flash_lp/hw/target/flash_lp_core/hw_dataflash.o \
./synergy/ssp/src/driver/r_flash_lp/hw/target/flash_lp_core/hw_flash_common.o 

C_DEPS += \
./synergy/ssp/src/driver/r_flash_lp/hw/target/flash_lp_core/hw_codeflash.d \
./synergy/ssp/src/driver/r_flash_lp/hw/target/flash_lp_core/hw_codeflash_extra.d \
./synergy/ssp/src/driver/r_flash_lp/hw/target/flash_lp_core/hw_dataflash.d \
./synergy/ssp/src/driver/r_flash_lp/hw/target/flash_lp_core/hw_flash_common.d 


# Each subdirectory must supply rules for building sources it contributes
synergy/ssp/src/driver/r_flash_lp/hw/target/flash_lp_core/%.o: ../synergy/ssp/src/driver/r_flash_lp/hw/target/flash_lp_core/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	C:\Renesas\e2_studio\eclipse\../Utilities/isdebuild arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g3 -D_RENESAS_SYNERGY_ -D__VTOR_PRESENT -I"C:\git\module_bootloader\Module_Bootloader\synergy_cfg\ssp_cfg\bsp" -I"C:\git\module_bootloader\Module_Bootloader\synergy_cfg\ssp_cfg\driver" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc\bsp" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc\bsp\cmsis\Include" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc\driver\api" -I"C:\git\module_bootloader\Module_Bootloader\synergy\ssp\inc\driver\instances" -I"C:\git\module_bootloader\Module_Bootloader\src" -I"C:\git\module_bootloader\Module_Bootloader\src\synergy_gen" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" -x c "$<"
	@echo 'Finished building: $<'
	@echo ' '


