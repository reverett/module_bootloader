#ifndef __FRAME_PROCESSING_H__
#define __FRAME_PROCESSING_H__

#include "payload_processing.h"
#include "jellyfish_common.h"
uint16_t get_calc_chksum_frame(void);
void calc_chk_sum_frame(unsigned char new_value);
void process_frame(void);
void set_calc_chksum_frame(void);
void init_flash_buffer(void);
record_status_t write_to_flash(uint32_t position, BOOL last_record);
uint32_t create_flash_buffer(unsigned char* rec_buff_ptr, uint32_t dest_address, unsigned char num_bytes);
#endif //__FRAME_PROCESSING_H__

