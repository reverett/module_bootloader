/*
 * time_out_handlers.h
 *
 *  Created on: Apr 21, 2017
 *      Author: reverett
 */

#ifndef TIME_OUT_HANDLERS_H_
#define TIME_OUT_HANDLERS_H_
#include "hal_data.h"

void refresh_timer(void);
void g_timer0_callback(timer_callback_args_t * p_args);
#endif /* TIME_OUT_HANDLERS_H_ */
