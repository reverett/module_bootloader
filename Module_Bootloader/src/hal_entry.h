#ifndef __HAL_ENTRY_H__
#define __HAL_ENTRY_H__

#include "jellyfish_common.h"
BOOL valid_app_image(void);
void init_firmware_info(void);
#endif //__HAL_ENTRY_H__
