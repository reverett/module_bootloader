/***********************************************************************************************************************
 * Copyright [2015] Renesas Electronics Corporation and/or its licensors. All Rights Reserved.
 *
 * This file is part of Renesas SynergyTM Software Package (SSP)
 *
 * The contents of this file (the "contents") are proprietary and confidential to Renesas Electronics Corporation
 * and/or its licensors ("Renesas") and subject to statutory and contractual protections.
 *
 * This file is subject to a Renesas SSP license agreement. Unless otherwise agreed in an SSP license agreement with
 * Renesas: 1) you may not use, copy, modify, distribute, display, or perform the contents; 2) you may not use any name
 * or mark of Renesas for advertising or publicity purposes or in connection with your use of the contents; 3) RENESAS
 * MAKES NO WARRANTY OR REPRESENTATIONS ABOUT THE SUITABILITY OF THE CONTENTS FOR ANY PURPOSE; THE CONTENTS ARE PROVIDED
 * "AS IS" WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT; AND 4) RENESAS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, OR
 * CONSEQUENTIAL DAMAGES, INCLUDING DAMAGES RESULTING FROM LOSS OF USE, DATA, OR PROJECTS, WHETHER IN AN ACTION OF
 * CONTRACT OR TORT, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THE CONTENTS. Third-party contents
 * included in this file may be subject to different terms.
 **********************************************************************************************************************/
/***********************************************************************************************************************
* File Name    : hal_entry.c
* Description  : This is a very simple example application that blinks all LEDs on a board.
***********************************************************************************************************************/

/* HAL-only entry function */
#include "hal_data.h"
#include "jellyfish_common.h"
//#include "uart_bootloader.h"
#include "frame_processing.h"
#include "int_prot_frame.h"
#include "rs485_driver.h"
#include "hal_entry.h"

#define IMAGE_APP_ADDRESS_RUN   (0x18000)                                   // First address of the image in flash to run (runtime address)

#define DATAFLASH_FLAG_ADDRESS  (0x40100000)
#define DATAFLASH_ADDRESS       0x40100000
#define BOOTLDR_INFO_ADDRESS    0x40100000
#define APP_INFO_ADDRESS        0x40100100

#define MAX_PROG_ADDRESS        0x40000 // limit of flash for S128 device used
typedef         void(*main_fnptr)(void);
main_fnptr      *p_jump_to_app;             // Function pointer to the app

// bootloader data
fw_update_flash_struct btldr_data BSP_PLACE_IN_SECTION(".data_flash") =
#if 1 // DOAO board @ 0x4010 0000

{.crc_loc[0] = 0, .crc_loc[1] = 0, .device_id = MY_ADDR, .fw_status = BL_ST_FW_IN_FLASH,

 .fw_info.ascii_info.productID[0] = 'd',	.fw_info.ascii_info.productID[1] = 'o', .fw_info.ascii_info.productID[2] = 'a', .fw_info.ascii_info.productID[3] = 'o',
 .fw_info.ascii_info.productID[4] = 'b',	.fw_info.ascii_info.productID[5] = 'd', .fw_info.ascii_info.productID[6] = ' ', .fw_info.ascii_info.productID[7] = 'b',
 .fw_info.ascii_info.productID[8] = 't',	.fw_info.ascii_info.productID[9] = '\0',

 .fw_info.ascii_info.skew[0] = 'j',	.fw_info.ascii_info.skew[1] = 'f', .fw_info.ascii_info.skew[2] = 'i', .fw_info.ascii_info.skew[3] = 's',
 .fw_info.ascii_info.skew[4] = 'h',	.fw_info.ascii_info.skew[5] = ' ', .fw_info.ascii_info.skew[6] = ' ', .fw_info.ascii_info.skew[7] = ' ',
 .fw_info.ascii_info.skew[8] = ' ',	.fw_info.ascii_info.skew[9] = '\0',

 .fw_info.ascii_info.version[0] = '0', .fw_info.ascii_info.version[1] = '0', .fw_info.ascii_info.version[2] = '0', .fw_info.ascii_info.version[3] = '0',
 .fw_info.ascii_info.version[4] = '0', .fw_info.ascii_info.version[5] = '0', .fw_info.ascii_info.version[6] = '1',

 .fw_info.ascii_info.date[0] = '0', .fw_info.ascii_info.date[1] = '4', .fw_info.ascii_info.date[2] = '1', .fw_info.ascii_info.date[3] = '1', .fw_info.ascii_info.date[4] = '1', .fw_info.ascii_info.date[5] = '7',
 .fw_info.ascii_info.time[0] = '0', .fw_info.ascii_info.time[1] = '4', .fw_info.ascii_info.time[2] = '4', .fw_info.ascii_info.time[3] = '8', .fw_info.ascii_info.time[4] = '0', .fw_info.ascii_info.time[5] = '1',
 .fw_info.ascii_info.unused_arr[0] = '\0',

 .fw_info.num_info.start_address[0] = 0x00, .fw_info.num_info.start_address[1] = 0x00, .fw_info.num_info.start_address[2] = 0x00, .fw_info.num_info.start_address[3] = 0x00,
 .fw_info.num_info.end_address[0] = 0x00, .fw_info.num_info.end_address[1] = 0x01, .fw_info.num_info.end_address[2] = 0x50, .fw_info.num_info.end_address[3] = 0x00,
 .fw_info.num_info.record_count[0] = 0x00, .fw_info.num_info.record_count[1] = 0x00, .fw_info.num_info.record_count[2] = 0x05, .fw_info.num_info.record_count[3] = 0x49,

};
#else // Simulator Board @ 0x4010 0000
{.crc_loc[0] = 0, .crc_loc[1] = 0, .device_id = MY_ADDR, .fw_status = BL_ST_FW_IN_FLASH,

 .fw_info.ascii_info.productID[0] = 's',	.fw_info.ascii_info.productID[1] = 'i', .fw_info.ascii_info.productID[2] = 'm', .fw_info.ascii_info.productID[3] = ' ',
 .fw_info.ascii_info.productID[4] = 'b',	.fw_info.ascii_info.productID[5] = 'd', .fw_info.ascii_info.productID[6] = ' ', .fw_info.ascii_info.productID[7] = 'b',
 .fw_info.ascii_info.productID[8] = 't',	.fw_info.ascii_info.productID[9] = '\0',

 .fw_info.ascii_info.skew[0] = 'j',	.fw_info.ascii_info.skew[1] = 'f', .fw_info.ascii_info.skew[2] = 'i', .fw_info.ascii_info.skew[3] = 's',
 .fw_info.ascii_info.skew[4] = 'h',	.fw_info.ascii_info.skew[5] = ' ', .fw_info.ascii_info.skew[6] = ' ', .fw_info.ascii_info.skew[7] = ' ',
 .fw_info.ascii_info.skew[8] = ' ',	.fw_info.ascii_info.skew[9] = '\0',

 .fw_info.ascii_info.version[0] = '0', .fw_info.ascii_info.version[1] = '0', .fw_info.ascii_info.version[2] = '0', .fw_info.ascii_info.version[3] = '0',
 .fw_info.ascii_info.version[4] = '0', .fw_info.ascii_info.version[5] = '0', .fw_info.ascii_info.version[6] = '1',

 .fw_info.ascii_info.date[0] = '0', .fw_info.ascii_info.date[1] = '4', .fw_info.ascii_info.date[2] = '1', .fw_info.ascii_info.date[3] = '1', .fw_info.ascii_info.date[4] = '1', .fw_info.ascii_info.date[5] = '7',
 .fw_info.ascii_info.time[0] = '0', .fw_info.ascii_info.time[1] = '4', .fw_info.ascii_info.time[2] = '4', .fw_info.ascii_info.time[3] = '8', .fw_info.ascii_info.time[4] = '0', .fw_info.ascii_info.time[5] = '1',
 .fw_info.ascii_info.unused_arr[0] = '\0',

 .fw_info.num_info.start_address[0] = 0x00, .fw_info.num_info.start_address[1] = 0x00, .fw_info.num_info.start_address[2] = 0x00, .fw_info.num_info.start_address[3] = 0x00,
 .fw_info.num_info.end_address[0] = 0x00, .fw_info.num_info.end_address[1] = 0x01, .fw_info.num_info.end_address[2] = 0x50, .fw_info.num_info.end_address[3] = 0x00,
 .fw_info.num_info.record_count[0] = 0x00, .fw_info.num_info.record_count[1] = 0x00, .fw_info.num_info.record_count[2] = 0x05, .fw_info.num_info.record_count[3] = 0x49,

};
#endif

uint8_t CRC_test;
uint8_t downld_new_app;
sw_info_struct firmware_info;

void run_app(void);
void flash_app(uint8_t image);

#if 0
void initialize_bootloader_info(void)
{
	app_info.device_id = MY_ADDR; // load in default address
	app_info.fw_status = BL_ST_GET_NEW_FW;
	memcpy (&firmware_info.app.ascii_info.productID[0], "sim app\0", 9);
	memcpy (&firmware_info.app.ascii_info.skew[0], "jfish\0", 6);
	memcpy (&firmware_info.app.ascii_info.version[0], "0000001\0", 8); // version 000.00.00

	btldr_info.device_id = MY_ADDR; // load in default address
	btldr_info.fw_status = BL_ST_FW_IN_FLASH;
	memcpy (&btldr_info.fw_info.ascii_info.productID[0], "sim boot\0", 9);
	memcpy (&btldr_info.fw_info.ascii_info.skew[0], "jfish\0", 6);
	memcpy (&btldr_info.fw_info.ascii_info.version[0], "0000000\0", 8); // version 000.00.00
}
#endif

void init_firmware_info(void)
{
    ssp_err_t err;

    // read data flash struct for application into RAM
    // check to see if application requested new image be downloaded
    err = g_flash0.p_api->read(g_flash0.p_ctrl, &firmware_info, DATAFLASH_ADDRESS, sizeof(sw_info_struct));
    if (SSP_SUCCESS != err)
    {
        while(1);
    }

}

BOOL valid_app_image(void)
{
    uint32_t num_bytes_in_app = 0;
    uint32_t end_address =0;
    uint32_t application_address = 0;
    uint8_t * ptr_to_application;
    uint16_t crc_calc = 0;
    uint16_t crc_expected = 0;
    BOOL image_valid = FALSE;

    // load end address of application
	for (int i = 0; i < 4; i++)
    {
		end_address =  (end_address << 8) | firmware_info.app.fw_info.num_info.end_address[i];
    }
	// load start address of application
    for (int i = 0; i < 4; i++)
    {
    	application_address = (application_address << 8) | firmware_info.app.fw_info.num_info.start_address[i];
    }

    // make sure our addresses make sense before doing a crc
    if (( application_address != IMAGE_APP_ADDRESS_RUN )||( end_address > MAX_PROG_ADDRESS ))
    {
    	image_valid = FALSE;
    	return(image_valid);
    }
    num_bytes_in_app = end_address - application_address;

    ptr_to_application = (uint8_t*)application_address;
	// calc the CRC of the image in flash
	// numBytesToCrc is a 16 bit value so do 2**16 - 1 at a time
	
	{
		while (num_bytes_in_app > 65535)
		{
		   crc_calc = crc_xmodem_update(crc_calc, ptr_to_application, 65535);
		   num_bytes_in_app = num_bytes_in_app - 65535;
		   ptr_to_application = ptr_to_application + 65535;
		}
		crc_calc = crc_xmodem_update(crc_calc, ptr_to_application, num_bytes_in_app);
	}
    // load in expected CRC for this image
    for (int i = 0; i < 2; i++)
    {
    	crc_expected = (crc_expected << 8) | firmware_info.app.crc_loc[i];
    }
    // check to see if application image is OK
	if (crc_calc == crc_expected)
	{
		image_valid = TRUE;

	}
	else
	{
		image_valid = FALSE;
	}
    return(image_valid);
}

void hal_entry(void)
{
    ssp_err_t err;
    ioport_level_t level = IOPORT_LEVEL_HIGH;
    uint32_t i, j;
    volatile unsigned char rcv_data = 'z';

   // Open flash driver
    err = g_flash0.p_api->open(g_flash0.p_ctrl, g_flash0.p_cfg);
    if (SSP_SUCCESS != err)
    {
        while(1);
    }

    // Open gp timer 0
    err = g_timer0.p_api->open(g_timer0.p_ctrl, g_timer0.p_cfg);

    if (err != SSP_SUCCESS)
    {
        while(1)
        {
            /** Check err. */
        }
    }

    rs485_disable_rw();

    // load data flash struct for bootloader into RAM
    //initialize_bootloader_info();

#if 1
    // read data flash struct for application into RAM
    // check to see if application requested new image be downloaded
    init_firmware_info();

    if (firmware_info.app.fw_status == BL_ST_FW_IN_FLASH)   // image has been flashed
    {
    	if (valid_app_image() == TRUE)
    	{
    		err = g_flash0.p_api->close(g_flash0.p_ctrl);

    		run_app(); // OK jump to application never return
    	}
    	// else failure setup serial channel, process frames of data

    }
#endif
    // else SC has new image or no header info -- assume there is no image in flash

    // initialize UART

	//uart_sci_ch0_disable_int();

	//uart_sci_ch0_setup();

    rs485_initialize();

    // init flash buffers
    init_flash_buffer();
    // init data in payload processing
    init_payload_processing();

	// download image file

	process_frame(); // stay in this function until quit command is received

	err = g_flash0.p_api->close(g_flash0.p_ctrl);

	run_app(); // OK jump to application never return

}

void run_app(void)
{
    // Point to the start reset vector for the  application
    p_jump_to_app = ((main_fnptr*)IMAGE_APP_ADDRESS_RUN) + 1U;

    // Disable interrupts
    __disable_irq();

    // Update the vector table to the reset vector of the application
    SCB->VTOR = (uint32_t)IMAGE_APP_ADDRESS_RUN;

    //Complete all explicit memory accesses
    __DSB();

    //Set stack for the application
    __set_MSP(*((uint32_t*)IMAGE_APP_ADDRESS_RUN));

    //Jump to application
    (*p_jump_to_app)();
}
