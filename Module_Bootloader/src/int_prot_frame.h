/*
* rhaframe.h
*
*  Created on: Aug 24, 2015
*      Author: root
*/
//#include <stdio.h>
#include "types.h"

#ifndef RHAFRAME_H_
#define RHAFRAME_H_

#define SOF 0xF1

//define minimum firmware version 1.5.7
#define MIN_VER_MAJOR 1
#define MIN_VER_MINOR 5
#define MIN_VER_BUILD 7

//Manufacture ID
#define MANUFACTURER_ID 0x109B		//fake id for testing...

//primary headers
#define PH_UTILITY 	0x55
#define PH_NETWORK_COMISSIONING      0x01
#define PH_SECURITY_CONFIG      0x02
#define PH_ZIGBEE_SUPPORT_CONFIG      0x03
#define PH_ZDO_MESSAGES      0x04
#define PH_ZCL_MESSAGES      0x05
#define PH_GENERAL_CLUSTERS      0x11
#define PH_HA_CLUSTERS      0x12
#define PH_BOOTLOAD      0x0b
#define PH_OTA_BOOTLOAD      0xb0
#define PH_DIAGNOSTICS      0xd1
#define PH_DEBUG      0xde

#define PH_IO_COMMAND      0x21

//IO commands Secondary Header Definitions
#define SH_IO_COMMAND_ACK              0x00
#define SH_IO_COMMAND_DO_WRITE         0x01
#define SH_IO_COMMAND_DO_REQUEST       0x02
#define SH_IO_COMMAND_DO_RESPONSE      0x03
#define SH_IO_COMMAND_AO_WRITE         0x04
#define SH_IO_COMMAND_ADCAO_REQUEST    0x05
#define SH_IO_COMMAND_ADCAO_RESPONSE   0x06
#define SH_IO_COMMAND_DI_REQUEST       0x07
#define SH_IO_COMMAND_DI_RESPONSE      0x08
#define SH_IO_COMMAND_AI_REQUEST       0x09
#define SH_IO_COMMAND_AI_RESPONSE      0x0A
#define SH_IO_COMMAND_LED_WRITE        0x0B
#define SH_IO_COMMAND_NZC_WRITE        0x0C
#define SH_IO_COMMAND_EOL_WRITE        0x0D
#define SH_IO_COMMAND_EOL_REQUEST      0x0E
#define SH_IO_COMMAND_EOL_RESPONSE     0x0F
#define SH_IO_COMMAND_ADC24V_REQUEST   0x10
#define SH_IO_COMMAND_ADC24V_RESPONSE  0x11
#define SH_IO_COMMAND_FWV_REQUEST 	   0x12
#define SH_IO_COMMAND_FWV_RESPONSE     0x13
#define SH_IO_COMMAND_VT_REQUEST       0x14
#define SH_IO_COMMAND_VT_RESPONSE      0x15
#define SH_IO_COMMAND_NVT_REQUEST      0x16
#define SH_IO_COMMAND_NVT_RESPONSE     0x17

#define SH_IO_COMMAND_INFO_REQUEST     0x1C
#define SH_IO_COMMAND_INFO_RESPONSE    0x1D

#define SH_IO_COMMAND_FW_UPGRADE       0x1E
#define SW_IO_COMMAND_FW_RESPONSE      0x1F

//Utility Secondary Header Definitions
#define SH_UTILITY_RESET      0x00
#define SH_UTILITY_MODULE_INFO_REQUEST      0x02
#define SH_UTILITY_MODULE_INFO_RESPONSE      0x03
#define SH_UTILITY_BOOTLOADER_VERSION_REQUEST      0x04
#define SH_UTILITY_BOOTLOADER_VERSION_RESPONSE      0x05
#define SH_UTILITY_RESTORE_DEFAULTS      0x10
#define SH_UTILITY_HOST_STARTUP_READY      0x20
#define SH_UTILITY_STARTUP_SYNC_REQUEST      0x21
#define SH_UTILITY_STARTUP_SYNC_COMPLETE      0x22
#define SH_UTILITY_ANTENNAE_CONFIGURATION_REQUEST      0x23
#define SH_UTILITY_ANTENNAE_CONFIGURATION_RESPONSE      0x24
#define SH_UTILITY_ANTENNAE_CONFIGURATION_WRITE      0x25
#define SH_UTILITY_LED_CONFIGURATION_REQUEST      0x26
#define SH_UTILITY_LED_CONFIGURATION_RESPONSE      0x27
#define SH_UTILITY_LED_CONFIGURATION_WRITE      0x28
#define SH_UTILITY_SERIAL_ACK_CONFIG_WRITE      0x30
#define SH_UTILITY_SERIAL_ACK_CONFIG_REQUEST      0x31
#define SH_UTILITY_SERIAL_ACK_CONFIG_RESPONSE      0x32
#define SH_UTILITY_MANUFACTURER_ID_REQUEST      0x40
#define SH_UTILITY_MANUFACTURER_ID_RESPONSE      0x41
#define SH_UTILITY_MANUFACTURER_ID_WRITE      0x42
#define SH_UTILITY_SLEEPY_PARAMETERS_REQUEST      0x50
#define SH_UTILITY_SLEEPY_PARAMETERS_RESPONSE      0x51
#define SH_UTILITY_SLEEPY_PARAMETERS_WRITE      0x52
#define SH_UTILITY_SLEEPY_HIBERNATE_DURATION_REQUEST      0x53
#define SH_UTILITY_SLEEPY_HIBERNATE_DURATION_RESPONSE      0x54
#define SH_UTILITY_SLEEPY_HIBERNATE_DURATION_WRITE      0x55
#define SH_UTILITY_SLEEPY_CONTROL_WAKEUP      0xA0
#define SH_UTILITY_SLEEPY_CONTROL_STAY_AWAKE_DURATION_WRITE      0x58
#define SH_UTILITY_SLEEPY_CONTROL_STAY_AWAKE_DURATION_READ      0x59
#define SH_UTILITY_SLEEPY_CONTROL_STAY_AWAKE_DURATION_RESPONSE      0x5a
#define SH_UTILITY_HOST_USER_ACTION_START      0x56
#define SH_UTILITY_HOST_USER_ACTION_COMPLETE      0x57
#define SH_UTILITY_ENABLE_FILTER_LAYER      0x60
#define SH_UTILITY_DISABLE_FILTER_LAYER      0x61
#define SH_UTILITY_STATUS_RESPONSE      0x80
#define SH_UTILITY_ERROR      0xe0

//Network Commissioning Secondary Header Definitions
#define SH_NETWORK_COMISSIONING_DISCOVER_NODE      0x21
#define SH_NETWORK_COMISSIONING_REGISTER_NODE      0x22
#define SH_NETWORK_COMISSIONING_ASSIGN_NODE        0x23
#define SH_NETWORK_COMISSIONING_CONFIRM_NODE       0x24

#define SH_NETWORK_COMISSIONING_FORM_NETWORK      0x01
#define SH_NETWORK_COMISSIONING_JOIN_NETWORK      0x00
#define SH_NETWORK_COMISSIONING_PERMIT_JOIN       0x03
#define SH_NETWORK_COMISSIONING_LEAVE_NETWORK      0x04
#define SH_NETWORK_COMISSIONING_REJOIN_NETWORK      0x05
#define SH_NETWORK_COMISSIONING_NETWORK_STATUS_REQUEST      0x08
#define SH_NETWORK_COMISSIONING_NETWORK_STATUS_RESPONSE      0x09
#define SH_NETWORK_COMISSIONING_TRUST_CENTER_DEVICE_UPDATE      0x10
#define SH_NETWORK_COMISSIONING_NETWORK_AUTO_JOIN      0x11
#define SH_NETWORK_COMISSIONING_NETWORK_RESET_AUTO_JOIN      0x12

//Security Config Secondary Header Definitions
#define SH_SECURITY_CONFIG_PRECONFIGURED_KEY_OPTION_WRITE      0x00
#define SH_SECURITY_CONFIG_PRECONFIGURED_KEY_OPTION_REQUEST      0x01
#define SH_SECURITY_CONFIG_PRECONFIGURED_KEY_OPTION_RESPONSE      0x02
#define SH_SECURITY_CONFIG_INSTALL_CODE_REQUEST      0x03
#define SH_SECURITY_CONFIG_INSTALL_CODE_RESPONSE      0x04
#define SH_SECURITY_CONFIG_LINK_KEY_WRITE      0x05
#define SH_SECURITY_CONFIG_LINK_KEY_REQUEST      0x06
#define SH_SECURITY_CONFIG_LINK_KEY_RESPONSE      0x07
#define SH_SECURITY_CONFIG_NETWORK_KEY_WRITE      0x08
#define SH_SECURITY_CONFIG_NETWORK_KEY_REQUEST      0x09
#define SH_SECURITY_CONFIG_NETWORK_KEY_RESPONSE      0x0a

//ZigBee Support Config Secondary Header Definitions
#define SH_ZIGBEE_SUPPORT_CONFIG_DEVICE_TYPE_WRITE      0x00
#define SH_ZIGBEE_SUPPORT_CONFIG_DEVICE_TYPE_REQUEST      0x01
#define SH_ZIGBEE_SUPPORT_CONFIG_DEVICE_TYPE_RESPONSE      0x02
#define SH_ZIGBEE_SUPPORT_CONFIG_ADD_ENDPOINT      0x10
#define SH_ZIGBEE_SUPPORT_CONFIG_ENDPOINT_LIST_REQUEST      0x11
#define SH_ZIGBEE_SUPPORT_CONFIG_ENDPOINT_LIST_RESPONSE      0x12
#define SH_ZIGBEE_SUPPORT_CONFIG_ENDPOINT_DESCRIPTOR_REQUEST      0x13
#define SH_ZIGBEE_SUPPORT_CONFIG_ENDPOINT_DESCRIPTOR_RESPONSE      0x14
#define SH_ZIGBEE_SUPPORT_CONFIG_ADD_ATTRIBUTES_TO_CLUSTER      0x20
#define SH_ZIGBEE_SUPPORT_CONFIG_ATTRIBUTE_LIST_REQUEST      0x21
#define SH_ZIGBEE_SUPPORT_CONFIG_ATTRIBUTE_LIST_RESPONSE      0x22
#define SH_ZIGBEE_SUPPORT_CONFIG_ATTRIBUTE_REQUEST      0x23
#define SH_ZIGBEE_SUPPORT_CONFIG_ATTRIBUTE_RESPONSE      0x24
#define SH_ZIGBEE_SUPPORT_CONFIG_ATTRIBUTE_WRITE      0x25
#define SH_ZIGBEE_SUPPORT_CONFIG_CLEAR_ENDPOINT_CONFIG      0x30
#define SH_ZIGBEE_SUPPORT_CONFIG_REGISTER_COMMANDS_PASSTHRU      0x80

//ZDO Messages Secondary Header Definitions
#define SH_ZDO_MESSAGES_SEND_ZDO_UNICAST      0x01
#define SH_ZDO_MESSAGES_SEND_ZDO_BROADCAST      0x02
#define SH_ZDO_MESSAGES_ZDO_SEND_STATUS      0x03
#define SH_ZDO_MESSAGES_ZDO_APS_ACKNOWLEDGEMENT      0x04
#define SH_ZDO_MESSAGES_ZDO_RESPONSE_RECEIVED      0x05
#define SH_ZDO_MESSAGES_ZDO_RESPONSE_TIMEOUT      0x06
#define SH_ZDO_MESSAGES_ZDO_DEVICE_ANNOUNCE_RECEIVED      0x1e

//ZCL Messages Secondary Header Definitions
#define SH_ZCL_MESSAGES_SEND_ZCL_UNICAST      0x00
#define SH_ZCL_MESSAGES_SEND_ZCL_MULTICAST      0x01
#define SH_ZCL_MESSAGES_SEND_ZCL_BROADCAST      0x02
#define SH_ZCL_MESSAGES_ZCL_SEND_STATUS      0x03
#define SH_ZCL_MESSAGES_ZCL_APS_ACKNOWLEDGEMENT      0x10
#define SH_ZCL_MESSAGES_ZCL_RESPONSE_RECEIVED      0x11
#define SH_ZCL_MESSAGES_ZCL_RESPONSE_TIMEOUT      0x12
#define SH_ZCL_MESSAGES_RECEIVED_WRITE_ATTRIBUTE      0x14
#define SH_ZCL_MESSAGES_ZCL_PASSTHROUGH_MESSAGE      0x20
#define SH_ZCL_MESSAGES_ZCL_READ_ATTRIBUTE_REQUEST      0x30
#define SH_ZCL_MESSAGES_ZCL_READ_ATTRIBUTE_RESPONSE      0x31
#define SH_ZCL_MESSAGES_ZCL_WRITE_ATTRIBUTE_REQUEST      0x32
#define SH_ZCL_MESSAGES_ZCL_WRITE_ATTRIBUTE_RESPONSE      0x33

//General Clusters Secondary Header Definitions
#define SH_GENERAL_CLUSTERS_RECEIVED_FACTORY_DEFAULT_RESET      0x00
#define SH_GENERAL_CLUSTERS_IDENTIFY_START      0x10
#define SH_GENERAL_CLUSTERS_IDENTIFY_STOP      0x11
#define SH_GENERAL_CLUSTERS_TIME_CLIENT_GET_TIME      0x40
#define SH_GENERAL_CLUSTERS_TIME_CLIENT_GET_TIME_RESPONSE      0x41
#define SH_GENERAL_CLUSTERS_TIME_CLIENT_TIME_CHANGED      0x42
#define SH_GENERAL_CLUSTERS_NETWORK_TIME_SYNC_PERIOD_WRITE      0x43
#define SH_GENERAL_CLUSTERS_NETWORK_TIME_SYNC_PERIOD_REQUEST      0x44
#define SH_GENERAL_CLUSTERS_NETWORK_TIME_SYNC_PERIOD_RESPONSE      0x45

//HA Clusters Secondary Header Definitions
#define SH_HA_CLUSTERS_ON_OFF_STATE_UPDATE      0x00
#define SH_HA_CLUSTERS_POWER_CONFIGURATION_CHECK_BATTERY_VOLTAGE_TO_SEND_ALARM      0x12
#define SH_HA_CLUSTERS_LEVEL_SERVER_MOVE_TO_LEVEL      0x20
#define SH_HA_CLUSTERS_LEVEL_SERVER_MOVE      0x21
#define SH_HA_CLUSTERS_LEVEL_SERVER_STEP      0x22
#define SH_HA_CLUSTERS_LEVEL_SERVER_STOP      0x23
#define SH_HA_CLUSTERS_LOCK_DOOR      0x60
#define SH_HA_CLUSTERS_UNLOCK_DOOR      0x61
#define SH_HA_CLUSTERS_LOCK_CONFIRM      0x63

#if 1
//Bootload Secondary Header Definitions
#define SH_BOOTLOAD_INFO       0x02
#define SH_BOOTLOAD_INFO_NAK   0x82
#define SH_BOOTLOAD_IMAGE      0x03
#define SH_BOOTLOAD_IMAGE_NAK  0x83
#define SH_BOOTLOAD_RD_DATA    0x04
#define SH_BOOTLOAD_RD_DATA_NAK 0x84
#define SH_BOOTLOAD_ER_DATA    0x05
#define SH_BOOTLOAD_ER_DATA_NAK 0x85
#define SH_BOOTLOAD_QUIT       0x06
#define SH_BOOTLOAD_QUIT_NAK   0x86
#define SH_BOOTLOAD_NVT        0x07
#define SH_BOOTLOAD_NVT_NAK    0x87

#define SH_BOOTLOAD_IMAGE_LAST 0xF3

#define SH_BOOTLOAD_SREC_ERR     0xE0
#define SH_BOOTLOAD_CKSUM_ERR    0xE1
#define SH_BOOTLOAD_CRC_ERR      0xE2
#define SH_BOOTLOAD_ERASE_ERR    0xE3
#define SH_BOOTLOAD_BLANK_ERR    0xE4
#define SH_BOOTLOAD_FLASH_ERR    0xE5
#define SH_BOOTLOAD_ORDER_ERR    0xE6
#define SH_BOOTLOAD_MSREC_ERR    0xE7
#define SH_BOOTLOAD_FRMCKSUM_ERR 0xE8

#else
//Bootload Secondary Header Definitions
#define SH_BOOTLOAD_QUERY_NEXT_IMAGE_RESPONSE      0x02
#define SH_BOOTLOAD_IMAGE_BLOCK_REQUEST      0x03
#define SH_BOOTLOAD_IMAGE_BLOCK_RESPONSE      0x05
#define SH_BOOTLOAD_UPGRADE_END_REQUEST      0x06
#define SH_BOOTLOAD_UPGRADE_END_RESPONSE      0x07
#define SH_BOOTLOAD_OTA_QUERY_CONFIGURATION_REQUEST      0x20
#define SH_BOOTLOAD_OTA_QUERY_CONFIGURATION_RESPONSE      0x21
#define SH_BOOTLOAD_OTA_QUERY_CONFIGURATION_WRITE      0x22
#define SH_BOOTLOAD_IMAGE_PROGRESS_NOTIFICATION      0x23
#define SH_BOOTLOAD_OTA_VERSION_CONFIGURATION_REQUEST      0x24
#define SH_BOOTLOAD_OTA_VERSION_CONFIGURATION_RESPONSE      0x25
#define SH_BOOTLOAD_OTA_VERSION_CONFIGURATION_WRITE      0x26
#endif


//OTA Bootload Secondary Header Definitions
#define SH_OTA_BOOTLOAD_OTA_IMAGE_NOTIFICATION      0x00
#define SH_OTA_BOOTLOAD_OTA_QUERY_NEXT_IMAGE_REQUEST      0x01
#define SH_OTA_BOOTLOAD_OTA_QUERY_NEXT_IMAGE_RESPONSE      0x02
#define SH_OTA_BOOTLOAD_OTA_IMAGE_BLOCK_REQUEST      0x03
#define SH_OTA_BOOTLOAD_OTA_IMAGE_BLOCK_RESPONSE      0x05
#define SH_OTA_BOOTLOAD_OTA_UPGRADE_END_REQUEST      0x06
#define SH_OTA_BOOTLOAD_OTA_UPGRADE_END_RESPONSE      0x07

//Diagnostics Secondary Header Definitions
#define SH_DIAGNOSTICS_NETWORK_SCAN_REQUEST      0x00
#define SH_DIAGNOSTICS_NETWORK_SCAN_RESPONSE      0x01
#define SH_DIAGNOSTICS_NETWORK_SCAN_COMPLETE      0x02
#define SH_DIAGNOSTICS_LATENCY_REQUEST      0x10
#define SH_DIAGNOSTICS_LATENCY_RESPONSE      0x11

//Debug Secondary Header Definitions
#define SH_DEBUG_SERIAL_ECHO_REQUEST_RESPONSE      0x01
#define SH_DEBUG_CERTIFICATE_ISSUER_REQUEST      0x02
#define SH_DEBUG_CERTIFICATE_ISSUER_RESPONSE      0x03
#define SH_DEBUG_MANUFACTURER_INFORMATION_REQUEST      0x04
#define SH_DEBUG_MANUFACTURER_INFORMATION_RESPONSE      0x05
#define SH_DEBUG_EXTERNAL_FLASH_INFORMATION_REQUEST      0x06
#define SH_DEBUG_EXTERNAL_FLASH_INFORMATION_RESPONSE      0x07
#define SH_DEBUG_BOARD_NAME_TOKEN_INFORMATION_REQUEST      0x08
#define SH_DEBUG_BOARD_NAME_TOKEN_INFORMATION_RESPONSE      0x09
#define SH_DEBUG_RF_POWER_OUTPUT_TOKEN_INFORMATION_REQUEST      0x0a
#define SH_DEBUG_RF_POWER_OUTPUT_TOKEN_INFORMATION_RESPONSE      0x0b
#define SH_DEBUG_ATTRIBUTE_REPORT_PASSTHROUGH_CONTROL      0x0c
#define SH_DEBUG_APPLICATION_ID_REQUEST      0x10
#define SH_DEBUG_APPLICATION_ID_RESPONSE      0x11
#define SH_DEBUG_APPLICATION_VERSION_COUNT_REQUEST      0x12
#define SH_DEBUG_APPLICATION_VERSION_COUNT_RESPONSE      0x13
#define SH_DEBUG_APPLICATION_VERSION_REQUEST      0x14
#define SH_DEBUG_APPLICATION_VERSION_RESPONSE      0x15
#define SH_DEBUG_MANUFACTURING_LIBRARY_START      0x80
#define SH_DEBUG_MANUFACTURING_LIBRARY_STOP      0x81
#define SH_DEBUG_MANUFACTURING_LIBRARY_TONE_CONTROL      0x82
#define SH_DEBUG_MANUFACTURING_LIBRARY_STREAM_CONTROL      0x83
#define SH_DEBUG_MANUFACTURING_LIBRARY_TX_PACKET      0x84
#define SH_DEBUG_MANUFACTURING_LIBRARY_RX_PACKET      0x85
#define SH_DEBUG_MANUFACTURING_LIBRARY_SET_CHANNEL      0x86
#define SH_DEBUG_MANUFACTURING_LIBRARY_GET_CHANNEL_REQUEST      0x87
#define SH_DEBUG_MANUFACTURING_LIBRARY_GET_CHANNEL_RESPONSE      0x88
#define SH_DEBUG_MANUFACTURING_LIBRARY_SET_POWER      0x89
#define SH_DEBUG_MANUFACTURING_LIBRARY_GET_POWER_REQUEST      0x8a
#define SH_DEBUG_MANUFACTURING_LIBRARY_GET_POWER_RESPONSE      0x8b
#define SH_DEBUG_MANUFACTURING_LIBRARY_SET_SYNC_OFFSET      0x8c
#define SH_DEBUG_MANUFACTURING_LIBRARY_GET_SYNC_OFFSET_REQUEST      0x8d
#define SH_DEBUG_MANUFACTURING_LIBRARY_GET_SYNC_OFFSET_RESPONSE      0x8e
#define SH_DEBUG_MANUFACTURING_TEST_CONT_MOD_CAL      0x8f


/*-----------------------------------------------------------------------------
*rha bytes masks
*-----------------------------------------------------------------------------*/
#define SOF_RX (1 << 0)
#define ADDR_RX (1 << 1)
#define PH_RX (1 << 2)
#define SH_RX (1 << 3)
#define SEQ_RX (1 << 4)
#define LEN_RX (1 << 5)
#define PAYLOAD_RX (1 << 6)
#define DEFAULT_ADDR 0xFA
#define MASTER_ADDR 0x00
#if 0 // test
#define MY_ADDR 0x01
#else
#define MY_ADDR DEFAULT_ADDR
#endif
#define BROADCAST_ADDR 0xFC
/*-----------------------------------------------------------------------------
*  RHA serial frame structure
*-----------------------------------------------------------------------------*/
typedef struct {
	uint8_t idx;
	uint8_t flags;
	uint8_t addr; // peripheral device address
	uint8_t ph;   // primary header
	uint8_t sh;   // secondary header
	uint8_t seq;  // message sequence index
	uint8_t len;  // payload length
	uint8_t *payload; // ptr to payload buffer
	uint16_t cs;      // checksum lsb
	uint16_t calc_cs; // checksum msb
} rha_frame_t;


#define MAX_PAYLOAD_LENGTH 127

/*-----------------------------------------------------------------------------
*  RHA serial frame handler structure
*-----------------------------------------------------------------------------*/
typedef struct {
	uint8_t ph;	//primary header
	uint8_t sh;	//secondary header
	uint8_t min_length;
	void(*fn)(rha_frame_t *); 	//pointer the to handler function
} rha_frame_handler_t;

#define UID_LENGTH 16

enum state_codes { discover, regist, assign, confirm };
enum module_types { NONE, DO, AODO, AIDI, DI };

typedef struct{
	uint8_t uid[UID_LENGTH];
	uint8_t cur_address;
	enum module_types type;
	enum state_codes  state;
}module_info_t;

extern uint8_t err_firmware_version;
extern uint8_t zb_port;		//zigbee module serial port
extern uint8_t running_state;	// = frame->payload[0];
extern uint8_t config_state;	// = frame->payload[1];
extern uint8_t my_addr;
extern uint8_t uid[16];
extern module_info_t module_info;

//void process_rha_frame(rha_frame_t *frame);
void rha_bytes(uint8_t *bytes, uint8_t len);
uint8_t * construct_rha_frame(uint8_t address, uint8_t primary_hearder, uint8_t secondary_header, uint8_t *payload, uint8_t payload_length);

//void io_command_ao_handler(rha_frame_t *frame);
//void io_command_do_handler(rha_frame_t *frame);
//void io_command_led_handler(rha_frame_t *frame);
//void io_command_zc_handler(rha_frame_t *frame);
//void io_command_ai_handler(rha_frame_t *frame);
//void io_command_di_handler(rha_frame_t *frame);
//void io_command_seteol_handler(rha_frame_t *frame);
//void io_command_geteol_handler(rha_frame_t *frame);
//void io_command_adcao_handler(rha_frame_t *frame);
//void io_command_adc24v_handler(rha_frame_t *frame);
void process_rha_frame(rha_frame_t *frame);
void default_handler(rha_frame_t *frame);

void send_ack(uint8_t primary_header, uint8_t secondary_header);

uint8_t * construct_rha_payload(uint8_t payload_length);
//void network_discover_node_handler(rha_frame_t *frame);
//void network_assign_node_handler(rha_frame_t *frame);
void io_command_get_nvt_table_handler(void);
void bootloader_version_handler(rha_frame_t *frame);
void send_info_to_controller(uint8_t message_type, uint8_t *ptr_to_data, uint8_t length);
/*
* ===  FUNCTION  ======================================================================
*         Name:  calc_fcs
*  Description:  Calculate and return checksum
* =====================================================================================
*/
static __inline uint16_t calc_fcs(uint8_t *p_msg, uint8_t len) {
	uint16_t result = 0;
	while (len--)
		result = (uint16_t)(result + *p_msg++);
	return result;
}

#endif /* RHAFRAME_H_ */
