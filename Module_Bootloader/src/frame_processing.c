//#include <stdio.h>
#include <types.h>
#include "int_prot_frame.h"
#include "payload_processing.h"
#include "frame_processing.h"
#include "rs485_driver.h"
#include "hal_entry.h"
#include "hal_data.h"
#include "time_out_handlers.h"
//extern FILE *binFilePtr;
//extern FILE *outFilePtr;

// packet 
// header structure
typedef struct {
	//uint8_t idx;
	//uint8_t flags;
	uint8_t addr; // peripheral device address
	uint8_t ph;   // primary header
	uint8_t sh;   // secondary header
	uint8_t seq;  // message sequence index
	uint8_t len;  // payload length
} frame_header_t;

// payload buffer


// check sum structure
typedef struct {
	uint16_t cs;      // checksum lsb
	uint16_t calc_cs; // checksum msb
} frame_chksum_t;

typedef enum {
	BUFF_EMPTY = 0,
	BUFF_FULL = 1,
	BUFF_CURR = 2
}buff_status_t;


// packet header
unsigned char frame_header_buff[sizeof(frame_header_t)];
frame_header_t *header_ptr = (frame_header_t *)&frame_header_buff[0];
unsigned char *frame_ptr = &frame_header_buff[0];

// calculated checksum for header and payload
static uint16_t calc_check_sum = 0;
static uint32_t num_chk_sum_bytes = 0;

// from our payload
unsigned char data_buff[100];

// for test
//unsigned char testBuffer[1024];

// buffer used to flash device
#define FLASH_BLOCK_SIZE 1024
#define MAX_BUFF 1024
unsigned char buffer_1[MAX_BUFF];
unsigned char buffer_2[MAX_BUFF];
static unsigned char * buff_ptr = &buffer_1[0];
static BOOL buffer_1_full = FALSE;
static BOOL buffer_2_full = FALSE;
static uint16_t calculated_crc = 0;
static uint32_t index = 0;
static buff_status_t buff1_status = BUFF_EMPTY;
static buff_status_t buff2_status = BUFF_EMPTY;
static BOOL first_record;
static uint32_t start_dest_addr1;
static uint32_t start_dest_addr2;

extern sw_info_struct firmware_info;

#define DATAFLASH_ADDRESS  (0x40100000)
#define TX_WRITE_DELAY     100 // 100 msec

void set_calc_chksum_frame(void)
{
	calc_check_sum = 0;
	num_chk_sum_bytes = 0;
}

uint16_t get_calc_chksum_frame(void)
{
	return (calc_check_sum);
}

// calculate checksum for frame header, and payload
void calc_chk_sum_frame(unsigned char new_value)
{
	num_chk_sum_bytes++;
	calc_check_sum = (uint16_t)calc_check_sum + new_value;
}

void init_flash_buffer(void)
{
	index = 0;
	first_record = TRUE;
	buff_ptr = &buffer_1[0];
	buff1_status = BUFF_CURR;
	buff2_status = BUFF_EMPTY;
	calculated_crc = 0;
}

uint32_t create_flash_buffer(unsigned char* rec_buff_ptr, uint32_t dest_address, unsigned char num_bytes)
{
	unsigned char rec_len = num_bytes;

	if (first_record)
	{
		first_record = FALSE;
		start_dest_addr1 = dest_address; // flash start addres for buffer1
	}

	// get data and move to buffer
	while (num_bytes)
	{
		*buff_ptr = *rec_buff_ptr;
		//printf("%02X", *buff_ptr);
		//fprintf(outFilePtr, "%02X", *buff_ptr);

		num_bytes = num_bytes - 1;

		if (index < MAX_BUFF - 1)
		{
			index = index + 1; // increment our index
			buff_ptr++;
			rec_buff_ptr++;
		}
		else
		{
			// switch buffers
			if (buff_ptr == &buffer_1[index]) // current buffer is 1 switch to 2
			{
				buff1_status = BUFF_FULL;
				// flash start_dest_addr = dest address of srec + the srec len - bytes that exceed the buffer 
				start_dest_addr2 = dest_address + rec_len - num_bytes; // flash start address for buffer 2
				buff_ptr = &buffer_2[0];
				buff2_status = BUFF_CURR;
			}
			else if (buff_ptr == &buffer_2[index]) // current buffer is 2 switch to 1
			{
				buff2_status = BUFF_FULL;
				// flash start_dest_addr = dest address of srec + the srec len - bytes that exceed the buffer 
				start_dest_addr1 = dest_address + rec_len - num_bytes; // flash start address for buffer 1
				buff_ptr = &buffer_1[0];
				buff1_status = BUFF_CURR;
			}
			index = 0;
			rec_buff_ptr++;
		}
	}
	return(index);
}
#define FLASH_DF_BLOCK_0     0x40100000 // Data Flash area
#define FLASH_DATA_BLOCK_SZ  0x400      // size of one block

record_status_t write_header_to_flash(uint8_t update_status)
{
	record_status_t rec_status = PRC_REC_RCVD_OK;

#if 1
	ssp_err_t err = SSP_SUCCESS;
	flash_result_t result;
	unsigned char *ptr_data_flash_buff = &firmware_info;


    // Disable interrupts
    //__disable_irq();

	// set our flag
	firmware_info.app.fw_status = update_status;


	//Erase application area
	err = g_flash0.p_api->erase(g_flash0.p_ctrl, FLASH_DF_BLOCK_0, 1); // erase 1 block
	if (SSP_SUCCESS != err)
	{
		rec_status = PRC_REC_ERASE_FAIL;
	}
	else
	{
		// Blank check 
		err = g_flash0.p_api->blankCheck(g_flash0.p_ctrl, FLASH_DF_BLOCK_0, FLASH_DATA_BLOCK_SZ, &result);
		if ((SSP_SUCCESS != err) || (FLASH_RESULT_BLANK != result))
		{
			rec_status = PRC_REC_BLANK_FAIL;
		}
		else
		{
			// Set destination address
			err = g_flash0.p_api->write(g_flash0.p_ctrl, (uint32_t)ptr_data_flash_buff, FLASH_DF_BLOCK_0, sizeof(sw_info_struct));
			if (SSP_SUCCESS != err)
			{
				rec_status = PRC_REC_FLASH_FAIL;
			}
			//else
			//{
				//err = g_flash0.p_api->read(g_flash0.p_ctrl, testBuffer, FLASH_DF_BLOCK_0, sizeof(sw_info_struct));
			//}
		}
	}
    // Enable interrupts
    //__enable_irq();
#endif


	return(rec_status);
}

record_status_t write_to_flash(uint32_t position, BOOL last_record)
{
	uint32_t length;
	unsigned char *ptr_to_flash_buff = &buffer_1[0];
	uint32_t flash_start_address;
	ssp_err_t err = SSP_SUCCESS;
	record_status_t rec_status = PRC_REC_RCVD_OK;
    flash_result_t result;

	if (last_record)
	{
		if (buff1_status == BUFF_CURR)
		{
			ptr_to_flash_buff = &buffer_1[0];
			flash_start_address = start_dest_addr1;
			length = position;
		}
		else if (buff2_status == BUFF_CURR)
		{
			ptr_to_flash_buff = &buffer_2[0];
			flash_start_address = start_dest_addr2;
			length = position;
		}
	}
	else
	{
		if (buff1_status == BUFF_FULL)
		{
			ptr_to_flash_buff = &buffer_1[0];
			flash_start_address = start_dest_addr1;
			length = FLASH_BLOCK_SIZE;
		}
		else if (buff2_status == BUFF_FULL)
		{
			ptr_to_flash_buff = &buffer_2[0];
			flash_start_address = start_dest_addr2;
			length = FLASH_BLOCK_SIZE;
		}
	}

	if ((buff1_status == BUFF_FULL) || (buff2_status == BUFF_FULL) || (last_record))
	{
		calculated_crc = crc_xmodem_update(calculated_crc, (uint8_t*)ptr_to_flash_buff, length);
		//fprintf(buffFilePtr, "\nbuffer1 last %08X\n", start_dest_addr1);
		//fprintf(buffFilePtr, "\n");
#if 0
		if (buff1_status == BUFF_FULL)
		{
			fprintf(buffFilePtr, "\nbuff1 flash_addr %08X\n", flash_start_address);
		}
		else if (buff2_status == BUFF_FULL)
		{
			fprintf(buffFilePtr, "\nbuff2 flash_addr %08X\n", flash_start_address);
		}
		else if (last_record)
		{
			fprintf(buffFilePtr, "\nlast record flash_addr %08X\n", flash_start_address);
		}
#endif
		//printBuffer(64, length, ptr_to_flash_buff, buffFilePtr); // 64 srec data length

#if 1
	    // Disable interrupts
	    //__disable_irq();
	    //Erase application area
	    err = g_flash0.p_api->erase(g_flash0.p_ctrl, flash_start_address, 1);
	    if (SSP_SUCCESS != err)
	    {
	    	rec_status = PRC_REC_ERASE_FAIL;
	    }
	    else
	    {
			// Blank check the application area
			err = g_flash0.p_api->blankCheck(g_flash0.p_ctrl, flash_start_address, FLASH_BLOCK_SIZE, &result);
			if ((SSP_SUCCESS != err) || (FLASH_RESULT_BLANK != result))
			{
				rec_status = PRC_REC_BLANK_FAIL;
			}
			else
			{
				// Set destination address
				err = g_flash0.p_api->write(g_flash0.p_ctrl, (uint32_t)ptr_to_flash_buff, flash_start_address, FLASH_BLOCK_SIZE);
				if (SSP_SUCCESS != err)
				{
					rec_status = PRC_REC_FLASH_FAIL;
				}
				//else
				//{
					//err = g_flash0.p_api->read(g_flash0.p_ctrl, testBuffer, flash_start_address, FLASH_BLOCK_SIZE);
				//}
			}
	    }
	    // Enable interrupts
	    //__enable_irq();
#endif
		if (last_record)
		{

			if (calculated_crc != getExpectedCRC())
			{
				rec_status = PRC_REC_IMG_CRC_ERR;
			}
			else
			{
				//printf("\ncrc good\n");
				rec_status = PRC_REC_IMG_SUCCESS;
			}
		}
		memset(ptr_to_flash_buff, 0, FLASH_BLOCK_SIZE); // clear our buffer
	}

	if (last_record)
	{
		if (buff1_status == BUFF_CURR)
		{
			buff1_status = BUFF_EMPTY;
		}
		else if (buff2_status == BUFF_CURR)
		{
			buff2_status = BUFF_EMPTY;
		}
	}
	else
	{
		if (buff1_status == BUFF_FULL)
		{
			buff1_status = BUFF_EMPTY;
		}
		else if (buff2_status == BUFF_FULL)
		{
			buff2_status = BUFF_EMPTY;
		}
	}
	return(rec_status);
}

BOOL process_payload_buffer(unsigned char* data_buff_ptr, uint32_t dest_addr, uint32_t num_bytes, record_status_t record_status)
{
	BOOL last_record = FALSE;
	uint32_t flash_len;

	if (record_status == PRC_REC_LAST_REC)
		last_record = TRUE;

	if ((record_status == PRC_REC_RCVD_OK) || (record_status == PRC_REC_LAST_REC))
	{
		if (num_bytes)
		{
			if (record_status != PRC_REC_LAST_REC)
			{
				flash_len = create_flash_buffer(data_buff_ptr, dest_addr, num_bytes);
			}
			else
			{
				flash_len = create_flash_buffer(data_buff_ptr, dest_addr, 0); // last record does not have data
			}
			// flash buffer here
			record_status = write_to_flash(flash_len, last_record);

			// flash header here after we are sure image is OK
			if (record_status == PRC_REC_IMG_SUCCESS)
			{
				record_status_t record_status;

				// write S0 header info to data flash
				record_status = write_header_to_flash(BL_ST_FW_IN_FLASH);
				// see if successfull
				if (record_status == PRC_REC_RCVD_OK)
				{
					record_status = PRC_REC_IMG_SUCCESS;
				}
			}
		}
	}

	if (header_ptr->addr != BROADCAST_ADDR) // dont send acks/nacks if its a broadcast message
	{
		// send ACK / NACK
		switch (record_status)
		{
		    case PRC_REC_FIRST_REC:
			case PRC_REC_RCVD_OK:
				//printf("send ack");
				send_ack(PH_BOOTLOAD, SH_BOOTLOAD_IMAGE);
				break;
			case PRC_REC_NUM_SREC:
				//printf("not all srec received");
				send_ack(PH_BOOTLOAD, SH_BOOTLOAD_SREC_ERR);
				break;
			case PRC_REC_SREC_CKSUM_ERR:
				//printf("payload checksum error");
				send_ack(PH_BOOTLOAD, SH_BOOTLOAD_CKSUM_ERR);
				break;
			case PRC_REC_SREC_ORDER_ERR:
				//printf("payload records not in order");
				send_ack(PH_BOOTLOAD, SH_BOOTLOAD_ORDER_ERR);
				break;
			case PRC_REC_IMG_CRC_ERR:
				//printf("image failure crc incorrect");
				send_ack(PH_BOOTLOAD, SH_BOOTLOAD_CRC_ERR);
				last_record = TRUE;
				break;
			case PRC_REC_ERASE_FAIL:
				//printf("failure to erase block of memory");
				send_ack(PH_BOOTLOAD, SH_BOOTLOAD_ERASE_ERR);
				break;
			case PRC_REC_BLANK_FAIL:
				//printf("blank failure");
				send_ack(PH_BOOTLOAD, SH_BOOTLOAD_BLANK_ERR);
				break;
			case PRC_REC_FLASH_FAIL:
				//printf("flash failure");
				send_ack(PH_BOOTLOAD, SH_BOOTLOAD_FLASH_ERR);
				break;
			case PRC_REC_SREC_LEN_ERR:
				//printf("srec length error");
				send_ack(PH_BOOTLOAD, SH_BOOTLOAD_MSREC_ERR);
				break;
			case PRC_REC_FRAME_CKSUM_ERR:
				//printf("frame checksum error");
				send_ack(PH_BOOTLOAD, SH_BOOTLOAD_FRMCKSUM_ERR);
				break;
			case PRC_REC_IMG_SUCCESS:
				//printf("last record received image flashed");
				send_ack(PH_BOOTLOAD, SH_BOOTLOAD_IMAGE_LAST);
				last_record = TRUE;
				break;
			default:
				break;
		}
	}
	return(last_record);
}
void process_frame(void)
{
	uint32_t cnt = 0;
	static record_status_t record_status = FALSE;
	unsigned char new_data;
	BOOL last = FALSE;
	BOOL exit = FALSE;
	uint32_t num_bytes_to_flash;
	uint32_t dest_address;

	// wait for start of frame
	rs485_set_rw_direction(RS485_READ);
	while (exit == FALSE)
	{
		set_calc_chksum_frame();
		new_data = rs485_rx_read_byte();
		switch (new_data)
		{
		case SOF:
		{
			cnt = 0;

			// start the receive timer
			refresh_timer();

			// read in the header
			while (cnt < sizeof(frame_header_t))
			{
				*frame_ptr = rs485_rx_read_byte();
				calc_chk_sum_frame(*frame_ptr);
				frame_ptr++;
				cnt++;
			}

			// stop the receive timer
			stop_timer();

			header_ptr = (frame_header_t*)&frame_header_buff[0]; // cast to header structure
			// process header
			if ((header_ptr->addr == firmware_info.app.mdle_info.cur_address) ||
				(header_ptr->addr == firmware_info.btldr.device_id) ||
				(header_ptr->addr == BROADCAST_ADDR))
			{
				// see if this message is for the bootloader application
				if (PH_BOOTLOAD == header_ptr->ph)
				{
					switch (header_ptr->sh)
					{
					case SH_BOOTLOAD_INFO:
						// get info about application
                        // read in firmware info
                        init_firmware_info();
						// send back to SC
						send_version_info_to_controller();
						break;
					case SH_BOOTLOAD_NVT:
                        // read in firmware info
                        init_firmware_info();
                        // send back to SC
					    io_command_get_nvt_table_handler();
					    break;
					case SH_BOOTLOAD_RD_DATA:
						// read memory
						// send back to SC
						break;
					case SH_BOOTLOAD_ER_DATA:
						// erase memory
						// send ack/nack
						break;
					case SH_BOOTLOAD_QUIT:
						// quit message
						// check CRC in flash
						if (valid_app_image() == TRUE)
						{
							// image OK send ACK message
							send_ack(PH_BOOTLOAD, SH_BOOTLOAD_QUIT);
							exit = TRUE;
						}
						else
						{
						    // read in firmware info - overwrites data read during upgrade
						    init_firmware_info();
							// image bad send error message
							send_ack(PH_BOOTLOAD, SH_BOOTLOAD_QUIT_NAK);
						}
						// wait here till all the message is transmitted
						R_BSP_SoftwareDelay(TX_WRITE_DELAY, BSP_DELAY_UNITS_MILLISECONDS);

						break;
					case SH_BOOTLOAD_IMAGE:
					{
						//uint16_t upper_cs;

						refresh_timer();

	                    record_status = process_image_record(&data_buff[0], &dest_address, &num_bytes_to_flash, header_ptr->len); // read in payload to data_buff

						last = process_payload_buffer(&data_buff[0], dest_address, num_bytes_to_flash, record_status); // copy payload buffer to larger buffer then flash to device

						// stop the receive timer
						stop_timer();

					}
						break;
					} // end switch ph
				}
			}

			frame_ptr = &frame_header_buff[0];
		}
			break;
		default: // ignore the data if we have not synced with controller
			break;
		}
	}
}
