//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  rs485_driver.c
//
//  Description: The RS485 I/O driver.
//               This driver is built on top of Renesas Synergy SSP's UART
//               Framework. It can be incorporated into a program built
//               with the SSP thread operating system ThreadX.
//
//               The RS485 UART is half duplex. The board upon powering up,
//               set the read enable as !RE and send enable as DE.
//
//               A circular buffer is maintained by this driver.
//-----------------------------------------------------------------------------

#include "jellyfish_common.h"
#include "hal_data.h"
#include "rs485_driver.h"
// #include "set_led_sw.h"

#define RX485_TX_WRITE_DELAY      4 //5 //200    // unit: milliseconds


//-----------------------------------------------------------------
// Local data definition
//-----------------------------------------------------------------

RS485_RX_BUFFER_T    rx_buffer;

static BOOL          tx_complete_flag = TRUE;

//-----------------------------------------------------------------
// Local Function Declarations
//-----------------------------------------------------------------

static BOOL             _rx_buffer_full(void);
static unsigned short   _inc_rx_buffer_index(unsigned short index);
static void             _rx_buffer_reset(void);



//------------------------------------------------------------------------------
// Name:                rs485_set_rw_direction
// Abstract:            This API function changes the UART direction to READ
//                      or WRITE as desired.
// Arguments:
//     RS485_DIRECTION: This parameter tells to set the RS485 for read or write.
// Return value(s):     None.
//------------------------------------------------------------------------------
void rs485_set_rw_direction(RS485_DIRECTION rs485_direction)
{

    if (RS485_READ == rs485_direction)
    {
         // Set the 485 port for reading
         g_ioport.p_api->pinWrite(RS485_RX_ENABLE_PIN, RS485_RX_ENABLE);
         g_ioport.p_api->pinWrite(RS485_TX_ENABLE_PIN, RS485_TX_DISABLE);
    }
    else
    {
         // Set the 485 port for writing
         g_ioport.p_api->pinWrite(RS485_RX_ENABLE_PIN, RS485_RX_DISABLE);
         g_ioport.p_api->pinWrite(RS485_TX_ENABLE_PIN, RS485_TX_ENABLE);
    }
}


//------------------------------------------------------------------------------
// Name:                rs485_disable_rw
// Abstract:            This API function disable both read and write on the
//                      RS485 bus.
// Arguments:           (none)
// Return value(s):     (none)
//------------------------------------------------------------------------------
void rs485_disable_rw(void)
{

    // Disable RS485 Writing
    g_ioport.p_api->pinWrite(RS485_TX_ENABLE_PIN, RS485_TX_DISABLE);

    // Disable RS485 Reading
    g_ioport.p_api->pinWrite(RS485_RX_ENABLE_PIN, RS485_RX_DISABLE);

}


//------------------------------------------------------------------------------
// Name:                _rx_buffer_reset
// Abstract:            This function reset the RS485 reset buffer and clears the
//                      associated status.
//                      The read_idx and write_idx always points to the next slot
//                      in the buffer to read from/write to.
// Arguments:           None
// Return value(s):     None.
//------------------------------------------------------------------------------
static void _rx_buffer_reset(void)
{

    rx_buffer.read_idx = 0;
    rx_buffer.write_idx = 0;
    rx_buffer.status = RS485_RX_BUFFER_ST_OK;

}


//------------------------------------------------------------------------------
// Name:                rs485_initialize
// Abstract:            This is API function which initializes the UART port
//                      for RS485 communication.
//                      This initialization also set the two indexes to initial
//                      value. Hence this function can be called to clear out
//                      and overflow/underflow problem.
// Arguments:           None
// Return value(s):     None.
//------------------------------------------------------------------------------
void rs485_initialize(void)
{

    // Initialize the buffer
    _rx_buffer_reset();
    tx_complete_flag = TRUE;

    // Set the RS485 port for reading at initialization
    rs485_set_rw_direction(RS485_READ);

    // open and initialize the serial port
    g_uart0.p_api->open(g_uart0.p_ctrl, g_uart0.p_cfg);

    // Make a call to the .read (non-blocking) driver to enable event
    // forwarding. Data on the UART port will be sent to the callback
    // functions by the UART driver.
    g_uart0.p_api->read(g_uart0.p_ctrl, NULL, 0);

}


//------------------------------------------------------------------------------
// Name:                user_uart_callback
//
// Abstract:            This is UART interrupt handler. It process the UART
//                      interrupt (i.e., event) and put data into or take data
//                      out of the RS485 circular buffer.
//                      This function's prototype is automatically generated by
//                      SSP project generator.
//
//                      Note, since the processing of both read/write a very
//                      simple and short, the code are reside in the callback
//                      event parsing body. If they are big, ideally they should
//                      in a separate function.
// Arguments:
//     uart_callback_args_t *: pointer to the data the UART event generator
//                             puts data in when calls the callback.
// Return value(s):     None.
//------------------------------------------------------------------------------
void user_uart_callback(uart_callback_args_t *p_args)
{

    uint32_t g_arg_event = p_args->event;

    // RS485 write complete
    if (UART_EVENT_TX_COMPLETE & g_arg_event)
    {
        tx_complete_flag = TRUE;
        return;
    }

    // RS485 reads in a byte, put in the buffer
    if (UART_EVENT_RX_CHAR & g_arg_event)
    {
        if (_rx_buffer_full() != TRUE)
        {
            rx_buffer.data[rx_buffer.write_idx] = (unsigned char)(p_args->data);
            rx_buffer.write_idx = _inc_rx_buffer_index(rx_buffer.write_idx);
        }
        else
        {
            rx_buffer.status = RS485_RX_BUFFER_ST_FULL;
        }
        return;
    }

    if ((UART_EVENT_RX_COMPLETE & g_arg_event) ||
        (UART_EVENT_BREAK_DETECT & g_arg_event))
    {
        return;
    }

    if ((UART_EVENT_ERR_PARITY   & g_arg_event) ||
        (UART_EVENT_ERR_FRAMING  & g_arg_event) ||
        (UART_EVENT_ERR_PARITY   & g_arg_event) ||
        (UART_EVENT_ERR_FRAMING  & g_arg_event) ||
        (UART_EVENT_BREAK_DETECT & g_arg_event) ||
        (UART_EVENT_ERR_OVERFLOW & g_arg_event) ||
        (UART_EVENT_ERR_RXBUF_OVERFLOW & g_arg_event))
    {
         rx_buffer.status &= RS485_RX_BUFFER_ST_ERR;
    }

}


//------------------------------------------------------------------------------
// Name:                rs485_rx_read_byte
//
// Abstract:            This API function returns 1 byte from the RS485 RX buffer,
//                      if there is data available.
//                      Since function will not check the status of the buffer,
//                      the caller must proceed this call with a call to the other
//                      API function rs485_rx_data_available() to determine
//                      if there is data in the read buffer.
//                      The actual moving of UART input data is done by the
//                      UART event handler.
// Arguments:           None.
// Return value(s):
//     unsigned char:   1st byte in the buffer pointed by the read index.
//------------------------------------------------------------------------------
#if 1
unsigned char rs485_rx_read_byte(void)
{
    unsigned char data_byte;

    // wait here until data is available
    while (rx_buffer.read_idx == rx_buffer.write_idx)
    {

    }
    // read a single byte
    if (rx_buffer.read_idx != rx_buffer.write_idx)
    {

		data_byte = rx_buffer.data[rx_buffer.read_idx];

		// Move the read index
		rx_buffer.read_idx = _inc_rx_buffer_index(rx_buffer.read_idx);
    }
    return data_byte;
}
#else
unsigned char rs485_rx_read_byte(void)
{
    unsigned char data_byte;

    data_byte = rx_buffer.data[rx_buffer.read_idx];

    // Move the read index
    rx_buffer.read_idx = _inc_rx_buffer_index(rx_buffer.read_idx);

    return data_byte;
}
#endif

//------------------------------------------------------------------------------
// Name:                rs485_tx_write_nbyte
//
// Abstract:            This API function returns actually send the data given
//                      by the calling function to the RS485 UART port.
//                      This is blocking call since the function will wait
//                      until all bytes are transmitted.
//                      In contrast to the read , RS485 driver does not maintain
//                      a write buffer.
// Arguments:
//     const unsigned char *:       pointer to the starting location of the bytes
//                                  to transmit
//     const unsgined int length:   number of byte to transmit
// Return value(s):     None.
//------------------------------------------------------------------------------
void rs485_tx_write_nbyte(const char *ptr, const unsigned short length)
{

    tx_complete_flag = FALSE;

    // set the RS485 port for writing
    rs485_set_rw_direction(RS485_WRITE);

    R_BSP_SoftwareDelay(1, BSP_DELAY_UNITS_MILLISECONDS);
    // turn on transmission LED
    // turn_on_send_led();

    // Send out the greeting message
    g_uart0.p_api->write(g_uart0.p_ctrl, (uint8_t *)ptr, length);

    // wait for the writing to complete
    while (tx_complete_flag != TRUE)
    {
        R_BSP_SoftwareDelay(RX485_TX_WRITE_DELAY, BSP_DELAY_UNITS_MILLISECONDS);
//      R_BSP_SoftwareDelay(100, BSP_DELAY_UNITS_MILLISECONDS);
    }

    // turn off transmission LED
    // turn_off_send_led();

    // This delay is critical so that the port is not
    // turn to listen to the bus immediately. Otherwise,
    // the message will be bounce back and forth endlessly.
    // R_BSP_SoftwareDelay(RX485_TX_WRITE_DELAY*3, BSP_DELAY_UNITS_MILLISECONDS);

    // set the RS485 port back to reading
    rs485_set_rw_direction(RS485_READ);

}


//------------------------------------------------------------------------------
// Name:                _inc_rx_buffer_index
//
// Abstract:            This function increase the read or write index to the
//                      RS485 RX buffer. If it reaches the end of the flat data
//                      array, it loops back so the buffer is circular.
// Arguments:
//     unsigned short index: the buffer index to be incremented
// Return value(s):
//     unsigned short: the result index after the increment.
//------------------------------------------------------------------------------
static unsigned short _inc_rx_buffer_index(unsigned short index)
{
    if (++index < RS485_RX_BUFFER_SIZE)
    {
        return index;
    }
    else
    {
        return 0;
    }
}


//------------------------------------------------------------------------------
// Name:                _rx_buffer_full
//
// Abstract:            This function determines if the RS485 RX buffer is
//                      full. Here the writing index is intentionally kept
//                      a one slot away from the reading index, to avoid
//                      the ambiguity of read_idx == write_idx situation
//                      (can't tell full or empty).
// Arguments:
// Return value(s):     None.
//------------------------------------------------------------------------------
static BOOL _rx_buffer_full(void)
{
    if((rx_buffer.write_idx==RS485_RX_BUFFER_SIZE-1) &&
       (rx_buffer.read_idx==0))
    {
        return TRUE;
    }
    else
    {
        if (rx_buffer.write_idx == rx_buffer.read_idx - 1)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}


//------------------------------------------------------------------------------
// Name:                rs485_rx_data_available
// Abstract:            This API function returns TRUE if there are data in the
//                      buffer and there is
// Arguments:
// Return value(s):     None.
//------------------------------------------------------------------------------
BOOL rs485_rx_data_available(void)
{

    if (rx_buffer.read_idx != rx_buffer.write_idx)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }

}

