/*
 * =====================================================================================
 *
 *       Filename:  rhaframe.c
 *
 *    Description:  serial frame construction and dispatcher functions
 *
 *        Version:  1.0
 *        Created:  08/26/2015 09:37:24 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Liang Huang (software developer), lhuang@intermatic.com
 *   Organization:  Intermatic Inc.
 *
 * =====================================================================================
 */
//#include "hal_data.h"
//#include "jellyfish_common.h"
//#include "control_thread_entry.h"

//#include <tx_api.h>
//#include "rs485_driver.h"
//#include "common_def.h"
//#include "db_api.h"
//#include "sc_command.h"
//#include "rs485_driver.h"


//#include <stddef.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <unistd.h>  //Sleep
//#include <fcntl.h>
//#include <errno.h>
//#include <int_prot_cmd.h>
#include "int_prot_frame.h"
//#include <inttypes.h>
//#include <stdbool.h>

//void module_version_handler(rha_frame_t *frame);
//void manufacturer_ID_handler(rha_frame_t *frame);
//void serial_ack_config_response_handler(rha_frame_t *frame);
//void startup_sync_request_handler(rha_frame_t *frame);
//void io_command_getdo_handler(rha_frame_t *frame);
#include "payload_processing.h"

extern sw_info_struct firmware_info;

rha_frame_t *rcv_frame = NULL;
rha_frame_t *snd_frame = NULL;
uint8_t running_state = 0x00;
uint8_t config_state = 0x00;
uint8_t rcv_buffer[sizeof(rha_frame_t)];
uint8_t rcv_payload[MAX_PAYLOAD_LENGTH];
uint8_t snd_buffer[200];

uint8_t err_firmware_version = 0;
uint8_t seq = 0;
uint8_t my_addr;
uint8_t config_ack = 0;


uint8_t * construct_rha_frame(uint8_t address,uint8_t primary_header, uint8_t secondary_header, uint8_t *payload, uint8_t payload_length)
{
	uint8_t i =0;
	uint8_t * buf = NULL;
	buf = snd_buffer;
	if (buf == NULL) return NULL;
	uint8_t * buf_idx1 = buf;
	uint8_t * buf_idx2 = buf;
	uint16_t cs;  //checksum

	if (seq == 0x80) seq =0;
	else seq++;
	*buf_idx1 = SOF;
	*++buf_idx1 = address;
	*++buf_idx1 = primary_header;
	*++buf_idx1 = secondary_header;
	*++buf_idx1 = seq;
	*++buf_idx1 = payload_length;
	for (i=0; i<payload_length;i++){*++buf_idx1 = *payload++;}
	cs = calc_fcs(++buf_idx2,(uint8_t)(payload_length+5));
	*++buf_idx1 = (uint8_t) (cs & 0xff);
	*++buf_idx1 = (uint8_t) ((cs >> 8) & 0xff);
	return buf;
}

#if 0

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  construct_rha_payload
 *  Description:  build the payload for snd_frame, returns payload pointer
 * =====================================================================================
 */
uint8_t * construct_rha_payload(uint8_t payload_length) {
	uint8_t * payload = NULL;
	if (!payload_length)
		payload = calloc(payload_length, 1);
	return payload;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  network_discover_node_handler
 *  Description:  network discover command handler function.
 * =====================================================================================
 */
void network_discover_node_handler(rha_frame_t *frame) {

	if(frame->addr != DEFAULT_ADDR){
		return;
	}

	//wait random delay of n*20ms,(max 127byte, @115200bps, ~16ms to transmit)
	int r = rand() % 10;		//say max 20 modules unassigned per rs458 bus, r is 0-24;
    R_BSP_SoftwareDelay((uint32_t)(20 + r*400), BSP_DELAY_UNITS_MILLISECONDS);	// 20ms steps, from 20ms~400ms delays

	//R_BSP_SoftwareDelay(10, BSP_DELAY_UNITS_MILLISECONDS);	// 20ms delay;

    //construct a register message back to master;
	uint8_t payload[127];
	uint8_t payload_length = sizeof(module_info);
	uint8_t *buffer;

	memcpy(payload,&module_info,sizeof(module_info));

	buffer = construct_rha_frame(MASTER_ADDR, PH_NETWORK_COMISSIONING, SH_NETWORK_COMISSIONING_REGISTER_NODE, payload, payload_length);
	rs485_tx_write_nbyte((char *)buffer, (unsigned short)(payload_length+8));
	memset(buffer,0,sizeof(snd_buffer));

	return;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  network_assign_node_handler
 *  Description:  network assign command handler function.
 * =====================================================================================
 */
void network_assign_node_handler(rha_frame_t *frame) {

	//printf("network_assign_node_handler...!!!");

	if(frame->addr != DEFAULT_ADDR){
		return;
	}

    //R_BSP_SoftwareDelay(20, BSP_DELAY_UNITS_MILLISECONDS);	// 20ms delay;

	//check uid ?= msg_uid, if yes, continue;

	//.....module_info.cur_address=frame->payload[0]

	//confirm the address by sending back module_info again with assigned address;

    //construct a register message back to master;
	uint8_t payload[127];
	uint8_t payload_length = sizeof(module_info);
	uint8_t *buffer;

	memcpy(module_info.uid,frame->payload,UID_LENGTH);
	module_info.cur_address=frame->payload[16];
	module_info.type=(enum module_types)frame->payload[17];
	module_info.state=(enum state_codes)frame->payload[18];

	if(module_info.state!=assign) return;

	module_info.state=confirm;

	R_BSP_SoftwareDelay(20, BSP_DELAY_UNITS_MILLISECONDS);	// 20ms delay; SC rs485 driver has a bug, will need to change SC 485 driver

	memcpy(payload,&module_info,sizeof(module_info));
	buffer = construct_rha_frame(MASTER_ADDR, PH_NETWORK_COMISSIONING, SH_NETWORK_COMISSIONING_CONFIRM_NODE, payload, payload_length);
	rs485_tx_write_nbyte((char *)buffer, (unsigned short)(payload_length+8));
	memset(buffer,0,sizeof(snd_buffer));

	return;
}


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  io_command_do_handler
 *  Description:  io command DO handler function.
 * =====================================================================================
 */
void io_command_do_handler(rha_frame_t *frame) {
	int channel = frame->payload[0] +1; //channel starts from 1,2,3,4
	int value = frame->payload[1];

	STATUS_E status = SUCCESS;

    // Initialize the "SET" command structure and fill the header
	sc_cmd_t *cmd_p = sc_cmd_init_set_cmd();

   	// Add instruction to operate Relay
    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_RELAY,
        		                               (unsigned char)channel,
											   (unsigned char)value))
        		!= SUCCESS)
        {
         	return;
        }

    if (cmd_p != NULL)
    {
	    if (control_send_msg_to_controller(cmd_p) == SUCCESS)	//send msgQ message to controller thread;
        {
	    		if (config_ack){
	    			R_BSP_SoftwareDelay(10, BSP_DELAY_UNITS_MILLISECONDS);	// 20ms delay;
	    			send_ack(PH_IO_COMMAND, SH_IO_COMMAND_ACK);
	    			}
        }
	    return;
	}
	return;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  io_command_ao_handler
 *  Description:  io command AO handler function.
 * =====================================================================================
 */
void io_command_ao_handler(rha_frame_t *frame) {
	int channel = frame->payload[0] +1; //channel starts from 1,2,3,4
	int value = frame->payload[1];

	STATUS_E status = SUCCESS;

    // Initialize the "SET" command structure and fill the header
	sc_cmd_t *cmd_p = sc_cmd_init_set_cmd();

   	// Add instruction to operate Relay
    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_ANALOG_PWM,
        		                               (unsigned char)channel,
											   (unsigned char)value))
        		!= SUCCESS)
        {
         	return;
        }

    if (cmd_p != NULL)
    {
	    if (control_send_msg_to_controller(cmd_p) == SUCCESS)	//send msgQ message to controller thread;
        {
    		if (config_ack){
    			R_BSP_SoftwareDelay(10, BSP_DELAY_UNITS_MILLISECONDS);	// 20ms delay;
    			send_ack(PH_IO_COMMAND, SH_IO_COMMAND_ACK);
    			}
        }
	    return;
	}
	return;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  io_command_led_handler
 *  Description:  io command LED handler function.
 * =====================================================================================
 */
void io_command_led_handler(rha_frame_t *frame) {
	int channel = frame->payload[0] +1; //channel starts from 1,2,3,4
	int value = frame->payload[1];

	STATUS_E status = SUCCESS;

    // Initialize the "SET" command structure and fill the header
	sc_cmd_t *cmd_p = sc_cmd_init_set_cmd();

   	// Add instruction to operate Relay
    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_LED,
        		                               (unsigned char)channel,
											   (unsigned char)value))
        		!= SUCCESS)
        {
         	return;
        }

    if (cmd_p != NULL)
    {
	    if (control_send_msg_to_controller(cmd_p) == SUCCESS)	//send msgQ message to controller thread;
        {
    		if (config_ack){
    			R_BSP_SoftwareDelay(10, BSP_DELAY_UNITS_MILLISECONDS);	// 20ms delay;
    			send_ack(PH_IO_COMMAND, SH_IO_COMMAND_ACK);
    			}
        }
	    return;
	}
	return;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  io_command_zc_handler
 *  Description:  io command ZeroCross handler function.
 * =====================================================================================
 */
void io_command_zc_handler(rha_frame_t *frame) {
	int channel = frame->payload[0] +1; //channel starts from 1,2,3,4
	int value = frame->payload[1];

	STATUS_E status = SUCCESS;

    // Initialize the "SET" command structure and fill the header
	sc_cmd_t *cmd_p = sc_cmd_init_set_cmd();

    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_ZC_DISABLE,
        		                               (unsigned char)channel,
											   (unsigned char)value))
        		!= SUCCESS)
        {
         	return;
        }

    if (cmd_p != NULL)
    {
	    if (control_send_msg_to_controller(cmd_p) == SUCCESS)	//send msgQ message to controller thread;
        {
    		if (config_ack){
    			R_BSP_SoftwareDelay(10, BSP_DELAY_UNITS_MILLISECONDS);	// 20ms delay;
    			send_ack(PH_IO_COMMAND, SH_IO_COMMAND_ACK);
    			}
        }
	    return;
	}
    //return an ERROR message;
	return;
}

/* ===  FUNCTION  ======================================================================
 *         Name:  io_command_seteol_handler
 *  Description:  io command set EOL handler function.
 * =====================================================================================
 */
void io_command_seteol_handler(rha_frame_t *frame) {
	int channel = frame->payload[0] +1; //channel starts from 1,2,3,4
	int value = frame->payload[1];

	STATUS_E status = SUCCESS;

    // Initialize the "SET" command structure and fill the header
	sc_cmd_t *cmd_p = sc_cmd_init_set_cmd();

    if ((status = sc_cmd_add_set_cmd_instr((unsigned char)SC_CMD_HW_RS485_EOL,
        		                               (unsigned char)channel,
											   (unsigned char)value))
        		!= SUCCESS)
        {
         	return;
        }

    if (cmd_p != NULL)
    {
	    if (control_send_msg_to_controller(cmd_p) == SUCCESS)	//send msgQ message to controller thread;
        {
    		if (config_ack){
    			R_BSP_SoftwareDelay(10, BSP_DELAY_UNITS_MILLISECONDS);	// 20ms delay;
    			send_ack(PH_IO_COMMAND, SH_IO_COMMAND_ACK);
    			}
        }
	    return;
	}
	return;
}

/* ===  FUNCTION  ======================================================================
*         Name:  io_command_geteol_handler
*  Description:  io command get EOL state handler function.
* =====================================================================================
*/
void io_command_geteol_handler(rha_frame_t *frame) {

	BOOL eol_status;
	_PARAMETER_NOT_USED_(frame);
	// Call DB API here
	if (db_vt_get_rs485_bus_eol(&eol_status) != SUCCESS)
	{
		return;		//NAK???
	}
    //construct a response message back to master;
	uint8_t payload[127];
	uint8_t payload_length = sizeof(eol_status);
	uint8_t *buffer;

	memcpy(payload,&eol_status,sizeof(eol_status));
	R_BSP_SoftwareDelay(10, BSP_DELAY_UNITS_MILLISECONDS);	// 20ms delay;

	buffer = construct_rha_frame(MASTER_ADDR, PH_IO_COMMAND, SH_IO_COMMAND_EOL_RESPONSE, payload, payload_length);
	rs485_tx_write_nbyte((char *)buffer, (unsigned short)(payload_length+8));
	memset(buffer,0,sizeof(snd_buffer));

	return;
}

/* ===  FUNCTION  ======================================================================
*         Name:  io_command_get_vt_table_handler
*  Description:  io command get volatile table handler function.
* =====================================================================================
*/
void io_command_get_vt_table_handler(rha_frame_t *frame) {

	doao_module_db_v_t db_v_tbl;
	_PARAMETER_NOT_USED_(frame);
	// Call DB API here
	if (db_vt_get_table(&db_v_tbl) != SUCCESS)
	{
		return;		//NAK???
	}
    //construct a response message back to master;
	uint8_t payload[127];
	uint8_t payload_length = sizeof(db_v_tbl);
	uint8_t *buffer;

	memcpy(payload,&db_v_tbl,sizeof(db_v_tbl));
	R_BSP_SoftwareDelay(10, BSP_DELAY_UNITS_MILLISECONDS);	// 20ms delay;

	buffer = construct_rha_frame(MASTER_ADDR, PH_IO_COMMAND, SH_IO_COMMAND_VT_RESPONSE, payload, payload_length);
	rs485_tx_write_nbyte((char *)buffer, (unsigned short)(payload_length+8));
	memset(buffer,0,sizeof(snd_buffer));

	return;
}

//io_command_get_nvt_table_handler
/* ===  FUNCTION  ======================================================================
*         Name:  io_command_get_nvt_table_handler
*  Description:  io command get non-volatile table handler function.
* =====================================================================================
*/
void io_command_get_nvt_table_handler(rha_frame_t *frame) {

	doao_module_db_nv_t db_nv_tbl;
	_PARAMETER_NOT_USED_(frame);
	// Call DB API here
	if (db_nvt_get_table(&db_nv_tbl) != SUCCESS)
	{
		return;		//NAK???
	}
    //construct a response message back to master;
	uint8_t payload[127];
	uint8_t payload_length = sizeof(db_nv_tbl);
	uint8_t *buffer;

	memcpy(payload,&db_nv_tbl,sizeof(db_nv_tbl));
	R_BSP_SoftwareDelay(10, BSP_DELAY_UNITS_MILLISECONDS);	// 20ms delay;

	buffer = construct_rha_frame(MASTER_ADDR, PH_IO_COMMAND, SH_IO_COMMAND_NVT_RESPONSE, payload, payload_length);
	rs485_tx_write_nbyte((char *)buffer, (unsigned short)(payload_length+8));
	memset(buffer,0,sizeof(snd_buffer));

	return;
}

/* ===  FUNCTION  ======================================================================
*         Name:  io_command_getdo_handler
*  Description:  io command get DO/Relay state handler function.
* =====================================================================================
*/
void io_command_getdo_handler(rha_frame_t *frame) {
	int channel = frame->payload[0] +1; //channel starts from 1,2,3,4
	SWITCH_STATE_E state;

    // Call DB API here
    if (db_vt_get_relay_state(channel, &state) != SUCCESS)
    {
    	return; //NAK???
    }
    //construct a response message back to master;
	uint8_t payload[127];
	uint8_t payload_length = sizeof(state);
	uint8_t *buffer;

	memcpy(payload,&state,sizeof(state));
	R_BSP_SoftwareDelay(10, BSP_DELAY_UNITS_MILLISECONDS);	// 20ms delay;

	buffer = construct_rha_frame(MASTER_ADDR, PH_IO_COMMAND, SH_IO_COMMAND_DO_RESPONSE, payload, payload_length);
	rs485_tx_write_nbyte((char *)buffer, (unsigned short)(payload_length+8));
	memset(buffer,0,sizeof(snd_buffer));

	return;
}

/* ===  FUNCTION  ======================================================================
*         Name:  io_command_adcao_handler
*  Description:  io command get ADC AO/PMW monitor voltage handler function.
* =====================================================================================
*/
void io_command_adcao_handler(rha_frame_t *frame) {
	int channel = frame->payload[0] +1; //channel starts from 1,2,3,4
	unsigned ao_adc_count;

    // Call DB API here
    if (db_vt_get_output_monitor((RELAY_ID_E)channel, &ao_adc_count)
	    		!= SUCCESS)
    {
    	return;
    }

    //construct a response message back to master;
	uint8_t payload[127];
	uint8_t payload_length = sizeof(ao_adc_count);
	uint8_t *buffer;

	memcpy(payload,&ao_adc_count,sizeof(ao_adc_count));
	R_BSP_SoftwareDelay(10, BSP_DELAY_UNITS_MILLISECONDS);	// 20ms delay;

	buffer = construct_rha_frame(MASTER_ADDR, PH_IO_COMMAND, SH_IO_COMMAND_ADCAO_RESPONSE, payload, payload_length);
	rs485_tx_write_nbyte((char *)buffer, (unsigned short)(payload_length+8));
	memset(buffer,0,sizeof(snd_buffer));

	return;
}

/* ===  FUNCTION  ======================================================================
*         Name:  io_command_adc24v_handler
*  Description:  io command get ADC 24V monitor voltage handler function.
* =====================================================================================
*/
void io_command_adc24v_handler(rha_frame_t *frame) {
	unsigned v24_input;

	_PARAMETER_NOT_USED_(frame);

	// Call DB API here
	if (db_vt_get_v24_input_monitor(&v24_input) != SUCCESS)
	{
		return;
	}

    //construct a response message back to master;
	uint8_t payload[127];
	uint8_t payload_length = sizeof(v24_input);
	uint8_t *buffer;

	memcpy(payload,&v24_input,sizeof(v24_input));
	R_BSP_SoftwareDelay(10, BSP_DELAY_UNITS_MILLISECONDS);	// 20ms delay;

	buffer = construct_rha_frame(MASTER_ADDR, PH_IO_COMMAND, SH_IO_COMMAND_ADC24V_RESPONSE, payload, payload_length);
	rs485_tx_write_nbyte((char *)buffer, (unsigned short)(payload_length+8));
	memset(buffer,0,sizeof(snd_buffer));

	return;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  io_command_di_handler
 *  Description:  io command DI handler function.
 * =====================================================================================
 */
void io_command_di_handler(rha_frame_t *frame) {
	//int channel = frame->payload[0] +1; //channel starts from 1,2,3,4
	//int value = frame->payload[1];
	_PARAMETER_NOT_USED_(frame);
	return;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  io_command_ai_handler
 *  Description:  io command AI handler function.
 * =====================================================================================
 */
void io_command_ai_handler(rha_frame_t *frame) {
	//int channel = frame->payload[0] +1; //channel starts from 1,2,3,4
	//int value = frame->payload[1];
	_PARAMETER_NOT_USED_(frame);
	return;
}


/*
 * ===  FUNCTION  ======================================================================
 *         Name:  bootloader_version_handler
 *  Description:  RHA firmware version handler function.
 * =====================================================================================
 */
void bootloader_version_handler(rha_frame_t *frame) {

	int i=0;

	for(i=0;i<8;i++){
		if (i==0)
			printf("Ember version: v");
		if (i==4)
			printf("\nMMB version: v");

		printf("%u.", frame->payload[i]);
	}

}

static inline uint8_t check_firmware_version(uint8_t major,uint8_t minor, uint8_t build){

	if (major < MIN_VER_MAJOR){
		err_firmware_version = 1;
		return err_firmware_version;
	}else if((major==MIN_VER_MAJOR)&&(minor<MIN_VER_MAJOR)){
		err_firmware_version = 1;
		return err_firmware_version;
	}else if((major==MIN_VER_MAJOR)&&(minor==MIN_VER_MINOR)&&(build< MIN_VER_BUILD)){
		err_firmware_version = 1;
		return err_firmware_version;
	}
	return 0;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  version_handler
 *  Description:  firmware version handler function.
 * =====================================================================================
 */
void module_version_handler(rha_frame_t *frame) {
	uint8_t major = frame->payload[0];
	uint8_t minor = frame->payload[1];
	uint8_t build = frame->payload[2];
	uint16_t app_info = (uint16_t)(frame->payload[3] + (frame->payload[4] << 8));

	printf("\nv%u.%u.%u ", major, minor, build);

	if (check_firmware_version(major, minor, build))
		printf("\n Zigbee module firmware version too low, upgrade firmware. ");

	if (app_info == 0x0002) {
		printf("RapidHA ");
	}

	printf("EUI64: ");
	uint8_t i=0;
	for(i=5;i<13;i++)
		printf("%x ",frame->payload[i]);

	printf("Hardware Type:%x ", frame->payload[13]);
	printf("Bootloader Type:%x\n", frame->payload[14]);


}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  manufacturer_ID_handler
 *  Description:
 * =====================================================================================
 */
void manufacturer_ID_handler(rha_frame_t *frame) {
	uint8_t major = frame->payload[0];
	uint8_t minor = frame->payload[1];
	uint16_t manu_id = (uint16_t)(major + (minor << 8));

	printf("fw: ");
	if (manu_id == 0x109A) {
		printf("MMB manufacturer id: ");
	}

	printf("0x%x%x\n", frame->payload[1],frame->payload[0]);
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  serial_ack_config_response_handler
 *  Description:
 * =====================================================================================
 */
void serial_ack_config_response_handler(rha_frame_t *frame) {
	config_ack = frame->payload[0];
	return;
}

//startup_sync_request_handler
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  startup_sync_request_handler
 *  Description: handle module startup sync request
 * =====================================================================================
 */
void startup_sync_request_handler(rha_frame_t *frame) {

	running_state = frame->payload[0];
	config_state = frame->payload[1];

	printf("\n running_state: 0x%x", running_state);
	printf("\n config_state: 0x%x", config_state);

	return;

}

unsigned char prompt_msg_int[]="\r\nNAK: Unknown command!!!\r\n";
#endif

void io_command_get_nvt_table_handler(void) {

    //construct a response message back to master;
    uint8_t payload[127];
    uint8_t payload_length = sizeof(firmware_info.app);
    uint8_t *buffer;

    memcpy(payload,&firmware_info.app,sizeof(firmware_info.app));
    R_BSP_SoftwareDelay(10, BSP_DELAY_UNITS_MILLISECONDS);  // 20ms delay;

    buffer = construct_rha_frame(MASTER_ADDR, PH_BOOTLOAD, SH_BOOTLOAD_NVT, payload, payload_length);
    rs485_tx_write_nbyte((char *)buffer, (unsigned short)(payload_length+8));
    memset(buffer,0,sizeof(snd_buffer));

    return;
}

void send_version_info_to_controller(void) {
	//for command that needs echo "ACK";
	uint8_t payload[16];
	uint8_t length = 7; //sizeof(firmware_info.app.fw_info.ascii_info.version);
	uint8_t payload_length = length;
	uint8_t *buffer;
	uint8_t *ptr_to_data = &firmware_info.app.fw_info.ascii_info.version[0];

    memcpy(payload, ptr_to_data, length);

	buffer = construct_rha_frame(MASTER_ADDR, PH_BOOTLOAD, SH_BOOTLOAD_INFO, payload, payload_length);
	rs485_tx_write_nbyte((char *)buffer, (unsigned short)(payload_length + 8));
	memset(buffer, 0, sizeof(snd_buffer));
	return;
}


void send_info_to_controller(uint8_t message_type, uint8_t *ptr_to_data, uint8_t length) {
	//for command that needs echo "ACK";
	uint8_t payload[256];
	uint8_t payload_length = length;
	uint8_t *buffer;

    memcpy(payload, ptr_to_data, length);

	buffer = construct_rha_frame(MASTER_ADDR, PH_BOOTLOAD, message_type, payload, payload_length);
	rs485_tx_write_nbyte((char *)buffer, (unsigned short)(payload_length + 8));
	memset(buffer, 0, sizeof(snd_buffer));
	return;
}


/* ===  FUNCTION  ======================================================================
*         Name:  io_command_getdo_handler
*  Description:  io command get DO/Relay state handler function.
* =====================================================================================
*/
void send_command(rha_frame_t *frame) {
    int channel = frame->payload[0] +1; //channel starts from 1,2,3,4
    SWITCH_STATE_E state;

    // Call DB API here
    //if (db_vt_get_relay_state(channel, &state) != SUCCESS)
    //{
        //return; //NAK???
    //}
    //construct a response message back to master;
    state = get_do_value(channel);
    uint8_t payload[127];
    uint8_t payload_length = sizeof(state);
    uint8_t *buffer;

    memcpy(payload,&state,sizeof(state));
    R_BSP_SoftwareDelay(10, BSP_DELAY_UNITS_MILLISECONDS);  // 20ms delay;

    buffer = construct_rha_frame(MASTER_ADDR, PH_IO_COMMAND, SH_IO_COMMAND_DO_RESPONSE, payload, payload_length);
    rs485_tx_write_nbyte((char *)buffer, (unsigned short)(payload_length+8));
    memset(buffer,0,sizeof(snd_buffer));

    return;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  send_ack
 *  Description:  for commands that needs ACK msg
 * =====================================================================================
 */
void send_ack(uint8_t primary_header, uint8_t secondary_header) {
	//for command that needs echo "ACK";
		uint8_t payload_length = 0;
		uint8_t *buffer;

		buffer = construct_rha_frame(MASTER_ADDR, primary_header, secondary_header, NULL, 0);
		rs485_tx_write_nbyte((char *)buffer, (unsigned short)(payload_length+8));
		memset(buffer,0,sizeof(snd_buffer));
		return;
}

#if 0
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  default_handler
 *  Description:  unknown command, reply NAK;
 * =====================================================================================
 */
void default_handler(rha_frame_t *frame) {
	_PARAMETER_NOT_USED_(frame);
	//unknown command, echo "NAK";
	if (config_ack){
		R_BSP_SoftwareDelay(1000, BSP_DELAY_UNITS_MILLISECONDS);	// 20ms delay;
		send_ack(PH_UTILITY,SH_UTILITY_ERROR);
		}

	return;
}

/*-----------------------------------------------------------------------------
 * Frame handler function dispatcher table
 *-----------------------------------------------------------------------------*/
rha_frame_handler_t handlers[] = {
	{ PH_UTILITY, SH_UTILITY_MODULE_INFO_RESPONSE, 15, module_version_handler },
	{ PH_UTILITY, SH_UTILITY_BOOTLOADER_VERSION_RESPONSE, 5, bootloader_version_handler },
	{ PH_UTILITY, SH_UTILITY_MANUFACTURER_ID_RESPONSE, 2, manufacturer_ID_handler },
	{ PH_UTILITY, SH_UTILITY_SERIAL_ACK_CONFIG_WRITE, 0, serial_ack_config_response_handler },
	{ PH_UTILITY, SH_UTILITY_STARTUP_SYNC_REQUEST, 1, startup_sync_request_handler },

	{ PH_NETWORK_COMISSIONING, SH_NETWORK_COMISSIONING_DISCOVER_NODE,0, network_discover_node_handler },
	{ PH_NETWORK_COMISSIONING, SH_NETWORK_COMISSIONING_ASSIGN_NODE,0, network_assign_node_handler },
//
	{ PH_IO_COMMAND, SH_IO_COMMAND_DO_WRITE,0, io_command_do_handler },
	{ PH_IO_COMMAND, SH_IO_COMMAND_AO_WRITE,0, io_command_ao_handler },
	{ PH_IO_COMMAND, SH_IO_COMMAND_LED_WRITE,0, io_command_led_handler },
	{ PH_IO_COMMAND, SH_IO_COMMAND_NZC_WRITE,0, io_command_zc_handler },
	{ PH_IO_COMMAND, SH_IO_COMMAND_EOL_WRITE,0, io_command_seteol_handler },
	{ PH_IO_COMMAND, SH_IO_COMMAND_EOL_REQUEST,0, io_command_geteol_handler },
	{ PH_IO_COMMAND, SH_IO_COMMAND_DO_REQUEST,0, io_command_getdo_handler },
	{ PH_IO_COMMAND, SH_IO_COMMAND_ADCAO_REQUEST,0, io_command_adcao_handler },
	{ PH_IO_COMMAND, SH_IO_COMMAND_ADC24V_REQUEST,0, io_command_adc24v_handler },
	{ PH_IO_COMMAND, SH_IO_COMMAND_DI_REQUEST,0, io_command_di_handler },
	{ PH_IO_COMMAND, SH_IO_COMMAND_AI_REQUEST,0, io_command_ai_handler },
	{ PH_IO_COMMAND, SH_IO_COMMAND_VT_REQUEST,0, io_command_get_vt_table_handler },
	{ PH_IO_COMMAND, SH_IO_COMMAND_NVT_REQUEST,0, io_command_get_nvt_table_handler }

	/*
	{ PH_TEST, SH_TEST_MODULE_INFO_REQUEST, 15, module_version_handler },
	{ PH_TEST, SH_TEST_BOOTLOADER_VERSION_REQUEST, 5, bootloader_version_handler },
	{ PH_TEST, SH_TEST_MANUFACTURER_ID_REQUEST, 2, manufacturer_ID_handler },
	{ PH_TEST, SH_TEST_SERIAL_ACK_CONFIG_WRITE, 1, serial_ack_config_response_handler },
	{ PH_TEST, SH_TEST_STARTUP_SYNC_REQUEST, 1, startup_sync_request_handler }
*/
};



/*
 * ===  FUNCTION  ======================================================================
 *         Name:  process_rha_frame
 *  Description:  process received frame, handler function dispatcher
 * =====================================================================================
 */
void process_rha_frame(rha_frame_t *frame) {
	size_t handler_count = sizeof(handlers) / sizeof(rha_frame_handler_t);
	size_t idx;
	bool handled = false;

	//my_addr = module_info.cur_address;	//DEFAULT_ADDR;	//MY_ADDR;

	//check address first;
	if(frame->addr != module_info.cur_address){	//my_addr){
		return;
	}

	for (idx = 0; idx < handler_count; idx++) {
		rha_frame_handler_t *handler = &handlers[idx];
		if (handler->ph == frame->ph && handler->sh == frame->sh)
			if (handler->min_length <= frame->len) {
				handler->fn(frame);
				handled = true;
			}
	}

	if (!handled)
		default_handler(frame);	//not handled command, send NAK

}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  rha_bytes
 *  Description:  rha serial port packet bytes check and allocate for frame process
 * =====================================================================================
 */
void rha_bytes(uint8_t *bytes, uint8_t len) {

	while (len--) {
		uint8_t b = *bytes++;

		if (NULL == rcv_frame)
			rcv_frame = calloc(sizeof(rha_frame_t), 1);//allocate memory for frame
			//rcv_frame = (rha_frame_t *)rcv_buffer;

		if (NULL == rcv_frame)
			continue;	//Essentially We're Dropping The Byte

		if (0 == (rcv_frame->flags & SOF_RX)) {
			if (SOF == b)
				rcv_frame->flags |= SOF_RX;
			continue;
		}

		rcv_frame->calc_cs = (uint16_t)(rcv_frame->calc_cs + b);
		if (0 == (rcv_frame->flags & ADDR_RX)) {
			rcv_frame->addr = b;
			rcv_frame->flags |= ADDR_RX;
		} else if (0 == (rcv_frame->flags & PH_RX)) {
			rcv_frame->ph = b;
			rcv_frame->flags |= PH_RX;
		} else if (0 == (rcv_frame->flags & SH_RX)) {
			rcv_frame->sh = b;
			rcv_frame->flags |= SH_RX;
		} else if (0 == (rcv_frame->flags & SEQ_RX)) {
			rcv_frame->seq = b;
			rcv_frame->flags |= SEQ_RX;
		} else if (0 == (rcv_frame->flags & LEN_RX)) {
			rcv_frame->len = b;
			rcv_frame->flags |= LEN_RX;
			rcv_frame->idx = 0;
			if (0 < rcv_frame->len) {
				//Need To Allocate Space For Payload
				rcv_frame->payload = calloc(rcv_frame->len, 1);
				//rcv_frame->payload = payload;
				if (NULL == rcv_frame->payload) {			//failed to allocate memory for payload, drop the packet;
					free(rcv_frame);
					rcv_frame = NULL;
				}
			} else {
				rcv_frame->flags |= PAYLOAD_RX;
			}
		} else if (0 == (rcv_frame->flags & PAYLOAD_RX)) {
			rcv_frame->payload[rcv_frame->idx++] = b;
			if (rcv_frame->len <= rcv_frame->idx) {
				rcv_frame->flags |= PAYLOAD_RX;
				rcv_frame->idx = 0;
			}
		} else {
			//In Checksum
			rcv_frame->calc_cs = (uint16_t)(rcv_frame->calc_cs - b);
			rcv_frame->cs = (uint16_t)(rcv_frame->cs+(b << (8 * rcv_frame->idx++)));
			if (2 <= rcv_frame->idx) {
				//have the checksum, compare packet checksum with calculated checksum;
				if (rcv_frame->cs == rcv_frame->calc_cs) {

					//pass checksum, process frame;
					process_rha_frame(rcv_frame);
				}

				//Free The Frame
				if (NULL != rcv_frame->payload) { //free allocated memory for payload;
					free(rcv_frame->payload);
				}
				free(rcv_frame);			//free allocated memory for frame
				rcv_frame = NULL;
			}
		}
	}

}
#endif
