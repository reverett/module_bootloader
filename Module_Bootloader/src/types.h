#ifndef __TYPES_H__
#define __TYPES_H__

// needed for simulation
//typedef unsigned char uint8_t;
//typedef unsigned short uint16_t;
//typedef unsigned long uint32_t;

//#include <stddef.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <unistd.h>  //Sleep
//#include <fcntl.h>
//#include <errno.h>
//#include <int_prot_cmd.h>
//#include <int_prot_frame.h>
//#include <termios.h>
#include <inttypes.h>
//#include <stdbool.h>
//#include "jnode.h"
#include <rs485_driver.h>
#include "jellyfish_common.h"

//#ifndef BOOL
//typedef enum {
	//TRUE = 1,
	//FALSE = 0
//} BOOL;
//#endif

#ifndef NULL
#define NULL  ((void *) 0)
#endif

//typedef enum {
	//RS485_READ = 0,
	//RS485_WRITE = 1
//} RS485_DIRECTION;

#ifndef _PARAMETER_NOT_USED_
#define _PARAMETER_NOT_USED_(p) (void) ((p))
#endif


#endif //__TYPES_H__
