//#include <stdio.h>
#include <types.h>
#include "payload_processing.h"
#include "frame_processing.h"
#include "rs485_driver.h"

#define FLASH_BLOCK_SIZE 1024
#define MAX_BUFF 70

uint32_t new_command;

static uint32_t next_dest_address = 0;
static uint16_t expected_crc = 0;

static uint32_t num_s0_records = 0; // running counts of the different s records
static uint32_t num_s1_records = 0;
static uint32_t num_s2_records = 0;
static uint32_t num_s3_records = 0;
static uint32_t num_s7_records = 0;
static uint32_t num_s8_records = 0;
static uint32_t num_s9_records = 0;
static uint32_t num_records = 0;     // value in s0 number of s records 

extern sw_info_struct firmware_info;
//extern FILE *binFilePtr;
//extern FILE *outFilePtr;
//extern FILE *buffFilePtr;

uint16_t getExpectedCRC(void)
{
	return(expected_crc);
}

#if 0
void printBuffer(int rowLength, int bufferLength, unsigned char * buff_ptr, FILE *buffFilePtr)
{
	int i = 0;
	int j = 0;

	while (bufferLength)
	{
		if (bufferLength >= rowLength)
		{

			if (j < rowLength)
			{
				fprintf(buffFilePtr, "%02X", *(buff_ptr + i));
				//printf("%02X", *(buff_ptr + i));
				j++;
				i++;
				bufferLength--;
			}
			else
			{
				j = 0;
				fprintf(buffFilePtr, "\n");
				//printf("\n");
			}

		}
		else
		{
			if (j < rowLength)
			{
				fprintf(buffFilePtr, "%02X", *(buff_ptr + i));
				//printf("%02X", *(buff_ptr + i));
				j++;
				i++;
				bufferLength--;
			}
			else
			{
				j = 0;
				fprintf(buffFilePtr, "\n");
				//printf("\n");
			}
		}
	}
	printf("\n");
}
#endif

uint16_t crc_xmodem_update(uint16_t initial_crc, uint8_t *pdata, uint16_t numBytesToCrc)
{
	uint16_t bitIdx, byteIdx;

	for (byteIdx = 0; byteIdx < numBytesToCrc; byteIdx++)
	{
		initial_crc = initial_crc ^ ((uint16_t)(*pdata) << 8);
		++pdata;
		for (bitIdx = 0; bitIdx < 8; bitIdx++)
		{
			if (initial_crc & 0x8000)
				initial_crc = (initial_crc << 1) ^ 0x1021;
			else
				initial_crc <<= 1;
		}
	}

	return initial_crc;
}
// reads in bytes computes 32bit value
uint32_t writeNumHeader(uint8_t * ptr_to_data, uint8_t size, uint8_t * checksum)
{
	uint8_t data_value;
	uint8_t * ptr_to_elem;
	uint32_t address;

	for (int i = 0; i < size; i++)
	{
		if (i == 0)
		{
			data_value = rs485_rx_read_byte();
			address = data_value;
		}
		else
		{
			data_value = rs485_rx_read_byte();
			address = address << 8;
			address = address | data_value;
		}
		checksum = (address && 0xFF) + checksum;
		calc_chk_sum_frame(address & 0xFF);

		ptr_to_elem = ptr_to_data + i;
		*ptr_to_elem = data_value;
	}
	return (address);
}

void init_payload_processing(void)
{
	num_s0_records = 0;
	num_s1_records = 0;
	num_s2_records = 0;
	num_s3_records = 0;
	num_s7_records = 0;
	num_s8_records = 0;
	num_s9_records = 0;
	num_records = 0;
}

// processes new image record 
record_status_t process_image_record(unsigned char* data_buffer_ptr, uint32_t* flash_dest_address, uint32_t* num_bytes_received, unsigned char payload_len)
{
	static unsigned char * buff_ptr;
	record_status_t rec_status = PRC_REC_RCVD_OK;
	buff_ptr = data_buffer_ptr;

	int byte_cnt;                             // byte_cnt in s record
	uint32_t dest_address;                    // dest address of data in s record
	uint32_t exec_address;                    // exec_address in s record
	uint32_t new_rectype;

	unsigned char calc_chksum_srec = 0;       // checksum calculated
	unsigned char not_calc_chksum_srec = 0;   // complement checksum
	unsigned char checksum_srec = 0;          // checksum in s record 
	unsigned char num_addr_bytes = 0;         // number address bytes in s record
	uint32_t total_srecords = 0;              // total number of s records in file
	
	new_command = rs485_rx_read_byte();
	calc_chk_sum_frame(new_command);
	switch (new_command)
	{
		
		case 'S':
		{
		
			new_rectype = rs485_rx_read_byte();
			calc_chk_sum_frame(new_rectype);
			switch (new_rectype)
			{
				
				// S0 Header file ignore
				case '0':
				{
					// new format: | S0 | byte count = 1 bytes | product ID = 10 bytes | skew = 10 bytes | version # = 14 bytes | unused = 24 bytes | start address = 4 bytes | record count = 4 bytes | app crc = 2 bytes | checksum = 1 byte |
					//printf("S0 ");
					
					init_payload_processing();
					*num_bytes_received = 0; 
					init_flash_buffer();
					byte_cnt = rs485_rx_read_byte();                           // byte count
					calc_chksum_srec = byte_cnt; // s record checksum
					calc_chk_sum_frame(byte_cnt);
					//printf("byte_cnt = %x", byte_cnt);
					// get data

					// ProductID
					for (int i = 0; i < sizeof(firmware_info.app.fw_info.ascii_info.productID); i++)
					{
						unsigned char dummy_char;
						dummy_char = rs485_rx_read_byte();                     
						//printf("%02X", dummy_char);
						//fprintf(outFilePtr, "%02X", dummy_char);
						firmware_info.app.fw_info.ascii_info.productID[i] = dummy_char;
						calc_chksum_srec = firmware_info.app.fw_info.ascii_info.productID[i] + calc_chksum_srec;
						calc_chk_sum_frame(firmware_info.app.fw_info.ascii_info.productID[i]);
					}
					// Skew
					for (int i = 0; i < sizeof(firmware_info.app.fw_info.ascii_info.skew); i++)
					{
						unsigned char dummy_char;
						dummy_char = rs485_rx_read_byte();
						//printf("%02X", dummy_char);
						//fprintf(outFilePtr, "%02X", dummy_char);
						firmware_info.app.fw_info.ascii_info.skew[i] = dummy_char;
						calc_chksum_srec = firmware_info.app.fw_info.ascii_info.skew[i] + calc_chksum_srec;
						calc_chk_sum_frame(firmware_info.app.fw_info.ascii_info.skew[i]);
					}
					// Version #
					for (int i = 0; i < sizeof(firmware_info.app.fw_info.ascii_info.version); i++)
					{
						unsigned char dummy_char;
						dummy_char = rs485_rx_read_byte();
						//printf("%02X", dummy_char);
						//fprintf(outFilePtr, "%02X", dummy_char);
						firmware_info.app.fw_info.ascii_info.version[i] = dummy_char;
						calc_chksum_srec = firmware_info.app.fw_info.ascii_info.version[i] + calc_chksum_srec;
						calc_chk_sum_frame(firmware_info.app.fw_info.ascii_info.version[i]);
					}
					// date
					for (int i = 0; i < sizeof(firmware_info.app.fw_info.ascii_info.date); i++)
					{
						unsigned char dummy_char;
						dummy_char = rs485_rx_read_byte();
						//printf("%02X", dummy_char);
						//fprintf(outFilePtr, "%02X", dummy_char);
						firmware_info.app.fw_info.ascii_info.date[i] = dummy_char;
						calc_chksum_srec = firmware_info.app.fw_info.ascii_info.date[i] + calc_chksum_srec;
						calc_chk_sum_frame(firmware_info.app.fw_info.ascii_info.date[i]);
					}
					// time
					for (int i = 0; i < sizeof(firmware_info.app.fw_info.ascii_info.time); i++)
					{
						unsigned char dummy_char;
						dummy_char = rs485_rx_read_byte();
						//printf("%02X", dummy_char);
						//fprintf(outFilePtr, "%02X", dummy_char);
						firmware_info.app.fw_info.ascii_info.time[i] = dummy_char;
						calc_chksum_srec = firmware_info.app.fw_info.ascii_info.time[i] + calc_chksum_srec;
						calc_chk_sum_frame(firmware_info.app.fw_info.ascii_info.time[i]);
					}
					// unused field
					for (int i = 0; i < sizeof(firmware_info.app.fw_info.ascii_info.unused_arr); i++)
					{
						unsigned char dummy_char;
						dummy_char = rs485_rx_read_byte();
						//printf("%02X", dummy_char);
						//fprintf(outFilePtr, "%02X", dummy_char);
						firmware_info.app.fw_info.ascii_info.unused_arr[i] = dummy_char;
						calc_chksum_srec = firmware_info.app.fw_info.ascii_info.unused_arr[i] + calc_chksum_srec;
						calc_chk_sum_frame(firmware_info.app.fw_info.ascii_info.unused_arr[i]);
					}
					// start address
					writeNumHeader(&firmware_info.app.fw_info.num_info.start_address[0], sizeof(firmware_info.app.fw_info.num_info.start_address), &calc_chksum_srec);
					// end address
					writeNumHeader(&firmware_info.app.fw_info.num_info.end_address[0], sizeof(firmware_info.app.fw_info.num_info.end_address), &calc_chksum_srec);
					// record count
					num_records = writeNumHeader(&firmware_info.app.fw_info.num_info.record_count[0], sizeof(firmware_info.app.fw_info.num_info.record_count), &calc_chksum_srec);

					expected_crc = rs485_rx_read_byte();                     // get expected crc for the data to be received
					firmware_info.app.crc_loc[0] = expected_crc;
					calc_chksum_srec = (expected_crc & 0xFF) + calc_chksum_srec;
					calc_chk_sum_frame(expected_crc & 0xFF);

					firmware_info.app.crc_loc[1] = rs485_rx_read_byte();
					expected_crc = (expected_crc << 8) + firmware_info.app.crc_loc[1];
					calc_chksum_srec = (expected_crc & 0xFF) + calc_chksum_srec;

					calc_chk_sum_frame(expected_crc & 0xFF);
					checksum_srec = rs485_rx_read_byte();                    // get the checksum for this srec
					calc_chk_sum_frame(checksum_srec);                       // add srec checksum to the frame checksum

					not_calc_chksum_srec = ~calc_chksum_srec;

					{
						uint16_t frame_checksum = 0;
						uint16_t upper_cs;
						uint16_t lower_cs;
						// get checksum at end of frame
						lower_cs = rs485_rx_read_byte();
						upper_cs = rs485_rx_read_byte();
						frame_checksum = (upper_cs << 8) | lower_cs;
						// see if calc checksum is same as checksum in file and frame checksum is OK
						if ((checksum_srec == not_calc_chksum_srec) &&
							(get_calc_chksum_frame() == frame_checksum))
						{
							rec_status = PRC_REC_FIRST_REC;
							num_s0_records++;
						}
						else
							rec_status = PRC_REC_SREC_CKSUM_ERR;
					}

				}
					break;

				case '1':
				case '2':
				case '3':
				{
					if (new_rectype == '1') // S1 record
					{
						//printf("S1 ");
						//fprintf(outFilePtr, "S1");
						num_addr_bytes = 2; // 16 bit address
					}
					else if (new_rectype == '2') // S2 record
					{
						//printf("S2 ");
						//fprintf(outFilePtr, "S2");
						num_addr_bytes = 3; // 24 bit address
					}
					else if (new_rectype == '3') // S3 record
					{
						//printf("S3 ");
						//fprintf(outFilePtr, "S3");
						num_addr_bytes = 4; // 32 bit address
					}

					// follows SREC format  
					// do conversion from ascii to hex
					//
					byte_cnt = rs485_rx_read_byte();                           // byte count

					//fprintf(outFilePtr, "%02X", byte_cnt);
					calc_chksum_srec = byte_cnt;
					calc_chk_sum_frame(byte_cnt);
					// 
					for (int i = 0; i < num_addr_bytes; i++)
					{
						if (i == 0)
							dest_address = rs485_rx_read_byte();
						else
						{
							dest_address = dest_address << 8;
							dest_address = dest_address | rs485_rx_read_byte();
						}
						calc_chksum_srec = (dest_address & 0xFF) + calc_chksum_srec;
						calc_chk_sum_frame(dest_address & 0xFF);
					}
					*flash_dest_address = dest_address;

					//printf("dest address = %x", dest_address);
#if 0
					if (num_addr_bytes == 2)
						fprintf(outFilePtr, "%04X", dest_address);
					else if (num_addr_bytes == 3)
						fprintf(outFilePtr, "%06X", dest_address);
					else if (num_addr_bytes == 4)
						fprintf(outFilePtr, "%08X", dest_address);
#endif
					//printf("byte_cnt = %x", byte_cnt);
					if ((byte_cnt != payload_len - 3) || (byte_cnt - (num_addr_bytes + 1) > MAX_SREC_LEN))
					{
						byte_cnt = MAX_SREC_LEN;
						rec_status = PRC_REC_SREC_LEN_ERR;
					}

					*num_bytes_received = byte_cnt - (num_addr_bytes + 1);
					// get data
					while (byte_cnt - (num_addr_bytes + 1))                            // number of bytes of data = byte_cnt - (num bytes for dest address + 1 byte for checksum) 
					{
						*buff_ptr = rs485_rx_read_byte();
						//printf("%02X", *buff_ptr);
						//fprintf(outFilePtr, "%02X", *buff_ptr);
						calc_chksum_srec = *buff_ptr + calc_chksum_srec;
						calc_chk_sum_frame(*buff_ptr);
						byte_cnt = byte_cnt - 1;

							buff_ptr++;
					}
					// get checksum
					checksum_srec = rs485_rx_read_byte();                          // checksum
					not_calc_chksum_srec = ~calc_chksum_srec;
					calc_chk_sum_frame(not_calc_chksum_srec);                // add srec checksum to the frame checksum
					//fprintf(outFilePtr, "%02X\n\r", not_calc_chksum_srec);
					//printf("calc checksum = %02x", not_calc_chksum_srec);
					// see if calc checksum is same as checksum in file

					{
						uint16_t frame_checksum = 0;
						uint16_t upper_cs;
						uint16_t lower_cs;
						// get checksum at end of frame
						lower_cs = rs485_rx_read_byte();
						upper_cs = rs485_rx_read_byte();
						frame_checksum = (upper_cs << 8) | lower_cs;

						// see if calc checksum is same as checksum in file and frame checksum is OK
						if ((checksum_srec == not_calc_chksum_srec) &&
							(get_calc_chksum_frame() == frame_checksum))
						{
							rec_status = PRC_REC_RCVD_OK;
							// if not contiguous and not the first record flag the error
							if ((next_dest_address != dest_address) && ((num_s1_records > 1) || (num_s2_records > 1) || (num_s3_records > 1)))
							{
								//printf("failure records not contigous\n\r");
								rec_status = PRC_REC_SREC_ORDER_ERR;
							}
							else // no errors
							{
								if (new_rectype == '1') // S1 record
								{
									num_s1_records++;
								}
								else if (new_rectype == '2') // S2 record
								{
									num_s2_records++;
								}
								else if (new_rectype == '3') // S3 record
								{
									num_s3_records++;
								}
								next_dest_address = dest_address + *num_bytes_received;    // calc for next record received  num bytes for dest address + 1 byte for checksum
							}
						}
						else
						{
							rec_status = PRC_REC_SREC_CKSUM_ERR;
						}
					}
					//printf(" \n\r");
				}
					break;

				// S7, S8, S9  Start Address	(Termination)
				case '7': // end 32 bit address
				case '8': // end 24 bit address
				case '9': // end 16 bit address
				{
				  //printf("last record");

				  if (new_rectype == '7') // S1 record
				  {
					  //printf("S7 ");
					  //fprintf(outFilePtr, "S7");
					  num_addr_bytes = 4; // 32 bit address
				  }
				  else if (new_rectype == '8') // S2 record
				  {
					  //printf("S8 ");
					  //fprintf(outFilePtr, "S8");
					  num_addr_bytes = 3; // 24 bit address
				  }
				  else if (new_rectype == '9') // S3 record
				  {
					  printf("S9 ");
					  //fprintf(outFilePtr, "S9");
					  num_addr_bytes = 2; // 16 bit address
				  }
				  byte_cnt = rs485_rx_read_byte();                           // byte count

				  //fprintf(outFilePtr, "%02X", byte_cnt);
				  calc_chksum_srec = byte_cnt;
				  calc_chk_sum_frame(byte_cnt);
				  // 
				  for (int i = 0; i < num_addr_bytes; i++)
				  {
					  if (i == 0)
						  exec_address = rs485_rx_read_byte();
					  else
						  exec_address = (exec_address << 8) + rs485_rx_read_byte();

					  calc_chksum_srec = (exec_address & 0xFF) + calc_chksum_srec;
					  calc_chk_sum_frame(exec_address & 0xFF);
				  }
				  //printf("exec address = %x", exec_address);
				  // get checksum
				  checksum_srec = rs485_rx_read_byte();                    // checksum
				  not_calc_chksum_srec = ~calc_chksum_srec;
				  calc_chk_sum_frame(not_calc_chksum_srec);                // add srec checksum to the frame checksum
				  //fprintf(outFilePtr, "%02X\n\r", not_calc_chksum_srec);
				  //printf("calc checksum = %02x", not_calc_chksum_srec);
				  // see if calc checksum is same as checksum in file

				  {
					  uint16_t frame_checksum = 0;
					  uint16_t upper_cs;
					  uint16_t lower_cs;
					  // get checksum at end of frame
					  lower_cs = rs485_rx_read_byte();
					  upper_cs = rs485_rx_read_byte();
					  frame_checksum = (upper_cs << 8) | lower_cs;

					  // see if calc checksum is same as checksum in file and frame checksum is OK
					  if ((checksum_srec == not_calc_chksum_srec) &&
						  (get_calc_chksum_frame() == frame_checksum))
					  {
						  rec_status = PRC_REC_LAST_REC;

						  if (new_rectype == '7') // S1 record
						  {
							  num_s7_records++;
						  }
						  else if (new_rectype == '8') // S2 record
						  {
							  num_s8_records++;
						  }
						  else if (new_rectype == '9') // S3 record
						  {
							  num_s9_records++;
						  }

						  // check to see if all records were received
						  total_srecords = num_s0_records + num_s1_records + num_s2_records + num_s3_records + num_s7_records + num_s8_records + num_s9_records;

						  if (total_srecords != num_records)
						  {
							  rec_status = PRC_REC_NUM_SREC;
						  }
					  }
					  else // checksum for record is incorrect
					  {
						  rec_status = PRC_REC_SREC_CKSUM_ERR;
					  }
				  }
				  //printf("\n\r");
				}

					break;
				}// end of switch
	}
		break;
	} // end of new_command switch
	//printf("\n\r");
	return(rec_status);
}
