/*
 * time_out_handlers.c
 *
 *  Created on: Apr 21, 2017
 *      Author: reverett
 */
#include "hal_data.h"
#include "time_out_handlers.h"

#define IMAGE_BOOT_ADDRESS_RUN   (0x00000)                                   // First address of the image in flash to run (runtime address)

typedef         void(*main_boot_fnptr)(void);
main_boot_fnptr      *p_jump_to_boot;             // Function pointer to the boot

static uint32_t g_arg_event;

void run_boot(void);



void refresh_timer(void)
{
	ssp_err_t err;
    // skip the first pulse measurement
    // restart the timer 0

    err = g_timer0.p_api->reset(g_timer0.p_ctrl);
    while (err)
    {
    }

    err = g_timer0.p_api->start(g_timer0.p_ctrl);
    while (err)
    {
    }
}

void stop_timer(void)
{
	ssp_err_t err;

    err = g_timer0.p_api->stop(g_timer0.p_ctrl);
    while (err)
    {
    }
}

// timer 0 is used to measure line period
// callback occurs if timer0 overflow occurs
void g_timer0_callback(timer_callback_args_t * p_args)
{
    g_arg_event = p_args->event;
    ssp_err_t err;
    //if (g_arg_event == TIMER_EVENT_EXPIRED)
    {
    	// count the overflow event
        //capture.overflows = capture.overflows + 1;
        // store this event in flash

        // setup configuration data

        err = g_flash0.p_api->close(g_flash0.p_ctrl);

        stop_timer();
        // jump to boot
        //run_boot();
        // reset
        NVIC_SystemReset(); // never return
    }
}

void run_boot(void)
{
    // Point to the start reset vector for the  application
    p_jump_to_boot = ((main_boot_fnptr*)IMAGE_BOOT_ADDRESS_RUN) + 1U;

    // Disable interrupts
    __disable_irq();

    // Update the vector table to the reset vector of the application
    SCB->VTOR = (uint32_t)IMAGE_BOOT_ADDRESS_RUN;

    //Complete all explicit memory accesses
    __DSB();

    //Set stack for the application
    __set_MSP(*((uint32_t*)IMAGE_BOOT_ADDRESS_RUN));

    //Jump to application
    (*p_jump_to_boot)();
}

