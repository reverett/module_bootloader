Synergy Configuration
  Board "Custom User Board (Any Device)"
  R7FS128783A01CFM
    part_number: R7FS128783A01CFM
    rom_size_bytes: 262144
    ram_size_bytes: 24576
    data_flash_size_bytes: 4096
    package_style: LQFP
    package_pins: 64
    
  S128
    series: 1
    
  S128 Family
    OFS0 register settings: Select fields below
         IWDT Start Mode: IWDT is Disabled
         IWDT Timeout Period: 2048 cycles
         IWDT Dedicated Clock Frequency Divisor: 128
         IWDT Window End Position:  0% (no window end position)
         IWDT Window Start Position: 100% (no window start position)
         IWDT Reset Interrupt Request Select: Reset is enabled
         IWDT Stop Control: Stop counting when in Sleep, Snooze mode, Software Standby, or Deep Software Standby mode
         WDT Start Mode Select: Stop WDT after a reset (register-start mode)
         WDT Timeout Period: 16384 cycles
         WDT Clock Frequency Division Ratio: 128
         WDT Window End Position:  0% (no window end position)
         WDT Window Start Position: 100% (no window start position)
         WDT Reset Interrupt Request: Reset
         WDT Stop Control: Stop counting when entering Sleep mode
    OFS1 register settings: Select fields below
         Voltage Detection 0 Circuit Start: Voltage monitor 0 reset is disabled after reset
         Voltage Detection 0 Level: 1.90 V
         HOCO OScillation Enable: HOCO oscillation is disabled after reset
    
  Synergy Common
    Main stack size (bytes): 0x1000
    Process stack size (bytes): 0
    Heap size (bytes) - A minimum of 4K (0x1000) is required if standard library functions are to be used.: 0x1000
    MCU Vcc (mV): 3300
    Parameter checking: Enabled
    Assert Failures: Return SSP_ERR_ASSERTION
    Error Log: No Error Log
    ID code 1: 0xFFFFFFFF
    ID code 2: 0xFFFFFFFF
    ID code 3: 0xFFFFFFFF
    ID code 4: 0xFFFFFFFF
    
  Clocks
    XTAL 16000000Hz
    HOCO 48MHz
    Clock Src: HOCO
    ICLK Div /2
    PCLKB Div /2
    PCLKD Div /1
    
  Pin Configurations
  Module "FMI Driver on r_fmi"
    Parameter Checking: Default (BSP)
    Use Custom Factory Flash: Disabled
    Custom Factory Flash Symbol Name: g_fmi_data
    
  Module "CGC Driver on r_cgc"
    Parameter Checking: Default (BSP)
    Main Oscillator Wait Time: 8163 cycles
    Main Oscillator Clock Source: Crystal or Resonator
    Oscillator Stop Detect: Enabled
    Subclock Drive: Standard (12.5pf)
    
  Module "ELC Driver on r_elc"
    Parameter Checking: Default (BSP)
    
  Module "I/O Port Driver on r_ioport"
    Parameter Checking: Default (BSP)
    
  Module "UART Driver on r_sci_uart"
    External RTS Operation: Disable
    Reception: Enable
    Transmission: Enable
    Parameter Checking: Default (BSP)
    
  Module "Timer Driver on r_gpt"
    Parameter Checking: Default (BSP)
    
  Module "ADC Driver on r_adc"
    Parameter Checking: Enabled
    
  HAL
    Instance "g_fmi FMI Driver on r_fmi"
      Name: g_fmi
      
    Instance "g_cgc CGC Driver on r_cgc"
      Name [Fixed]: g_cgc
      
    Instance "g_elc ELC Driver on r_elc"
      Name [Fixed]: g_elc
      
    Instance "g_ioport I/O Port Driver on r_ioport"
      Name [Fixed]: g_ioport
      
    Instance "g_uart0 UART Driver on r_sci_uart"
      Name: g_uart0
      Channel: 0
      Baud Rate: 115200
      Data Bits: 8bits
      Parity: None
      Stop Bits: 1bit
      CTS/RTS Selection: RTS (CTS is disabled)
      Name of UART callback function to be defined by user: user_uart_callback
      Name of UART callback function for the RTS external pin control to be defined by user: NULL
      Clock Source: Internal Clock
      Baudrate Clock Output from SCK pin: Disable
      Start bit detection: Falling Edge
      Noise Cancel: Disable
      Bit Rate Modulation Enable: Enable
      Receive Interrupt Priority: Priority 2
      Transmit Interrupt Priority: Priority 2
      Transmit End Interrupt Priority: Priority 2
      Error Interrupt Priority: Priority 2
      
      Instance "g_transfer0 Transfer Driver on r_dtc Event SCI0 TXI"
        Name: g_transfer0
        Mode: Normal
        Transfer Size: 1 Byte
        Destination Address Mode: Fixed
        Source Address Mode: Incremented
        Repeat Area (Unused in Normal Mode): Source
        Interrupt Frequency: After all transfers have completed
        Destination Pointer: NULL
        Source Pointer: NULL
        Number of Transfers: 0
        Number of Blocks (Valid only in Block Mode): 0
        Activation Source (Must enable IRQ): Event SCI0 TXI
        Auto Enable: False
        Callback (Only valid with Software start): NULL
        ELC Software Event Interrupt Priority: Disabled
        
      Instance "g_transfer1 Transfer Driver on r_dtc Event SCI0 RXI"
        Name: g_transfer1
        Mode: Normal
        Transfer Size: 1 Byte
        Destination Address Mode: Incremented
        Source Address Mode: Fixed
        Repeat Area (Unused in Normal Mode): Destination
        Interrupt Frequency: After all transfers have completed
        Destination Pointer: NULL
        Source Pointer: NULL
        Number of Transfers: 0
        Number of Blocks (Valid only in Block Mode): 0
        Activation Source (Must enable IRQ): Event SCI0 RXI
        Auto Enable: False
        Callback (Only valid with Software start): NULL
        ELC Software Event Interrupt Priority: Disabled
        
    Instance "g_fmi0 FMI Driver on r_fmi"
      Name: g_fmi0
      
  Thread "control_thread"
    Symbol: control_thread
    Name: control_thread
    Stack size (bytes): 1024
    Priority: 1
    Auto start: Enabled
    Time slicing interval (ticks): 1
    
  Thread "rs485_comm_thread"
    Symbol: rs485_comm_thread
    Name: rs485_comm_thread
    Stack size (bytes): 1024
    Priority: 1
    Auto start: Enabled
    Time slicing interval (ticks): 1
    
  Thread "blinky_thread"
    Symbol: blinky_thread
    Name: blinky_thread
    Stack size (bytes): 1024
    Priority: 1
    Auto start: Enabled
    Time slicing interval (ticks): 1
    
  Thread "hw_thread"
    Symbol: hw_thread
    Name: hw_thread
    Stack size (bytes): 1024
    Priority: 1
    Auto start: Enabled
    Time slicing interval (ticks): 1
    
    Instance "g_timer2 Timer Driver on r_gpt"
      Name: g_timer2
      Channel: 2
      Mode: Periodic
      Period Value: 5
      Period Unit: Milliseconds
      Duty Cycle Value: 50
      Duty Cycle Unit: Unit Raw Counts
      Auto Start: True
      GTIOCA Output Enabled: False
      GTIOCA Stop Level: Pin Level Low
      GTIOCB Output Enabled: False
      GTIOCB Stop Level: Pin Level Low
      Callback: switch_callback
      Interrupt Priority: Priority 2
      
    Instance "g_adc0 ADC Driver on r_adc"
      Name: g_adc0
      Unit: 0
      Resolution: 12-Bit
      Alignment: Right
      Clear after read: On
      Mode: Single Scan
      Channel Scan Mask: Select channels below
          Channel 0: Use in Normal/Group A
          Channel 1: Use in Normal/Group A
          Channel 2: Unused
          Channel 3: Use in Normal/Group A
          Channel 4: Unused
          Channel 5: Unused
          Channel 6: Unused
          Channel 7 (S3A7/S124 Only): Unused
          Channel 8 (S3A7/S124 Only): Unused
          Channel 9 (S3A7/S124 Only): Unused
          Channel 10 (S3A7/S124 Only): Unused
          Channel 11 (S3A7 Only): Unused
          Channel 12 (S3A7 Only): Unused
          Channel 13 (S3A7 Only): Unused
          Channel 14 (S3A7 Only): Unused
          Channel 15 (S3A7 Only): Unused
          Channel 16: Unused
          Channel 17: Unused
          Channel 18: Unused
          Channel 19: Unused
          Channel 20: Unused
          Channel 21 (Unit 0 Only): Unused
          Channel 22 (S3A7/S124 Only): Unused
          Channel 23 (S3A7 Only): Unused
          Channel 24 (S3A7 Only): Unused
          Channel 25 (S3A7 Only): Unused
          Channel 26 (S3A7 Only): Unused
          Channel 27 (S3A7 Only): Unused
          Temperature Sensor: Unused
          Voltage Sensor: Unused
      Normal/Group A Trigger: Software
      Group B Trigger (Valid only in Group Scan Mode): ELC Event (The only valid trigger for either group in Group Scan Mode)
      Group Priority (Valid only in Group Scan Mode): Group A cannot interrupt Group B
      Add/Average Count: Disabled
      Addition/Averaging Mask: Select channels to perform addition/averaging below
          Channel 0: Disabled
          Channel 1: Disabled
          Channel 2: Disabled
          Channel 3: Disabled
          Channel 4: Disabled
          Channel 5: Disabled
          Channel 6: Disabled
          Channel 7: Disabled
          Channel 8: Disabled
          Channel 9: Disabled
          Channel 10: Disabled
          Channel 11: Disabled
          Channel 12: Disabled
          Channel 13: Disabled
          Channel 14: Disabled
          Channel 15: Disabled
          Channel 16: Disabled
          Channel 17: Disabled
          Channel 18: Disabled
          Channel 19: Disabled
          Channel 20: Disabled
          Channel 21: Disabled
          Channel 22: Disabled
          Channel 23: Disabled
          Channel 24: Disabled
          Channel 25: Disabled
          Channel 26: Disabled
          Channel 27: Disabled
          Temperature Sensor: Disabled
          Voltage Sensor: Disabled
      Sample and Hold Mask: Select channels for which individual sample and hold circuit is to be enabled
          Channel 0: Disabled
          Channel 1: Disabled
          Channel 2: Disabled
      Sample Hold States (Applies only to the 3 channels selected above): 24
      Callback: adc_callback
      Scan End Interrupt Priority: Priority 2
      Scan End Group B Interrupt Priority: Disabled
      
    Instance "g_timer4_pwm Timer Driver on r_gpt"
      Name: g_timer4_pwm
      Channel: 4
      Mode: Periodic
      Period Value: 250
      Period Unit: Microseconds
      Duty Cycle Value: 50
      Duty Cycle Unit: Unit Raw Counts
      Auto Start: False
      GTIOCA Output Enabled: True
      GTIOCA Stop Level: Pin Level Low
      GTIOCB Output Enabled: False
      GTIOCB Stop Level: Pin Level Low
      Callback: pwm4_callback
      Interrupt Priority: Priority 2
      
  Messaging
