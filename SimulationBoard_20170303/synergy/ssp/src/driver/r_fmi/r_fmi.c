/***********************************************************************************************************************
 * Copyright [2015] Renesas Electronics Corporation and/or its licensors. All Rights Reserved.
 *
 * This file is part of Renesas SynergyTM Software Package (SSP)
 *
 * The contents of this file (the "contents") are proprietary and confidential to Renesas Electronics Corporation
 * and/or its licensors ("Renesas") and subject to statutory and contractual protections.
 *
 * This file is subject to a Renesas SSP license agreement. Unless otherwise agreed in an SSP license agreement with
 * Renesas: 1) you may not use, copy, modify, distribute, display, or perform the contents; 2) you may not use any name
 * or mark of Renesas for advertising or publicity purposes or in connection with your use of the contents; 3) RENESAS
 * MAKES NO WARRANTY OR REPRESENTATIONS ABOUT THE SUITABILITY OF THE CONTENTS FOR ANY PURPOSE; THE CONTENTS ARE PROVIDED
 * "AS IS" WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT; AND 4) RENESAS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, OR
 * CONSEQUENTIAL DAMAGES, INCLUDING DAMAGES RESULTING FROM LOSS OF USE, DATA, OR PROJECTS, WHETHER IN AN ACTION OF
 * CONTRACT OR TORT, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THE CONTENTS. Third-party contents
 * included in this file may be subject to different terms.
 **********************************************************************************************************************/

/**********************************************************************************************************************
 * File Name    : r_fmi.c
 * Description  : FMI HAL API
 **********************************************************************************************************************/

/***********************************************************************************************************************
 * Includes
 **********************************************************************************************************************/
#include "r_fmi.h"
#include "r_fmi_cfg.h"
#include "hw/hw_fmi_private.h"
#include "hw/common/hw_fmi_common.h"

/***********************************************************************************************************************
 * Macro definitions
 **********************************************************************************************************************/
/** Macro for error logger. */
#ifndef FMI_ERROR_RETURN
/*LDRA_INSPECTED 77 S This macro does not work when surrounded by parentheses. */
#define FMI_ERROR_RETURN(a, err) SSP_ERROR_RETURN((a), (err), &g_module_name[0], &g_fmi_version)
#endif

/* Compact address bit shift. */
#define FMI_PRIV_COMPACT_ADDR_SHIFT    (8U)

/* Number of channels in compact format. */
#define FMI_PRIV_COMPACT_CHANNEL_COUNT (1U)

/* Value in record_continue if next unit exists. */
#define FMI_PRIV_NEXT_UNIT_EXISTS      (1UL)

/* FMI variant data bit indicating that the IP exists on the die. */
#define FMI_VARIANT_IP_PINNED_OUT_ON_DIE (0x2U)

/***********************************************************************************************************************
 * Typedef definitions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 * Private function prototypes
 **********************************************************************************************************************/

/***********************************************************************************************************************
 * Private global variables
 **********************************************************************************************************************/
#if defined(__GNUC__)

/* This structure is affected by warnings from a GCC compiler bug. This pragma suppresses warnings in this structure 
 * only.*/
/*LDRA_INSPECTED 69 S */
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif
/** Version data structure used by error logger macro. */
static const ssp_version_t g_fmi_version =
{
    .api_version_minor  = FMI_API_VERSION_MINOR,
    .api_version_major  = FMI_API_VERSION_MAJOR,
    .code_version_major = FMI_CODE_VERSION_MAJOR,
    .code_version_minor = FMI_CODE_VERSION_MINOR
};
#if defined(__GNUC__)
/* Restore warning settings for 'missing-field-initializers' to as specified on command line. */
/*LDRA_INSPECTED 69 S */
#pragma GCC diagnostic pop
#endif

const char g_module_name[] = "fmi";

/***********************************************************************************************************************
 * Functions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 * Macro definitions
 **********************************************************************************************************************/

/***********************************************************************************************************************
 * Private Functions
 **********************************************************************************************************************/
static uint32_t r_fmi_size_lookup(fmi_priv_ip_t * pIP, fmi_priv_format_enum_t format);

/***********************************************************************************************************************
 * Functions
 **********************************************************************************************************************/

/*******************************************************************************************************************//**
 * @addtogroup FMI
 * @{
 **********************************************************************************************************************/

/*******************************************************************************************************************//**
 * @brief Initializes factory flash base pointer.
 *
 * @retval SSP_SUCCESS               Factory flash initialization successful.
 **********************************************************************************************************************/
static ssp_err_t R_FMI_Init (void)
{
#if FMI_CFG_USE_CUSTOM_BASE_ADDRESS && defined(FMI_CFG_FACTORY_FLASH_BASE_ADDRESS)
    /** Set factory flash base address.  If a custom base address is provided, calculate checksum and verify it is
     * correct. */
    R_FMI_TABLE = (fmi_priv_table_t const *) FMI_CFG_FACTORY_FLASH_BASE_ADDRESS;
    uint32_t checksum  = 0U;
    uint32_t * p_fmi32 = (uint32_t *) R_FMI_TABLE;
    for (uint32_t i = 0U; i < FMI_TABLE_LENGTH_WORDS; i++)
    {
        checksum += p_fmi32[i];
    }
    /** Checksum (sum of all bytes in factory flash) should equal 0 if data is valid. */

    /** If the checksum calculation fails, use the default base address. The default base address points to data
     * programmed in the Renesas factory.  It is assumed that the checksum would not fail. */
    if (0 != checksum)
    {
        R_FMI_TABLE = (fmi_priv_table_t const *) *R_FMIFRT;
    }

#else /* if FMI_CFG_USE_CUSTOM_BASE_ADDRESS && defined(FMI_CFG_FACTORY_FLASH_BASE_ADDRESS) */
    /** If no custom base address is provided, use the default base address. */
    R_FMI_TABLE = (fmi_priv_table_t const *) *R_FMIFRT;
#endif /* if FMI_CFG_USE_CUSTOM_BASE_ADDRESS && defined(FMI_CFG_FACTORY_FLASH_BASE_ADDRESS) */

    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief        Get pointer to Factory MCU Information first record.
 * @retval        SSP_SUCCESS          Caller's pointer set to Product Information Record.
 * @retval        SSP_ERR_ASSERTION    Caller's pointer is NULL.
 *
 **********************************************************************************************************************/
static ssp_err_t R_FMI_ProductInfoGet (fmi_product_info_t ** pp_product_info)
{
#if FMI_CFG_PARAM_CHECKING_ENABLE
    SSP_ASSERT(NULL != pp_product_info);
#endif

    fmi_header_t * product_info_address;

    product_info_address = HW_FMI_RecordLocate(FMI_PRODUCT_INFORMATION);
    *pp_product_info = (fmi_product_info_t *) product_info_address;

    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief      Get unique ID for this device.
 *
 * @param[out]  p_unique_id  Unique ID stored here
 *
 * @retval      SSP_SUCCESS                Unique ID stored in p_unique_id
 * @retval      SSP_ERR_ASSERTION          p_unique_id was NULL
 **********************************************************************************************************************/
static ssp_err_t R_FMI_UniqueIdGet(fmi_unique_id_t * p_unique_id)
{
#if FMI_CFG_PARAM_CHECKING_ENABLE
    SSP_ASSERT(NULL != p_unique_id);
#endif

    fmi_priv_table_t const * p_reg = *R_FMIFRT;
    fmi_product_info_t * p_product_info = (fmi_product_info_t *) &p_reg->data[p_reg->offset[FMI_PRODUCT_INFORMATION]];

    p_unique_id->unique_id[0] = (uint32_t) p_product_info->unique_id[0];
    p_unique_id->unique_id[1] = (uint32_t) p_product_info->unique_id[4];
    p_unique_id->unique_id[2] = (uint32_t) p_product_info->unique_id[8];
    p_unique_id->unique_id[3] = (uint32_t) p_product_info->unique_id[12];

    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief      Get feature information for the requested feature.
 *
 * @param[in]  p_feature  Pointer to feature definition
 * @param[out] p_info     Feature information stored here
 *
 * @retval      SSP_SUCCESS                Feature information stored in p_info
 * @retval      SSP_ERR_ASSERTION          p_feature was NULL or p_info was NULL
 **********************************************************************************************************************/
static ssp_err_t R_FMI_FeatureGet (ssp_feature_t const * const p_feature, fmi_feature_info_t * const p_info)
{
#if FMI_CFG_PARAM_CHECKING_ENABLE
    SSP_ASSERT(NULL != p_feature);
    SSP_ASSERT(NULL != p_info);
#endif
    ssp_err_t              err = SSP_ERR_INTERNAL;
    fmi_priv_format_enum_t ip_format;
    fmi_priv_ip_t          * pIP;
    pIP = HW_FMI_IPLocate(p_feature->id, &ip_format);
    FMI_ERROR_RETURN(NULL != pIP, SSP_ERR_IP_HARDWARE_NOT_PRESENT);

    /* If unit is used, locate the unit. */
    if (0U != p_feature->unit)
    {
        /* Unit not yet supported in compact format. */
        FMI_ERROR_RETURN((FMI_PRIV_FORMAT_COMPACT != ip_format), SSP_ERR_IP_UNIT_NOT_PRESENT);

        uint32_t unit_countdown = p_feature->unit;

        while (unit_countdown > 0U)
        {
            unit_countdown--;

            /* Make sure the next unit exists. */
            FMI_ERROR_RETURN((FMI_PRIV_NEXT_UNIT_EXISTS == pIP->verbose.record_continue),
                    SSP_ERR_IP_UNIT_NOT_PRESENT);

            /* Increment unit pointer. */
            pIP = (fmi_priv_ip_t *) ((uint32_t) pIP + r_fmi_size_lookup(pIP, ip_format));
            ip_format = (fmi_priv_format_enum_t) pIP->verbose.format;
        }
    }

    switch (ip_format)
    {
        case FMI_PRIV_FORMAT_COMPACT:
        {
            p_info->version_major = pIP->compact.version_major;
            p_info->version_minor = pIP->compact.version_minor;

            /* Determine channel count.  Always 1 in this format */
            p_info->channel_count = FMI_PRIV_COMPACT_CHANNEL_COUNT;

            FMI_ERROR_RETURN(p_feature->channel < p_info->channel_count, SSP_ERR_IP_CHANNEL_NOT_PRESENT);

            /* Calculate peripheral address.  Peripheral address = base address + (compact address << 8). */
            p_info->ptr = (uint32_t *) (fmi_compact_base +
                    (uint32_t) (pIP->compact.addr_16 << FMI_PRIV_COMPACT_ADDR_SHIFT));

            /* In this format, all channels have same variant */
            p_info->variant_data        = pIP->compact.variant_data;
            /* Determine extended data info...N/A in this format */
            p_info->extended_data_count = 0U;
            p_info->ptr_extended_data   = NULL;

            err = SSP_SUCCESS;
            break;
        }

        case FMI_PRIV_FORMAT_VERBOSE_MULTICHANNEL_COMMON_VARIANT:
        case FMI_PRIV_FORMAT_VERBOSE_MULTICHANNEL_MULTI_VARIANT:
        case FMI_PRIV_FORMAT_VERBOSE_MULTICHANNEL_MULTI_VARIANT_MASK:
        {
            /* This block of data is common for verbose formats. */
            p_info->version_major = pIP->verbose.version_major;
            p_info->version_minor = pIP->verbose.version_minor;

            /* Determine channel count */
            p_info->channel_count = pIP->format_verbose.channel_count;

            FMI_ERROR_RETURN(p_feature->channel < p_info->channel_count, SSP_ERR_IP_CHANNEL_NOT_PRESENT);

            /* Calculate peripheral address */
            p_info->ptr = (uint32_t *) (pIP->format_verbose.base_address +
                              (uint32_t) (p_feature->channel * pIP->format_verbose.channel_size));

            /* Grab variant data */
            switch (ip_format)
            {
                default:
                case FMI_PRIV_FORMAT_VERBOSE_MULTICHANNEL_COMMON_VARIANT:
                    /* In this format, all channels have same variant */
                    p_info->variant_data = pIP->verbose.variant_data;
                    break;

                case FMI_PRIV_FORMAT_VERBOSE_MULTICHANNEL_MULTI_VARIANT:
                case FMI_PRIV_FORMAT_VERBOSE_MULTICHANNEL_MULTI_VARIANT_MASK:
                    /* Find the variant information for this channel.  Channel is checked to be valid above. */
                    p_info->variant_data = (uint32_t) pIP->format_verbose.variant_data[p_feature->channel];
                    /* If the variant data is zero this channel is not present */
                    FMI_ERROR_RETURN(0U != p_info->variant_data, SSP_ERR_IP_CHANNEL_NOT_PRESENT);
                    break;
            }

            /* grab extended data */
            switch (ip_format)
            {
                default:
                case FMI_PRIV_FORMAT_VERBOSE_MULTICHANNEL_COMMON_VARIANT:
                case FMI_PRIV_FORMAT_VERBOSE_MULTICHANNEL_MULTI_VARIANT:
                    /* N/A in this format */
                    p_info->extended_data_count = 0U;
                    p_info->ptr_extended_data   = NULL;
                    break;

                case FMI_PRIV_FORMAT_VERBOSE_MULTICHANNEL_MULTI_VARIANT_MASK:
                    /* How much extended data per channel */
                    p_info->extended_data_count = pIP->verbose.contents;
                    /* Find the extended information for this channel */
                    /* Step over attribute data -> ((info->channel_count + 3)/4 bytes per word) */
                    /* Each channel has 1 byte of attribute data.  Attribute data storage is rounded up to the nearest
                     * multiple of 4 bytes (1 word).  The mask array is uint32_t type, so add the number of bytes per word
                     * minus 1, then divide by the number of bytes per word to get the number of words of attribute data. */
                    /* Each channel has info->extended_data_count words of mask data, so index into the mask data using
                     * the channel number multiplied by the number of extended data count words per channel. */
                    p_info->ptr_extended_data =
                        &pIP->format_verbose.mask[((p_info->channel_count + (sizeof(uint32_t) - 1U)) / sizeof(uint32_t))
                                                  + (uint32_t) (p_feature->channel * p_info->extended_data_count)];
                    break;
            }

            return SSP_SUCCESS;
        }
        case FMI_PRIV_FORMAT_VERBOSE_NON_CHANNEL:
        {
            /* This block of data is common for verbose formats. */
            p_info->version_major = pIP->verbose.version_major;
            p_info->version_minor = pIP->verbose.version_minor;

            /* In this format, all channels have same variant */
            p_info->variant_data = pIP->verbose.variant_data;

            /* No channels or variant data in this format. */
            p_info->channel_count = 0U;
            p_info->extended_data_count = 0U;
            p_info->ptr_extended_data = NULL;

            /* Base address from verbose format. */
            p_info->ptr = (uint32_t *) pIP->format_verbose_non_channel_address;

            if (p_info->variant_data & FMI_VARIANT_IP_PINNED_OUT_ON_DIE)
            {
                err = SSP_SUCCESS;
            }
            else
            {
                err = SSP_ERR_IP_UNIT_NOT_PRESENT;
            }
            break;
        }

        case FMI_PRIV_FORMAT_VERBOSE_FLASH:
        {
            /* This block of data is common for verbose formats. */
            p_info->version_major = pIP->verbose.version_major;
            p_info->version_minor = pIP->verbose.version_minor;

            /* In this format, all channels have same variant */
            p_info->variant_data = pIP->verbose.variant_data;

            /* This format has 4 words of data. */
            p_info->channel_count = pIP->verbose.contents;
            p_info->extended_data_count = 4U;
            p_info->ptr = NULL;
            p_info->ptr_extended_data = &(pIP->format_verbose_flash[0].start);
            err = SSP_SUCCESS;
            break;
        }
        case FMI_PRIV_FORMAT_VERBOSE_RAM:
        default:
            err = SSP_ERR_INTERNAL;
    }

    FMI_ERROR_RETURN(SSP_SUCCESS == err, err);
    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief      Get event information for the requested feature and signal.
 *
 * @param[in]  p_feature  Pointer to feature definition
 * @param[in]  signal     Signal associated with p_feature
 * @param[out] p_info     Event information stored here
 *
 * @retval      SSP_SUCCESS                Event information stored in p_info
 * @retval      SSP_ERR_ASSERTION          p_feature was NULL or p_info was NULL
 * @retval      SSP_ERR_IRQ_BSP_DISABLED   Event information could not be found.  p_info::irq is set to
 *                                         SSP_INVALID_VECTOR and p_info::event is set to 0xFF.
 **********************************************************************************************************************/
static ssp_err_t R_FMI_EventInfoGet (ssp_feature_t const * const p_feature,
                                     ssp_signal_t                signal,
                                     fmi_event_info_t * const    p_info)
{
#if FMI_CFG_PARAM_CHECKING_ENABLE
    SSP_ASSERT(NULL != p_feature);
    SSP_ASSERT(NULL != p_info);
#endif

    /* Calculate ELC event. */
    ssp_err_t err = HW_FMI_EventGet(p_feature, signal, &p_info->irq, &p_info->event);

    FMI_ERROR_RETURN(SSP_SUCCESS == err, err);
    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief      Get the driver version based on compile time macros.
 *
 * @retval     SSP_SUCCESS          Caller's structure written.
 * @retval     SSP_ERR_ASSERTION    Caller's pointer is NULL.
 *
 **********************************************************************************************************************/
static ssp_err_t R_FMI_VersionGet (ssp_version_t * const p_version)
{
#if FMI_CFG_PARAM_CHECKING_ENABLE
    SSP_ASSERT(NULL != p_version);
#endif

    p_version->version_id = g_fmi_version.version_id;

    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief      Get the driver version based on compile time macros.
 *
 * @param[in]  pIP         Pointer to the IP information
 * @param[in]  format      Format of the IP information in pIP
 * @return     size of pIP in bytes
 **********************************************************************************************************************/
static uint32_t r_fmi_size_lookup(fmi_priv_ip_t * pIP, fmi_priv_format_enum_t format)
{
    uint32_t size_words = 0U;
    uint32_t channel_count = 0U;
    uint32_t contents = 0U;

    /* Size lookup is based on fmi_priv_ip_t. */
    switch (format)
    {
    case FMI_PRIV_FORMAT_VERBOSE_MULTICHANNEL_COMMON_VARIANT:
    {
        /* sizeof()...always 3 words */
        /* 1 word for fmi_priv_ip_t::verbose. */
        /* 2 words for fmi_priv_ip_t::format_verbose (through fmi_priv_ip_t::format_verbose::base_address,
         * extra variant data array is not used in the common variant format). */
        size_words = 3U;
        break;
    }
    case FMI_PRIV_FORMAT_VERBOSE_MULTICHANNEL_MULTI_VARIANT:
    {
        /* sizeof()... 3 + ((channel_count+3) / 4) words */
        /* 1 word for fmi_priv_ip_t::verbose. */
        /* 2 words for fmi_priv_ip_t::format_verbose (through fmi_priv_ip_t::format_verbose::base_address,
         * extra variant data array size calculated in next comment). */
        /* Each channel has 1 byte of variant data.  Round up to the nearest multiple of 4 bytes (1 word) by adding
         * 3 to the channel count, then dividing by 4. */
        channel_count = pIP->format_verbose.channel_count;
        size_words = 3U + ((channel_count + 3U) / 4);
        break;
    }
    case FMI_PRIV_FORMAT_VERBOSE_MULTICHANNEL_MULTI_VARIANT_MASK:
    {
        /* sizeof()... 3 + ((channel_count+3) / 4) + (channel_count * contents) words */
        /* 1 word for fmi_priv_ip_t::verbose. */
        /* 2 words for fmi_priv_ip_t::format_verbose (through fmi_priv_ip_t::format_verbose::base_address,
         * extra variant data array size calculated in next comment). */
        /* Each channel has 1 byte of variant data.  Round up to the nearest multiple of 4 bytes (1 word) by adding
         * 3 to the channel count, then dividing by 4. */
        /* In this format, each channel has one or more words of mask data.  The number of words of mask data per
         * channel is defined by fmi_priv_ip_t::verbose::contents. */
        channel_count = pIP->format_verbose.channel_count;
        contents = pIP->verbose.contents;
        size_words = 3U + ((channel_count + 3U) / 4) + (channel_count * contents);
        break;
    }
    case FMI_PRIV_FORMAT_VERBOSE_FLASH:
    {
        /* sizeof()... 1 + (4 * contents) words */
        /* 1 word for fmi_priv_ip_t::verbose. */
        /* 4 words per region for start, end, erase, and write. The number of regions is defined in
         * fmi_priv_ip_t::verbose::contents. */
        contents = pIP->verbose.contents;
        size_words = 1U + (4U * contents);
        break;
    }
    case FMI_PRIV_FORMAT_VERBOSE_NON_CHANNEL:
    {
        /* sizeof()... always 2 words */
        /* 1 word for fmi_priv_ip_t::verbose. */
        /* 1 word for fmi_priv_ip_t::format_verbose_non_channel_address. */
        size_words = 2U;
        break;
    }
    case FMI_PRIV_FORMAT_VERBOSE_RAM:
    {
        /* Not yet supported. sizeof()... 1 + ((contents+3) / 4) + (contents * 2)) words */
        /* 1 word for fmi_priv_ip_t::verbose. */
        /* Each region has 1 byte of variant data.  The number of regions is defined in
         * fmi_priv_ip_t::verbose::contents.  Round up to the nearest multiple of 4 bytes (1 word) by adding
         * 3 to the number of regions, then dividing by 4. */
        /* Each region has 2 words for region start and end.  The number of regions is defined in
         * fmi_priv_ip_t::verbose::contents. */
        uint32_t regions = pIP->verbose.contents;
        size_words = 1U + ((regions + 3U) / 4) + (regions * 2U);
        break;
    }
    default:
        break;
    }

    return size_words * sizeof(uint32_t);
}

/***********************************************************************************************************************
 * Global Variables
 **********************************************************************************************************************/
const fmi_api_t g_fmi_on_fmi =
{
    .init              = R_FMI_Init,
    .productInfoGet    = R_FMI_ProductInfoGet,
    .productFeatureGet = R_FMI_FeatureGet,
    .versionGet        = R_FMI_VersionGet,
    .eventInfoGet      = R_FMI_EventInfoGet,
    .uniqueIdGet       = R_FMI_UniqueIdGet
};

/*******************************************************************************************************************//**
 * @} (end defgroup FMI)
 **********************************************************************************************************************/
