/***********************************************************************************************************************
 * Copyright [2015] Renesas Electronics Corporation and/or its licensors. All Rights Reserved.
 * 
 * This file is part of Renesas SynergyTM Software Package (SSP)
 *
 * The contents of this file (the "contents") are proprietary and confidential to Renesas Electronics Corporation
 * and/or its licensors ("Renesas") and subject to statutory and contractual protections.
 *
 * This file is subject to a Renesas SSP license agreement. Unless otherwise agreed in an SSP license agreement with
 * Renesas: 1) you may not use, copy, modify, distribute, display, or perform the contents; 2) you may not use any name
 * or mark of Renesas for advertising or publicity purposes or in connection with your use of the contents; 3) RENESAS
 * MAKES NO WARRANTY OR REPRESENTATIONS ABOUT THE SUITABILITY OF THE CONTENTS FOR ANY PURPOSE; THE CONTENTS ARE PROVIDED
 * "AS IS" WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT; AND 4) RENESAS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, OR
 * CONSEQUENTIAL DAMAGES, INCLUDING DAMAGES RESULTING FROM LOSS OF USE, DATA, OR PROJECTS, WHETHER IN AN ACTION OF
 * CONTRACT OR TORT, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THE CONTENTS. Third-party contents
 * included in this file may be subject to different terms.
 **********************************************************************************************************************/

/**********************************************************************************************************************
 * File Name    : r_sci_uart.c
 * Description  : UART on SCI HAL driver
 **********************************************************************************************************************/

/***********************************************************************************************************************
 * Includes
 **********************************************************************************************************************/
#include "bsp_api.h"
#include "r_sci_uart.h"
#include "hw/hw_sci_uart_private.h"
#include "r_sci_uart_private_api.h"
#include "r_cgc.h"
#include "hw/hw_sci_common.h"

/***********************************************************************************************************************
 * Macro definitions
 **********************************************************************************************************************/
#ifndef SCI_UART_ERROR_RETURN
/*LDRA_INSPECTED 77 S This macro does not work when surrounded by parentheses. */
#define SCI_UART_ERROR_RETURN(a, err) SSP_ERROR_RETURN((a), (err), &g_module_name[0], &module_version)
#endif

/** SCI FIFO depth is defined by b2:3 of the variant data.  FIFO depth values defined in g_sci_uart_fifo_depth. */
#define SCI_UART_VARIANT_FIFO_DEPTH_MASK    (0x0CU)
#define SCI_UART_VARIANT_FIFO_DEPTH_SHIFT   (2U)

#define NUM_DIVISORS_ASYNC      (13U)

/***********************************************************************************************************************
 * Typedef definitions
 **********************************************************************************************************************/


/***********************************************************************************************************************
 * Private function prototypes
 **********************************************************************************************************************/
#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
static ssp_err_t r_sci_uart_open_param_check  (sci_uart_instance_ctrl_t const * const p_ctrl, uart_cfg_t const * const p_cfg);

static ssp_err_t r_sci_read_write_param_check (sci_uart_instance_ctrl_t const * const p_ctrl,
                                               uint8_t const * const     addr,
                                               transfer_instance_t const * const p_transfer,
                                               uint32_t const            bytes);
#endif /* #if (SCI_UART_CFG_PARAM_CHECKING_ENABLE) */

static ssp_err_t    r_sci_uart_config_set        (R_SCI0_Type * p_sci_reg, uart_cfg_t const * const p_cfg);

static ssp_err_t    r_sci_uart_transfer_open     (uart_cfg_t const * const p_cfg);

static ssp_err_t    r_sci_uart_baud_set          (R_SCI0_Type * p_sci_reg, sci_clk_src_t clk_src, uint32_t baudrate);

static void r_sci_uart_fifo_reset (R_SCI0_Type * p_sci_reg, uart_cfg_t const * const p_cfg, uint8_t fifo_depth);

static void r_sci_uart_fifo_enable (R_SCI0_Type * p_sci_reg);

static inline ssp_err_t r_sci_irq_cfg(ssp_feature_t * p_feature, ssp_signal_t signal, uint8_t ipl,
        void * p_ctrl, IRQn_Type * p_irq);

static inline void r_sci_close_hardware (sci_uart_instance_ctrl_t * p_ctrl);

#if (SCI_UART_CFG_EXTERNAL_RTS_OPERATION)
static void r_sci_uart_external_rts_operation_enable (sci_uart_instance_ctrl_t * const p_ctrl, uart_cfg_t const * const p_cfg);
#endif

void sci_uart_rxi_isr (void);
void sci_uart_txi_isr (void);
void sci_uart_tei_isr (void);
void sci_uart_eri_isr (void);


/***********************************************************************************************************************
 * Private global variables
 **********************************************************************************************************************/
/** Name of module used by error logger macro */
#if BSP_CFG_ERROR_LOG != 0
static const char g_module_name[] = "sci_uart";
#endif
/** Baud rate divisor information(UART mode) */
static const baud_setting_t async_baud[NUM_DIVISORS_ASYNC] =
{
    {   6U,  0U,  0U,  1U,  0U }, /* divisor, BGDM, ABCS, ABCSE, n */
    {   8U,  1U,  1U,  0U,  0U },
    {  16U,  0U,  1U,  0U,  0U },
    {  24U,  0U,  0U,  1U,  1U },
    {  32U,  0U,  0U,  0U,  0U },
    {  64U,  0U,  1U,  0U,  1U },
    {  96U,  0U,  0U,  1U,  2U },
    { 128U,  0U,  0U,  0U,  1U },
    { 256U,  0U,  1U,  0U,  2U },
    { 384U,  0U,  0U,  1U,  3U },
    { 512U,  0U,  0U,  0U,  2U },
    { 1024U, 0U,  1U,  0U,  3U },
    { 2048U, 0U,  0U,  0U,  3U }
};

/** FIFO depth values, use variant data b2:3 as index. */
static const uint8_t g_sci_uart_fifo_depth[] =
{
    0U, 4U, 8U, 16U
};

#if defined(__GNUC__)
/* This structure is affected by warnings from a GCC compiler bug. This pragma suppresses the warnings in this
 * structure only.*/
/*LDRA_INSPECTED 69 S */
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif
/** SCI UART HAL module version data structure */
static const ssp_version_t module_version =
{
    .api_version_minor  = UART_API_VERSION_MINOR,
    .api_version_major  = UART_API_VERSION_MAJOR,
    .code_version_major = SCI_UART_CODE_VERSION_MAJOR,
    .code_version_minor = SCI_UART_CODE_VERSION_MINOR
};
#if defined(__GNUC__)
/* Restore warning settings for 'missing-field-initializers' to as specified on command line. */
/*LDRA_INSPECTED 69 S */
#pragma GCC diagnostic pop
#endif

/** UART on SCI HAL API mapping for UART interface */
const uart_api_t  g_uart_on_sci =
{
    .open       = R_SCI_UartOpen,
    .close      = R_SCI_UartClose,
#if (SCI_UART_CFG_TX_ENABLE)
    .write      = R_SCI_UartWrite,
#else
    .write      = NULL,
#endif
#if (SCI_UART_CFG_RX_ENABLE)
    .read       = R_SCI_UartRead,
#else
    .read       = NULL,
#endif
    .infoGet    = R_SCI_UartInfoGet,
    .baudSet    = R_SCI_UartBaudSet,
    .versionGet = R_SCI_UartVersionGet
};

/*******************************************************************************************************************//**
 * @addtogroup UARTonSCI
 * @{
 **********************************************************************************************************************/

/***********************************************************************************************************************
 * Functions
 **********************************************************************************************************************/

/*******************************************************************************************************************//**
 * @retval  SSP_SUCCESS                  Channel opened successfully.
 * @retval  SSP_ERR_IN_USE               Channel already in use.
 * @retval  SSP_ERR_ASSERTION            Pointer to UART control block or configuration structure is NULL.
 * @retval  SSP_ERR_HW_LOCKED            Channel is locked.
 * @retval  SSP_ERR_INVALID_MODE         Channel is used for non-UART mode or illegal mode is set.
 * @retval  SSP_ERR_INVALID_ARGUMENT     Invalid parameter setting found in the configuration structure.
 * @note This function is reentrant.
 **********************************************************************************************************************/
ssp_err_t R_SCI_UartOpen (uart_ctrl_t * const p_api_ctrl, uart_cfg_t const * const p_cfg)
{
    ssp_err_t         err = SSP_SUCCESS;
    uart_on_sci_cfg_t * pextend;

    sci_uart_instance_ctrl_t * p_ctrl = (sci_uart_instance_ctrl_t *) p_api_ctrl;
#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
    /** Check parameters. */
    err = r_sci_uart_open_param_check(p_ctrl, p_cfg);        /** check arguments */
    SCI_UART_ERROR_RETURN((SSP_SUCCESS == err), err);
#endif

    ssp_feature_t ssp_feature;
    ssp_feature.channel = p_cfg->channel;
    ssp_feature.unit = 0U;
    ssp_feature.id = SSP_IP_SCI;
    fmi_feature_info_t info;
    err = g_fmi_on_fmi.productFeatureGet(&ssp_feature, &info);
    SCI_UART_ERROR_RETURN(SSP_SUCCESS == err, err);
    p_ctrl->p_reg = info.ptr;
    R_SCI0_Type * p_sci_reg = (R_SCI0_Type *) p_ctrl->p_reg;
    uint32_t fifo_depth_index = (uint32_t) (info.variant_data & SCI_UART_VARIANT_FIFO_DEPTH_MASK)
            >> SCI_UART_VARIANT_FIFO_DEPTH_SHIFT;
    p_ctrl->fifo_depth = g_sci_uart_fifo_depth[fifo_depth_index];

    err = r_sci_irq_cfg(&ssp_feature, SSP_SIGNAL_SCI_RXI, p_cfg->rxi_ipl, p_ctrl, &p_ctrl->rxi_irq);
    SCI_UART_ERROR_RETURN(SSP_SUCCESS == err, err);
    err = r_sci_irq_cfg(&ssp_feature, SSP_SIGNAL_SCI_TXI, p_cfg->txi_ipl, p_ctrl, &p_ctrl->txi_irq);
    SCI_UART_ERROR_RETURN(SSP_SUCCESS == err, err);
    err = r_sci_irq_cfg(&ssp_feature, SSP_SIGNAL_SCI_TEI, p_cfg->tei_ipl, p_ctrl, &p_ctrl->tei_irq);
    SCI_UART_ERROR_RETURN(SSP_SUCCESS == err, err);
    err = r_sci_irq_cfg(&ssp_feature, SSP_SIGNAL_SCI_ERI, p_cfg->eri_ipl, p_ctrl, &p_ctrl->eri_irq);
    SCI_UART_ERROR_RETURN(SSP_SUCCESS == err, err);

    pextend = (uart_on_sci_cfg_t *) p_cfg->p_extend;

    /** lock specified SCI channel */
    err = R_BSP_HardwareLock(&ssp_feature);
    SCI_UART_ERROR_RETURN((SSP_SUCCESS == err), err);

    p_ctrl->p_transfer_rx = p_cfg->p_transfer_rx;
    p_ctrl->p_transfer_tx = p_cfg->p_transfer_tx;

    err = r_sci_uart_transfer_open(p_cfg);
    SCI_UART_ERROR_RETURN(SSP_SUCCESS == err, err);

    R_BSP_ModuleStart(&ssp_feature);                        /** applies power to channel */
    HW_SCI_RegisterReset(p_sci_reg);                        /** sets registers to reset values */
    HW_SCI_TransmitterLevelSet(p_sci_reg, 1U);              /** set default level of TX pin to 1 */

    /** Disable interrupts. */
    p_ctrl->channel = p_cfg->channel;
    HW_SCI_RxIrqDisable(p_sci_reg, p_ctrl);
    HW_SCI_TxIrqDisable(p_sci_reg, p_ctrl);
    HW_SCI_TeIrqDisable(p_sci_reg, p_ctrl);

    HW_SCI_ReceiverDisable(p_sci_reg);                     /** disables receiver */
    HW_SCI_TransmitterDisable(p_sci_reg);                  /** disables transmitter */

    if (p_ctrl->fifo_depth > 0U)
    {
        r_sci_uart_fifo_reset(p_sci_reg, p_cfg, p_ctrl->fifo_depth);                      /** configure FIFO related registers */
    }
    else
    {
        HW_SCI_FifoDisable (p_sci_reg);               /**< disables FIFO mode */
    }

    if ((pextend) && (pextend->rx_edge_start))
    {
        HW_SCI_StartBitFallingEdgeSet(p_sci_reg);     /** starts reception when RXD has falling edge */
    }
    else
    {
        HW_SCI_StartBitLowLevelSet(p_sci_reg);        /** starts reception when RXD becomes low level */
    }

    if ((pextend) && (pextend->noisecancel_en))
    {
        HW_SCI_NoiseFilterSet(p_sci_reg, NOISE_CANCEL_LVL1);          /** enables the noise cancellation, the effect level should
                                                            *   be fixed to the minimum */
    }
    else
    {
        HW_SCI_NoiseFilterClear(p_sci_reg);           /** disables the noise cancellation */
    }

    if ((pextend) && (pextend->bitrate_modulation))
    {
        HW_SCI_BitRateModulationEnable(p_sci_reg, true);          /** enables the bitrate modulation function */

    }
    else
    {
        HW_SCI_BitRateModulationEnable(p_sci_reg, false);         /** disables the bitrate modulation function */
    }

    err = r_sci_uart_config_set(p_sci_reg, p_cfg);                    /** configure UART related registers */

    if (err != SSP_SUCCESS)
    {
#if (SCI_UART_CFG_RX_ENABLE)
        if (NULL != p_ctrl->p_transfer_rx)
        {
            p_ctrl->p_transfer_rx->p_api->close(p_ctrl->p_transfer_rx->p_ctrl);
        }
#endif
#if (SCI_UART_CFG_TX_ENABLE)
        if (NULL != p_ctrl->p_transfer_tx)
        {
            p_ctrl->p_transfer_tx->p_api->close(p_ctrl->p_transfer_tx->p_ctrl);
        }
#endif
        HW_SCI_StartBitLowLevelSet(p_sci_reg);        /** set default setting */
        R_BSP_ModuleStop(&ssp_feature);               /** removes power to channel */
        R_BSP_HardwareUnlock(&ssp_feature);
        return err;
    }

    if (p_ctrl->fifo_depth > 0U)
    {
        r_sci_uart_fifo_enable(p_sci_reg);                         /** configure FIFO related registers */
    }


#if (SCI_UART_CFG_RX_ENABLE)
    HW_SCI_RXIeventSelect(p_sci_reg);             /** selects RXI when detecting a reception data ready */
#endif

    p_ctrl->channel                          = p_cfg->channel;
    p_ctrl->p_context                        = p_cfg->p_context;  /** saves UART device context inside SCI HAL
                                                                   * driver */
    p_ctrl->p_callback                       = p_cfg->p_callback; /** registers callback function from higher layer */
    p_ctrl->p_tx_src                         = NULL;
    p_ctrl->tx_src_bytes                     = 0U;
    p_ctrl->rx_transfer_in_progress          = false;

#if (SCI_UART_CFG_RX_ENABLE)
    HW_SCI_ReceiverEnable(p_sci_reg);                      /** enables receiver */
    HW_SCI_RxIrqEnable(p_sci_reg, p_ctrl);
#endif

    /** Transmitter and its interrupt are enabled in R_SCI_UartWrite() */

#if (SCI_UART_CFG_EXTERNAL_RTS_OPERATION)
    r_sci_uart_external_rts_operation_enable(p_ctrl, p_cfg);            /** configure FIFO related registers */
#endif

    return SSP_SUCCESS;
}  /* End of function R_SCI_UartOpen() */

/*******************************************************************************************************************//**
 * @retval  SSP_SUCCESS              Channel successfully closed.
 * @retval  SSP_ERR_ASSERTION        Pointer to UART control block is NULL.
 * @note This function is reentrant.
 **********************************************************************************************************************/
ssp_err_t R_SCI_UartClose (uart_ctrl_t * const p_api_ctrl)
{
    sci_uart_instance_ctrl_t * p_ctrl = (sci_uart_instance_ctrl_t *) p_api_ctrl;
#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
    SSP_ASSERT(p_ctrl);
#endif

    /** Close transfer interface. */
#if (SCI_UART_CFG_RX_ENABLE)
    if (NULL != p_ctrl->p_transfer_rx)
    {
#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
        SSP_ASSERT(NULL != p_ctrl->p_transfer_rx->p_api);
        SSP_ASSERT(NULL != p_ctrl->p_transfer_rx->p_ctrl);
#endif
        p_ctrl->p_transfer_rx->p_api->close(p_ctrl->p_transfer_rx->p_ctrl);
    }
#endif
#if (SCI_UART_CFG_TX_ENABLE)
    if (NULL != p_ctrl->p_transfer_tx)
    {
#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
        SSP_ASSERT(NULL != p_ctrl->p_transfer_tx->p_api);
        SSP_ASSERT(NULL != p_ctrl->p_transfer_tx->p_ctrl);
#endif
        p_ctrl->p_transfer_tx->p_api->close(p_ctrl->p_transfer_tx->p_ctrl);
    }
#endif

    /** clears control block parameters */
    p_ctrl->p_callback   = NULL;

    /** disables the associated interrupts<br>
     *  disables receiver<br>
     *  disables transmitter<br>
     *  removes power to the SCI channel<br>
     *  unlocks specified SCI channel<br>
     *  clears device context<br>
     */
    r_sci_close_hardware(p_ctrl);

    return SSP_SUCCESS;
}  /* End of function R_SCI_UartClose() */

#if (SCI_UART_CFG_RX_ENABLE)

/*******************************************************************************************************************//**
 * @retval  SSP_SUCCESS                  Data reception successfully ends.
 * @retval  SSP_ERR_HW_LOCKED            Channel is locked.
 * @retval  SSP_ERR_ASSERTION            Pointer to UART control block is NULL.
 * @retval  SSP_ERR_INVALID_MODE         Channel is used for non-UART mode.
 * @retval  SSP_ERR_INVALID_ARGUMENT     Destination address or data size is invalid against data length.
 * @note This function is reentrant. This API is only valid when SCI_UART_CFG_RX_ENABLE is enabled.
 *       If 9-bit data length is specified at R_SCI_UartOpen call, dest must be aligned 16-bit boundary.
 **********************************************************************************************************************/
ssp_err_t R_SCI_UartRead (uart_ctrl_t * const p_api_ctrl, uint8_t const * const p_dest, uint32_t const bytes)
{
    sci_uart_instance_ctrl_t * p_ctrl = (sci_uart_instance_ctrl_t *) p_api_ctrl;
    ssp_err_t err        = SSP_SUCCESS;
    uint32_t  data_bytes = 1;

#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
    /** checks arguments */
    err = r_sci_read_write_param_check(p_ctrl, p_dest, p_ctrl->p_transfer_rx, bytes);
    SCI_UART_ERROR_RETURN((SSP_SUCCESS == err), err);
#endif

    if (0U == bytes)
    {
        /** Nothing to do. */
        return SSP_SUCCESS;
    }

    /** checks data byte length */
    R_SCI0_Type * p_sci_reg = (R_SCI0_Type *) p_ctrl->p_reg;
    if (HW_SCI_IsDataLength9bits(p_sci_reg))
    {
        data_bytes = 2U;               /* data length per data entry is 2byte if 9bits data length */
    }

    /** Read from SCI channel. */
    if (NULL != p_ctrl->p_transfer_rx)
    {
        uint32_t size = bytes / data_bytes;
        if(0xFFFFU < size)
        {
            size = 0xFFFFU;
        }
        /* Cast to a value acceptable by the transfer interface. */
        uint8_t const * p_src = (uint8_t const *) HW_SCI_ReadAddrGet(p_sci_reg, data_bytes);
        p_ctrl->rx_transfer_in_progress = true;
        err = p_ctrl->p_transfer_rx->p_api->reset(p_ctrl->p_transfer_rx->p_ctrl, p_src, (void *) p_dest, (uint16_t)size);
        SCI_UART_ERROR_RETURN(SSP_SUCCESS == err, err);
    }
    else
    {
        /** Do nothing in non-transfer case.  Bytes will come in through the callback. */
    }

    return err;
}  /* End of function R_SCI_UartRead() */
#endif /* if (SCI_UART_CFG_RX_ENABLE) */

#if (SCI_UART_CFG_TX_ENABLE)

/*******************************************************************************************************************//**
 * @retval  SSP_SUCCESS                  Data transmission finished successfully.
 * @retval  SSP_ERR_ASSERTION            Pointer to UART control block is NULL.
 * @retval  SSP_ERR_INVALID_MODE         Channel is used for non-UART mode or illegal mode is set in handle.
 * @retval  SSP_ERR_INVALID_ARGUMENT     Source address or data size is invalid against data length.
 * @retval  SSP_ERR_HW_LOCKED            Could not lock hardware.
 * @note This function is reentrant.
 *       If 9-bit data length is specified at R_SCI_UartOpen call, the source must be aligned on a 16-bit boundary.
 **********************************************************************************************************************/
ssp_err_t R_SCI_UartWrite (uart_ctrl_t * const p_api_ctrl, uint8_t const * const p_src, uint32_t const bytes)
{
    sci_uart_instance_ctrl_t * p_ctrl = (sci_uart_instance_ctrl_t *) p_api_ctrl;
    ssp_err_t err        = SSP_SUCCESS;
    uint32_t  data_bytes = 1;

#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
    /** checks arguments */
    err = r_sci_read_write_param_check(p_ctrl, p_src, p_ctrl->p_transfer_tx, bytes);
    if (SSP_SUCCESS != err)
    {
        return err;
    }
    SCI_UART_ERROR_RETURN(NULL == p_ctrl->p_tx_src, SSP_ERR_IN_USE);
#endif

    R_SCI0_Type * p_sci_reg = (R_SCI0_Type *) p_ctrl->p_reg;
    if (HW_SCI_IsDataLength9bits(p_sci_reg))
    {
        data_bytes = 2U;
    }

    if ((p_ctrl->p_transfer_tx) && (bytes > 1U))
    {
        /** Configure a transfer to transfer all but the last byte.  The last byte is sent in the ISR to enable the
         * transmit end ISR, which does not work if the last byte is sent with DMAC or DTC. */

        /** Store last character to transmit from ISR. */
        p_ctrl->p_tx_src = &p_src[bytes - data_bytes];
        p_ctrl->tx_src_bytes = data_bytes;

        /* Cast the register to a value acceptable to the transfer interface. */
        uint8_t * p_dest = (uint8_t *) HW_SCI_WriteAddrGet(p_sci_reg, data_bytes);
        uint32_t num_transfers = (bytes / data_bytes) - 1U;
        if(0xFFFFU < num_transfers)
        {
            num_transfers = 0xFFFFU;
        }
        uint8_t const * p_transfer_src = p_src;
        err = p_ctrl->p_transfer_tx->p_api->reset(p_ctrl->p_transfer_tx->p_ctrl, p_transfer_src, p_dest, (uint16_t)num_transfers);
        SCI_UART_ERROR_RETURN(SSP_SUCCESS == err, err);
    }
    else
    {
        /** All bytes will be transmitted from ISR. */
        p_ctrl->p_tx_src = p_src;
        p_ctrl->tx_src_bytes = bytes;
    }

    HW_SCI_TransmitterEnable(p_sci_reg);          /** enables transmitter */
    HW_SCI_TxIrqEnable(p_sci_reg, p_ctrl);

    if (0U == p_ctrl->fifo_depth)
    {
        uint16_t first_data = 0U;
        p_ctrl->p_tx_src = p_src + data_bytes;
        first_data = (uint16_t)(*(p_src + 0));
        /* Non FIFO case */
        /** writes first byte(s) to data register directly below step (non-FIFO mode procedure)
         * This is to generate TXI interrupt first and remaining bytes will be transfered in the TX handler */
        if (HW_SCI_IsDataLength9bits (p_sci_reg))
        {
            HW_SCI_Write9bits (p_sci_reg, first_data);
        }
        else
        {
            HW_SCI_Write (p_sci_reg, (uint8_t)first_data);
        }
    }
    err = SSP_SUCCESS;

    return err;
}  /* End of function R_SCI_UartWrite() */
#endif /* if (SCI_UART_CFG_TX_ENABLE) */

/*******************************************************************************************************************//**
 * @retval  SSP_SUCCESS                  Baud rate was successfully changed.
 * @retval  SSP_ERR_ASSERTION            Pointer to UART control block is NULL.
 * @retval  SSP_ERR_INVALID_ARGUMENT     Illegal baud rate value is specified.
 * @retval  SSP_ERR_HW_LOCKED            Could not lock hardware.
 * @note This function is reentrant. Clock source cannot be changed by this API, need to open again if it is needed.
 **********************************************************************************************************************/
ssp_err_t R_SCI_UartBaudSet (uart_ctrl_t * const p_api_ctrl, uint32_t const baudrate)
{
    sci_uart_instance_ctrl_t * p_ctrl = (sci_uart_instance_ctrl_t *) p_api_ctrl;
    ssp_err_t     err = SSP_SUCCESS;
    sci_clk_src_t clk_src;

#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
    SSP_ASSERT(p_ctrl);
#endif

    /** disables interrupts */
    R_SCI0_Type * p_sci_reg = (R_SCI0_Type *) p_ctrl->p_reg;
    HW_SCI_TxIrqDisable(p_sci_reg, p_ctrl);
    HW_SCI_RxIrqDisable(p_sci_reg, p_ctrl);
    HW_SCI_TeIrqDisable(p_sci_reg, p_ctrl);

    /** disables transmitter. This API does not resume transmission but terminate it */
    HW_SCI_TransmitterDisable(p_sci_reg);

    /** disables receiver */
    HW_SCI_ReceiverDisable(p_sci_reg);

    /** sets baud-rate related registers */
    if (HW_SCI_IsBaudRateInternalClkSelected(p_sci_reg))
    {
        clk_src = SCI_CLK_SRC_INT;
    }
    else
    {
        if (HW_SCI_IsBaudRateGenClkDivideBy8Selected(p_sci_reg))
        {
            clk_src = SCI_CLK_SRC_EXT8X;
        }
        else
        {
            clk_src = SCI_CLK_SRC_EXT16X;
        }
    }

    err = r_sci_uart_baud_set(p_sci_reg, clk_src, baudrate);

    /** enables receiver */
    HW_SCI_ReceiverEnable(p_sci_reg);

    /** enables interrupts */
    HW_SCI_RxIrqEnable(p_sci_reg, p_ctrl);

    return err;
}  /* End of function R_SCI_UartBaudSet() */

/*******************************************************************************************************************//**
 * @retval  SSP_SUCCESS                  Baud rate was successfully changed.
 * @retval  SSP_ERR_ASSERTION            Pointer to UART control block is NULL.
 * @note This function is reentrant.
 **********************************************************************************************************************/
ssp_err_t R_SCI_UartInfoGet (uart_ctrl_t * const p_api_ctrl, uart_info_t * const p_info)
{
    sci_uart_instance_ctrl_t * p_ctrl = (sci_uart_instance_ctrl_t *) p_api_ctrl;
    ssp_err_t     err = SSP_SUCCESS;

#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
    SSP_ASSERT(p_ctrl);
    SSP_ASSERT(p_info);
#endif

    p_info->read_bytes_max = 0U;
    p_info->write_bytes_max = 0U;

    if (NULL != p_ctrl->p_transfer_rx)
    {
#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
        SSP_ASSERT(p_ctrl->p_transfer_rx->p_api);
        SSP_ASSERT(p_ctrl->p_transfer_rx->p_api->infoGet);
#endif
        transfer_properties_t properties;
        err = p_ctrl->p_transfer_rx->p_api->infoGet(p_ctrl->p_transfer_rx->p_ctrl, &properties);
        SCI_UART_ERROR_RETURN(SSP_SUCCESS == err, err);

        p_info->read_bytes_max = properties.transfer_length_max;
    }
    else
    {
        /* No limit to number of bytes to read. */
        p_info->read_bytes_max = 0xFFFFFFFFUL;
    }

    if (NULL != p_ctrl->p_transfer_tx)
    {
#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
        SSP_ASSERT(p_ctrl->p_transfer_tx->p_api);
        SSP_ASSERT(p_ctrl->p_transfer_tx->p_api->infoGet);
#endif
        transfer_properties_t properties;
        err = p_ctrl->p_transfer_tx->p_api->infoGet(p_ctrl->p_transfer_tx->p_ctrl, &properties);
        SCI_UART_ERROR_RETURN(SSP_SUCCESS == err, err);

        p_info->write_bytes_max = properties.transfer_length_max;
    }
    else
    {
        /* No limit to number of bytes to write. */
        p_info->write_bytes_max = 0xFFFFFFFFUL;
    }

    return err;
}  /* End of function R_SCI_UartInfoGet() */

/*******************************************************************************************************************//**
 * @retval   Version number
 * @note This function is reentrant.
 **********************************************************************************************************************/
ssp_err_t R_SCI_UartVersionGet (ssp_version_t * p_version)
{
    *p_version = module_version;
    return SSP_SUCCESS;
} /* End of function R_SCI_UartVersionGet() */

/*******************************************************************************************************************//**
 * @} (end addtogroup UARTonSCI)
 **********************************************************************************************************************/

/***********************************************************************************************************************
 * Private Functions
 **********************************************************************************************************************/
#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)

/*******************************************************************************************************************//**
 * Parameter error check function for open processing
 * @param[in] p_ctrl   Pointer to the control block for the channel
 * @param[in] p_cfg    Pointer to the configuration structure specific to UART mode
 * @retval  SSP_SUCCESS                  No parameter error found
 * @retval  SSP_ERR_IN_USE               Channel already in use
 * @retval  SSP_ERR_ASSERTION            Pointer to UART control block or configuration structure is NULL
 * @retval  SSP_ERR_INVALID_ARGUMENT     Invalid parameter setting found in the configuration structure
 **********************************************************************************************************************/
static ssp_err_t r_sci_uart_open_param_check (sci_uart_instance_ctrl_t const * const p_ctrl, uart_cfg_t const * const p_cfg)
{
    ssp_err_t err = SSP_SUCCESS;

    SSP_ASSERT(p_ctrl);
    SSP_ASSERT(p_cfg);

    SCI_UART_ERROR_RETURN((((UART_DATA_BITS_7  == p_cfg->data_bits) || (UART_DATA_BITS_8  == p_cfg->data_bits))
                           || (UART_DATA_BITS_9  == p_cfg->data_bits)), SSP_ERR_INVALID_ARGUMENT);

    SCI_UART_ERROR_RETURN(((UART_STOP_BITS_1 == p_cfg->stop_bits) || (UART_STOP_BITS_2 == p_cfg->stop_bits))
                          , SSP_ERR_INVALID_ARGUMENT);

    SCI_UART_ERROR_RETURN((((UART_PARITY_OFF  == p_cfg->parity) || (UART_PARITY_EVEN == p_cfg->parity))
                           || (UART_PARITY_ODD  == p_cfg->parity)), SSP_ERR_INVALID_ARGUMENT);

    if (p_cfg->p_extend)
    {
        if (SCI_CLK_SRC_INT == ((uart_on_sci_cfg_t *) (p_cfg->p_extend))->clk_src)
        {
            SCI_UART_ERROR_RETURN((0U != p_cfg->baud_rate), SSP_ERR_INVALID_ARGUMENT);
        }
        else if ((SCI_CLK_SRC_EXT8X  == ((uart_on_sci_cfg_t *) (p_cfg->p_extend))->clk_src)
                 || (SCI_CLK_SRC_EXT16X == ((uart_on_sci_cfg_t *) (p_cfg->p_extend))->clk_src)
                 )
        {
            /* No error */
        }
        else
        {
            return SSP_ERR_INVALID_ARGUMENT;
        }
    }

    return err;
}  /* End of function r_sci_uart_open_param_check() */

/*******************************************************************************************************************//**
 * Parameter error check function for read/write processing
 * @param[in] p_ctrl Pointer to the control block for the channel
 * @param[in] addr   Pointer to the buffer
 * @param[in] size   Data size
 * @retval  SSP_SUCCESS              No parameter error found
 * @retval  SSP_ERR_ASSERTION        Pointer to UART control block or configuration structure is NULL
 * @retval  SSP_ERR_INVALID_MODE     Channel is used for non-UART mode or illegal mode is set
 * @retval  SSP_ERR_INVALID_ARGUMENT Address is not aligned to 2-byte boundary or size is the odd number when the data
 * length is 9-bit
 * @note This function is reentrant.
 **********************************************************************************************************************/
static ssp_err_t r_sci_read_write_param_check (sci_uart_instance_ctrl_t const * const p_ctrl,
                                               uint8_t const * const     addr,
                                               transfer_instance_t const * const p_transfer,
                                               uint32_t const            bytes)
{
    SSP_ASSERT(p_ctrl);
    SSP_ASSERT(addr);

    R_SCI0_Type * p_sci_reg = (R_SCI0_Type *) p_ctrl->p_reg;
    if (HW_SCI_IsDataLength9bits(p_sci_reg))
    {
        /* Do not allow odd buffer address if data length is 9bits. */
        SCI_UART_ERROR_RETURN((0 == ((uint32_t) addr % 2)), SSP_ERR_INVALID_ARGUMENT);

        /* Do not allow odd number of data size if data length is 9bits. */
        SCI_UART_ERROR_RETURN((0 == (bytes % 2)), SSP_ERR_INVALID_ARGUMENT);
    }

    if (NULL != p_transfer)
    {
#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
        SSP_ASSERT(NULL != p_ctrl->p_transfer_rx->p_api);
        SSP_ASSERT(NULL != p_ctrl->p_transfer_rx->p_ctrl);
#endif
    }

    return SSP_SUCCESS;
}  /* End of function r_sci_read_write_param_check() */
#endif /* if (SCI_UART_CFG_PARAM_CHECKING_ENABLE) */

/*******************************************************************************************************************//**
 * Configures UART related transfer driverss (if enabled).
 * @param[in]     p_cfg   Pointer to UART specific configuration structure
 * @retval        none
 **********************************************************************************************************************/
static ssp_err_t    r_sci_uart_transfer_open     (uart_cfg_t const * const p_cfg)
{
#if (SCI_UART_CFG_RX_ENABLE)
    if (NULL != p_cfg->p_transfer_rx)
    {
        /** Configure the RX transfer, if enabled. */
#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
        SSP_ASSERT(NULL != p_cfg->p_transfer_rx->p_api);
        SSP_ASSERT(NULL != p_cfg->p_transfer_rx->p_ctrl);
        SSP_ASSERT(NULL != p_cfg->p_transfer_rx->p_cfg);
        SSP_ASSERT(NULL != p_cfg->p_transfer_rx->p_cfg->p_info);
#endif
        transfer_info_t * p_info = p_cfg->p_transfer_rx->p_cfg->p_info;
        p_info->mode = TRANSFER_MODE_NORMAL;
        p_info->dest_addr_mode = TRANSFER_ADDR_MODE_INCREMENTED;
        p_info->src_addr_mode = TRANSFER_ADDR_MODE_FIXED;
        p_info->irq = TRANSFER_IRQ_END;
        if (UART_DATA_BITS_9 == p_cfg->data_bits)
        {
            p_info->size = TRANSFER_SIZE_2_BYTE;
        }
        else
        {
            p_info->size = TRANSFER_SIZE_1_BYTE;
        }
        transfer_cfg_t cfg = *(p_cfg->p_transfer_rx->p_cfg);
        fmi_event_info_t event_info;
        ssp_feature_t ssp_feature;
        ssp_feature.channel = p_cfg->channel;
        ssp_feature.unit = 0U;
        ssp_feature.id = SSP_IP_SCI;
        ssp_err_t err = g_fmi_on_fmi.eventInfoGet(&ssp_feature, SSP_SIGNAL_SCI_RXI, &event_info);
#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
        /* Check to make sure the interrupt is enabled. */
        SSP_ASSERT(SSP_INVALID_VECTOR != event_info.irq);
#endif
        cfg.activation_source = event_info.event;
        cfg.auto_enable = false;
        err = p_cfg->p_transfer_rx->p_api->open(p_cfg->p_transfer_rx->p_ctrl, &cfg);
        SCI_UART_ERROR_RETURN(SSP_SUCCESS == err, err);
    }
#endif
#if (SCI_UART_CFG_TX_ENABLE)
    if (NULL != p_cfg->p_transfer_tx)
    {
        /** Configure the TX transfer, if enabled. */
#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
        SSP_ASSERT(NULL != p_cfg->p_transfer_tx->p_api);
        SSP_ASSERT(NULL != p_cfg->p_transfer_tx->p_ctrl);
        SSP_ASSERT(NULL != p_cfg->p_transfer_tx->p_cfg);
        SSP_ASSERT(NULL != p_cfg->p_transfer_tx->p_cfg->p_info);
#endif
        transfer_info_t * p_info = p_cfg->p_transfer_tx->p_cfg->p_info;
        p_info->mode = TRANSFER_MODE_NORMAL;
        p_info->dest_addr_mode = TRANSFER_ADDR_MODE_FIXED;
        p_info->src_addr_mode = TRANSFER_ADDR_MODE_INCREMENTED;
        p_info->irq = TRANSFER_IRQ_END;
        if (UART_DATA_BITS_9 == p_cfg->data_bits)
        {
            p_info->size = TRANSFER_SIZE_2_BYTE;
        }
        else
        {
            p_info->size = TRANSFER_SIZE_1_BYTE;
        }
        transfer_cfg_t cfg = *(p_cfg->p_transfer_tx->p_cfg);
        fmi_event_info_t event_info;
        ssp_feature_t ssp_feature;
        ssp_feature.channel = p_cfg->channel;
        ssp_feature.unit = 0U;
        ssp_feature.id = SSP_IP_SCI;
        ssp_err_t err = g_fmi_on_fmi.eventInfoGet(&ssp_feature, SSP_SIGNAL_SCI_TXI, &event_info);
#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
        /* Check to make sure the interrupt is enabled. */
        SSP_ASSERT(SSP_INVALID_VECTOR != event_info.irq);
#endif
        cfg.activation_source = event_info.event;
        cfg.auto_enable = false;
        err = p_cfg->p_transfer_tx->p_api->open(p_cfg->p_transfer_tx->p_ctrl, &cfg);
        SCI_UART_ERROR_RETURN(SSP_SUCCESS == err, err);
    }
#endif
    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * Configures UART related registers
 * @param[in]     p_cfg   Pointer to UART specific configuration structure
 * @param[in]     p_sci_reg
 * @retval        none
 **********************************************************************************************************************/
static ssp_err_t r_sci_uart_config_set (R_SCI0_Type * p_sci_reg, uart_cfg_t const * const p_cfg)
{
    ssp_err_t         err;
    uart_on_sci_cfg_t * pextend = (uart_on_sci_cfg_t *) p_cfg->p_extend;
    sci_clk_src_t     clk_src;

    HW_SCI_AsyncModeSet(p_sci_reg);               /* applies ASYNC mode */

    HW_SCI_ClockPhaseDelayDisable(p_sci_reg);     /* disables SCKn clock delay */

    HW_SCI_ClockPorarityNormalSet(p_sci_reg);     /* sets SCKn clock polarity normal */

    if (UART_PARITY_OFF != p_cfg->parity)
    {
        HW_SCI_ParityBitEnable(p_sci_reg);        /* enables parity */
    }
    else
    {
        HW_SCI_ParityBitDisable(p_sci_reg);       /* disables parity */
    }

    if (UART_PARITY_ODD == p_cfg->parity)
    {
        HW_SCI_ParityOddSelect(p_sci_reg);        /* selects odd parity */
    }
    else
    {
        HW_SCI_ParityEvenSelect(p_sci_reg);       /* selects even parity */
    }

    if (UART_DATA_BITS_7 == p_cfg->data_bits)
    {
        HW_SCI_DataBits7bitsSelect(p_sci_reg);    /* selects 7-bit data length */
    }
    else if (UART_DATA_BITS_9 == p_cfg->data_bits)
    {
        HW_SCI_DataBits9bitsSelect(p_sci_reg);    /* selects 9-bit data length */
    }
    else
    {
        HW_SCI_DataBits8bitsSelect(p_sci_reg);    /* selects 8-bit data length */
    }

    if (UART_STOP_BITS_2 == p_cfg->stop_bits)
    {
        HW_SCI_StopBits2bitsSelect(p_sci_reg);    /* selects 2-bit stop bit length */
    }
    else
    {
        HW_SCI_StopBits1bitSelect(p_sci_reg);     /* selects 1-bit stop bit length */
    }

    if (p_cfg->ctsrts_en)
    {
        HW_SCI_CtsInEnable(p_sci_reg);            /* enables CTS hardware flow control on RTSn#/CTSn# pin */
    }
    else
    {
        HW_SCI_RtsOutEnable(p_sci_reg);           /* enables RTS hardware flow control on RTSn#/CTSn# pin */
    }

    /** sets baud rate */
    if ((pextend)
        && (((SCI_CLK_SRC_INT    == pextend->clk_src)
             ||   (SCI_CLK_SRC_EXT8X  == pextend->clk_src))
            ||   (SCI_CLK_SRC_EXT16X == pextend->clk_src))
        )
    {
        clk_src = pextend->clk_src;
    }
    else
    {
        clk_src = SCI_CLK_SRC_INT;
    }

    err = r_sci_uart_baud_set(p_sci_reg, clk_src, p_cfg->baud_rate);
    if (err == SSP_ERR_INVALID_ARGUMENT)
    {
        HW_SCI_BaudClkOutputDisable(p_sci_reg);       /** set default setting */
        return err;
    }

    if ((pextend) && (pextend->baudclk_out))
    {
        HW_SCI_BaudClkOutputEnable(p_sci_reg);        /** enables Baud rate clock output */
    }
    else
    {
        HW_SCI_BaudClkOutputDisable(p_sci_reg);       /** disables Baud rate clock output */
    }

    return SSP_SUCCESS;
}  /* End of function r_sci_uart_config_set() */

/*******************************************************************************************************************//**
 * This function enables external RTS(using a GPIO) operation
 * @param[in] p_cfg    Pointer to UART configuration structure
 * @retval  SSP_SUCCESS                  Circular buffer for a channel initialized successfully
 * @note This function is reentrant.
 **********************************************************************************************************************/
#if (SCI_UART_CFG_EXTERNAL_RTS_OPERATION)
static void r_sci_uart_external_rts_operation_enable (sci_uart_instance_ctrl_t * const p_ctrl, uart_cfg_t const * const p_cfg)
{
    uart_on_sci_cfg_t * pextend = (uart_on_sci_cfg_t *) p_cfg->p_extend;

    if ((p_cfg->ctsrts_en) && (pextend->p_extpin_ctrl))
    {
        p_ctrl->p_extpin_ctrl = pextend->p_extpin_ctrl;
        p_ctrl->p_extpin_ctrl(p_cfg->channel, 0);   /** user definition function call to control
                                                                            * GPIO */
    }
}  /* End of function r_sci_uart_external_rts_operation_enable () */
#endif /* if (SCI_UART_CFG_EXTERNAL_RTS_OPERATION) */

/*******************************************************************************************************************//**
 * Resets FIFO related registers
 * @param[in]     p_cfg   Pointer to UART specific configuration structure
 * @param[in]     fifo_depth FIFO depth
 * @param[in]     p_sci_reg
 * @retval        none
 **********************************************************************************************************************/
static void r_sci_uart_fifo_reset (R_SCI0_Type * p_sci_reg, uart_cfg_t const * const p_cfg, uint8_t fifo_depth)
{
    HW_SCI_FifoDisable(p_sci_reg);                    /** disables FIFO mode */

#if (SCI_UART_CFG_RX_ENABLE)
    HW_SCI_ReceiveFifoReset(p_sci_reg);                              /** resets receive FIFO mode */

    if (NULL != p_cfg->p_transfer_tx)
    {
        /** Set receive trigger number to 0 if DTC is used. */
        HW_SCI_RxTriggerNumberSet(p_sci_reg, 0);
    }
    else
    {
        /** Otherwise, set receive trigger number to maximum for efficiency. */
        HW_SCI_RxTriggerNumberSet(p_sci_reg, (uint16_t) (fifo_depth - 1U));
    }
    HW_SCI_RTSTriggerNumberSet(p_sci_reg, (uint16_t) (fifo_depth - 1U)); /* sets RTS trigger number */
#endif
#if (SCI_UART_CFG_TX_ENABLE)
    HW_SCI_TransmitFifoReset(p_sci_reg);                            /** resets transmit FIFO mode */
    if (NULL != p_cfg->p_transfer_tx)
    {
        /** Set transfer trigger number to 0 to facilitate transfer. */
        HW_SCI_TxTriggerNumberSet(p_sci_reg, 0U);
    }
    else
    {
        /** Set FIFO trigger to trigger when one byte is remaining. */
        HW_SCI_TxTriggerNumberSet(p_sci_reg, (uint16_t) (fifo_depth - 1U));
    }
#endif
}  /* End of function r_sci_uart_fifo_reset() */

/*******************************************************************************************************************//**
 * Enable FIFO
 * @param[in]     p_sci_reg
 * @retval        none
 **********************************************************************************************************************/
static void r_sci_uart_fifo_enable (R_SCI0_Type * p_sci_reg)
{
#if (SCI_UART_CFG_RX_ENABLE)
    while (0 != HW_SCI_ReceiveFifoResetStatusRead(p_sci_reg))
    {
        /* FIFO reset status is automatically cleared after 1 peripheral clock for SCI */
    }
#endif
#if (SCI_UART_CFG_TX_ENABLE)
    while (0 != HW_SCI_TransmitFifoResetStatusRead(p_sci_reg))
    {
        /* FIFO reset status is automatically cleared after 1 peripheral clock for SCI */
    }
#endif

    HW_SCI_FifoEnable(p_sci_reg);                 /** enables FIFO mode */
}  /* End of function r_sci_uart_fifo_enable() */

/*******************************************************************************************************************//**
 * Sets interrupt priority and initializes vector info
 * @param[in]     p_feature  SSP feature
 * @param[in]     signal     SSP signal ID
 * @param[in]     ipl        Interrupt priority level
 * @param[in]     p_ctrl     Pointer to driver control block
 * @param[out]    p_irq      Pointer to IRQ for this signal, set here
 *
 * @retval        SSP_SUCCESS               Interrupt enabled
 * @retval        SSP_ERR_IRQ_BSP_DISABLED  Interrupt does not exist in the vector table
 **********************************************************************************************************************/
static inline ssp_err_t r_sci_irq_cfg(ssp_feature_t * p_feature, ssp_signal_t signal, uint8_t ipl,
        void * p_ctrl, IRQn_Type * p_irq)
{
    fmi_event_info_t event_info;
    ssp_vector_info_t * p_vector_info;
    ssp_err_t err = g_fmi_on_fmi.eventInfoGet(p_feature, signal, &event_info);
    *p_irq = event_info.irq;
    if (SSP_SUCCESS == err)
    {
        NVIC_SetPriority(*p_irq, ipl);
        R_SSP_VectorInfoGet(*p_irq, &p_vector_info);
        *(p_vector_info->pp_ctrl) = p_ctrl;
    }

    return err;
}

static inline void r_sci_close_hardware (sci_uart_instance_ctrl_t * p_ctrl)
{
    /** disables interrupt */
    R_SCI0_Type * p_sci_reg = (R_SCI0_Type *) p_ctrl->p_reg;
    HW_SCI_TxIrqDisable(p_sci_reg, p_ctrl);
    HW_SCI_RxIrqDisable(p_sci_reg, p_ctrl);
    HW_SCI_TeIrqDisable(p_sci_reg, p_ctrl);

    HW_SCI_ReceiverDisable (p_sci_reg);               ///< disables receiver

    HW_SCI_TransmitterDisable (p_sci_reg);            ///< disables transmitter

    HW_SCI_BaudClkOutputDisable (p_sci_reg);          ///< set default setting

    HW_SCI_StartBitLowLevelSet (p_sci_reg);           ///< set default setting

    /** sets default value to Noise Filter related register */
    HW_SCI_NoiseFilterClear (p_sci_reg);

    /** removes power to channel */
    ssp_feature_t ssp_feature;
    ssp_feature.channel = p_ctrl->channel;
    ssp_feature.unit = 0U;
    ssp_feature.id = SSP_IP_SCI;
    R_BSP_ModuleStop(&ssp_feature);

    /** unlocks specified SCI channel */
    R_BSP_HardwareUnlock(&ssp_feature);

    /** Clear external pin function. */
    p_ctrl->p_extpin_ctrl = NULL;

}  /* End of function r_sci_close_common() */

/*******************************************************************************************************************//**
 * Changes baud rate. It evaluates and determines the best possible settings set to the baud rate related registers.
 * @param[in] p_sci_reg
 * @param[in] clk_src      A clock source for SCI module (SCI_CLK_SRC_INT|SCI_CLK_SRC_EXT8X|SCI_CLK_SRC_EXT16X)
 * @param[in] baudrate     Baud rate[bps] e.g. 19200, 57600, 115200, etc.
 * @retval  SSP_SUCCESS                  Baud rate is set successfully
 * @retval  SSP_ERR_INVALID_ARGUMENT     Baud rate is '0' or cannot set properly
 * @note   The transmitter and receiver (TE and RE bits in SCR) must be disabled prior to calling this function.
 **********************************************************************************************************************/
static ssp_err_t    r_sci_uart_baud_set          (R_SCI0_Type * p_sci_reg, sci_clk_src_t clk_src, uint32_t baudrate)
{
    uint32_t       i   = 8;
    uint32_t       hit = 0;
    uint32_t       brr = 255;
    uint32_t       temp_brr;
    int32_t        bit_err;
    int32_t        temp_bit_err = 10000;
    uint32_t       divisor;
    baud_setting_t * pbaudinfo;
    uint32_t       freq_hz;
    ssp_err_t      err;
    bsp_feature_sci_t sci_feature;

#if (SCI_UART_CFG_PARAM_CHECKING_ENABLE)
    SCI_UART_ERROR_RETURN((0 != baudrate), SSP_ERR_INVALID_ARGUMENT);
#endif

    R_BSP_FeatureSciGet(&sci_feature);
    err = g_cgc_on_cgc.systemClockFreqGet((cgc_system_clocks_t) sci_feature.clock, &freq_hz);
    SCI_UART_ERROR_RETURN(SSP_SUCCESS == err, SSP_ERR_INVALID_ARGUMENT);

    /* selects proper table based upon mode */
    pbaudinfo = (baud_setting_t *) async_baud;

    if (SCI_CLK_SRC_INT == clk_src)
    {
        /* FIND BEST_BRR_VALUE
         *  In table async_baud", divisor value is associated with BGDM, ABCS, ABCSE and N values, so once best divisor
         * value is found,
         *  baud rate related register setting values are decided. The formula to calculate BRR is as follows and it
         * must be 255 or less.
         *  BRR = (PCLKA/(div_coefficient * baud)) - 1
         */
        for (i = 0U; i < NUM_DIVISORS_ASYNC; i++)
        {
            temp_brr = freq_hz / ((uint32_t) pbaudinfo[i].div_coefficient * baudrate);
            if (0U < temp_brr)
            {
                temp_brr -= 1U;

                /* Calculate the bit rate error and have a best selection. The formula is as follows.
                 *  bit error[%] = {(PCLK / (baud * div_coefficient * (BRR + 1)) - 1} x 100
                 *  calculates bit error[%] to two decimal places
                 */
                divisor   = (temp_brr + 1U) * (uint32_t) pbaudinfo[i].div_coefficient;
                divisor  *= baudrate;
                divisor >>= 9;  /* This prevents overflow beyond 32-bit size.  */
                bit_err   = (int32_t) ((((freq_hz >> 9) * 10000) / divisor) - 10000);
                if (temp_brr < 256U)
                {
                    if (bit_err < temp_bit_err)
                    {
                        hit          = i;
                        brr          = temp_brr;
                        temp_bit_err = bit_err;
                    }
                }
            }
        }

        SCI_UART_ERROR_RETURN((10000 != temp_bit_err), SSP_ERR_INVALID_ARGUMENT);

        HW_SCI_BaudRateGenInternalClkSelect(p_sci_reg);

        SCI_UART_ERROR_RETURN(0xFFU >= brr, SSP_ERR_ASSERTION);

        HW_SCI_UartBitRateSet(p_sci_reg, (uint8_t)brr, &pbaudinfo[hit]);

        /* Check Bitrate Modulation function is enabled or not.
         * If it is enabled,set the MBBR register to correct the bit rate generated by the on-chip baud rate generator */
        if(HW_SCI_BitRateModulationCheck(p_sci_reg))
        {
            uint32_t temp_mbbr = 0 ;
            volatile uint32_t mddr = 0;

            /* Calculate the MBBR (M) value,
             * The formula to calculate MBBR (from the M and N relationship given in the HM) is as follows and it
             * must between 128 and 256.
             * MBBR = ((div_coefficient * baud * 256) * (BRR + 1)) / PCLKA */
            temp_mbbr = ((uint32_t) pbaudinfo[hit].div_coefficient * baudrate * 256);
            mddr = (temp_mbbr * (brr+1)) / freq_hz;

            /* Set MDDR register only for values between 128 and 256, do not set otherwise */
            if ((mddr >= 128) && (mddr <= 256))
            {
                HW_SCI_UartBitRateModulationSet(p_sci_reg, (uint8_t)mddr);
            }
        }

    }
    else
    {
        HW_SCI_BitRateDefaultSet(p_sci_reg);

        HW_SCI_BaudRateGenExternalClkSelect(p_sci_reg);

        if (SCI_CLK_SRC_EXT8X == clk_src)
        {
            HW_SCI_BaudRateGenExternalClkDivideBy8(p_sci_reg);
        }
        else
        {
            HW_SCI_BaudRateGenExternalClkDivideBy16(p_sci_reg);
        }
    }

    return SSP_SUCCESS;
}  /* End of function r_sci_uart_baud_set() */

/*******************************************************************************************************************//**
 * TXI interrupt processing for UART mode. TXI interrupt happens when the data in the data register or FIFO register has
 *  to be transferred to the data shift register, and the next writing data to it is available. This function is
 *  repeatedly called from TXI ISR and continues data transfer until the transmit circular buffer becoming empty.
 * Finally
 *  this function disables TXI interrupt and enables TEI interrupt(@see r_sci_uart_tei()).
 * @retval    none
 **********************************************************************************************************************/
void sci_uart_txi_isr (void)
{
    /* Save context if RTOS is used */
    SF_CONTEXT_SAVE;

    /** Clear pending IRQ to make sure it doesn't fire again after exiting */
    R_BSP_IrqStatusClear(R_SSP_CurrentIrqGet());

    ssp_vector_info_t * p_vector_info = NULL;
    R_SSP_VectorInfoGet(R_SSP_CurrentIrqGet(), &p_vector_info);
    sci_uart_instance_ctrl_t * p_ctrl = (sci_uart_instance_ctrl_t *) *(p_vector_info->pp_ctrl);
    R_SCI0_Type * p_sci_reg = (R_SCI0_Type *) p_ctrl->p_reg;

    uint32_t fifo_remaining=0U;
    int32_t fifo_open=0;
    if (p_ctrl->fifo_depth > 0U)
    {
        fifo_remaining = HW_SCI_FIFO_WriteCount(p_sci_reg);
        fifo_open = (int32_t) p_ctrl->fifo_depth - (int32_t) fifo_remaining;
    }
    uint16_t data;

    /** checks data byte length */
    uint32_t data_bytes = 1U;
    if (HW_SCI_IsDataLength9bits(p_sci_reg))
    {
        data_bytes = 2U;
    }

    if (NULL == p_ctrl->p_transfer_tx)
    {

        for (uint32_t cnt = 0U; cnt < p_ctrl->tx_src_bytes; cnt += data_bytes)
        {
            if ((p_ctrl->fifo_depth > 0U) && (fifo_open <= 0))
            {
                /* FIFO full. */
                break;
            }

            if (1U == data_bytes)
            {
                data = (uint16_t) *(p_ctrl->p_tx_src);
            }
            else
            {
                data = (uint16_t) *((uint16_t *) p_ctrl->p_tx_src);    /* 9-bit data length needs 2 bytes */
            }

            if (p_ctrl->fifo_depth > 0U)
            {
                /** Write to the hardware FIFO. */
                HW_SCI_WriteFIFO(p_sci_reg, data);
                fifo_open--;
            }
            else
            {
                /** Write to the hardware data register. */
                if (HW_SCI_IsDataLength9bits (p_sci_reg))
                {
                    HW_SCI_Write9bits (p_sci_reg, data);
                }
                else
                {
                    HW_SCI_Write (p_sci_reg, (uint8_t)data);
                }
            }

            p_ctrl->tx_src_bytes -= data_bytes;
            p_ctrl->p_tx_src += data_bytes;
        }

        if(p_ctrl->fifo_depth > 0U)
        {
            HW_SCI_TDFEClear(p_sci_reg);
        }
    }
    else
    {
        if (0U == fifo_remaining)
        {
            HW_SCI_WriteFIFO(p_sci_reg, (uint16_t) *(p_ctrl->p_tx_src));
            p_ctrl->tx_src_bytes -= data_bytes;
            p_ctrl->p_tx_src += data_bytes;
        }
    }
    if (0U == p_ctrl->tx_src_bytes)
    {
        HW_SCI_TeIrqEnable(p_sci_reg, p_ctrl);
        HW_SCI_TxIrqDisable(p_sci_reg, p_ctrl);
        p_ctrl->p_tx_src = NULL;
    }

    /* Restore context if RTOS is used */
    SF_CONTEXT_RESTORE;
}  /* End of function sci_uart_txi_isr() */

/*******************************************************************************************************************//**
 * RXI interrupt processing for UART mode. RXI interrupt happens when data arrives to the data register or the FIFO
 *  register.  This function calls callback function when it meets conditions below.
 *  - The number of data which has been read reaches to the number specified in R_SCI_UartRead()
 *(UART_EVENT_RX_COMPLETE)
 *  This function also calls the callback function for RTS pin control if it is registered in R_SCI_UartOpen(). This is
 *  special functionality to expand SCI hardware capability and make RTS/CTS hardware flow control possible. If macro
 *  'SCI_UART_CFG_EXTERNAL_RTS_OPERATION' is set, it is called at the beginning in this function,
 *  data done, it is called again (just before leaving this function). SCI UART module does not control any GPIOs but
 *  this callback function let user know the timing of RTS signal assert or negate but user have to control the GPIO pin
 *  which is used for RTS pin.
 * @retval    none
 **********************************************************************************************************************/
void sci_uart_rxi_isr (void)
{
    /* Save context if RTOS is used */
    SF_CONTEXT_SAVE;

    ssp_vector_info_t * p_vector_info = NULL;
    R_SSP_VectorInfoGet(R_SSP_CurrentIrqGet(), &p_vector_info);
    sci_uart_instance_ctrl_t * p_ctrl = (sci_uart_instance_ctrl_t *) *(p_vector_info->pp_ctrl);
    uint8_t channel = p_ctrl->channel;
    R_SCI0_Type * p_sci_reg = (R_SCI0_Type *) p_ctrl->p_reg;

    uint32_t             data             = 0;
    uart_callback_args_t args;
    uint32_t             read_cnt;

#if (SCI_UART_CFG_EXTERNAL_RTS_OPERATION)
    if (p_ctrl->p_extpin_ctrl)
    {
        p_ctrl->p_extpin_ctrl(channel, 1);         /** user definition function call to control GPIO */
    }
#endif

    if (p_ctrl->fifo_depth > 0U)
    {
        read_cnt = HW_SCI_FIFO_ReadCount(p_sci_reg);
    }
    else
    {
        read_cnt = 1U;
    }

    /** checks data byte length */
    uint32_t data_bytes = 1U;
    if (HW_SCI_IsDataLength9bits(p_sci_reg))
    {
        data_bytes = 2U;
    }

    if ((NULL != p_ctrl->p_transfer_rx) && p_ctrl->rx_transfer_in_progress)
    {
        p_ctrl->rx_transfer_in_progress = false;

        /* Do callback if available */
        if (NULL != p_ctrl->p_callback)
        {
            args.channel        = channel;
            args.data           = data;
            args.p_context      = p_ctrl->p_context;
            args.event = UART_EVENT_RX_COMPLETE;
            p_ctrl->p_callback(&args);
        }
    }

    /** Flush read FIFO. */
    while (read_cnt > 0U)
    {
        /* Read data */
        if(p_ctrl->fifo_depth > 0U)
        {
            data = HW_SCI_ReadFIFO(p_sci_reg);
        }
        else
        {
            if (HW_SCI_IsDataLength9bits (p_sci_reg))
            {
                data = HW_SCI_Read9bits (p_sci_reg);
            }
            else
            {
                data = HW_SCI_Read (p_sci_reg);
            }
        }

        /* Do callback if available */
        if (NULL != p_ctrl->p_callback)
        {
            args.channel        = channel;
            args.data           = data;
            args.p_context      = p_ctrl->p_context;
            args.event = UART_EVENT_RX_CHAR;
            p_ctrl->p_callback(&args);
            if (2U == data_bytes)
            {
                args.data       = (data >> 8);
                p_ctrl->p_callback(&args);
            }
        }
        read_cnt--;
    }

#if (SCI_UART_CFG_EXTERNAL_RTS_OPERATION)
    if (p_ctrl->p_extpin_ctrl)
    {
        p_ctrl->p_extpin_ctrl(channel, 0);         /** user definition function call to control GPIO */
    }
#endif

    /** Clear pending IRQ to make sure it doesn't fire again after exiting */
    HW_SCI_RDFClear(p_sci_reg);
    R_BSP_IrqStatusClear(R_SSP_CurrentIrqGet());

    /* Restore context if RTOS is used */
    SF_CONTEXT_RESTORE;
}  /* End of function sci_uart_rxi_isr () */

/*******************************************************************************************************************//**
 * TEI interrupt processing for UART mode. TEI interrupt happens at the timing of data transmit completion. The user
 * callback function is called with UART_EVENT_TX_COMPLETE event code (if it is registered in R_SCI_UartOpen()).
 * @retval    none
 **********************************************************************************************************************/
void sci_uart_tei_isr (void)
{
    /* Save context if RTOS is used */
    SF_CONTEXT_SAVE;

    /** Clear pending IRQ to make sure it doesn't fire again after exiting */
    R_BSP_IrqStatusClear(R_SSP_CurrentIrqGet());

    ssp_vector_info_t * p_vector_info = NULL;
    R_SSP_VectorInfoGet(R_SSP_CurrentIrqGet(), &p_vector_info);
    sci_uart_instance_ctrl_t * p_ctrl = (sci_uart_instance_ctrl_t *) *(p_vector_info->pp_ctrl);
    uint8_t channel = p_ctrl->channel;
    R_SCI0_Type * p_sci_reg = (R_SCI0_Type *) p_ctrl->p_reg;

    uart_callback_args_t args;

    /* Receiving TEI(transmit end interrupt) means the completion of
     * transmission, so call call-back function here
     */
    HW_SCI_TeIrqDisable(p_sci_reg, p_ctrl);
    if (NULL != p_ctrl->p_callback)
    {
        args.channel   = channel;
        args.data      = 0U;
        args.event     = UART_EVENT_TX_COMPLETE;
        args.p_context = p_ctrl->p_context;
        p_ctrl->p_callback(&args);
    }

    /* Restore context if RTOS is used */
    SF_CONTEXT_RESTORE;
}  /* End of function sci_uart_tei_isr () */

/*******************************************************************************************************************//**
 * ERI interrupt processing for UART mode. When ERI interrupt happens, the user callback function is called if it is
 *  registered in R_SCI_UartOpen() with the event code which happen at the time.
 * @retval    none
 **********************************************************************************************************************/
void sci_uart_eri_isr (void)
{
    /* Save context if RTOS is used */
    SF_CONTEXT_SAVE;

    ssp_vector_info_t * p_vector_info = NULL;
    R_SSP_VectorInfoGet(R_SSP_CurrentIrqGet(), &p_vector_info);
    sci_uart_instance_ctrl_t * p_ctrl = (sci_uart_instance_ctrl_t *) *(p_vector_info->pp_ctrl);
    uint8_t channel = p_ctrl->channel;
    R_SCI0_Type * p_sci_reg = (R_SCI0_Type *) p_ctrl->p_reg;

    uint32_t             data = 0U;
    uart_callback_args_t args = {0U};

    /* Read data */
    data = HW_SCI_ReadFIFO(p_sci_reg);

    HW_SCI_RDFClear(p_sci_reg);

    /* Error check */
    if (HW_SCI_OverRunErrorCheck(p_sci_reg))
    {
        args.event = UART_EVENT_ERR_OVERFLOW;
    }
    else if (HW_SCI_FramingErrorCheck(p_sci_reg))
    {
        if (HW_SCI_BreakDetectionCheck(p_sci_reg))
        {
            args.event = UART_EVENT_BREAK_DETECT;
        }
        else
        {
            args.event = UART_EVENT_ERR_FRAMING;
        }
    }
    else
    {
        args.event = UART_EVENT_ERR_PARITY;
    }

    /* Do callback if available */
    if (NULL != p_ctrl->p_callback)
    {
        args.channel   = channel;
        args.data      = data;
        args.p_context = p_ctrl->p_context;
        p_ctrl->p_callback(&args);
    }

    /* Clear error condition */
    HW_SCI_ErrorConditionClear (p_sci_reg);

    /** Clear pending IRQ to make sure it doesn't fire again after exiting */
    R_BSP_IrqStatusClear(R_SSP_CurrentIrqGet());

    /* Restore context if RTOS is used */
    SF_CONTEXT_RESTORE;
}  /* End of function sci_uart_eri_isr () */
