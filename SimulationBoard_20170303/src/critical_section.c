//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  critical_section.c
//
//  Description:
//
//-----------------------------------------------------------------------------

#include "jellyfish_common.h"
#include "bsp_api.h"
#include "critical_section.h"

static BOOL interrupts_enabled;

#define irqs_are_enabled()     (__get_PRIMASK() == 0)       ///< Returns true if interrupts are globally enabled.

void enter_critical_section(void)
{
    /* Are interrupts currently enabled? If so insure we re-enable them before exiting */
    interrupts_enabled = irqs_are_enabled();

    if (interrupts_enabled == TRUE)
    {
        __disable_irq();
    }
}

void exit_critical_section(void)
{
    if (interrupts_enabled == FALSE)
    {
        __enable_irq();
    }
}
