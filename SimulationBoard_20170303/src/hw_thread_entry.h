//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  adc_scan.h
//
//  Description: Define public data type and function prototypes for
//               ADC scan and PWM output monitoring.
//
//-----------------------------------------------------------------------------

#ifndef __HW_THREAD_ENTRY_H__
#define __HW_THREAD_ENTRY_H__
#include "common_def.h"
#include "hw_thread.h"


unsigned get_adc_value(unsigned char chan);
unsigned get_do_value(unsigned char chan);
#endif // __HW_THREAD_ENTRY_H__






