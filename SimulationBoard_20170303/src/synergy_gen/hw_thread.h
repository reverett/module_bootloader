/* generated thread header file - do not edit */
#ifndef HW_THREAD_H_
#define HW_THREAD_H_
#include "bsp_api.h"
#include "tx_api.h"
#include "hal_data.h"
#ifdef __cplusplus 
extern "C" void hw_thread_entry(void);
#else 
extern void hw_thread_entry(void);
#endif
#include "r_gpt.h"
#include "r_timer_api.h"
#include "r_adc.h"
#include "r_adc_api.h"
#ifdef __cplusplus
extern "C"
{
#endif
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer4_pwm;
#ifndef pwm4_callback
void pwm4_callback(timer_callback_args_t * p_args);
#endif
/** ADC on ADC Instance. */
extern const adc_instance_t g_adc0;
/** Timer on GPT Instance. */
extern const timer_instance_t g_timer2;
#ifndef switch_callback
void switch_callback(timer_callback_args_t * p_args);
#endif
#ifdef __cplusplus
} /* extern "C" */
#endif
#endif /* HW_THREAD_H_ */
