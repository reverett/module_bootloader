//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  rs485_driver.h
//
//  Description: The RS485 module declarations for globals and functions.
//
//-----------------------------------------------------------------------------
#ifndef __RS485_DRIVER_H__
#define __RS485_DRIVER_H__

#include "hal_data.h"

#include "jellyfish_common.h"

// #define BOARD_VERSION_PRE_ALPHA

#ifdef BOARD_VERSION_PRE_ALPHA

// It is Pre-Alpha board.
#define RS485_RX_ENABLE_PIN      IOPORT_PORT_02_PIN_06 // !RE pin, active Low
#define RS485_TX_ENABLE_PIN      IOPORT_PORT_04_PIN_01 // DE pin, active high
#define RS485_RX_PIN             IPPORT_PORT_04_PIN_10 // RxD pin
#define RS485_TX_PIN             IOPORT_PORT_04_PIN_11 // TxD pin

#else

// It is Alpha board.
#define RS485_RX_ENABLE_PIN      IOPORT_PORT_01_PIN_06 // !RE pin, active low
#define RS485_TX_ENABLE_PIN      IOPORT_PORT_01_PIN_05 // DE pin, active high
#define RS485_RX_PIN             IPPORT_PORT_01_PIN_04 // RxD pin
#define RS485_TX_PIN             IOPORT_PORT_01_PIN_12 // TxD pin

#endif

#define RS485_RX_ENABLE      IOPORT_LEVEL_LOW
#define RS485_RX_DISABLE     IOPORT_LEVEL_HIGH
#define RS485_TX_ENABLE      IOPORT_LEVEL_HIGH
#define RS485_TX_DISABLE     IOPORT_LEVEL_LOW


typedef enum {
    RS485_READ     = 0,
    RS485_WRITE    = 1
} RS485_DIRECTION;

#if 0
#ifndef BOOL
typedef enum {
    TRUE         = 1,
    FALSE        = 0
} BOOL;
#endif
#endif

// The RS485 Buffer

#define RS485_RX_BUFFER_SIZE           1024

#define RS485_RX_BUFFER_ST_OK             0
#define RS485_RX_BUFFER_ST_FULL    (1 << 0)
#define RS485_RX_BUFFER_ST_ERR     (1 << 1)

typedef struct RS485_BUFFER {
    unsigned char     data[RS485_RX_BUFFER_SIZE];
    unsigned short    read_idx;
    unsigned short    write_idx;
    unsigned short    status;
} RS485_RX_BUFFER_T;


//-----------------------------------------------------------------
// Global Function Declarations
//-----------------------------------------------------------------
void            rs485_initialize(void);
BOOL            rs485_rx_data_available(void);
unsigned char   rs485_rx_read_byte(void);
void            rs485_tx_write_nbyte(const char *ptr, const unsigned short length);
void            rs485_set_rw_direction(RS485_DIRECTION rs485_direction);
void            rs485_disable_rw(void);

// This is to avoid compiler warning when a parameter
// of a function is not used - purposely.

#ifndef PARAMETER_NOT_USEd
#define PARAMETER_NOT_USED(p) (void) ((p))
#endif


#endif // __RS485_DRIVER_H__
