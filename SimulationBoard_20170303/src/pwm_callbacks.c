//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  pwm_callbacks.c
//
//  Description: Callback functions for pwm counter overflow.
//
//-----------------------------------------------------------------------------
/* HAL-only entry function */
#include "hal_data.h"
#include "hw_thread.h"

void pwm4_callback(timer_callback_args_t * p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
}
