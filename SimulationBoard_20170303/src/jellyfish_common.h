//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  jellyfish_common.h
//
//  Description: Defines data types and constants common for both the
//               various modules and the System Controller.
//               Currently the only module with this implementation is
//               the DO_AO module (i.e., the Relay Module).
//
//-----------------------------------------------------------------------------
#ifndef __JELLYFISH_COMMON_H__
#define __JELLYFISH_COMMON_H__



#ifndef BOOL
typedef enum {
    TRUE      = 1,
    FALSE     = 0
} BOOL;
#endif

typedef enum {          // this is for both the Relays
    ON            = 1,  // push-button switches
    OFF           = 0
} SWITCH_STATE_E;

typedef enum {
    MANUAL_SW = 1,
    AUTO_SW = 0
} SWITCH_TYPE_E;

typedef enum {
    ZC_ENABLE     = 1,
    ZC_DISABLE    = 0
} ZC_ENABLE_E;

// LED state
typedef enum {
    LED_OFF       = 0,   // turn led off
    LED_ON        = 1,   // turn led on
    LED_REVERSE   = 2    // reverse the led
} LED_STATE_E;

// port state
typedef enum {
    PORT_OFF       = 0,   // turn led off
    PORT_ON        = 1,   // turn led on
    PORT_REVERSE   = 2    // reverse the led
} PORT_STATE_E;


typedef enum {
    RS485_EOL_ENABLE     = 1,
    RS485_EOL_DISABLE    = 0
} RS485_EOL_ENABLE_E;

#ifndef NULL
#define NULL  ((void *) 0)
#endif

// This is to avoid compiler warning when a parameter
// of a function is not used - purposely.

#ifndef _PARAMETER_NOT_USED_
#define _PARAMETER_NOT_USED_(p) (void) ((p))
#endif


#endif // __JELLYFISH_COMMON_H__
