//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  sys_config.h
//
//  Description: Defines all data of system configuration, which include
//               time stamps, parameters, etc.
//
//-----------------------------------------------------------------------------

#ifndef _SYS_CONFIG_H_ // Note, __SYS_CONFIG_H__ is a macro used by GCC.
#define _SYS_CONFIG_H_ //       we have to use a different one.

// Include files of standard C language


// Include files of SSP and ThreadX


// Include files of Jellyfish DO-AO Module

//---------------------------------------------------------------------
// Macro and Constant Definitions
//---------------------------------------------------------------------


//---------------------------------------------------------------------
// Define the thread delay time cross all threads
//---------------------------------------------------------------------
// Define the units to be used with the ThreadX sleep function
#define THREADX_TICK_RATE_HZ       100

// Set the blink frequency (must be <= threadx_tick_rate_Hz
#define FREQUENCE_IN_HZ              2

// Calculate the delay in terms of the ThreadX tick rate every
//         50 * 10msec = 500 mSec
// all the ADC channels are read and saved into database
#define THREAD_COMMON_DELAY         (THREADX_TICK_RATE_HZ/FREQUENCE_IN_HZ)


//---------------------------------------------------------------------
// MCU Pin Configuration
//---------------------------------------------------------------------

#if 0
//------------------------------------------------------
// Relay Make (Close) and Break (Open) Pin Assignments
//------------------------------------------------------

#ifdef BOARD_VERSION_PRE_ALPHA
#else
// it is Alpha Board
#define RELAY1_MAKE_PIN       IOPORT_PORT_04_PIN_00
#define RELAY1_BREAK_PIN      IOPORT_PORT_04_PIN_01

#define RELAY2_MAKE_PIN       IOPORT_PORT_04_PIN_02
#define RELAY2_BREAK_PIN      IOPORT_PORT_04_PIN_03

#define RELAY3_MAKE_PIN       IOPORT_PORT_01_PIN_00
#define RELAY3_BREAK_PIN      IOPORT_PORT_05_PIN_00

#define RELAY4_MAKE_PIN       IOPORT_PORT_05_PIN_01
#define RELAY4_BREAK_PIN      IOPORT_PORT_05_PIN_02

#endif

//------------------------------------------------------
// Push-Button Switch Pin Assignments
//------------------------------------------------------

#ifdef BOARD_VERSION_PRE_ALPHA
#else
// it is Alpha Board
#define SWITCH_RELAY1_PIN      IOPORT_PORT_02_PIN_12
#define SWITCH_RELAY2_PIN      IOPORT_PORT_02_PIN_13
#define SWITCH_RELAY3_PIN      IOPORT_PORT_02_PIN_14
#define SWITCH_RELAY4_PIN      IOPORT_PORT_02_PIN_15
#define SWTICH_PB_PIN          IOPORT_PORT_01_PIN_03
#endif

//-------------------------------------
// Zero-Cross Control Pin Assignments
//-------------------------------------

#ifdef BOARD_VERSION_PRE_ALPHA
#else
// it is Alpha Board
#define ZC_ENABLE_PIN_RELAY1      IOPORT_PORT_04_PIN_11
#define ZC_ENABLE_PIN_RELAY2      IOPORT_PORT_04_PIN_10
#define ZC_ENABLE_PIN_RELAY3      IOPORT_PORT_04_PIN_09
#define ZC_ENABLE_PIN_RELAY4      IOPORT_PORT_04_PIN_08
#endif

//-----------------------
// LED Pin Assignments
//-----------------------

#ifdef BOARD_VERSION_PRE_ALPHA
// it is Pre-Alpha Board
#define LED_PIN_RELAY1       IOPORT_PORT_02_PIN_00 // "R4 LED", DS9, pin27, uncontrollable this hw rev
#define LED_PIN_RELAY2       IOPORT_PORT_03_PIN_04 // "R3 LED", DS8, pin28
#define LED_PIN_RELAY3       IOPORT_PORT_03_PIN_03 // "R2 LED", DS7, pin29
#define LED_PIN_RELAY4       IOPORT_PORT_03_PIN_02 // "R1 LED", DS6, pin30
#define LED_PIN_RS485        IOPORT_PORT_03_PIN_01 // "RS485 Comm",  pin31

#else
// it is Alpha Board
#define LED_PIN_RELAY1       IOPORT_PORT_03_PIN_01 // DS4, "R1 LED"
#define LED_PIN_RELAY2       IOPORT_PORT_03_PIN_02 // DS5, "R2 LED"
#define LED_PIN_RELAY3       IOPORT_PORT_03_PIN_03 // DS6, "R3 LED"
#define LED_PIN_RELAY4       IOPORT_PORT_03_PIN_04 // DS7, "R4 LED"
#define LED_PIN_RS485        IOPORT_PORT_01_PIN_13 // DS2, "RS485 Communication"
#define LED_PIN_UNUSED       IOPORT_PORT_01_PIN_11 // DS1, "Commissioned (active)"
#endif
#endif

#define LED1                 IOPORT_PORT_01_PIN_07
#define LED2                 IOPORT_PORT_01_PIN_13
#define LED3                 IOPORT_PORT_01_PIN_11
#define LED4                 IOPORT_PORT_01_PIN_09

//-----------------------------
// RS485 EOL Pin Assignments
//-----------------------------

#ifdef BOARD_VERSION_PRE_ALPHA
#else
// it is Alpha Board
// This pin determines if any module connected to this
// It is floating-high, active-low
#define RS485_EOL_POWER_PIN       IOPORT_PORT_02_PIN_00

// This pin controls whether to allow RS485 Communication
// flow to the next node. Set to high (close resister
// switch) to disable the flow, set to low to allow
// the flow.
#define RS485_EOL_RESISTER_PIN    IOPORT_PORT_02_PIN_06
#endif


//---------------------------------------------------------------------
// Function Declarations
//---------------------------------------------------------------------

#endif // _SYS_CONFIGI_H_
