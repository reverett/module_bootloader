################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/synergy_gen/blinky_thread.c \
../src/synergy_gen/common_data.c \
../src/synergy_gen/control_thread.c \
../src/synergy_gen/hal_data.c \
../src/synergy_gen/hw_thread.c \
../src/synergy_gen/main.c \
../src/synergy_gen/pin_data.c \
../src/synergy_gen/rs485_comm_thread.c 

OBJS += \
./src/synergy_gen/blinky_thread.o \
./src/synergy_gen/common_data.o \
./src/synergy_gen/control_thread.o \
./src/synergy_gen/hal_data.o \
./src/synergy_gen/hw_thread.o \
./src/synergy_gen/main.o \
./src/synergy_gen/pin_data.o \
./src/synergy_gen/rs485_comm_thread.o 

C_DEPS += \
./src/synergy_gen/blinky_thread.d \
./src/synergy_gen/common_data.d \
./src/synergy_gen/control_thread.d \
./src/synergy_gen/hal_data.d \
./src/synergy_gen/hw_thread.d \
./src/synergy_gen/main.d \
./src/synergy_gen/pin_data.d \
./src/synergy_gen/rs485_comm_thread.d 


# Each subdirectory must supply rules for building sources it contributes
src/synergy_gen/%.o: ../src/synergy_gen/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	C:\Renesas\e2_studio\eclipse\../Utilities/isdebuild arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -O2 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g3 -D_RENESAS_SYNERGY_ -I"F:\REverett\git\SimulationBoard_20170303\src" -I"F:\REverett\git\SimulationBoard_20170303\src\synergy_gen" -I"F:\REverett\git\SimulationBoard_20170303\synergy_cfg\ssp_cfg\bsp" -I"F:\REverett\git\SimulationBoard_20170303\synergy_cfg\ssp_cfg\driver" -I"F:\REverett\git\SimulationBoard_20170303\synergy\ssp\inc" -I"F:\REverett\git\SimulationBoard_20170303\synergy\ssp\inc\bsp" -I"F:\REverett\git\SimulationBoard_20170303\synergy\ssp\inc\bsp\cmsis\Include" -I"F:\REverett\git\SimulationBoard_20170303\synergy\ssp\inc\driver\api" -I"F:\REverett\git\SimulationBoard_20170303\synergy\ssp\inc\driver\instances" -I"F:\REverett\git\SimulationBoard_20170303\synergy_cfg\ssp_cfg\framework\el" -I"F:\REverett\git\SimulationBoard_20170303\synergy\ssp\inc\framework\el" -I"F:\REverett\git\SimulationBoard_20170303\synergy\ssp\src\framework\el\tx" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" -x c "$<"
	@echo 'Finished building: $<'
	@echo ' '


