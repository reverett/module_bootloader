################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/adc_scan.c \
../src/blinky_thread_entry.c \
../src/control_thread_entry.c \
../src/critical_section.c \
../src/diag_test_sim_bd.c \
../src/hal_entry.c \
../src/hw_io_api.c \
../src/hw_thread_entry.c \
../src/int_prot_frame.c \
../src/led_state.c \
../src/pwm_callbacks.c \
../src/rs485_comm_thread_entry.c \
../src/rs485_driver.c \
../src/sc_command.c \
../src/switch_state.c \
../src/sys_manager.c 

OBJS += \
./src/adc_scan.o \
./src/blinky_thread_entry.o \
./src/control_thread_entry.o \
./src/critical_section.o \
./src/diag_test_sim_bd.o \
./src/hal_entry.o \
./src/hw_io_api.o \
./src/hw_thread_entry.o \
./src/int_prot_frame.o \
./src/led_state.o \
./src/pwm_callbacks.o \
./src/rs485_comm_thread_entry.o \
./src/rs485_driver.o \
./src/sc_command.o \
./src/switch_state.o \
./src/sys_manager.o 

C_DEPS += \
./src/adc_scan.d \
./src/blinky_thread_entry.d \
./src/control_thread_entry.d \
./src/critical_section.d \
./src/diag_test_sim_bd.d \
./src/hal_entry.d \
./src/hw_io_api.d \
./src/hw_thread_entry.d \
./src/int_prot_frame.d \
./src/led_state.d \
./src/pwm_callbacks.d \
./src/rs485_comm_thread_entry.d \
./src/rs485_driver.d \
./src/sc_command.d \
./src/switch_state.d \
./src/sys_manager.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	C:\Renesas\e2_studio\eclipse\../Utilities/isdebuild arm-none-eabi-gcc -mcpu=cortex-m0plus -mthumb -O2 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wunused -Wuninitialized -Wall -Wextra -Wmissing-declarations -Wconversion -Wpointer-arith -Wshadow -Wlogical-op -Waggregate-return -Wfloat-equal  -g3 -D_RENESAS_SYNERGY_ -I"F:\REverett\git\SimulationBoard_20170303\src" -I"F:\REverett\git\SimulationBoard_20170303\src\synergy_gen" -I"F:\REverett\git\SimulationBoard_20170303\synergy_cfg\ssp_cfg\bsp" -I"F:\REverett\git\SimulationBoard_20170303\synergy_cfg\ssp_cfg\driver" -I"F:\REverett\git\SimulationBoard_20170303\synergy\ssp\inc" -I"F:\REverett\git\SimulationBoard_20170303\synergy\ssp\inc\bsp" -I"F:\REverett\git\SimulationBoard_20170303\synergy\ssp\inc\bsp\cmsis\Include" -I"F:\REverett\git\SimulationBoard_20170303\synergy\ssp\inc\driver\api" -I"F:\REverett\git\SimulationBoard_20170303\synergy\ssp\inc\driver\instances" -I"F:\REverett\git\SimulationBoard_20170303\synergy_cfg\ssp_cfg\framework\el" -I"F:\REverett\git\SimulationBoard_20170303\synergy\ssp\inc\framework\el" -I"F:\REverett\git\SimulationBoard_20170303\synergy\ssp\src\framework\el\tx" -std=c99 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" -x c "$<"
	@echo 'Finished building: $<'
	@echo ' '


