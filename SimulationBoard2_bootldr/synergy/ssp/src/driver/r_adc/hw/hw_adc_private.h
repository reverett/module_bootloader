/***********************************************************************************************************************
 * Copyright [2015] Renesas Electronics Corporation and/or its licensors. All Rights Reserved.
 * 
 * This file is part of Renesas SynergyTM Software Package (SSP)
 *
 * The contents of this file (the "contents") are proprietary and confidential to Renesas Electronics Corporation
 * and/or its licensors ("Renesas") and subject to statutory and contractual protections.
 *
 * This file is subject to a Renesas SSP license agreement. Unless otherwise agreed in an SSP license agreement with
 * Renesas: 1) you may not use, copy, modify, distribute, display, or perform the contents; 2) you may not use any name
 * or mark of Renesas for advertising or publicity purposes or in connection with your use of the contents; 3) RENESAS
 * MAKES NO WARRANTY OR REPRESENTATIONS ABOUT THE SUITABILITY OF THE CONTENTS FOR ANY PURPOSE; THE CONTENTS ARE PROVIDED
 * "AS IS" WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT; AND 4) RENESAS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, OR
 * CONSEQUENTIAL DAMAGES, INCLUDING DAMAGES RESULTING FROM LOSS OF USE, DATA, OR PROJECTS, WHETHER IN AN ACTION OF
 * CONTRACT OR TORT, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THE CONTENTS. Third-party contents
 * included in this file may be subject to different terms.
 **********************************************************************************************************************/
/**********************************************************************************************************************
* File Name    : hw_adc_private.h
* Description  : ADC LLD implementation
***********************************************************************************************************************/

/*******************************************************************************************************************//**
 * @addtogroup ADC
 * @{
***********************************************************************************************************************/

#ifndef HW_ADC_PRIVATE_H
#define HW_ADC_PRIVATE_H

/**********************************************************************************************************************
Includes
***********************************************************************************************************************/
#include "bsp_api.h"
#include "r_adc.h"


/** Common macro for SSP header files. There is also a corresponding SSP_FOOTER macro at the end of this file. */
SSP_HEADER

/**********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
#ifdef R_S12ADC0_BASE
/*LDRA_INSPECTED 77 S This macro does not work when surrounded by parentheses. */
#define ADC_BASE_PTR  R_S12ADC0_Type *
#elif defined(R_S14ADC_BASE)
/*LDRA_INSPECTED 77 S This macro does not work when surrounded by parentheses. */
#define ADC_BASE_PTR  R_S14ADC_Type *
#endif

/** Defines the registers settings for the ADC synchronous ELC trigger*/
#define ADC_ELC_TRIGGER            (0x09U)
#define ADC_ELC_TRIGGER_GROUP_B    (0x0AU)
#define ADC_ELC_TRIGGER_DISABLED   (0x3FU)

/** Sample and hold bypass applies to these channels. */
#define ADC_MASK_SAMPLE_HOLD_BYPASS_CHANNELS  (0x3U)

/** Sample and hold bypass starts at bit 8. */
#define ADC_MASK_SAMPLE_HOLD_BYPASS_SHIFT     (9U)

/**********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/
/** ADCSR register ADCS field */
typedef enum e_adc_adcs
{
    ADC_ADCS_SINGLE_SCAN=0U,
    ADC_ADCS_GROUP_SCAN=1U,
    ADC_ADCS_CONT_SCAN=2U,
    ADC_ADCS_MAX
} adc_adcs_t;

/** State of ADST bit*/
typedef enum e_hw_adc_adcsr_adst
{
    HW_ADC_ADCSR_ADCST_NOT_SET= (0x0),
    HW_ADC_ADCSR_ADCST_SET    = (0x1),
    HW_ADC_ADCSR_ADCST_MAX
} hw_adc_adcsr_adst_t;

/** State of TRGE bit*/
typedef enum e_hw_adc_adcsr_trge
{
    HW_ADC_ADCSR_TRGE_NOT_SET= (0x0),
    HW_ADC_ADCSR_TRGE_SET    = (0x1),
    HW_ADC_ADCSR_TRGE_MAX
} hw_adc_adcsr_trge_t;

/** State of ADIE bit*/
typedef enum e_hw_adc_adcsr_adie
{
    HW_ADC_ADCSR_ADIE_NOT_SET= (0x0),
    HW_ADC_ADCSR_ADIE_SET    = (0x1),
    HW_ADC_ADCSR_ADIE_MAX
} hw_adc_adcsr_adie_t;

/** State of GBADIE bit*/
typedef enum e_hw_adc_adcsr_gbadie
{
    HW_ADC_ADCSR_GBADIE_NOT_SET= (0x0),
    HW_ADC_ADCSR_GBADIE_SET    = (0x1),
    HW_ADC_ADCSR_GBADIE_MAX
} hw_adc_adcsr_gbadie_t;

/***********************************************************************************************************************
 * Private Global Variables
 **********************************************************************************************************************/
static R_TSN_Type * gp_tsn_regs = NULL;

/***********************************************************************************************************************
 * Private Functions
 **********************************************************************************************************************/

/** Set function for ADC.ADCSR.ADST */
__STATIC_INLINE void HW_ADC_ADCSR_ADST_Set(ADC_BASE_PTR p_regs, hw_adc_adcsr_adst_t value)
{
    p_regs->ADCSR_b.ADST = value;
}
/** Get function for ADC.ADCSR.ADST */
__STATIC_INLINE hw_adc_adcsr_adst_t HW_ADC_ADCSR_ADST_Get(ADC_BASE_PTR p_regs)
{
    return((hw_adc_adcsr_adst_t)p_regs->ADCSR_b.ADST);
}
/** Set function for ADC.ADCSR.TRGE */
__STATIC_INLINE void HW_ADC_ADCSR_TRGE_Set(ADC_BASE_PTR p_regs, hw_adc_adcsr_trge_t value)
{
    p_regs->ADCSR_b.TRGE = value;
}
/** Set function for ADC.ADCSR.ADIE */
__STATIC_INLINE void HW_ADC_ADCSR_ADIE_Set(ADC_BASE_PTR p_regs, hw_adc_adcsr_adie_t value)
{
    p_regs->ADCSR_b.ADIE = value;
}
/** Set function for ADC.ADCSR.GBADIE */
__STATIC_INLINE void HW_ADC_ADCSR_GBADIE_Set(ADC_BASE_PTR p_regs, hw_adc_adcsr_gbadie_t value)
{
    p_regs->ADCSR_b.GBADIE = value;
}

/*******************************************************************************************************************//**
 * @brief   HW_ADC_ScanCfgCheckSampleHoldGroup
 *
 * This function checks the Sample and Hold arguments to verify group restrictions are not violated.
 *
 * @param[in]  unit           :  ADC unit number
 * @param[in]  p_cfg          : The configuration argument passed to R_ADC_ScanConfigure.
 **********************************************************************************************************************/
__STATIC_INLINE ssp_err_t HW_ADC_ScanCfgCheckSampleHoldGroup(ADC_BASE_PTR p_regs, adc_channel_cfg_t const * const p_cfg)
{
    uint32_t a_mask;
    uint32_t b_mask;

    /** Sample and Hold  channels cannot not be split across groups  */
    a_mask = p_cfg->sample_hold_mask & p_cfg->scan_mask;
    b_mask = p_cfg->sample_hold_mask & p_cfg->scan_mask_group_b;
    if (ADC_ADCS_GROUP_SCAN == p_regs->ADCSR_b.ADCS)
    {
        if ((a_mask != p_cfg->sample_hold_mask) && (b_mask != p_cfg->sample_hold_mask))
        {
            return SSP_ERR_INVALID_ARGUMENT;
        }
    }
    else if (a_mask != p_cfg->sample_hold_mask)
    {
        return SSP_ERR_INVALID_ARGUMENT;
    }
    else
    {
        /** Will not get here: Branch created to meet coding standard */
    }

    /** Sample and Hold  channels cannot be a double trigger channel (can be in group B) */
    if ((1UL == p_regs->ADCSR_b.DBLE) && (0 != a_mask))
    {
        return SSP_ERR_INVALID_ARGUMENT;
    }

    /** Sample and Hold  channels cannot be in GroupB if GroupA priority enabled     */
    if ((0 != b_mask) && (ADC_GROUP_A_PRIORITY_OFF != p_cfg->priority_group_a))
    {
        return SSP_ERR_INVALID_ARGUMENT;
    }

    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief   HW_ADC_ScanCfgCheckSampleHold
 *
 * This function checks the Sample and Hold arguments
 *
 * @param[in]  unit           :  ADC unit number
 * @param[in]  p_cfg          : The configuration argument passed to R_ADC_ScanConfigure.
 **********************************************************************************************************************/
__STATIC_INLINE ssp_err_t HW_ADC_ScanCfgCheckSampleHold(ADC_BASE_PTR p_regs, adc_channel_cfg_t const * const p_cfg)
{
    /** If a valid value is set in the mask. */
    if (0 != p_cfg->sample_hold_mask)
    {
        /** If sample and hold is not supported, the mask should be 0. */
        bsp_feature_adc_t adc_feature = {0U};
        R_BSP_FeatureAdcGet(&adc_feature);
        if (!adc_feature.has_sample_hold_reg)
        {
            return SSP_ERR_INVALID_ARGUMENT;
        }

        /** Sample and Hold channels can only be 0,1,2 and must have at least minimum state count specified */
        if ((ADC_SAMPLE_HOLD_CHANNELS < p_cfg->sample_hold_mask)
                || (ADC_SAMPLE_STATE_HOLD_COUNT_MIN > p_cfg->sample_hold_states))
        {
            return SSP_ERR_INVALID_ARGUMENT;
        }

        return HW_ADC_ScanCfgCheckSampleHoldGroup(p_regs, p_cfg);
    }
    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief   HW_ADC_ScanCfgSetSampleHold
 *
 * This function sets the sample and Hold arguments
 *
 * @param[in]  p_cfg          : The configuration argument passed to R_ADC_ScanConfigure.
 * @param[in]  unit           :  ADC unit number
 **********************************************************************************************************************/

__STATIC_INLINE void HW_ADC_ScanCfgSetSampleHold(ADC_BASE_PTR p_regs, adc_channel_cfg_t const * const p_cfg)
{
    bsp_feature_adc_t adc_feature = {0U};
    R_BSP_FeatureAdcGet(&adc_feature);
    if (adc_feature.has_sample_hold_reg)
    {
        uint16_t adshcr = p_cfg->sample_hold_states;
        adshcr |= (uint16_t) ((uint16_t) (p_cfg->sample_hold_mask & ADC_MASK_SAMPLE_HOLD_BYPASS_CHANNELS) << ADC_MASK_SAMPLE_HOLD_BYPASS_SHIFT);

        p_regs->ADSHCR = adshcr;
    }
}

/*******************************************************************************************************************//**
 * @brief   HW_ADC_ScanCfgCheckSensorsExclusive
 *
 * This function checks the Temperature and Voltage sensor arguments for restrictions that apply to MCUs that do not
 * allow both sensors to be used at once.
 *
 * @param[in]  p_ctrl         : The ADC instance control block.
 * @param[in]  p_cfg          : The configuration argument passed to R_ADC_ScanConfigure.
 **********************************************************************************************************************/
__STATIC_INLINE ssp_err_t HW_ADC_ScanCfgCheckSensorsExclusive(adc_instance_ctrl_t * p_ctrl, adc_channel_cfg_t const * const p_cfg)
{
    /** Both sensors cannot be used at the same time on some MCUs. */
    if ((p_cfg->scan_mask & ADC_MASK_TEMPERATURE) && (p_cfg->scan_mask & ADC_MASK_VOLT))
    {
        return SSP_ERR_INVALID_ARGUMENT;
    }

    /** If any of the sensors are being used, then none of the channels can be used at the same time on some MCUs. */
    if ((p_cfg->scan_mask & ADC_MASK_SENSORS) && (p_cfg->scan_mask &
                          (ADC_MASK_CHANNEL_0 | ADC_MASK_CHANNEL_1 | ADC_MASK_CHANNEL_2 | ADC_MASK_CHANNEL_3 |
                           ADC_MASK_CHANNEL_4 | ADC_MASK_CHANNEL_5 | ADC_MASK_CHANNEL_6 | ADC_MASK_CHANNEL_7 |
                           ADC_MASK_CHANNEL_8 | ADC_MASK_CHANNEL_9 | ADC_MASK_CHANNEL_10 | ADC_MASK_CHANNEL_11 |
                           ADC_MASK_CHANNEL_12 | ADC_MASK_CHANNEL_13 | ADC_MASK_CHANNEL_14 | ADC_MASK_CHANNEL_15 |
                           ADC_MASK_CHANNEL_16 | ADC_MASK_CHANNEL_17 | ADC_MASK_CHANNEL_18 | ADC_MASK_CHANNEL_19 |
                           ADC_MASK_CHANNEL_20 | ADC_MASK_CHANNEL_21 | ADC_MASK_CHANNEL_22 | ADC_MASK_CHANNEL_23 |
                           ADC_MASK_CHANNEL_24 | ADC_MASK_CHANNEL_25 | ADC_MASK_CHANNEL_26 | ADC_MASK_CHANNEL_27 )))
    {
                return SSP_ERR_INVALID_ARGUMENT;
    }

    /** Sensors cannot be used in any mode other than single scan mode on some MCUs. */
    if ((p_cfg->scan_mask & ADC_MASK_SENSORS) && (ADC_MODE_SINGLE_SCAN != p_ctrl->mode))
    {
        return SSP_ERR_INVALID_ARGUMENT;
    }

    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief   HW_ADC_ScanCfgCheckSensors
 *
 * This function checks the Temperature and Voltage sensor arguments
 *
 * @param[in]  p_ctrl         : The ADC instance control block.
 * @param[in]  p_cfg          : The configuration argument passed to R_ADC_ScanConfigure.
 **********************************************************************************************************************/
__STATIC_INLINE ssp_err_t HW_ADC_ScanCfgCheckSensors(adc_instance_ctrl_t * p_ctrl, adc_channel_cfg_t const * const p_cfg)
{
    ADC_BASE_PTR p_regs = (ADC_BASE_PTR) p_ctrl->p_reg;

    bsp_feature_adc_t adc_feature = {0U};
    R_BSP_FeatureAdcGet(&adc_feature);
    if (!adc_feature.group_b_sensors_allowed)
    {
        /** Sensors are not supported in Group B in some MCUs. */
        if (p_cfg->scan_mask_group_b & ADC_MASK_SENSORS)
        {
            return SSP_ERR_INVALID_ARGUMENT;
        }
    }

    /** If sensors specified in Normal/Group A, verify in legal configuration
     * If sensors are used, then Double Trigger and Disconnect Detection cannot be used */
    if ((p_cfg->scan_mask & ADC_MASK_SENSORS))
    {
        if (1UL == p_regs->ADCSR_b.DBLE)  /*** Double trigger mode          */
        {
            return SSP_ERR_INVALID_ARGUMENT;
        }
        if (!adc_feature.sensors_exclusive)
        {
            /** Disconnect detection register must be 0 to use sensors on some MCUs.  */
            if (0UL != p_regs->ADDISCR)
            {
                return SSP_ERR_INVALID_ARGUMENT;
            }
        }
    }

    if (adc_feature.sensors_exclusive)
    {
        return HW_ADC_ScanCfgCheckSensorsExclusive(p_ctrl, p_cfg);
    }

    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief   HW_ADC_SensorCfgTemperatureGroupA
 *
 * This function set the sensor bits to enable the temperature sensor for group A/normal mode.
 *
 * @param[in]  p_cfg          : The configuration argument passed to R_ADC_ScanConfigure.
 * @param[in]  unit           :  ADC unit number
***********************************************************************************************************************/
__STATIC_INLINE void HW_ADC_SensorCfgTemperatureGroupA(ADC_BASE_PTR p_regs)
{
    p_regs->ADEXICR_b.TSSA = 1U;      /** Select temperature output GroupA */
    p_regs->ADEXICR_b.TSSB = 0U;
}
/*******************************************************************************************************************//**
 * @brief   HW_ADC_SensorCfgTemperatureGroupB
 *
 * This function set the sensor bits to enable the temperature sensor for group B.
 *
 * @param[in]  p_cfg          : The configuration argument passed to R_ADC_ScanConfigure.
 * @param[in]  unit           :  ADC unit number
***********************************************************************************************************************/
__STATIC_INLINE void HW_ADC_SensorCfgTemperatureGroupB(ADC_BASE_PTR p_regs)
{
    p_regs->ADEXICR_b.TSSA = 0U;
    p_regs->ADEXICR_b.TSSB = 1U;      /** Select temperature output GroupB */
}

/*******************************************************************************************************************//**
 * @brief   HW_ADC_SensorCfgTemperature
 *
 * This function set the sensor bits to enable the temperature sensor for group A/normal mode.
 *
 * @param[in]  unit           :  ADC unit number
 * @param[in]  p_cfg          : The configuration argument passed to R_ADC_ScanConfigure.
 **********************************************************************************************************************/

__STATIC_INLINE ssp_err_t HW_ADC_SensorCfgTemperature(ADC_BASE_PTR p_regs, adc_channel_cfg_t const * const p_cfg)
{
    /** Get temperature sensor control register. */
    ssp_feature_t ssp_feature = {{(ssp_ip_t) 0U}};
    ssp_feature.id = SSP_IP_TSN;
    ssp_feature.channel = 0U;
    ssp_feature.unit = 0U;
    fmi_feature_info_t feature_info = {0U};
    ssp_err_t err = g_fmi_on_fmi.productFeatureGet(&ssp_feature, &feature_info);
    if (SSP_SUCCESS != err)
    {
        return err;
    }
    gp_tsn_regs = (R_TSN_Type *) feature_info.ptr;

    /** Power on the temperature sensor */
    R_BSP_ModuleStart(&ssp_feature);

    if (p_cfg->scan_mask & ADC_MASK_TEMPERATURE)
    {
        HW_ADC_SensorCfgTemperatureGroupA(p_regs);      /** Select temperature output GroupA */
    }
    else
    {
        HW_ADC_SensorCfgTemperatureGroupB(p_regs);
    }
    /** Enable temperature addition mode if set */
    p_regs->ADEXICR_b.TSSAD = (uint16_t) ((p_cfg->add_mask & ADC_MASK_TEMPERATURE) ? 1U : 0U);

    gp_tsn_regs->TSCR_b.TSEN = 1UL;             /** Enable temperature sensor           */
    /** 30us delay  to stabilize the clock before an accurate reading can be taken.
     * This only applies when the sensor is powered on the first time */
    gp_tsn_regs->TSCR_b.TSOE = 1UL;             /** Enable temperature sensor output to ADC */

    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief   HW_ADC_SensorCfgVoltageGroupA
 *
 * This function set the sensor  bits to enable the voltage sensor for group A/normal mode.
 *
 * @param[in]  p_cfg          : The configuration argument passed to R_ADC_ScanConfigure.
 * @param[in]  unit           :  ADC unit number
 **********************************************************************************************************************/
__STATIC_INLINE void HW_ADC_SensorCfgVoltageGroupA(ADC_BASE_PTR p_regs)
{
    p_regs->ADEXICR_b.OCSA = 1U;      /** Select voltage output to GroupA  */
    p_regs->ADEXICR_b.OCSB = 0U;
}
/*******************************************************************************************************************//**
 * @brief   HW_ADC_SensorCfgVoltageGroupB
 *
 * This function set the sensor bits to enable the voltage sensor for group B.
 *
 * @param[in]  p_cfg          : The configuration argument passed to R_ADC_ScanConfigure.
 * @param[in]  unit           :  ADC unit number
 **********************************************************************************************************************/
__STATIC_INLINE void HW_ADC_SensorCfgVoltageGroupB(ADC_BASE_PTR p_regs)
{
    p_regs->ADEXICR_b.OCSA = 0U;
    p_regs->ADEXICR_b.OCSB = 1U;      /** Select voltage output to GroupB  */
}
/*******************************************************************************************************************//**
 * @brief   HW_ADC_S12adiEnable
 *
 * This function clears the S12ADI interrupt flag and enables interrupts in the peripheral (for IR flag usage).
 * If priority is not 0, interrupts are enabled in the ICU.
 *
 * @param[in]  unit :  ADC unit number
 *
 * @note: This has nothing to do with enabling triggers.
 **********************************************************************************************************************/
__STATIC_INLINE void HW_ADC_S12adiEnable(ADC_BASE_PTR p_regs)
{
    p_regs->ADCSR_b.ADIE = 1U;                /*** Enable in peripheral  */
}

/*******************************************************************************************************************//**
 * @brief   HW_ADC_S12gbadiEnable
 *
 * This function clears the S12GBADI interrupt flag and enables interrupts in the peripheral (for IR flag usage).
 * If priority is not 0, interrupts are enabled in the ICU.
 *
 * @param[in]  unit :  ADC unit number
 *
 * @note: This has nothing to do with enabling triggers.
 **********************************************************************************************************************/
__STATIC_INLINE void HW_ADC_S12gbadiEnable(ADC_BASE_PTR p_regs)
{
    p_regs->ADCSR_b.GBADIE = 1U;                /*** Enable in peripheral  */
}

/*******************************************************************************************************************//**
 * @brief   HW_ADC_S12gbadiDisable
 *
 * This function clears the S12GBADI interrupt flag and disables interrupts in the peripheral (for IR flag usage).
 *
 * @param[in]  unit :  ADC unit number
 *
 * @note: This has nothing to do with disabling triggers.
 **********************************************************************************************************************/
__STATIC_INLINE void HW_ADC_S12gbadiDisable(ADC_BASE_PTR p_regs)
{
    p_regs->ADCSR_b.GBADIE = 0UL;                /*** Disable in peripheral  */
}
/*******************************************************************************************************************//**
 * @brief HW_ADC_Close
 *
 * The Close function stops power and clocking to the A/D peripheral, disables interrupts and turns off the channels and
 * sensors.
 *
 * @param[in]  Unit  : The ADC Unit to stop
 *
 * @retval  None
 **********************************************************************************************************************/
__STATIC_INLINE void HW_ADC_Close(ADC_BASE_PTR p_regs, adc_instance_ctrl_t * p_ctrl)
{
    /** If Group Priority was enabled, the disable it*/
    if (1U == p_regs->ADGSPCR_b.PGS)
    {
        p_regs->ADGSPCR_b.PGS = 0UL;
    }

    /** If Group Mode was enabled, then disable the triggers for both groups */
    if (1U == p_regs->ADCSR_b.ADCS)
    {
        p_regs->ADSTRGR = 0x3F3FU;
    }
    /** If Group Mode was not enabled, then disable the trigger for normal mode */
    else
    {
        p_regs->ADSTRGR_b.TRSA = 0x3FU;
    }

    /** Clear all configurations*/
    p_regs->ADCSR = 0UL;

    /** Power down peripheral   */
    ssp_feature_t adc_feature = {{(ssp_ip_t) 0U}};
    adc_feature.id = SSP_IP_ADC;
    adc_feature.channel = p_ctrl->unit;
    adc_feature.unit = 0U;
    R_BSP_ModuleStop(&adc_feature);
    if (0 != (p_ctrl->scan_mask & ADC_MASK_TEMPERATURE))
    {
        if (NULL != gp_tsn_regs)
        {
            gp_tsn_regs->TSCR = 0UL;                /*** Disable temperature sensor      */
        }
        ssp_feature_t tsn_feature = {{(ssp_ip_t) 0U}};
        tsn_feature.id = SSP_IP_TSN;
        tsn_feature.channel = 0U;
        tsn_feature.unit = 0U;
        R_BSP_ModuleStop(&tsn_feature);
    }
}

/*******************************************************************************************************************//**
 * @brief HW_ADC_Open
 *
 * The Open function applies power to the A/D peripheral, sets the operational mode, trigger sources, and
 * configurations common to all channels and sensors.
 *
 * @param[in]  p_cfg  : Pointer to configuration structure
 *
 * @retval  None
***********************************************************************************************************************/
__STATIC_INLINE void HW_ADC_Open(ADC_BASE_PTR p_regs, adc_cfg_t const * const p_cfg)
{
    /** Apply power to peripheral   */
    ssp_feature_t adc_feature = {{(ssp_ip_t) 0U}};
    adc_feature.id = SSP_IP_ADC;
    adc_feature.channel = p_cfg->unit;
    adc_feature.unit = 0U;
    R_BSP_ModuleStart(&adc_feature);

    /** Clear all settings including disabling interrupts*/
    p_regs->ADCSR = 0UL;
    /** Clear the sensor settings*/
    p_regs->ADEXICR = 0UL;

    /** Set mode related register fields    */
    if (ADC_MODE_GROUP_SCAN == p_cfg->mode)
    {
        p_regs->ADCSR_b.ADCS = ADC_ADCS_GROUP_SCAN;
    }
    else
    {
        if ((ADC_MODE_CONTINUOUS_SCAN == p_cfg->mode))
        {
            p_regs->ADCSR_b.ADCS = ADC_ADCS_CONT_SCAN;
        }
        else
        {
            /** Do Nothing: Branch created to meet coding standard*/
        }
    /** other modes have ADCS=0  */
    }

    /** Disable double trigger since this is not currently supported */
    p_regs->ADCSR_b.DBLE = 0UL;

    /** Configure trigger if its a hardware trigger*/
    if (ADC_TRIGGER_SYNC_ELC == p_cfg->trigger)
    {
        /** Set the ELC value for normal/groupA trigger*/
        p_regs->ADSTRGR_b.TRSA = ADC_ELC_TRIGGER;
    }
    /** Configure external trigger for async trigger*/
    else if (ADC_TRIGGER_ASYNC_EXT_TRG0 == p_cfg->trigger)
    {
        /** Enable Asynchronous external trigger*/
        p_regs->ADCSR_b.EXTRG = 1U;
        /** Set TRSA to 0x00 if using Asynchronous external trigger*/
        p_regs->ADSTRGR_b.TRSA = 0U;
    }
    else
    {
        /** Do Nothing: Branch created to meet coding standard*/
    }

    /** Configure ADC resolution for this unit */
    p_regs->ADCER_b.ADPRC = p_cfg->resolution;
    /** Configure ADC alignment for this unit */
    p_regs->ADCER_b.ADRFMT = (ADC_ALIGNMENT_LEFT == p_cfg->alignment) ? 1U : 0U;
    /** Configure is the result register should be cleared after reading for this unit */
    p_regs->ADCER_b.ACE = (ADC_CLEAR_AFTER_READ_ON == p_cfg->clearing) ? 1U : 0U;
    /** Configure the count for result addition or averaging*/
    p_regs->ADADC = p_cfg->add_average_count;

}

/*******************************************************************************************************************//**
 * @brief   HW_ADC_SensorCfg
 *
 * This function set the sensor bits taking into account group inclusion and addition/average mode.
 *
 * @param[in]  p_cfg          : The configuration argument passed to R_ADC_ScanConfigure.
 * @param[in]  unit           :  ADC unit number
***********************************************************************************************************************/
__STATIC_INLINE ssp_err_t HW_ADC_SensorCfg(ADC_BASE_PTR p_regs, adc_channel_cfg_t const * const p_cfg)
{
    /** Temperature sensor  */
    ssp_err_t err;
    if ((p_cfg->scan_mask & ADC_MASK_TEMPERATURE) || (p_cfg->scan_mask_group_b & ADC_MASK_TEMPERATURE))
    {
        p_regs->ADSSTRT = 0x80;                  /**sample state registers are set to 0x80*/
        err = HW_ADC_SensorCfgTemperature(p_regs, p_cfg);
        if (SSP_SUCCESS != err)
        {
            return err;
        }
    }

    /** Internal reference voltage sensor   */
    if ((p_cfg->scan_mask & ADC_MASK_VOLT) || (p_cfg->scan_mask_group_b & ADC_MASK_VOLT))
    {
        p_regs->ADSSTRO = 0x80;                   /**sample state registers are set to 0x80*/
        if (p_cfg->scan_mask & ADC_MASK_VOLT)
        {
            HW_ADC_SensorCfgVoltageGroupA(p_regs);    /** Select voltage output to GroupA  */
        }
        else
        {
            HW_ADC_SensorCfgVoltageGroupB(p_regs);      /** Select voltage output to GroupB  */
        }

        p_regs->ADEXICR_b.OCSAD = (uint16_t) ((p_cfg->add_mask & ADC_MASK_VOLT) ? 1U : 0U); /** Enable voltage addition mode if set */
    }

    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief   HW_ADC_ScanCfgCheckAddition
 *
 * This function checks the addition arguments
 *
 * @param[in]  p_regs         : Pointer to ADC registers for this unit
 * @param[in]  p_cfg          : The configuration argument passed to R_ADC_ScanConfigure.
 **********************************************************************************************************************/
#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
__STATIC_INLINE ssp_err_t HW_ADC_ScanCfgCheckAddition(ADC_BASE_PTR p_regs, adc_channel_cfg_t const * const p_cfg)
{
    /** Verify that if addition is enabled, then at least one proper channel is selected*/
    if (ADC_ADD_OFF != p_regs->ADADC_b.ADC)
    {
        /** Addition mask should not include bits from inactive channels.
         * This also serves as a check for valid channels in the addition mask */
        uint32_t tmp_mask = 0;
        tmp_mask = p_cfg->scan_mask_group_b;
        tmp_mask |= p_cfg->scan_mask;   /*** tmp_mask is Group A and B combined   */
        /*** Bit-AND with 1s-complement   */
        if ((0U != (p_cfg->add_mask & ~tmp_mask)) || (0U == p_cfg->add_mask))
        {
            return SSP_ERR_INVALID_ARGUMENT;
        }
    }
    /** Channels are selected for addition despite addition being disabled.
        WARNING! Other features messed up if add_mask is non-zero when addition is turned off! */
    else
    {
        if (ADC_MASK_ADD_OFF != p_cfg->add_mask)
        {
             return SSP_ERR_INVALID_ARGUMENT;
        }
    }

    return SSP_SUCCESS;
}
#endif

/*******************************************************************************************************************//**
 * @brief   HW_ADC_ScanCfgCheck
 *
 * This function does extensive checking on channel mask settings based upon operational mode.
 *
 * NOTE: A negative number is stored in two's complement form.
 *       A quick way to change a binary number into two's complement is to
 *       start at the right (LSB) and moving left, don't change any bits
 *       until after the first "1" is reached.
 *       Number          2's complement
 *       0010 0110       1101 1010
 *       Another way is to do a 1's complement on the number, then add 1 to that.
 *       Number          1's complement  + 1
 *       0010 0110       1101 1001       1101 1010
 *
 * @param[in]  p_ctrl   :  ADC unit control block
 * @param[in]  p_cfg    :  Pointer to channel configuration structure containing masks
 *
 * @retval  SSP_SUCCESS -               Successful
 * @retval  SSP_ERR_INVALID_ARGUMENT -  Parameter has invalid/illegal value.
***********************************************************************************************************************/
#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
__STATIC_INLINE ssp_err_t HW_ADC_ScanCfgCheck(adc_instance_ctrl_t * p_ctrl, adc_channel_cfg_t const * const p_cfg,
        uint32_t * p_valid_channels)
{
    ssp_err_t err;
    uint16_t unit = p_ctrl->unit;
    ADC_BASE_PTR p_regs = (ADC_BASE_PTR) p_ctrl->p_reg;

    /** Verify at least one bonded channel is selected  */
    uint32_t valid_channels = p_valid_channels[unit] | ADC_MASK_TEMPERATURE | ADC_MASK_VOLT;
    if ((0U == p_cfg->scan_mask)
     || (0U != (p_cfg->scan_mask & (~valid_channels))))
    {
        return SSP_ERR_INVALID_ARGUMENT;
    }

 /** Verify at least one unique bonded channel is selected for Group B
     * and GroupA priority setting is valid. */
    if (ADC_ADCS_GROUP_SCAN == p_regs->ADCSR_b.ADCS)
    {
        if ((0U == p_cfg->scan_mask_group_b)
         || (0U != (p_cfg->scan_mask_group_b & (~valid_channels))))
        {
            return SSP_ERR_INVALID_ARGUMENT;
        }
        else if (0 != (p_cfg->scan_mask & p_cfg->scan_mask_group_b))
        {
            return SSP_ERR_INVALID_ARGUMENT;         /*** Same channel in both groups */
        }
        else
        {
            /** Settings are valid, do nothing. */
        }
    }
    /*** If Group mode was not enabled, but channels were selected for group B*/
    else
    {
        if (ADC_MASK_GROUP_B_OFF != p_cfg->scan_mask_group_b)
        {
            return SSP_ERR_INVALID_ARGUMENT;
        }
    }

    /** Verify sensors are in legal combination */
    err = HW_ADC_ScanCfgCheckSensors(p_ctrl, p_cfg);
    if (SSP_SUCCESS != err)
    {
        return err;
    }

    /** Check addition settings. */
    err = HW_ADC_ScanCfgCheckAddition(p_regs, p_cfg);
    if (SSP_SUCCESS != err)
    {
        return err;
    }

    /** Check sample and hold settings. */
    err = HW_ADC_ScanCfgCheckSampleHold(p_regs, p_cfg);

    return err;
}
#endif

/*******************************************************************************************************************//**
 * @brief   HW_ADC_InterruptsCfg
 *
 * Configures interrupts based on user settings.
 *
 * @param[in]  p_ctrl : ADC instance control block
 * @param[in]  p_cfg  : Pointer to channel configuration structure containing masks
***********************************************************************************************************************/
__STATIC_INLINE void HW_ADC_InterruptsCfg(adc_instance_ctrl_t * p_ctrl, adc_channel_cfg_t const * const p_cfg)
{
    ADC_BASE_PTR p_regs = (ADC_BASE_PTR) p_ctrl->p_reg;

    /** For all modes other than continuous scan mode, enable the scan completion interrupt*/
    if (ADC_MODE_CONTINUOUS_SCAN != p_ctrl->mode)
    {
        /** Enable Group B scan completion interrupt */
        if (ADC_ADCS_GROUP_SCAN == p_regs->ADCSR_b.ADCS)
        {
            /** If the group priority is not set to group B continuous scan mode, then enable the group B interrupt*/
            if (ADC_GROUP_A_GROUP_B_CONTINUOUS_SCAN !=  p_cfg->priority_group_a)
            {
                if (SSP_INVALID_VECTOR != p_ctrl->scan_end_b_irq)
                {
                    R_BSP_IrqStatusClear (p_ctrl->scan_end_b_irq) ;    /*** Clear flag in ICU     */
                    NVIC_ClearPendingIRQ (p_ctrl->scan_end_b_irq) ;    /*** Clear flag in NVIC    */
                    NVIC_EnableIRQ (p_ctrl->scan_end_b_irq) ;          /*** Enable in NVIC        */
                }
                HW_ADC_S12gbadiEnable(p_regs);

                /** Set the ELC value for groupB trigger*/
                p_regs->ADSTRGR_b.TRSB = ADC_ELC_TRIGGER_GROUP_B;
            }
            /** If the group priority mode is group B in continuous scan mode, then disable the group B
             * synchronous trigger and disable the group B interrupt to prevent the ISR from triggering continuously */
            else
            {
                p_regs->ADSTRGR_b.TRSB = ADC_ELC_TRIGGER_DISABLED;
                if (SSP_INVALID_VECTOR != p_ctrl->scan_end_b_irq)
                {
                    NVIC_DisableIRQ (p_ctrl->scan_end_b_irq) ;         /*** Disable in NVIC        */
                    NVIC_ClearPendingIRQ (p_ctrl->scan_end_b_irq) ;    /*** Clear flag in NVIC    */
                    R_BSP_IrqStatusClear (p_ctrl->scan_end_b_irq) ;    /*** Clear flag in ICU     */
                }
                HW_ADC_S12gbadiDisable(p_regs);
            }
        }
        /** Enable the Normal Mode/Group A scan completion interrupt */
        if (SSP_INVALID_VECTOR != p_ctrl->scan_end_irq)
        {
            R_BSP_IrqStatusClear (p_ctrl->scan_end_irq) ;    /*** Clear flag in ICU     */
            NVIC_ClearPendingIRQ (p_ctrl->scan_end_irq) ;    /*** Clear flag in NVIC    */
            NVIC_EnableIRQ (p_ctrl->scan_end_irq) ;          /*** Enable in NVIC        */
        }
        HW_ADC_S12adiEnable(p_regs);
    }
}

/*******************************************************************************************************************//**
 * @brief   HW_ADC_ScanCfg
 *
 * This function does extensive checking on channel mask settings based upon operational mode. Mask registers are
 * initialized and interrupts enabled in peripheral. Interrupts are also enabled in ICU if corresponding priority
 * is not 0.
 *
 * @param[in]  unit  : ADC unit number
 * @param[in]  p_cfg : Pointer to channel configuration structure containing masks
 *
 * @retval  SSP_SUCCESS -               Successful
 * @retval  SSP_ERR_INVALID_POINTER -   p_cfg pointer is NULL
 * @retval  SSP_ERR_INVALID_ARGUMENT -  Parameter has invalid/illegal value.
***********************************************************************************************************************/
__STATIC_INLINE ssp_err_t HW_ADC_ScanCfg(adc_instance_ctrl_t * p_ctrl, adc_channel_cfg_t const * const p_cfg,
        uint32_t * p_valid_channels)
{
    ADC_BASE_PTR p_regs = (ADC_BASE_PTR) p_ctrl->p_reg;
    ssp_err_t err = SSP_SUCCESS;

    /** parameter checking  */
#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
    err = HW_ADC_ScanCfgCheck(p_ctrl, p_cfg, p_valid_channels);
    if (SSP_SUCCESS != err)
    {
        return err;
    }
#endif

    /** Set mask for channels and sensors for channels 0 to 15.
     * At this point, the mask values in scan_mask, scan_mask_group_b and add_mask
     * have already been determined to be valid for this MCU by the HW_ADC_ScanCfgCheck()
     * function so the values in those variables can be directly written to the
     * registers. */
    p_regs->ADANSA0 = (uint16_t) (p_cfg->scan_mask);
    p_regs->ADANSB0 = (uint16_t) (p_cfg->scan_mask_group_b);
    p_regs->ADADS0 = (uint16_t) (p_cfg->add_mask);
    /** Set mask for channels and sensors for higher channels   */
    p_regs->ADANSA1 = (uint16_t) ((p_cfg->scan_mask >> 16));
    p_regs->ADANSB1 = (uint16_t) ((p_cfg->scan_mask_group_b >> 16));
    p_regs->ADADS1 = (uint16_t) ((p_cfg->add_mask >> 16));

    /** Configure Sensors*/
    err = HW_ADC_SensorCfg(p_regs, p_cfg);
    if (SSP_SUCCESS != err)
    {
        return err;
    }

    /** NOTE: S&H adds to scan time because normal state machine still runs.
     adds 12 or more sample_hold_states ADCLKS to scan time */
    HW_ADC_ScanCfgSetSampleHold(p_regs, p_cfg);

    /** Set group A priority action (not interrupt priority!)
     * This must be set prior to configuring the interrupts for all modes other than ADC_GROUP_A_GROUP_B_CONTINUOUS_SCAN */
    if (ADC_GROUP_A_GROUP_B_CONTINUOUS_SCAN !=  p_cfg->priority_group_a)
    {
        p_regs->ADGSPCR = p_cfg->priority_group_a;
    }

    /** Configure ADC interrupts. */
    HW_ADC_InterruptsCfg(p_ctrl, p_cfg);

    /** Set group A priority action (not interrupt priority!)
     * This will also start the Group B scans if configured for ADC_GROUP_A_GROUP_B_CONTINUOUS_SCAN.
     * This must be configured after the interrupts/triggers have been setup for the ADC_GROUP_A_GROUP_B_CONTINUOUS_SCAN */
    if (ADC_GROUP_A_GROUP_B_CONTINUOUS_SCAN ==  p_cfg->priority_group_a)
    {
        p_regs->ADGSPCR = p_cfg->priority_group_a;
    }

    return err;
}

/** Common macro for SSP header files. There is also a corresponding SSP_HEADER macro at the top of this file. */
SSP_FOOTER

#endif /* HW_ADC_PRIVATE_H */

/*******************************************************************************************************************//**
 * @} (end addtogroup ADC)
***********************************************************************************************************************/
