/***********************************************************************************************************************
 * Copyright [2015] Renesas Electronics Corporation and/or its licensors. All Rights Reserved.
 * 
 * This file is part of Renesas SynergyTM Software Package (SSP)
 *
 * The contents of this file (the "contents") are proprietary and confidential to Renesas Electronics Corporation
 * and/or its licensors ("Renesas") and subject to statutory and contractual protections.
 *
 * This file is subject to a Renesas SSP license agreement. Unless otherwise agreed in an SSP license agreement with
 * Renesas: 1) you may not use, copy, modify, distribute, display, or perform the contents; 2) you may not use any name
 * or mark of Renesas for advertising or publicity purposes or in connection with your use of the contents; 3) RENESAS
 * MAKES NO WARRANTY OR REPRESENTATIONS ABOUT THE SUITABILITY OF THE CONTENTS FOR ANY PURPOSE; THE CONTENTS ARE PROVIDED
 * "AS IS" WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, AND NON-INFRINGEMENT; AND 4) RENESAS SHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, OR
 * CONSEQUENTIAL DAMAGES, INCLUDING DAMAGES RESULTING FROM LOSS OF USE, DATA, OR PROJECTS, WHETHER IN AN ACTION OF
 * CONTRACT OR TORT, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THE CONTENTS. Third-party contents
 * included in this file may be subject to different terms.
 **********************************************************************************************************************/
/***********************************************************************************************************************
* File Name    : r_adc.c
* Description  : Primary source code for 12-bit A/D Converter driver.
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes   <System Includes> , "Project Includes"
***********************************************************************************************************************/

#include "bsp_api.h"
#include "r_adc_api.h"
/* Configuration for this package. */
#include "r_adc_cfg.h"
/* Private header file for this package. */
#include "r_adc_private_api.h"
#include "r_adc.h"
#include "./hw/hw_adc_private.h"

/**********************************************************************************************************************
 * Macro definitions
 **********************************************************************************************************************/

/** Macro for error logger. */
#ifndef ADC_ERROR_RETURN
/*LDRA_INSPECTED 77 S This macro does not work when surrounded by parentheses. */
#define ADC_ERROR_RETURN(a, err)  SSP_ERROR_RETURN((a), (err), &g_module_name[0], &s_adc_version)
#endif

/** ADC resolution is defined by b2:4 of the variant data. ADC resolution is 8 bits when resolution_variant = 0,
 * 10 bits when resolution_variant = 1, 12 bits when resolution_variant = 2, and 14 bits when resolution_variant = 3.
 */
#define ADC_VARIANT_RESOLUTION_MASK    (0x1C)
#define ADC_VARIANT_RESOLUTION_SHIFT   (2)

/** Maximum number of units on any Synergy ADC. */
#define ADC_MAX_UNITS                  (2)

/** Length of extended data provided by the FMI for ADC. */
#define ADC_FMI_EXTENDED_DATA_COUNT_WORDS (2U)

/***********************************************************************************************************************
Typedef definitions
***********************************************************************************************************************/

/***********************************************************************************************************************
Private global variables and functions
***********************************************************************************************************************/
#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
static ssp_err_t r_adc_open_cfg_check(adc_mode_t const mode, adc_cfg_t const * const p_cfg);
static ssp_err_t r_adc_open_cfg_align_add_clear_check(adc_cfg_t const * const p_cfg);
static ssp_err_t r_adc_open_cfg_trigger_mode_check(adc_cfg_t const * const p_cfg);
static ssp_err_t r_adc_open_cfg_resolution_check(adc_cfg_t const * const p_cfg, uint8_t resolution);
static ssp_err_t r_adc_sample_state_cfg_check(adc_instance_ctrl_t * p_ctrl, adc_sample_state_t * p_sample);
#endif /* (1 == ADC_CFG_PARAM_CHECKING_ENABLE) */

ssp_err_t r_adc_interrupts_configure(adc_instance_ctrl_t * p_ctrl,
                                     adc_cfg_t     const * const p_cfg,
                                     ssp_feature_t       * p_feature);

ssp_err_t r_adc_fmi_query(adc_instance_ctrl_t * p_ctrl,
                          adc_cfg_t     const * const p_cfg,
                          ssp_feature_t       * p_feature);

void adc_scan_end_b_isr(void);
void adc_scan_end_isr(void);

#if defined(__GNUC__)
/* This structure is affected by warnings from a GCC compiler bug. This pragma suppresses the warnings in this 
 * structure only.*/
/*LDRA_INSPECTED 69 S */
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif
/** Version data structure used by error logger macro. */
static const ssp_version_t s_adc_version =
{
        .api_version_minor  = ADC_API_VERSION_MINOR,
        .api_version_major  = ADC_API_VERSION_MAJOR,
        .code_version_major = ADC_CODE_VERSION_MAJOR,
        .code_version_minor = ADC_CODE_VERSION_MINOR
};
#if defined(__GNUC__)
/* Restore warning settings for 'missing-field-initializers' to as specified on command line. */
/*LDRA_INSPECTED 69 S */
#pragma GCC diagnostic pop
#endif

/** Name of module used by error logger macro */
#if BSP_CFG_ERROR_LOG != 0
static const char g_module_name[] = "adc";
#endif

/** Mask of valid channels on this MCU. */
static uint32_t g_adc_valid_channels[ADC_MAX_UNITS] = {0};

/***********************************************************************************************************************
 Global Variables
 **********************************************************************************************************************/

/** ADC Implementation of ADC*/
/*LDRA_INSPECTED 27 D This structure must be accessible in user code. It cannot be static. */
const adc_api_t g_adc_on_adc =
{
        .open                   = R_ADC_Open,
        .scanCfg                = R_ADC_ScanConfigure,
        .infoGet                = R_ADC_InfoGet,
        .scanStart              = R_ADC_ScanStart,
        .scanStop               = R_ADC_ScanStop,
        .scanStatusGet          = R_ADC_CheckScanDone,
        .sampleStateCountSet    = R_ADC_SetSampleStateCount,
        .read                   = R_ADC_Read,
        .close                  = R_ADC_Close,
        .versionGet             = R_ADC_VersionGet
};

/*******************************************************************************************************************//**
 * @addtogroup ADC
 * @{
 **********************************************************************************************************************/

/***********************************************************************************************************************
 * Functions
 **********************************************************************************************************************/

/*******************************************************************************************************************//**
 * @brief
 * The Open function applies power to the A/D peripheral, sets the operational mode, trigger sources, interrupt
 * priority, and configurations for the peripheral as a whole. If interrupt priority is non-zero in BSP_IRQ_Cfg.h,
 * the function takes a callback function pointer for notifying the user at interrupt level whenever a scan has completed.
 *
 * @retval  SSP_SUCCESS                Call successful.
 * @retval  SSP_ERR_ASSERTION          The parameter p_ctrl or p_cfg is NULL.
 * @retval  SSP_ERR_IN_USE             Peripheral is still running in another mode; perform R_ADC_Close() first.
 * @retval  SSP_ERR_INVALID_ARGUMENT   Mode or element of p_cfg structure has invalid value or is illegal based on mode.
 * @retval  SSP_ERR_IN_USE             Unit has already been initialized.
***********************************************************************************************************************/
ssp_err_t R_ADC_Open(adc_ctrl_t * p_api_ctrl,  adc_cfg_t const * const p_cfg)
{
    adc_instance_ctrl_t * p_ctrl = (adc_instance_ctrl_t *) p_api_ctrl;
    ssp_err_t err = SSP_SUCCESS;

    /**  Perform parameter checking */
#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
    /** Verify the pointers are valid */
    SSP_ASSERT (NULL != p_ctrl);
    SSP_ASSERT (NULL != p_cfg);

    /** Verify the configuration parameters are valid   */
    err = r_adc_open_cfg_check(p_cfg->mode, p_cfg);
    ADC_ERROR_RETURN(SSP_SUCCESS == err, err);
#endif

    /** Verify this unit has not already been initialized   */
    ADC_ERROR_RETURN(false == p_ctrl->opened, SSP_ERR_IN_USE);

    /** Confirm the requested unit exists on this MCU and record available channels. */
    ssp_feature_t ssp_feature = {{(ssp_ip_t) 0U}};
    ssp_feature.channel = p_cfg->unit;
    ssp_feature.unit = 0U;
    ssp_feature.id = SSP_IP_ADC;
    err = r_adc_fmi_query(p_ctrl, p_cfg, &ssp_feature);
    ADC_ERROR_RETURN(SSP_SUCCESS == err, err);

#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
    /** Check for valid argument values for options that are unique to the IP */
    err = r_adc_open_cfg_resolution_check(p_cfg, p_ctrl->max_resolution);
    ADC_ERROR_RETURN(SSP_SUCCESS == err, err);
#endif

    err = r_adc_interrupts_configure(p_ctrl, p_cfg, &ssp_feature);
    ADC_ERROR_RETURN(SSP_SUCCESS == err, err);

    /** Lock specified ADC channel */
    err = R_BSP_HardwareLock(&ssp_feature);
    ADC_ERROR_RETURN((SSP_SUCCESS == err), err);

    HW_ADC_Close(p_ctrl->p_reg, p_ctrl);
    /** Initialize the hardware based on the configuration*/
    HW_ADC_Open(p_ctrl->p_reg, p_cfg);
    /** Save callback function pointer  */
    p_ctrl->callback = p_cfg->p_callback;
    /** Store the Unit number into the control structure*/
    p_ctrl->unit = p_cfg->unit;
    /** Store the user context into the control structure*/
    p_ctrl->p_context = p_cfg->p_context;
    /** Store the mode into the control structure*/
    p_ctrl->mode = p_cfg->mode;
    /** Save the regular mode/Group A trigger in the internal control block*/
    p_ctrl->trigger = p_cfg->trigger;
    /** Save the context */
    p_ctrl->p_context = p_cfg->p_context;
    /** Mark driver as opened for this unit */
    p_ctrl->opened = true;
    /** Invalid scan mask (set later). */
    p_ctrl->scan_mask = 0U;

    /** Return the error code */
    return err;
}

/*******************************************************************************************************************//**
 * @brief  Set the sample state count for individual channels. This only needs to be set
 *                                    for special use cases. Normally, use the default values out of Reset.
 *
 * @retval  SSP_SUCCESS                Call successful.
 * @retval  SSP_ERR_ASSERTION          The parameter p_ctrl or p_sample is NULL.
 * @retval  SSP_ERR_NOT_OPEN           Unit is not open.
 * @retval  SSP_ERR_INVALID_ARGUMENT   Parameter has invalid value.
***********************************************************************************************************************/
ssp_err_t R_ADC_SetSampleStateCount(adc_ctrl_t * p_api_ctrl, adc_sample_state_t * p_sample)
{
    adc_instance_ctrl_t * p_ctrl = (adc_instance_ctrl_t *) p_api_ctrl;
    ssp_err_t err = SSP_SUCCESS;

    /** Perform parameter checking */
#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
    /** Verify the pointers are valid */
    SSP_ASSERT (NULL != p_ctrl);
    SSP_ASSERT (NULL != p_sample);

    /** Ensure ADC Unit is already open */
    if (false == p_ctrl->opened)
    {
        return SSP_ERR_NOT_OPEN;
    }
    /** Verify arguments are legal */
    err = r_adc_sample_state_cfg_check(p_ctrl, p_sample);
    if (SSP_SUCCESS != err)
    {
        return err;
    }
    SSP_ASSERT (NULL != p_ctrl->p_reg);
#endif

    /** Set the sample state count for the specified register */
    ADC_BASE_PTR p_regs = (ADC_BASE_PTR) p_ctrl->p_reg;
    p_regs->ADSSTR0n[p_sample->reg_id] = p_sample->num_states;

    /** Return the error code */
    return err;
}

/*******************************************************************************************************************//**
 * @brief  Configure the ADC scan parameters. Channel specific settings are set in this function.
 *
 * @retval  SSP_SUCCESS                Call successful.
 * @retval  SSP_ERR_ASSERTION          The parameter p_ctrl or p_ch_cfg is NULL.
 * @retval  SSP_ERR_NOT_OPEN           Unit is not open.
 * @retval  SSP_ERR_INVALID_ARGUMENT   Parameter has invalid value.
 *
 * @note If the Group Mode Priority configuration is set to ADC_GROUP_A_GROUP_B_CONTINUOUS_SCAN, then since Group B
 * will be scanning continuously, Group B Interrupts are disabled and the application will not receive a callback
 * for Group B scan completion even if a callback is provided. The application will still receive a callback for
 * Group A scan completion if a callback is provided.
***********************************************************************************************************************/
ssp_err_t R_ADC_ScanConfigure(adc_ctrl_t * p_api_ctrl, adc_channel_cfg_t const * const p_channel_cfg)
{
    adc_instance_ctrl_t * p_ctrl = (adc_instance_ctrl_t *) p_api_ctrl;
    ssp_err_t err = SSP_SUCCESS;

    /**  Perform parameter checking */
#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
    /** Verify the pointers are valid */
    SSP_ASSERT (NULL != p_ctrl);
    SSP_ASSERT (NULL != p_channel_cfg);

    /** Ensure ADC Unit is already open  */
    if (false == p_ctrl->opened)
    {
        return SSP_ERR_NOT_OPEN;
    }
    SSP_ASSERT (NULL != p_ctrl->p_reg);
#endif

    /** Configure the hardware based on the configuration */
    err = HW_ADC_ScanCfg(p_ctrl, p_channel_cfg, &g_adc_valid_channels[0]);

    /** Save the scan mask locally; this is required for the infoGet function*/
    p_ctrl->scan_mask = p_channel_cfg->scan_mask;

    /** Return the error code */
    return err;
}

/*******************************************************************************************************************//**
 * @brief  This function returns the address of the lowest number configured channel and the total number of bytes
 * to be read in order to read the results of the configured channels and return the ELC Event name.
 *
 * @retval  SSP_SUCCESS                Call successful.
 * @retval  SSP_ERR_ASSERTION          The parameter p_ctrl is NULL.
 * @retval  SSP_ERR_NOT_OPEN           Unit is not open.
 * @retval  SSP_ERR_INVALID_ARGUMENT   Parameter has invalid value.
 * @retval  SSP_ERR_IN_USE             Running scan is still in progress
 *
 * @note: Currently this function call does not support Group Mode operation.
***********************************************************************************************************************/
ssp_err_t R_ADC_InfoGet(adc_ctrl_t * p_api_ctrl, adc_info_t * p_adc_info)
{
    adc_instance_ctrl_t * p_ctrl = (adc_instance_ctrl_t *) p_api_ctrl;
    ssp_err_t err = SSP_SUCCESS;
    uint32_t adc_mask = 0;
    uint32_t adc_mask_result = 0;
    uint32_t adc_mask_count = 0;
    __I uint16_t * end_address;

     /** Perform parameter checking  */
#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
    /** Verify the pointers are valid */
    SSP_ASSERT (NULL != p_ctrl);
    SSP_ASSERT (NULL != p_adc_info);

    /** Ensure ADC Unit is already open  */
    if (false == p_ctrl->opened)
    {
        return SSP_ERR_NOT_OPEN;
    }

    /** Return an error if mode is Group Mode since that is not
     * supported currently */
    if (ADC_MODE_GROUP_SCAN == p_ctrl->mode)
    {
        return SSP_ERR_INVALID_ARGUMENT;
    }
    SSP_ASSERT (NULL != p_ctrl->p_reg);
#endif
    /** Get a pointer to the base register for the current unit */
    ADC_BASE_PTR p_regs = (ADC_BASE_PTR) p_ctrl->p_reg;
    /** Retrieve the scan mask of active channels from the control structure */
    adc_mask = p_ctrl->scan_mask;
    /** Determine the lowest channel that is configured*/
    while (0 == adc_mask_result)
    {
        adc_mask_result = (uint32_t)(adc_mask & ((uint32_t)1 << adc_mask_count));
        adc_mask_count++;
    }
    p_adc_info->p_address = &p_regs->ADDRn[adc_mask_count-1];

    /** Determine the highest channel that is configured*/
    /** Set the mask count so that we start with the highest bit of the 32 bit mask */
    adc_mask_count = 31U;
    /** Initialize the mask result */
    adc_mask_result = 0U;
    while (0 == adc_mask_result)
    {
        adc_mask_result = (uint32_t)(adc_mask & ((uint32_t)1 << adc_mask_count));
        adc_mask_count--;
    }
    end_address = &p_regs->ADDRn[adc_mask_count+1];

    /** Determine the size of data that must be read to read all the channels between and including the
     * highest and lowest channels.*/
    p_adc_info->length = (uint32_t)((end_address - p_adc_info->p_address) + 1);

    /** Specify the peripheral name in the ELC list */
    fmi_event_info_t event_info = {(IRQn_Type) 0U};
    ssp_feature_t ssp_feature = {{(ssp_ip_t) 0U}};
    ssp_feature.channel = p_ctrl->unit;
    ssp_feature.unit = 0U;
    ssp_feature.id = SSP_IP_ADC;
    g_fmi_on_fmi.eventInfoGet(&ssp_feature, SSP_SIGNAL_ADC_SCAN_END, &event_info);
    p_adc_info->elc_event = event_info.event;
    p_adc_info->elc_peripheral = (elc_peripheral_t) (ELC_PERIPHERAL_ADC0 + (2 * p_ctrl->unit));

    return err;
}
/*******************************************************************************************************************//**
 * @brief  This function starts a software scan or enables the hardware trigger for a scan depending
 *                          on how the triggers were configured in the Open() call. If the Unit was configured for
 *                          hardware triggering, then this function  simply allows the trigger signal (hardware or
 *                          software) to get to the ADC Unit. The function is not able to control the generation of the
 *                          trigger itself. If the Unit was configured for software triggering, then this function
 *                          starts the software triggered scan.
 *
 * @retval  SSP_SUCCESS                Call successful.
 * @retval  SSP_ERR_ASSERTION          The parameter p_ctrl is NULL.
 * @retval  SSP_ERR_NOT_OPEN           Unit is not open.
 * @retval  SSP_ERR_INVALID_ARGUMENT   Parameter has invalid value.
 * @retval  SSP_ERR_IN_USE             Running scan is still in progress
***********************************************************************************************************************/
ssp_err_t R_ADC_ScanStart(adc_ctrl_t * p_api_ctrl)
{
    adc_instance_ctrl_t * p_ctrl = (adc_instance_ctrl_t *) p_api_ctrl;
    ssp_err_t err = SSP_SUCCESS;

    /** Perform parameter checking  */
#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
    /** Verify the pointers are valid */
    SSP_ASSERT (NULL != p_ctrl);

    /** Ensure ADC Unit is already open  */
    if (false == p_ctrl->opened)
    {
        return SSP_ERR_NOT_OPEN;
    }
    SSP_ASSERT (NULL != p_ctrl->p_reg);
#endif

    /** If the the normal/GroupA trigger is not set to software, then that the Unit is configured for hardware triggering */
    if (ADC_TRIGGER_SOFTWARE != p_ctrl->trigger)
    {
        HW_ADC_ADCSR_TRGE_Set(p_ctrl->p_reg, HW_ADC_ADCSR_TRGE_SET);
    }
    /** Otherwise, enable software triggering */
    else
    {
        /** Check to see if there is an ongoing scan else start the scan */
        if (HW_ADC_ADCSR_ADCST_NOT_SET == HW_ADC_ADCSR_ADST_Get(p_ctrl->p_reg))
        {
            HW_ADC_ADCSR_ADST_Set(p_ctrl->p_reg, HW_ADC_ADCSR_ADCST_SET);
        }
        else
        {
            err = SSP_ERR_IN_USE;
        }
    }

    /** Return the error code */
    return err;
}

/*******************************************************************************************************************//**
 * @brief  This function stops the software scan or disables the Unit from being triggered by the
 *                         hardware trigger (internal or external) based on what type of trigger the unit was configured
 *                         for in the Open() function. Stopping a hardware triggered scan via this function does not abort
 *                         an ongoing scan, but  prevents the next scan from occurring. Stopping a software triggered
 *                         scan aborts an ongoing scan.
 *
 * @retval  SSP_SUCCESS                Call successful.
 * @retval  SSP_ERR_ASSERTION          The parameter p_ctrl is NULL.
 * @retval  SSP_ERR_NOT_OPEN           Unit is not open.
 * @retval  SSP_ERR_INVALID_ARGUMENT   Parameter has invalid value.
 * @note    Stopping a software scan results in immediate stoppage of the scan irrespective of current state of
 *          of the scan. Stopping the hardware scan results in disabling the trigger to prevent future scans
 *          from starting but does not affect the current scan.
***********************************************************************************************************************/
ssp_err_t R_ADC_ScanStop(adc_ctrl_t * p_api_ctrl)
{
    adc_instance_ctrl_t * p_ctrl = (adc_instance_ctrl_t *) p_api_ctrl;

    /**  Perform parameter checking */
#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
    /** Verify the pointers are valid */
    SSP_ASSERT (NULL != p_ctrl);

    /** Ensure ADC Unit is already open  */
    if (false == p_ctrl->opened)
    {
        return SSP_ERR_NOT_OPEN;
    }
    SSP_ASSERT (NULL != p_ctrl->p_reg);
#endif

    /** If the trigger is not software scan, then disallow hardware triggering*/
    if (ADC_TRIGGER_SOFTWARE != p_ctrl->trigger)
    {
        HW_ADC_ADCSR_TRGE_Set(p_ctrl->p_reg, HW_ADC_ADCSR_TRGE_NOT_SET);
    }
    /** Otherwise, disable software triggering*/
    else
    {
        HW_ADC_ADCSR_ADST_Set(p_ctrl->p_reg, HW_ADC_ADCSR_ADCST_NOT_SET);
    }

    /** Return the error code */
    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief  This function returns the status of any scan process that was started.
 *
 * @retval  SSP_SUCCESS                Successful; the scan is complete.
 * @retval  SSP_ERR_ASSERTION          The parameter p_ctrl is NULL.
 * @retval  SSP_ERR_NOT_OPEN           Unit is not open.
 * @retval  SSP_ERR_INVALID_ARGUMENT   Parameter has invalid value.
 * @retval  SSP_ERR_IN_USE             Running scan is still in progress.
 *
 * @note    If the peripheral was configured in single scan mode, then the return value of this function is an
 *          indication of the scan status. However, if the peripheral was configured in group mode, then the return
 *          value of this function could be an indication of either the group A or group B scan state. This is because
 *          the ADST bit is set when a scan is ongoing and cleared when the scan is done. This function should normally
 *          only be used when using software trigger in single scan mode.
***********************************************************************************************************************/
ssp_err_t R_ADC_CheckScanDone(adc_ctrl_t * p_api_ctrl)
{
    adc_instance_ctrl_t * p_ctrl = (adc_instance_ctrl_t *) p_api_ctrl;
    ssp_err_t err = SSP_SUCCESS;

    /**  Perform parameter checking */
#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
    /** Verify the pointers are valid */
    SSP_ASSERT (NULL != p_ctrl);

    /** Ensure ADC Unit is already open  */
    if (false == p_ctrl->opened)
    {
        return SSP_ERR_NOT_OPEN;
    }

    SSP_ASSERT (NULL != p_ctrl->p_reg);
#endif

    /** Read the status of the ADST bit*/
    if (HW_ADC_ADCSR_ADCST_SET == HW_ADC_ADCSR_ADST_Get(p_ctrl->p_reg))
    {
            err = SSP_ERR_IN_USE;
    }

    /** Return the error code */
    return err;
}

/*******************************************************************************************************************//**
 * @brief   This function reads conversion results from a single channel or sensor
 *          register.
 * @retval  SSP_SUCCESS                Call successful.
 * @retval  SSP_ERR_ASSERTION          The parameter p_ctrl is NULL.
 * @retval  SSP_ERR_INVALID_POINTER    The parameter p_data is NULL.
 * @retval  SSP_ERR_NOT_OPEN           Unit is not open.
 * @retval  SSP_ERR_INVALID_ARGUMENT   Parameter has invalid value.
***********************************************************************************************************************/
ssp_err_t R_ADC_Read(adc_ctrl_t * p_api_ctrl, adc_register_t const  reg_id, adc_data_size_t * const p_data)
{
    adc_instance_ctrl_t * p_ctrl = (adc_instance_ctrl_t *) p_api_ctrl;
    ADC_BASE_PTR p_regs;

    /** Perform parameter checking*/
#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
    /** Verify the pointers are valid */
    SSP_ASSERT (NULL != p_ctrl);

    /** Verify that the channel is valid for this MCU */
    if ((reg_id > ADC_REG_CHANNEL_27) || (reg_id < ADC_REG_TEMPERATURE))
    {
        return SSP_ERR_INVALID_ARGUMENT;
    }
    if (reg_id >= ADC_REG_CHANNEL_0)
    {
        uint32_t requested_channel_mask = (1U << (uint32_t) reg_id);
        if (0 == (requested_channel_mask & g_adc_valid_channels[p_ctrl->unit]))
        {
            return SSP_ERR_INVALID_ARGUMENT;
        }
    }

    /** Verify that the ADC is already open */
    if (false == p_ctrl->opened)
    {
        return SSP_ERR_NOT_OPEN;
    }

    if (NULL == p_data)
    {
        return SSP_ERR_INVALID_POINTER;
    }
    SSP_ASSERT (NULL != p_ctrl->p_reg);
#endif
    /** Get pointer to appropriate base address. This is repeated here in case
     * parameter checking is disabled. */
    p_regs = (ADC_BASE_PTR) p_ctrl->p_reg;
    /** Read the data from the requested ADC conversion register and return it */
    *p_data = (adc_data_size_t) p_regs->ADDRn[reg_id];

    /** Return the error code */
    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief  This function ends any scan in progress, disables interrupts, and removes power to the A/D
 *                       peripheral.
 *
 * @retval  SSP_SUCCESS                Call successful.
 * @retval  SSP_ERR_ASSERTION          The parameter p_ctrl is NULL.
 * @retval  SSP_ERR_NOT_OPEN           Unit is not open.
 * @retval  SSP_ERR_INVALID_ARGUMENT   Parameter has invalid value.
***********************************************************************************************************************/
ssp_err_t R_ADC_Close(adc_ctrl_t * p_api_ctrl)
{
    adc_instance_ctrl_t * p_ctrl = (adc_instance_ctrl_t *) p_api_ctrl;

    /** Perform parameter checking*/
#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
    /** Verify the pointers are valid */
    SSP_ASSERT (NULL != p_ctrl);

    /** Verify that the ADC is already open */
    if (false == p_ctrl->opened)
    {
        return SSP_ERR_NOT_OPEN;
    }
    SSP_ASSERT (NULL != p_ctrl->p_reg);
#endif

    /** Perform hardware stop for the specific unit*/
    ssp_vector_info_t * p_vector_info;
    if (SSP_INVALID_VECTOR != p_ctrl->scan_end_irq)
    {
        NVIC_DisableIRQ (p_ctrl->scan_end_irq);         /*** Disable interrupts in ICU    */
        NVIC_ClearPendingIRQ (p_ctrl->scan_end_irq);    /*** Clear interrupt flag         */
        R_BSP_IrqStatusClear (p_ctrl->scan_end_irq);
        R_SSP_VectorInfoGet(p_ctrl->scan_end_irq, &p_vector_info);
        *(p_vector_info->pp_ctrl) = NULL;
    }
    if (SSP_INVALID_VECTOR != p_ctrl->scan_end_b_irq)
    {
        NVIC_DisableIRQ (p_ctrl->scan_end_b_irq);         /*** Disable interrupts in ICU    */
        NVIC_ClearPendingIRQ (p_ctrl->scan_end_b_irq);    /*** Clear interrupt flag         */
        R_BSP_IrqStatusClear (p_ctrl->scan_end_b_irq);
        R_SSP_VectorInfoGet(p_ctrl->scan_end_b_irq, &p_vector_info);
        *(p_vector_info->pp_ctrl) = NULL;
    }
    HW_ADC_Close(p_ctrl->p_reg, p_ctrl);

    /** Mark driver as closed   */
    p_ctrl->opened = false;

    /** Release the lock */
    ssp_feature_t ssp_feature = {{(ssp_ip_t) 0U}};
    ssp_feature.channel = p_ctrl->unit;
    ssp_feature.unit = 0U;
    ssp_feature.id = SSP_IP_ADC;
    R_BSP_HardwareUnlock(&ssp_feature);

    /** Return the error code */
    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief   Retrieve the API version number.
 *
 * @retval  SSP_SUCCESS        Successful return.
 * @retval  SSP_ERR_ASSERTION  The parameter p_version is NULL.
***********************************************************************************************************************/
ssp_err_t R_ADC_VersionGet(ssp_version_t * const p_version)
{
#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
    /** Verify parameters are valid */
    SSP_ASSERT(NULL != p_version);
#endif
    /** Return the version number */
    p_version->version_id =  s_adc_version.version_id;

    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @} (end addtogroup ADC)
 **********************************************************************************************************************/

/***********************************************************************************************************************
 Private Functions
 **********************************************************************************************************************/
/*******************************************************************************************************************//**
 * @brief   r_adc_interrupts_configure
 *
 * This function configures ADC interrupts
 *
 * @param[in]  p_ctrl          :  ADC control structure.
 * @param[in]  p_cfg           :  Pointer to configuration structure
 * @param[in]  p_feature       :  Pointer to ADC feature
***********************************************************************************************************************/
ssp_err_t r_adc_interrupts_configure(adc_instance_ctrl_t * p_ctrl,
                                     adc_cfg_t     const * const p_cfg,
                                     ssp_feature_t       * p_feature)
{
    /** Set the interrupt priorities. */
    ssp_vector_info_t * p_vector_info;
    fmi_event_info_t event_info = {(IRQn_Type) 0U};
    g_fmi_on_fmi.eventInfoGet(p_feature, SSP_SIGNAL_ADC_SCAN_END, &event_info);
    p_ctrl->scan_end_irq = event_info.irq;

    /** If a callback is used, then make sure the scan end interrupt is enabled */
    if (NULL != p_cfg->p_callback)
    {
        ADC_ERROR_RETURN(SSP_INVALID_VECTOR != p_ctrl->scan_end_irq, SSP_ERR_IRQ_BSP_DISABLED);
    }
    if (SSP_INVALID_VECTOR != p_ctrl->scan_end_irq)
    {
        R_SSP_VectorInfoGet(p_ctrl->scan_end_irq, &p_vector_info);
        NVIC_SetPriority(p_ctrl->scan_end_irq, p_cfg->scan_end_ipl);
        *(p_vector_info->pp_ctrl) = p_ctrl;

        /** Clear any pending interrupt requests in the NVIC or the peripheral*/
        NVIC_DisableIRQ (p_ctrl->scan_end_irq);         /*** Disable interrupts in ICU    */
        NVIC_ClearPendingIRQ (p_ctrl->scan_end_irq);    /*** Clear interrupt flag         */
        R_BSP_IrqStatusClear (p_ctrl->scan_end_irq);
    }
    g_fmi_on_fmi.eventInfoGet(p_feature, SSP_SIGNAL_ADC_SCAN_END_B, &event_info);
    p_ctrl->scan_end_b_irq = event_info.irq;

    /** If group mode is enabled and a callback is used, then make sure group mode interrupts
     * are enabled */
    if ((p_cfg->mode == ADC_MODE_GROUP_SCAN) && (NULL != p_cfg->p_callback))
    {
        ADC_ERROR_RETURN(SSP_INVALID_VECTOR != p_ctrl->scan_end_b_irq, SSP_ERR_IRQ_BSP_DISABLED);
    }
    if (SSP_INVALID_VECTOR != p_ctrl->scan_end_b_irq)
    {
        R_SSP_VectorInfoGet(p_ctrl->scan_end_b_irq, &p_vector_info);
        NVIC_SetPriority(p_ctrl->scan_end_b_irq, p_cfg->scan_end_b_ipl);
        *(p_vector_info->pp_ctrl) = p_ctrl;

        /** Clear any pending interrupt requests in the NVIC or the peripheral*/
        NVIC_DisableIRQ (p_ctrl->scan_end_b_irq);         /*** Disable interrupts in ICU    */
        NVIC_ClearPendingIRQ (p_ctrl->scan_end_b_irq);    /*** Clear interrupt flag         */
        R_BSP_IrqStatusClear (p_ctrl->scan_end_b_irq);
    }

    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief   r_adc_fmi_query
 *
 * This function queries the factory flash to ensure the unit exists and record the valid channel information.
 *
 * @param[in]  p_ctrl          :  ADC control structure.
 * @param[in]  p_cfg           :  Pointer to configuration structure
 * @param[in]  p_feature       :  Pointer to ADC feature
***********************************************************************************************************************/
ssp_err_t r_adc_fmi_query(adc_instance_ctrl_t * p_ctrl,
                          adc_cfg_t     const * const p_cfg,
                          ssp_feature_t       * p_feature)
{
    /** Confirm the requested unit exists on this MCU. */
    fmi_feature_info_t info = {0U};
    ssp_err_t err = g_fmi_on_fmi.productFeatureGet(p_feature, &info);
    ADC_ERROR_RETURN(SSP_SUCCESS == err, err);
    p_ctrl->p_reg = info.ptr;
    uint8_t max_resolution_bits = (uint8_t) ((info.variant_data & ADC_VARIANT_RESOLUTION_MASK)
            >> ADC_VARIANT_RESOLUTION_SHIFT);
    p_ctrl->max_resolution = (uint8_t) ((max_resolution_bits * 2U) + 8U);

    /** Set the valid channel mask based on variant data. */
    if (0U == g_adc_valid_channels[p_cfg->unit])
    {
        uint32_t * p_extended_data = (uint32_t *) info.ptr_extended_data;
        /* ADC_FMI_EXTENDED_DATA_COUNT_WORDS == info.extended_data_count, but this is not available in all
         * factory flash versions. */
        for (uint32_t j = 0U; j < ADC_FMI_EXTENDED_DATA_COUNT_WORDS; j += 1U)
        {
            if (0U == j)
            {
                /* The first word of extended data has a valid channel bitmask for the first 16 channels in the upper
                 * 16 bits. */
                g_adc_valid_channels[p_cfg->unit] = (p_extended_data[j] >> 16) & 0xFFFFU;
            }
            else
            {
                /* The second word of extended data has a valid channel bitmask for the any channels above channel 16
                 * in the upper 16 bits. */
                g_adc_valid_channels[p_cfg->unit] |= (p_extended_data[j] & 0xFFFF0000UL);
            }
        }
    }

    return SSP_SUCCESS;
}

#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
/*******************************************************************************************************************//**
 * @brief   r_adc_sample_state_cfg_check
 *
 * This function checks the Sample and Hold arguments
 *
 * @param[in]  p_ctrl          :  ADC control structure.
 * @param[in]  p_sample        :  Sample State Configuration
***********************************************************************************************************************/
static ssp_err_t r_adc_sample_state_cfg_check(adc_instance_ctrl_t * p_ctrl, adc_sample_state_t * p_sample)
{
    /** Used to prevent compiler warning */
    SSP_PARAMETER_NOT_USED(p_ctrl);

    adc_sample_state_reg_t reg_id = p_sample->reg_id;
    if ((reg_id > ADC_SAMPLE_STATE_CHANNEL_15) || (reg_id < ADC_SAMPLE_STATE_CHANNEL_16_TO_27))
    {
        return SSP_ERR_INVALID_ARGUMENT;
    }
    if (reg_id >= ADC_SAMPLE_STATE_CHANNEL_0)
    {
        uint32_t requested_channel_mask = (1U << reg_id);
        if (0 == (requested_channel_mask & g_adc_valid_channels[p_ctrl->unit]))
        {
            return SSP_ERR_INVALID_ARGUMENT;
        }
    }

    if (ADC_SAMPLE_STATE_COUNT_MIN > p_sample->num_states)
    {
        return SSP_ERR_INVALID_ARGUMENT;
    }
    else
    {
        return SSP_SUCCESS;
    }
}
#endif /* (1 == ADC_CFG_PARAM_CHECKING_ENABLE) */

#if (1 == ADC_CFG_PARAM_CHECKING_ENABLE)
/*******************************************************************************************************************//**
 * @brief   r_adc_open_cfg_check : ADC check open function configuration
 *
 * This function validates the configuration arguments for illegal combinations or options.
 *
 * @param[in]  unit  :  ADC unit number
 * @param[in]  mode  :  Operational mode (see enumeration below)
 * @param[in]  p_cfg :  Pointer to configuration structure (see below)
 *
 * @retval  SSP_SUCCESS -       Successful
 * @retval  SSP_ERR_INVALID_ARGUMENT -  Mode or element of p_cfg structure has invalid value or is invalid based on mode
 * @retval  SSP_ERR_IRQ_BSP_DISABLED -  IRQ is disabled in the BSP when a callback function is passed
***********************************************************************************************************************/
static ssp_err_t r_adc_open_cfg_check(adc_mode_t const  mode, adc_cfg_t const * const p_cfg)
{
    ssp_err_t err = SSP_SUCCESS;
    /** Check for valid argument values for alignment, add_average_count, clearing, trigger, and mode. */
    err = r_adc_open_cfg_align_add_clear_check(p_cfg);
    if (SSP_SUCCESS != err)
    {
        return err;
    }
    err = r_adc_open_cfg_trigger_mode_check(p_cfg);
    if (SSP_SUCCESS != err)
    {
        return err;
    }

    /** If 16 time addition is used only 12 bit accuracy can be selected*/
    if ((ADC_RESOLUTION_12_BIT != p_cfg->resolution) && (ADC_ADD_SIXTEEN == p_cfg->add_average_count))
    {
        return SSP_ERR_INVALID_ARGUMENT;
    }

    /** If the mode is continuous, then the callback has to be NULL */
    if (p_cfg->p_callback != NULL)
    {
        if (p_cfg->mode == ADC_MODE_CONTINUOUS_SCAN)
        {
            return SSP_ERR_INVALID_ARGUMENT;
        }
    }

    /** Group checking; only synchronous triggers (ELC) allowed.  */
    if ((ADC_MODE_GROUP_SCAN == mode))
    {
        if ((ADC_TRIGGER_SYNC_ELC  != p_cfg->trigger)
                ||(ADC_TRIGGER_SYNC_ELC != p_cfg->trigger_group_b))
        {
            err = SSP_ERR_INVALID_ARGUMENT;
        }
    }


    return err;
}

/*******************************************************************************************************************//**
 * @brief   r_adc_open_cfg_align_add_clear_check : ADC check open function configuration
 *
 * This function validates the configuration arguments for illegal combinations or options.
 *
 * @param[in]  p_cfg       :  Pointer to configuration structure
 *
 * @retval  SSP_SUCCESS -               Successful
 * @retval  SSP_ERR_INVALID_ARGUMENT -  Mode or element of p_cfg structure has invalid value or is invalid based on mode
***********************************************************************************************************************/
static ssp_err_t r_adc_open_cfg_align_add_clear_check(adc_cfg_t const * const p_cfg)
{
    /** Check for valid argument values for alignment, add_average_count, and clearing. */
    if ((ADC_ALIGNMENT_RIGHT != p_cfg->alignment) && (ADC_ALIGNMENT_LEFT != p_cfg->alignment))
    {
        return SSP_ERR_INVALID_ARGUMENT;
    }
    if ((ADC_ADD_FOUR < p_cfg->add_average_count)
            && (ADC_ADD_AVERAGE_TWO != p_cfg->add_average_count)
            && (ADC_ADD_AVERAGE_FOUR != p_cfg->add_average_count)
            && (ADC_ADD_AVERAGE_SIXTEEN != p_cfg->add_average_count))
    {
        return SSP_ERR_INVALID_ARGUMENT;
    }
    if ((ADC_CLEAR_AFTER_READ_OFF != p_cfg->clearing) && (ADC_CLEAR_AFTER_READ_ON != p_cfg->clearing))
    {
        return SSP_ERR_INVALID_ARGUMENT;
    }

    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief   r_adc_open_cfg_trigger_mode_check : ADC check open function configuration
 *
 * This function validates the configuration arguments for illegal combinations or options.
 *
 * @param[in]  p_cfg       :  Pointer to configuration structure
 *
 * @retval  SSP_SUCCESS -               Successful
 * @retval  SSP_ERR_INVALID_ARGUMENT -  Mode or element of p_cfg structure has invalid value or is invalid based on mode
***********************************************************************************************************************/
static ssp_err_t r_adc_open_cfg_trigger_mode_check(adc_cfg_t const * const p_cfg)
{
    /** Check for valid argument values for trigger and mode. */
    if ((ADC_TRIGGER_ASYNC_EXT_TRG0 != p_cfg->trigger) &&
            (ADC_TRIGGER_SYNC_ELC != p_cfg->trigger)
            && (ADC_TRIGGER_SOFTWARE != p_cfg->trigger))
    {
        return SSP_ERR_INVALID_ARGUMENT;
    }
    if ((ADC_MODE_SINGLE_SCAN != p_cfg->mode) &&
            (ADC_MODE_CONTINUOUS_SCAN != p_cfg->mode)
            && (ADC_MODE_GROUP_SCAN != p_cfg->mode))
    {
        return SSP_ERR_INVALID_ARGUMENT;
    }

    return SSP_SUCCESS;
}

/*******************************************************************************************************************//**
 * @brief   r_adc_open_cfg_resolution_check : ADC check open function configuration
 *
 * This function validates the configuration arguments for illegal combinations or options.
 *
 * @param[in]  p_cfg       :  Pointer to configuration structure
 * @param[in]  resolution  :  ADC maximum resolution
 *
 * @retval  SSP_SUCCESS -       Successful
 * @retval  SSP_ERR_INVALID_ARGUMENT -  Mode or element of p_cfg structure has invalid value or is invalid based on mode
***********************************************************************************************************************/
static ssp_err_t r_adc_open_cfg_resolution_check(adc_cfg_t const * const p_cfg, uint8_t resolution)
{
    if (12 == resolution)
    {
        if ((ADC_RESOLUTION_12_BIT != p_cfg->resolution)
             && (ADC_RESOLUTION_10_BIT != p_cfg->resolution)
             && (ADC_RESOLUTION_8_BIT != p_cfg->resolution))
        {
            return SSP_ERR_INVALID_ARGUMENT;
        }
    }
    if (14 == resolution)
    {
        if ((ADC_RESOLUTION_14_BIT != p_cfg->resolution)
             && (ADC_RESOLUTION_12_BIT != p_cfg->resolution))
        {
            return SSP_ERR_INVALID_ARGUMENT;
        }
    }

    return SSP_SUCCESS;
}
#endif

/*******************************************************************************************************************//**
 * @brief ADC Channel 0 ISR routine
 *
 * This function implements the unit 0 interrupt handler for normal/Group A/double trigger scan complete.
 *
***********************************************************************************************************************/
void adc_scan_end_isr(void)
{
    /** Save context if RTOS is used */
    SF_CONTEXT_SAVE;

    ssp_vector_info_t * p_vector_info = NULL;
    R_SSP_VectorInfoGet(R_SSP_CurrentIrqGet(), &p_vector_info);
    adc_instance_ctrl_t * p_ctrl = (adc_ctrl_t *) *(p_vector_info->pp_ctrl);
    adc_callback_args_t   args;

     /** Clear the BSP IRQ Flag     */
    R_BSP_IrqStatusClear (R_SSP_CurrentIrqGet());

#if ADC_CFG_PARAM_CHECKING_ENABLE
    if (NULL == p_ctrl)
    {
        /* The interrupt fired while the driver is not open. */
        BSP_CFG_HANDLE_UNRECOVERABLE_ERROR(0);
        return;
    }
#endif

    /** Store the event into the callback argument */
    args.event = ADC_EVENT_SCAN_COMPLETE;
    /** Store the Unit number into the callback argument */
    args.unit = p_ctrl->unit;
    /** Populate the context field*/
    args.p_context = p_ctrl->p_context;
    /** If a callback was provided, call it with the argument */
    if (NULL != p_ctrl->callback)
    {
        p_ctrl->callback(&args);
    }

    /** Restore context if RTOS is used */
    SF_CONTEXT_RESTORE;
}

/*******************************************************************************************************************//**
 * @brief   ADC Scan End Group B ISR routine
 *
 * This function implements the interrupt handler for Group B scan complete.
 *
***********************************************************************************************************************/
void adc_scan_end_b_isr(void)
{
    /** Save context if RTOS is used */
    SF_CONTEXT_SAVE;

    ssp_vector_info_t * p_vector_info = NULL;
    R_SSP_VectorInfoGet(R_SSP_CurrentIrqGet(), &p_vector_info);
    adc_instance_ctrl_t * p_ctrl = (adc_ctrl_t *) *(p_vector_info->pp_ctrl);
    adc_callback_args_t   args;

    /** Clear the BSP IRQ Flag     */
    R_BSP_IrqStatusClear (R_SSP_CurrentIrqGet());

#if ADC_CFG_PARAM_CHECKING_ENABLE
    if (NULL == p_ctrl)
    {
        /* The interrupt fired while the driver is not open. */
        BSP_CFG_HANDLE_UNRECOVERABLE_ERROR(0);
        return;
    }
#endif

    /** Store the event into the callback argument */
    args.event = ADC_EVENT_SCAN_COMPLETE_GROUP_B;
    /** Store the Unit number into the callback argument */
    args.unit = p_ctrl->unit;
    /** Populate the context field*/
    args.p_context = p_ctrl->p_context;
    /** If a callback was provided, call it with the argument */
    if (NULL != p_ctrl->callback)
    {
        p_ctrl->callback(&args);
    }

    /** Restore context if RTOS is used */
    SF_CONTEXT_RESTORE;
}
