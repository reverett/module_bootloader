:: ----------------------------------------------------------------------------
:: 
:: get a application hex file ready for flashing device
::
:: create_sim_app_hex.bat "project folder name" "version number" "devID"
:: 
:: example: create_sim_app_hex.bat SimulationBoard2_btld_loc 0000001 1
::
:: in this case batch file will look for SimulationBoard2_btld_loc.srec file version is 0000001 deviceID is 1
::
:: 0x00018000 0x00027000 is the location of the application 
:: 0x40100100 0x40101000 is the location of the application flash
:: 0x40100100, 0x40100101 is location of the application CRC
set projName=%1
set verNum=%2
set devID=%3

::del *.hex

:: make a copy of the original hex file
::
copy %projName% %projName%_org.srec

:: create a data only file sim_app_data.hex
::
:: crop out the data flash area and store in sim_app_data.hex file
:: exclude the application crc location
::

..\..\flash_boards\tools\srec_cat.exe %projName% -Motorola -crop 0x40100102 0x40101000 --address-length=4 -o sim_app_data.hex -Motorola -Line_Length 143

:: create a prog only file sim_app_prog.hex
::
::
..\..\flash_boards\tools\srec_cat.exe %projName% -Motorola -crop 0x00018000 0x00027000 --address-length=4 -o sim_app_prog.hex -Motorola -Line_Length 143

:: fill blank areas
::
:: address-length=4 uses S3 records 
:: Line_Length 143 gives records of byte count 64
:: store new file in sim_app_prog_fill.hex
::
..\..\flash_boards\tools\srec_cat.exe sim_app_prog.hex -Motorola --fill 0xff 0x00018000 0x00027000 --address-length=4 -o sim_app_prog_fill.hex -Motorola -Line_Length 143

:: calc CRC
::
:: calc crc of hex file over the range 0x00018000 0x00027000 with fill 
:: then put the crc at location 0x40100100 in data space
::
..\..\flash_boards\tools\srec_cat.exe sim_app_prog_fill.hex -Motorola -crop 0x00018000 0x00027000 -Big_Endian_CRC16 0x40100100 -Cyclic_Redundancy_Check_16_XMODEM --address-length=4 -o sim_app_prog_crc.hex -Motorola -Line_Length 143

:: crop CRC
::
:: file sim_app_prog_complete.hex is used to flash app using Jlink JTAG program
..\..\flash_boards\tools\srec_cat.exe sim_app_prog_crc.hex -Motorola -crop 0x40100100 0x40100102 -o sim_app_data_crc.hex -Motorola -Line_Length 143

:: crop program only
..\..\flash_boards\tools\srec_cat.exe sim_app_prog_crc.hex -Motorola -crop 0x00018000 0x00027000 --address-length=4 -o sim_app_prog_complete.hex -Motorola -Line_Length 143

:: add CRC to data
::
:: file sim_data_complete.hex is combined with btlr data hex file using batch file create_btldr_hex.bat
..\..\flash_boards\tools\srec_cat.exe sim_app_data_crc.hex sim_app_data.hex -o app_data_complete.hex -Motorola -Line_Length 143

:: copy app data to our flash directory
::
::
copy app_data_complete.hex ..\..\flash_boards\simulator_crc\app_data_complete.hex

:: copy app prog
::
copy sim_app_prog_complete.hex ..\..\flash_boards\simulator_crc\sim_app_prog_complete.srec

:: create bin file for upgrades using the sc
:: 
:: prodID is sim verNum, srec file are inputs
:: out.bin is the output file the sc will use
..\..\flash_boards\tools\CreateBinFile_04132017.exe sim %verNum% sim_app_prog_complete.hex out_test.txt out.bin

:: copy out.bin to our flash directory
::
::
copy out.bin ..\..\flash_boards\sim_upgrade\out.bin


:: create bin file for upgrades using win PC
::
:: note: uses broadcast address 252, a bin for a specific device can be created by changing 252 to the device ID 
:: deviceAddress inputBinFile outputBinFile
:: outputBinFile is used by the PC application
..\..\flash_boards\tools\CreateSCPacket_02212017.exe 252 out.bin packet_broad.bin

:: note: uses device address %devID%,
:: deviceAddress inputBinFile outputBinFile
:: outputBinFile is used by the PC application
..\..\flash_boards\tools\CreateSCPacket_02212017.exe %devID% out.bin packet_%devID%.bin

:: copy packet_broad.bin to our flash directory
::
::
copy packet_broad.bin ..\..\flash_boards\sim_upgrade\packet_broad.bin

copy packet_%devID%.bin ..\..\flash_boards\sim_upgrade\packet_%devID%.bin
