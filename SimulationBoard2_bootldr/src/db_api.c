//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  db_api.c
//
//  Description: The API functions for accessing the DO-AO Module's database.
//               Note, similar to the SQL database, each query function is
//               returning directly a status code. This code must be checked
//               first to see if the query itself was executed successful
//               or not before access the fetched data which is pointed to
//               a memory location provided by the caller.
//
//-----------------------------------------------------------------------------

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hal_data.h"
#include "jellyfish_common.h"
#include "common_def.h"
#include "db_def.h"
#include "db_api.h"
#include "db.h"
#include "critical_section.h"
#include "occ_sensor.h"


//--------------------------------------------------------------------------------
// NAME      : db_vt_get_table
// ABSTRACT  : This API calls is to get the entire database's volatile table.
//             If the call is successful, the requested database table is
//             stored in the memory location pointed by the caller's parameter.
//             Otherwise, the memory location will not be changed
//
// ARGUMENTS :
//   doao_module_db_v_t *db_v_tbl_p: pointer to memory location that caller
//             provided for copying the database table into.
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_LOCK  - database busy
//              ERR_DB_INIT  - database has not be initialized
//              ERR_DB_UNCHANGED - there has not been any database changes
//--------------------------------------------------------------------------------
#ifdef DOAO_MODULE
STATUS_E db_vt_get_table(doao_module_db_v_t *db_v_tbl_p)
#else
STATUS_E db_vt_get_table(scm_occ_module_db_v_t *db_v_tbl_p)
#endif
{

	if (db_lock() != SUCCESS)
	{
		return ERR_DB_LOCK;
	}

	if (db_is_initialized() != TRUE)
	{
		return ERR_DB_INIT;
	}

	if (db_is_changed()==FALSE)
	{
		return ERR_DB_UNCHANGED;
	}

#ifdef DOAO_MODULE
	memcpy((void *)db_v_tbl_p, (void *)&db_v_tbl, sizeof(doao_module_db_v_t));
#else
	memcpy((void *)db_v_tbl_p, (void *)&db_v_tbl, sizeof(scm_occ_module_db_v_t));
#endif

    // clear the provisioning button event
    db_v_tbl.provision_switch_state = OFF;

#ifdef DOAO_MODULE
    // clear the manual button events
    for (int i=0; i<4; i++)
    {
        db_v_tbl.override_state[i] = OFF;
    }
#else

#endif

    // clear the database changed flag;
	db_set_changed_flg(FALSE);

	// unlock the database
	db_unlock();

	return SUCCESS;

}


//--------------------------------------------------------------------------------
// NAME      : db_vt_set_provsion_switch_state
// ABSTRACT  : This database API function set the provision switch state
//             to the input value.
// ARGUMENTS :
//   SWITCH_STATE_E state: state to change to.
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_set_provsion_switch_state(SWITCH_STATE_E state)
{
    // db_lock is not needed here values is set in interrupt routine

    // Enter Critical Section
    //enter_critical_section();

	// change the data
	db_v_tbl.provision_switch_state = state;

	// clear the database changed flag;
	db_set_changed_flg(TRUE);

    // Exit Critical Section
    //exit_critical_section();

	return SUCCESS;
}

//--------------------------------------------------------------------------------
// NAME      : db_vt_get_provsion_switch_state
// ABSTRACT  : This database API function retrieves the provision switch state.
// ARGUMENTS :
//   SWITCH_STATE_E *state: pointer to location to copy the provision state value
//                          into.
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_INIT  - database has not be initialized
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_get_provsion_switch_state(SWITCH_STATE_E *state)
{

	// db_lock is not needed here values is set in interrupt routine

	if (db_is_initialized() != TRUE)
	{
		return ERR_DB_INIT;
	}

	// Enter Critical Section
	enter_critical_section();

	// copy the data
	*state = db_v_tbl.provision_switch_state;

    // after reading location clear the location
	db_v_tbl.provision_switch_state = OFF;

	// Exit Critical Section
	exit_critical_section();

	return SUCCESS;
}

#ifdef DOAO_MODULE
//--------------------------------------------------------------------------------
// NAME      : db_vt_set_relay_state
// ABSTRACT  : This database API function set the indexed relay state
//             to the input value.
// ARGUMENTS :
//   RELAY_ID_E: relay_id: the realy's id (1, 2,3 or 4)
//   SWITCH_STATE_E state: state to change to.
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_set_relay_state(RELAY_ID_E relay_id, SWITCH_STATE_E state)
{
	if (db_lock() != SUCCESS)
	{
		return ERR_DB_LOCK;
	}

	// change the data
	db_v_tbl.relay_state[relay_id-1] = state;

	// clear the database changed flag;
	db_set_changed_flg(TRUE);

	// unlock the database
	db_unlock();

	return SUCCESS;
}

//--------------------------------------------------------------------------------
// NAME      : db_vt_get_relay_state
// ABSTRACT  : This database API function retrieves the indexed relay state.
// ARGUMENTS :
//   RELAY_ID_E relay_id:  the relay's id (1, 2, 3, or 4)
//   SWITCH_STATE_E *state: pointer to where to copy the relay's state value to.
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_INIT  - database has not be initialized
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_get_relay_state(RELAY_ID_E relay_id, SWITCH_STATE_E *state)
{
	if (db_lock() != SUCCESS)
	{
		return ERR_DB_LOCK;
	}
	if (db_is_initialized() != TRUE)
	{
		return ERR_DB_INIT;
	}
	// copy the data
	*state = db_v_tbl.relay_state[relay_id-1];

	// unlock the database
	db_unlock();

	return SUCCESS;
}
#endif

#ifdef DOAO_MODULE
//--------------------------------------------------------------------------------
// NAME      : db_vt_set_override_state
// ABSTRACT  : This database API function set the indexed relay's override state
//             to the input value.
// ARGUMENTS :
//   MANUAL_SWITCH_ID_E switch_id:  the switch's id (1, 2, 3, or 4)
//   SWITCH_STATE_E state: state to change to.
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_set_override_state(MANUAL_SWITCH_ID_E switch_id, SWITCH_STATE_E state)
{
	if (db_lock() != SUCCESS)
	{
		return ERR_DB_LOCK;
	}

	// change the data
	db_v_tbl.override_state[switch_id-1] = state;

	// clear the database changed flag;
	db_set_changed_flg(TRUE);

	// unlock the database
	db_unlock();

	return SUCCESS;
}

//--------------------------------------------------------------------------------
// NAME      : db_vt_get_override_state
// ABSTRACT  : This database API function retrieves the indexed relay state.
// ARGUMENTS :
//   MANUAL_SWITCH_ID_E switch_id:  the switch's id (1, 2, 3, or 4)
//   SWITCH_STATE_E *state: pointer to where to copy the relay's override state
//                          value to.
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_INIT  - database has not be initialized
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_get_override_state(MANUAL_SWITCH_ID_E switch_id, SWITCH_STATE_E *state)
{

	// db_lock is not needed here values is set in interrupt routine

	if (db_is_initialized() != TRUE)
	{
		return ERR_DB_INIT;
	}

	// Enter Critical Section
	enter_critical_section();

	// copy the data
	*state = db_v_tbl.override_state[switch_id-1];

	// clear the event after reading
	db_v_tbl.override_state[switch_id-1] = OFF;

	// Exit Critical Section
	exit_critical_section();

	return SUCCESS;
}
#else
//--------------------------------------------------------------------------------
// NAME      : db_vt_set_occ_state
// ABSTRACT  : This database API function set the occ state
//             to the input value.
// ARGUMENTS :
//   SWITCH_STATE_E state: state to change to.
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_set_occ_state(OCC_STATES_E state)
{

    // db_lock is not needed here values is set in interrupt routine

    // Enter Critical Section
    //enter_critical_section();
    // change the data
    db_v_tbl.occ_state = state;

    // clear the database changed flag;
    db_set_changed_flg(TRUE);

    // Exit Critical Section
    //exit_critical_section();


    return SUCCESS;
}

//--------------------------------------------------------------------------------
// NAME      : db_vt_get_occ_state
// ABSTRACT  : This database API function retrieves the occ state.
// ARGUMENTS :
//   SWITCH_STATE_E *state: pointer to where to copy the relay's override state
//                          value to.
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_INIT  - database has not be initialized
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_get_occ_state(OCC_STATES_E *state)
{

    // db_lock is not needed here values is set in interrupt routine

    // Enter Critical Section
    enter_critical_section();

    // copy the data
    *state = db_v_tbl.occ_state;

    // Exit Critical Section
    exit_critical_section();

    return SUCCESS;
}
#endif

#ifdef DOAO_MODULE
//--------------------------------------------------------------------------------
// NAME      : db_vt_set_line_period
// ABSTRACT  : This database API function set the indexed relay's line period
//             to the input value.
// ARGUMENTS :
//   RELAY_ID_E relay_id:  the relay's id (1, 2, 3, or 4)
//   int line_period: the line period value to change to
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_set_line_period(RELAY_ID_E relay_id, unsigned line_period)
{
	if (db_lock() != SUCCESS)
	{
		return ERR_DB_LOCK;
	}

	// change the data
	db_v_tbl.line_period[relay_id-1] = line_period/48; // store in microsec

	// clear the database changed flag;
	db_set_changed_flg(TRUE);

	// unlock the database
	db_unlock();

	return SUCCESS;
}

//--------------------------------------------------------------------------------
// NAME      : db_vt_get_line_period
// ABSTRACT  : This database API function retrieves the indexed relay's line period
//             value.
// ARGUMENTS :
//   RELAY_ID_E relay_id:  the relay's id (1, 2, 3, or 4)
//   int *line_period: pointer to where to copy the relay's line period value to.
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_INIT  - database has not be initialized
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_get_line_period(RELAY_ID_E relay_id, unsigned *line_period)
{
	if (db_lock() != SUCCESS)
	{
		return ERR_DB_LOCK;
	}
	if (db_is_initialized() != TRUE)
	{
		return ERR_DB_INIT;
	}

	// copy the data
	*line_period = db_v_tbl.line_period[relay_id-1];

	// unlock the database
	db_unlock();

	return SUCCESS;
}
#endif

#ifdef DOAO_MODULE
//--------------------------------------------------------------------------------
// NAME      : db_vt_set_pwm_output
// ABSTRACT  : This database API function set the indexed relay's WPM output
//             to the input value.
// ARGUMENTS :
//   RELAY_ID_E relay_id:  the relay's id (1, 2, 3, or 4)
//   unsigned pwm: the PWM value to change to
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_set_pwm_output(PWM_ID_E pwm_id, unsigned pwm)
{
	if (db_lock() != SUCCESS)
	{
		return ERR_DB_LOCK;
	}

	// change the data
	db_v_tbl.pwm_output[pwm_id-1] = pwm;

	// clear the database changed flag;
	db_set_changed_flg(TRUE);

	// unlock the database
	db_unlock();

	return SUCCESS;
}
#endif

#ifdef DOAO_MODULE
//--------------------------------------------------------------------------------
// NAME      : db_vt_set_output_monitor
// ABSTRACT  : This database API function set the indexed relay's output monitored
//             value to the input value.
// ARGUMENTS :
//   RELAY_ID_E relay_id:  the relay's id (1, 2, 3, or 4)
//   unsigned output_monitor: the monitored output value to change to
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_set_output_monitor(RELAY_ID_E relay_id, unsigned output_monitor)
{
    if (db_lock() != SUCCESS)
    {
        return ERR_DB_LOCK;
    }

    // change the data
    db_v_tbl.output_monitor[relay_id-1] = output_monitor;

    // clear the database changed flag;
    db_set_changed_flg(TRUE);

    // unlock the database
    db_unlock();

    return SUCCESS;
}

//--------------------------------------------------------------------------------
// NAME      : db_vt_get_output_monitor
// ABSTRACT  : This database API function retrieves the indexed relay's monitored
//             output value.
// ARGUMENTS :
//   RELAY_ID_E relay_id:  the relay ID of the analog channel (1, 2, 3, or 4)
//   int *output_monitor: pointer to where to copy the relay's monitored output value to.
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_INIT  - database has not be initialized
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_get_output_monitor(RELAY_ID_E relay_id, unsigned *output_monitor)
{
	if (db_lock() != SUCCESS)
	{
		return ERR_DB_LOCK;
	}
	if (db_is_initialized() != TRUE)
	{
		return ERR_DB_INIT;
	}
	// copy the data
	*output_monitor = db_v_tbl.output_monitor[relay_id-1];

	// unlock the database
	db_unlock();

	return SUCCESS;
}


#else
//--------------------------------------------------------------------------------
// NAME      : db_vt_set_lux_monitor
// ABSTRACT  : This database API function set the lux monitored
//             value to the input value.
// ARGUMENTS :
//   unsigned output_monitor: the monitored output value to change to
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_set_lux_monitor(unsigned output_monitor)
{
    if (db_lock() != SUCCESS)
    {
        return ERR_DB_LOCK;
    }

    // change the data
    db_v_tbl.lux_level = output_monitor;

    // clear the database changed flag;
    db_set_changed_flg(TRUE);

    // unlock the database
    db_unlock();

    return SUCCESS;
}

//--------------------------------------------------------------------------------
// NAME      : db_vt_get_lux_monitor
// ABSTRACT  : This database API function retrieves the indexed relay's monitored
//             output value.
// ARGUMENTS :
//   int *output_monitor: pointer to where to copy the relay's monitored output value to.
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_INIT  - database has not be initialized
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_get_lux_monitor(unsigned *output_monitor)
{
	if (db_lock() != SUCCESS)
	{
		return ERR_DB_LOCK;
	}
	if (db_is_initialized() != TRUE)
	{
		return ERR_DB_INIT;
	}
	// copy the data
	*output_monitor = db_v_tbl.lux_level;

	// unlock the database
	db_unlock();

	return SUCCESS;
}
#endif

//--------------------------------------------------------------------------------
// NAME      : db_vt_set_v24_input_monitor
// ABSTRACT  : This database API function retrieves the indexed relay's monitored
//             output value.
// ARGUMENTS :
//   int input_monitor: value to change to
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_set_v24_input_monitor(unsigned v24_input_monitor)
{
    if (db_lock() != SUCCESS)
    {
        return ERR_DB_LOCK;
    }

    // copy the data
    db_v_tbl.v24_input_monitor= v24_input_monitor;

    // clear the database changed flag;
    db_set_changed_flg(TRUE);

    // unlock the database
    db_unlock();

    return SUCCESS;
}

//--------------------------------------------------------------------------------
// NAME      : db_vt_get_v24_input_monitor
// ABSTRACT  : This database API function retrieves the indexed relay's monitored
//             output value.
// ARGUMENTS :
//   int *input_monitor: pointer to where to copy the relay's monitored input value to.
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_INIT  - database has not be initialized
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_get_v24_input_monitor(unsigned *v24_input_monitor)
{
	if (db_lock() != SUCCESS)
	{
		return ERR_DB_LOCK;
	}
	if (db_is_initialized() != TRUE)
	{
		return ERR_DB_INIT;
	}

	// copy the data
	*v24_input_monitor = db_v_tbl.v24_input_monitor;

	// unlock the database
	db_unlock();

	return SUCCESS;
}


//--------------------------------------------------------------------------------
// NAME      : db_vt_set_rs485_bus_eol
// ABSTRACT  : This database API function retrieves the module's RS485 End of Line
//             status.
// ARGUMENTS :
//   int eol_flg: the End of Line value to change to.
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_set_rs485_bus_eol(BOOL eol_flg)
{
	if (db_lock() != SUCCESS)
	{
		return ERR_DB_LOCK;
	}

	// copy the data
	db_v_tbl.rs485_bus_eol= eol_flg;

	// clear the database changed flag;
	db_set_changed_flg(TRUE);

	// unlock the database
	db_unlock();

	return SUCCESS;
}


//--------------------------------------------------------------------------------
// NAME      : db_vt_get_rs485_bus_eol
// ABSTRACT  : This database API function retrieves the indexed relay's monitored
//             output value.
// ARGUMENTS :
//   int *eol_flg: pointer to where to copy the module's eol status to.
// RETURN    :
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_INIT  - database has not be initialized
//              ERR_DB_LOCK  - database busy
//--------------------------------------------------------------------------------
STATUS_E db_vt_get_rs485_bus_eol(BOOL *eol_flg)
{
	if (db_lock() != SUCCESS)
	{
		return ERR_DB_LOCK;
	}
	if (db_is_initialized() != TRUE)
	{
		return ERR_DB_INIT;
	}
	// copy the data
	*eol_flg = db_v_tbl.rs485_bus_eol;

	// unlock the database
	db_unlock();

	return SUCCESS;
}

//STATUS_E   db_nvt_get_everything(void);    // this is just a dummy placeholder
//------------------------------------------------------------------------------
// Name:         db_nvt_get_firmware_info
// Abstract:    This function access the database and retrieves the firmware
//              version info structure and stores in a caller provided memory
//              location.
//
// Arguments:
//   firmware_tag_t *fmw_p - location where to store the firmware information
//                             structure.
// Return:
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_INIT  - database has not be initialized
//              ERR_DB_LOCK  - database busy
//------------------------------------------------------------------------------
#ifdef DOAO_MODULE
STATUS_E db_nvt_get_table(doao_module_db_nv_t *db_nv_tbl_p)
#else
STATUS_E db_nvt_get_table(scm_occ_module_db_nv_t *db_nv_tbl_p)
#endif
{

	if (db_lock() != SUCCESS)
	{
		return ERR_DB_LOCK;
	}

  	if (db_is_initialized() != TRUE)
	{
		return ERR_DB_INIT;
	}

#ifdef DOAO_MODULE
	memcpy(db_nv_tbl_p, &db_nv_tbl, sizeof(doao_module_db_nv_t));
#else
	memcpy(db_nv_tbl_p, &db_nv_tbl, sizeof(scm_occ_module_db_nv_t));
#endif
	// unlock the database
	db_unlock();

	return SUCCESS;
}


//------------------------------------------------------------------------------
// Name:         db_nvt_get_firmware_info
// Abstract:    This function access the database and retrieves the firmware
//              version info structure and stores in a caller provided memory
//              location.
//
// Arguments:
//   firmware_tag_t *fmw_p - location where to store the firmware information
//                             structure.
// Return:
//   STATUS_E : SUCCESS      - successful, data is return to the pointed location.
//              ERR_DB_INIT  - database has not be initialized
//              ERR_DB_LOCK  - database busy
//------------------------------------------------------------------------------
STATUS_E db_nvt_get_firmware_version(version_tag_t *fmw_ver_p)
{

	if (db_lock() != SUCCESS)
	{
		return ERR_DB_LOCK;
	}

  	if (db_is_initialized() != TRUE)
	{
		return ERR_DB_INIT;
	}

	memcpy(fmw_ver_p, &db_nv_tbl.firmware_info.app.fw_info.ascii_info.version, sizeof(db_nv_tbl.firmware_info.app.fw_info.ascii_info.version));

	// unlock the database
	db_unlock();

	return SUCCESS;
}
