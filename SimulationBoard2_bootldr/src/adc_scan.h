//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  adc_scan.h
//
//  Description: Define public data type and function prototypes for
//               ADC scan and PWM output monitoring.
//
//-----------------------------------------------------------------------------

#ifndef __ADC_SCAN_H__
#define __ADC_SCAN_H__
#include "common_def.h"
#include "hw_thread.h"


STATUS_E    adc_init_hw_timer(void);
STATUS_E    adc_init_adc_channel(void);
STATUS_E    adc_create_adc_thx_timer(void);
void        adc_callback(adc_callback_args_t *p_args);
void        adc_set_pwm_init_level(void);
BOOL        adc_test_rs485bus_eol(void);
BOOL adc_test_rs485bus_eol(void);
#endif // __ADC_SCAN_H__
