//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  relay_state.h
//
//  Description:
//
//-----------------------------------------------------------------------------
#ifndef __LED_STATE_H__
#define __LED_STATE_H__

#include "switch_state.h"
#include "common_def.h"
#include "jellyfish_common.h"
typedef enum {
    LED_STATE_OFF = 0,
    LED_STATE_ON = 1
}LED_STATES_E;

// IDs for the 8 push button switches
typedef enum {
    LED_1   = 1,
    LED_2   = 2,
    LED_3   = 3,
    LED_4   = 4,
    LED_5   = 5,
    LED_6   = 6,
    LED_7   = 7,
    LED_8   = 8,
    LED_9   = 9,
    LED_10  = 10,
    LED_11  = 11,
    LED_12  = 12,
    LED_13  = 13,
    LED_14  = 14,
    LED_15  = 15
} LED_IDS_E;

typedef struct led_context
{
    LED_STATES_E led_pwr_up_state;
    LED_STATES_E led_old_state;
}led_context_t;

LED_STATES_E getLEDState(LED_IDS_E led_id);
LED_STATES_E toggleLED(MANUAL_SWITCH_ID_E switch_id);
STATUS_E set_led_state(LED_IDS_E led_id, LED_STATES_E led_state);

#endif // __LED_STATE_H__
