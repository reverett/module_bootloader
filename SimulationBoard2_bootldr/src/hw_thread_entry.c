#include "hw_thread.h"
#include "adc_scan.h"
#include "sys_config.h"
#include "db_api.h"
#include "hw_thread_entry.h"

static uint16_t   data0;
static uint16_t   data1;
static uint16_t   data2;
static uint16_t   mon_24v;
static uint16_t   data4;

ioport_level_t get_do_value(unsigned char chan)
{
    ioport_level_t port_status;
    switch (chan)
    {
        case 1:
            g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_03, &port_status);
        break;
        case 2:
            g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_02, &port_status);
        break;
        case 3:
            g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_01, &port_status);
        break;
        case 4:
            g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_00, &port_status);
            break;
        case 5:
            g_ioport.p_api->pinRead(IOPORT_PORT_04_PIN_01, &port_status);
            break;
        case 6:
            g_ioport.p_api->pinRead(IOPORT_PORT_04_PIN_02, &port_status);
            break;
        case 7:
            g_ioport.p_api->pinRead(IOPORT_PORT_04_PIN_00, &port_status);
            break;
        case 8:
            g_ioport.p_api->pinRead(IOPORT_PORT_04_PIN_03, &port_status);
            break;

        default:
            break;
    }
    return(port_status);
}

uint16_t get_adc_value(unsigned char chan)
{
    uint16_t adc_count = 0;
    switch (chan)
    {
        case 1:
        adc_count = data0;
        break;
        case 2:
        adc_count = data1;
        break;
        case 3:
        adc_count = mon_24v;
        break;
        default:
            break;
    }
    return(adc_count);
}

/* hw_thread entry function */
void hw_thread_entry(void)
{
    // gpt switch input
    g_timer2.p_api->open(g_timer2.p_ctrl, g_timer2.p_cfg);
    // scan a/d
    g_adc0.p_api->scanStart(g_adc0.p_ctrl);

    /* TODO: add your own code here */
    while (1)
    {
        if (g_adc0.p_api->scanStatusGet(g_adc0.p_ctrl) == SSP_SUCCESS)
        {
            //tx_timer_deactivate(&adc_timer);

            // setup one shot timer
            //tx_timer_change(&adc_timer, ADC_TIMEOUT, ONE_SHOT);
                                                                              // module#             schematic
            g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_0, &data0);     // P000        AN000   AN1_FB
            g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_1, &data1);     // P001        AN001   AN2_FB
            //g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_2, &data2);     // P002        AN002   AN3_FB
            g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_3, &mon_24v);   // P003        AN003   24V_MON
            //g_adc0.p_api->read(g_adc0.p_ctrl, ADC_REG_CHANNEL_4, &data4);     // P004        AN004   AN4_FB

#if DOAO_MODULE
            db_vt_set_output_monitor(RELAY_1, data0);
            db_vt_set_output_monitor(RELAY_2, data1);
            db_vt_set_output_monitor(RELAY_3, data2);
            db_vt_set_v24_input_monitor(mon_24v);
            db_vt_set_output_monitor(RELAY_4, data4);
#else
            db_vt_set_lux_monitor(data0);
            db_vt_set_v24_input_monitor(mon_24v);
#endif
            // check end of line and store in database
            if (adc_test_rs485bus_eol() == TRUE)
            {
                db_vt_set_rs485_bus_eol(TRUE);
            }
            else
            {
                db_vt_set_rs485_bus_eol(FALSE);
            }

            //set_adc_timer_expired(FALSE); // clear timer expired flag before starting timer
            //timer_status = tx_timer_activate(&adc_timer); // start the adc timer again

            // scan a/d
            g_adc0.p_api->scanStart(g_adc0.p_ctrl);
        }
        tx_thread_sleep (THREAD_COMMON_DELAY);
    }
}
