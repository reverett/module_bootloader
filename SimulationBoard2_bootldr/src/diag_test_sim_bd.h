#ifndef __DIAG_TEST_H__
#define __DIAG_TEST_H__

#include "hal_data.h"

void diag_test_init_led(void);

void diag_test_turn_on_led(void);
#endif //__DIAG_TEST_H__
