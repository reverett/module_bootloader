//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  hw_io_api.h
//
//  Description:
//
//-----------------------------------------------------------------------------

#ifndef __HW_IO_API_H__
#define __HW_IO_API_H__

#ifndef __JELLYFISH_COMMON_H__
#include "jellyfish_common.h"
#endif
#include "common_def.h"

#define RELAY_ID_MASK_1      (0x01) << 0
#define RELAY_ID_MASK_2      (0x01) << 1
#define RELAY_ID_MASK_3      (0x01) << 2
#define RELAY_ID_MASK_4      (0x01) << 3

#define IO_MODULE_RELAY_NUM               4
#define RENESAS_PROCESSOR_ID_SIZE        16
#define INTERMATIC_MODEL_NUM_SIZE        10
#define PRODUCT_SERIAL_NUM_SIZE          10
#define ENCRYPTION_KEY_SIZE             128


//------------------------------------------------------------
// API functions that access the IO Module hardware
//------------------------------------------------------------
STATUS_E      hw_get_mcu_id(unsigned char *mcu_id_p);

//STATUS_E      hw_set_relay_state(RELAY_ID_E relay_id, SWITCH_STATE_E relay_state);
//void          hw_set_relay_off_immediately(RELAY_ID_E relay_id);
//void          hw_set_all_relay_off_immediately(void);
//STATUS_E      hw_set_zc_enable(ZC_RELAY_ID_E zc_relay_id, ZC_ENABLE_E zc_enable);
//void          hw_set_enable_all_zc(void);
STATUS_E      hw_set_relay_pwm_output(PWM_ID_E pwm_id, unsigned pwm_value);
void          hw_set_port(PORT_ID_E port_id, PORT_STATE_E port_state);
void          hw_set_led(LED_ID_E led_id, LED_STATE_E led_state);
void          hw_set_all_leds(LED_STATE_E led_state);
void          hw_set_rs485bus_eol(RS485_EOL_ENABLE_E set_rs485_eol);
void          hw_init_eol_circuit(void);

#endif // __HW_IO_API_H__

