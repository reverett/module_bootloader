//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  critical_section.h
//
//  Description:
//
//-----------------------------------------------------------------------------
#ifndef __CRITICAL_SECTION_H__
#define __CRITICAL_SECTION_H__

void enter_critical_section(void);
void exit_critical_section(void);

#endif // __CRITICAL_SECTION_H__
