#include "hal_data.h"
#include "diag_test_sim_bd.h"
#include "led_state.h"
void diag_test_init_led(void)
{
    g_ioport.p_api->pinWrite(IOPORT_PORT_03_PIN_04, IOPORT_LEVEL_LOW); // LED 8 OFF
    g_ioport.p_api->pinWrite(IOPORT_PORT_02_PIN_04, IOPORT_LEVEL_LOW); // LED 9 OFF
    g_ioport.p_api->pinWrite(IOPORT_PORT_02_PIN_05, IOPORT_LEVEL_LOW); // LED 10 OFF
    g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_07, IOPORT_LEVEL_LOW); // LED 11 OFF
    g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_08, IOPORT_LEVEL_LOW); // LED 12 OFF
    g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_09, IOPORT_LEVEL_LOW); // LED 13 OFF
    g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_10, IOPORT_LEVEL_LOW); // LED 14 OFF
    g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_11, IOPORT_LEVEL_LOW); // LED 15 OFF
}


void diag_test_turn_on_led(void)
{
    static LED_IDS_E led_id = LED_8;
    static ioport_level_t level = IOPORT_LEVEL_HIGH;

    switch (led_id)
    {
        case LED_8:
            g_ioport.p_api->pinWrite(IOPORT_PORT_03_PIN_04, level); // LED 8
            led_id = LED_9;
            break;
        case LED_9:
            g_ioport.p_api->pinWrite(IOPORT_PORT_02_PIN_04, level); // LED 9
            led_id = LED_10;
            break;
        case LED_10:
            g_ioport.p_api->pinWrite(IOPORT_PORT_02_PIN_05, level); // LED 10
            led_id = LED_11;
            break;
        case LED_11:
            g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_07, level); // LED 11
            led_id = LED_12;
            break;
        case LED_12:
            g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_08, level); // LED 12
            led_id = LED_13;
            break;
        case LED_13:
            g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_09, level); // LED 13
            led_id = LED_14;
            break;
        case LED_14:
            g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_10, level); // LED 14
            led_id = LED_15;
            break;
        case LED_15:
            g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_11, level); // LED 15

            if (level == IOPORT_LEVEL_HIGH)
                level = IOPORT_LEVEL_LOW;
            else
                level = IOPORT_LEVEL_HIGH;
            led_id = LED_8;
            break;
        default:
            break;
    }
}
