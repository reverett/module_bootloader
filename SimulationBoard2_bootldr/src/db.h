//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  db.h
//
//  Description: Functions used by the database implementation.
//               These are not the Database APIs.
//               Data defined here are supposed to be referenced by only the
//               database implementation module and the database API module.
//-----------------------------------------------------------------------------

#ifndef __DB_H__
#define __DB_H__

#include "jellyfish_common.h"
#include "common_def.h"

#include "db_def.h"

#ifdef DOAO_MODULE
extern doao_module_db_v_t    db_v_tbl;
extern doao_module_db_nv_t   db_nv_tbl;
#else
extern scm_occ_module_db_v_t    db_v_tbl;
extern scm_occ_module_db_nv_t   db_nv_tbl;
#endif

STATUS_E         db_create_mutex(void);
STATUS_E         db_lock(void);
STATUS_E         db_unlock(void);
STATUS_E         db_initialization(void);
BOOL             db_is_initialized(void);
BOOL             db_is_changed(void);
void             db_set_changed_flg(BOOL changed_flg);



#endif // __DB_H__



