//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  hw_io_api.c
//
//  Description:
//
//-----------------------------------------------------------------------------


#include "jellyfish_common.h"
#include "common_def.h"
#include "hal_data.h"
#include "hw_io_api.h"
//#include "relay_state.h"
//#include "relay_thread.h"
#include "critical_section.h"
#include "db_api.h"
#include "sys_config.h"

#ifdef DOAO_MODULE
#include "relay_state.h"
#include "relay_thread.h"
#else
#include "hw_thread.h"
#endif
#include "adc_scan.h"
#define A_OUTPUT 0
#define B_OUTPUT 1

#if 0
//--------------------------------------------------------------------------------
// NAME      : hw_get_mcu_id
// ABSTRACT  : This API calls teh Renesas SSP package to read the MCU's unique
//             16-byte ID.
// ARGUMENTS :
//   unsigned char *mcu_id_p: pointer to a memory array of size unsigned char
//    for example: uint8_t mcu_id[16]
//                            to store the MCU ID.
// RETURN    :
//   STATUS_E : result status code.
//--------------------------------------------------------------------------------
STATUS_E hw_get_mcu_id(unsigned char *mcu_id_p)
{
    ssp_err_t ssp_error = SSP_SUCCESS;
    STATUS_E id_status = SUCCESS;
    fmi_product_info_t      *p_fmi_info;

    ssp_error = g_fmi0.p_api->productInfoGet(&p_fmi_info);

    if ((ssp_error == SSP_SUCCESS)&&(mcu_id_p != NULL))
    {
        id_status = SUCCESS;

        for (int i= 0; i<16; i++)
        {
            mcu_id_p[i] = p_fmi_info->unique_id[i];
        }
    }
    else
    {
        id_status =  ERR_HW_SSP;
    }

    return id_status;

}
#endif
#ifdef AODO_MODULE
//--------------------------------------------------------------------------------
// NAME      : hw_set_relay_state
// ABSTRACT  : This API sets a relay to desired state.
// ARGUMENTS :
//   RELAY_DI_E     : id of the relay;
//   SWITCH_STATE_E : state to set the relay to;
// RETURN    :
//   STATUS_E       : result of the function execution (note, this is not the
//                    resulted relay state).
//--------------------------------------------------------------------------------

STATUS_E hw_set_relay_state(RELAY_ID_E relay_id, SWITCH_STATE_E relay_state)
{
    // set relay state to relay_state
    set_relay_state(relay_id, relay_state, AUTO_SW);

    return SUCCESS;
}


//------------------------------------------------------------------------------
// Name:        hw_set_relay_off_immediately
// Abstract:    This function set a relay off forcefully without waiting for
//              state transition to complete. It is intended for initial startup
//              or testing purpose only, not for normal operation since it will
//              leave the state machine likely inconsistent.
// Arguments:
//   RELAY_ID_E relay_id : id of the relay;
// Return: (none)
//------------------------------------------------------------------------------
void hw_set_relay_off_immediately(RELAY_ID_E relay_id)
{
    ioport_port_pin_t relay_port_pin;

    switch (relay_id)
    {
      case 1:
          relay_port_pin = RELAY1_BREAK_PIN;
          break;
      case 2:
          relay_port_pin = RELAY2_BREAK_PIN;
          break;
      case 3:
          relay_port_pin = RELAY3_BREAK_PIN;
          break;
      case 4:
          relay_port_pin = RELAY4_BREAK_PIN;
          break;
      default:
          return;
    }
    g_ioport.p_api->pinWrite(relay_port_pin, IOPORT_LEVEL_HIGH);
    R_BSP_SoftwareDelay(15, BSP_DELAY_UNITS_MILLISECONDS);
    g_ioport.p_api->pinWrite(relay_port_pin, IOPORT_LEVEL_LOW);

}


//------------------------------------------------------------------------------
// Name:        hw_set_all_relay_off_immediately
// Abstract:    This function set all relay off forcefully without waiting for
//              state transition to complete. It is intended for initial startup
//              or testing purpose only, not for normal operation since it will
//              leave the state machine likely inconsistent.
// Arguments: (none)
// Return: (none)
//------------------------------------------------------------------------------
void hw_set_all_relay_off_immediately(void)
{
    hw_set_relay_off_immediately( RELAY_1 );
    hw_set_relay_off_immediately( RELAY_2 );
    hw_set_relay_off_immediately( RELAY_3 );
    hw_set_relay_off_immediately( RELAY_4 );
}


//--------------------------------------------------------------------------------
// NAME      : hw_set_zc_enable
// ABSTRACT  : This API enables/disables the Zero-Cross function of a relay.
// ARGUMENTS :
//   ZC_RELAY_DI_E  : id of the relay of the ZC function;
//   ZC_DISABLE_E   : set the ZC function to DISABLE/NOT_DISABLE
// RETURN    :
//   STATUS_E       : result of the function execution (note, this is not the
//                    resulted relay state).
//--------------------------------------------------------------------------------
STATUS_E hw_set_zc_enable(ZC_RELAY_ID_E zc_relay_id, ZC_ENABLE_E zc_enable)
{

    switch (zc_relay_id)
    {
      case RELAY_1:
          if (zc_enable == ZC_ENABLE)
              g_ioport.p_api->pinWrite(ZC_ENABLE_PIN_RELAY1, IOPORT_LEVEL_HIGH);
          else
              g_ioport.p_api->pinWrite(ZC_ENABLE_PIN_RELAY1, IOPORT_LEVEL_LOW);
        break;

      case RELAY_2:
          if (zc_enable == ZC_ENABLE)
              g_ioport.p_api->pinWrite(ZC_ENABLE_PIN_RELAY2, IOPORT_LEVEL_HIGH);
          else
              g_ioport.p_api->pinWrite(ZC_ENABLE_PIN_RELAY2, IOPORT_LEVEL_LOW);
        break;

      case RELAY_3:
          if (zc_enable == ZC_ENABLE)
              g_ioport.p_api->pinWrite(ZC_ENABLE_PIN_RELAY3, IOPORT_LEVEL_HIGH);
          else
              g_ioport.p_api->pinWrite(ZC_ENABLE_PIN_RELAY3, IOPORT_LEVEL_LOW);
        break;

      case RELAY_4:
          if (zc_enable == ZC_ENABLE)
              g_ioport.p_api->pinWrite(ZC_ENABLE_PIN_RELAY4, IOPORT_LEVEL_HIGH);
          else
              g_ioport.p_api->pinWrite(ZC_ENABLE_PIN_RELAY4, IOPORT_LEVEL_LOW);
        break;
    }

    return SUCCESS;
}


//--------------------------------------------------------------------------------
// NAME      : enable_all_relay_zc
// ABSTRACT  : This API enables each individual Relay's Zero-Cross function.
//             It is to be called at the module's startup so that all relays
//             are set to have the zero-cross function enabled as default.
//             Only in test case, the function may be disabled.
// ARGUMENTS : (none)
// RETURN    : (none)
//--------------------------------------------------------------------------------
void hw_set_enable_all_zc(void)
{
    hw_set_zc_enable(ZC_RELAY_1, ZC_ENABLE);
    hw_set_zc_enable(ZC_RELAY_2, ZC_ENABLE);
    hw_set_zc_enable(ZC_RELAY_3, ZC_ENABLE);
    hw_set_zc_enable(ZC_RELAY_4, ZC_ENABLE);
}
#endif
// detects and sets the correct end of line configuration
// eol configuration can be overridden by SC commands
void hw_init_eol_circuit(void)
{
	if (adc_test_rs485bus_eol() == TRUE)
	{
		hw_set_rs485bus_eol(RS485_EOL_ENABLE);
	}
	else
	{
		hw_set_rs485bus_eol(RS485_EOL_DISABLE);
	}
}

#ifdef AODO_BOARD
//--------------------------------------------------------------------------------
// NAME      : hw_set_relay_pwm_output
// ABSTRACT  : This API sets and returns a relay's PWM (Pulse Width Modulation)
//             output, i.e., percent duty cycle 0-100%.
// ARGUMENTS :
//   RELAY_DI_E  : id of the relay;
// RETURN    :
//   STATUS_E    : result of the function execution (note, this is not the
//                 resulted relay state).
//--------------------------------------------------------------------------------
STATUS_E hw_set_relay_pwm_output(PWM_ID_E pwm_id, unsigned pwm_value)
{
    // set relay_pwm_value as hardware PWM value;
    STATUS_E  status = SUCCESS;
    ssp_err_t ssp_err = SSP_SUCCESS;

    switch(pwm_id)
    {
        case PWM_RELAY1:
            ssp_err = g_timer4_pwm_1_2.p_api->dutyCycleSet(g_timer4_pwm_1_2.p_ctrl,pwm_value,TIMER_PWM_UNIT_PERCENT,A_OUTPUT);
            break;
        case PWM_RELAY2:
            ssp_err = g_timer4_pwm_1_2.p_api->dutyCycleSet(g_timer4_pwm_1_2.p_ctrl,pwm_value,TIMER_PWM_UNIT_PERCENT,B_OUTPUT);
            break;
        case PWM_RELAY3:
            ssp_err = g_timer1_pwm_3_4.p_api->dutyCycleSet(g_timer1_pwm_3_4.p_ctrl,pwm_value,TIMER_PWM_UNIT_PERCENT,A_OUTPUT);
            break;
        case PWM_RELAY4:
            ssp_err = g_timer1_pwm_3_4.p_api->dutyCycleSet(g_timer1_pwm_3_4.p_ctrl,pwm_value,TIMER_PWM_UNIT_PERCENT,B_OUTPUT);
            break;
        default:
            break;
    }
    if (ssp_err == SSP_SUCCESS)
    {
        db_vt_set_pwm_output(pwm_id, pwm_value); // update database
        status = SUCCESS;
    }
    else
    {
        status = FAILURE;
    }
    return(status);
}
#else
STATUS_E hw_set_relay_pwm_output(PWM_ID_E pwm_id, unsigned pwm_value)
{
    // set relay_pwm_value as hardware PWM value;
    STATUS_E  status = SUCCESS;
    ssp_err_t ssp_err = SSP_SUCCESS;

    switch(pwm_id)
    {
        case PWM_RELAY1:
            ssp_err = g_timer4_pwm.p_api->dutyCycleSet(g_timer4_pwm.p_ctrl,pwm_value,TIMER_PWM_UNIT_PERCENT,A_OUTPUT);
            break;
        default:
            break;
    }
    if (ssp_err == SSP_SUCCESS)
    {
        //db_vt_set_pwm_output(pwm_id, pwm_value); // update database
        status = SUCCESS;
    }
    else
    {
        status = FAILURE;
    }
    return(status);
}
#endif

//--------------------------------------------------------------------------------
// NAME      : hw_set_rs485bus_eol
// ABSTRACT  : This API sets the RS485 bus circuit to control on the DO-AO
//             module so that it is the End-of-Line. RS485 traffic will not
//             flow out to the next module even if there is one.
//
// ARGUMENTS :
//   RS485_EOL_ENABLE_E: set_rs485_eol:
//                  to indicate enabling or disabling the circuit so that
//                  this module is act as the End of Line on the RS485 bus.
// RETURN    :
//   (void)
//--------------------------------------------------------------------------------
void hw_set_rs485bus_eol(RS485_EOL_ENABLE_E set_rs485_eol)
{

    switch (set_rs485_eol)
    {
      case RS485_EOL_ENABLE:
        {
            g_ioport.p_api->pinWrite(RS485_EOL_RESISTER_PIN, IOPORT_LEVEL_HIGH);
            break;
        }
      case RS485_EOL_DISABLE:
        {
            g_ioport.p_api->pinWrite(RS485_EOL_RESISTER_PIN, IOPORT_LEVEL_LOW);
            break;
        }
      default:
        {
            // It is an error if code gets to here
            break;
        }
    }
}


//--------------------------------------------------------------------------------
// NAME      : hw_set_led
// ABSTRACT  : This function calls set one of the IO Module's LED to desired
//             states: on, off, or, flashing.
// ARGUMENTS :
//   LED_ID_E: led_id;
//   LED_STATE_E: led_state
// RETURN    :
//   (none)
//--------------------------------------------------------------------------------
void hw_set_port(PORT_ID_E port_id, PORT_STATE_E port_state)
{
    // set LED indicated by led_id to the desired "led_state".

#if 0
    switch(port_id)
    {
        ioport_level_t port_status;
        case PORT1: // SSLA00SW1A
          {
            if (port_state == PORT_ON)
                g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_03, IOPORT_LEVEL_HIGH);
            else if (port_state == PORT_OFF)
                g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_03, IOPORT_LEVEL_LOW);
            else
            {
                g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_03, &port_status);
                if (IOPORT_LEVEL_HIGH == port_status)
                    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_03, IOPORT_LEVEL_LOW);
                else
                    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_03, IOPORT_LEVEL_HIGH);
            }
          }
          break;

        case PORT2: // RSPCKA0SW2
          {
            if (port_state == PORT_ON)
                g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_02, IOPORT_LEVEL_HIGH);
            else if (port_state == PORT_OFF)
                g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_02, IOPORT_LEVEL_LOW);
            else
            {
                g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_02, &port_status);
                if (IOPORT_LEVEL_HIGH == port_status)
                    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_02, IOPORT_LEVEL_LOW);
                else
                    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_02, IOPORT_LEVEL_HIGH);
            }
          }
          break;

        case PORT3: // MOSIA0SW3
          {
            if (port_state == PORT_ON)
                g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_01, IOPORT_LEVEL_HIGH);
            else if (port_state == PORT_OFF)
                g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_01, IOPORT_LEVEL_LOW);
            else
            {
                g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_01, &port_status);
                if (IOPORT_LEVEL_HIGH == port_status)
                    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_01, IOPORT_LEVEL_LOW);
                else
                    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_01, IOPORT_LEVEL_HIGH);
            }
          }
          break;

        case PORT4: // MISOA_SW4A
          {
            if (port_state == PORT_ON)
                g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_00, IOPORT_LEVEL_HIGH);
            else if (port_state == PORT_OFF)
                g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_00, IOPORT_LEVEL_LOW);
            else
            {
                g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_00, &port_status);
                if (IOPORT_LEVEL_HIGH == port_status)
                    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_00, IOPORT_LEVEL_LOW);
                else
                    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_00, IOPORT_LEVEL_HIGH);
            }
          }
          break;
        case PORT5: // TXD10SW1
          {
            if (port_state == PORT_ON)
                g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_01, IOPORT_LEVEL_HIGH);
            else if (port_state == PORT_OFF)
                g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_01, IOPORT_LEVEL_LOW);
            else
            {
                g_ioport.p_api->pinRead(IOPORT_PORT_04_PIN_01, &port_status);
                if (IOPORT_LEVEL_HIGH == port_status)
                    g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_01, IOPORT_LEVEL_LOW);
                else
                    g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_01, IOPORT_LEVEL_HIGH);
            }
          }
          break;

        case PORT6: // RXD1_SW2B
          {
            if (port_state == PORT_ON)
                g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_02, IOPORT_LEVEL_HIGH);
            else if (port_state == PORT_OFF)
                g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_02, IOPORT_LEVEL_LOW);
            else
            {
                g_ioport.p_api->pinRead(IOPORT_PORT_04_PIN_02, &port_status);
                if (IOPORT_LEVEL_HIGH == port_status)
                    g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_02, IOPORT_LEVEL_LOW);
                else
                    g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_02, IOPORT_LEVEL_HIGH);
            }
          }
          break;

        case PORT7: // GPIO1_SW3B
          {
            if (port_state == PORT_ON)
                g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_00, IOPORT_LEVEL_HIGH);
            else if (port_state == PORT_OFF)
                g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_00, IOPORT_LEVEL_LOW);
            else
            {
                g_ioport.p_api->pinRead(IOPORT_PORT_04_PIN_00, &port_status);
                if (IOPORT_LEVEL_HIGH == port_status)
                    g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_00, IOPORT_LEVEL_LOW);
                else
                    g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_00, IOPORT_LEVEL_HIGH);
            }
          }
          break;

        case PORT8: // GPIO2_SW4B
          {
            if (port_state == PORT_ON)
                g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_03, IOPORT_LEVEL_HIGH);
            else if (port_state == PORT_OFF)
                g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_03, IOPORT_LEVEL_LOW);
            else
            {
                g_ioport.p_api->pinRead(IOPORT_PORT_04_PIN_03, &port_status);
                if (IOPORT_LEVEL_HIGH == port_status)
                    g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_03, IOPORT_LEVEL_LOW);
                else
                    g_ioport.p_api->pinWrite(IOPORT_PORT_04_PIN_03, IOPORT_LEVEL_HIGH);
            }
          }
          break;
        default:
          break;

    }
    return;
#endif
}

//--------------------------------------------------------------------------------
// NAME      : hw_set_led
// ABSTRACT  : This function calls set one of the IO Module's LED to desired
//             states: on, off, or, flashing.
// ARGUMENTS :
//   LED_ID_E: led_id;
//   LED_STATE_E: led_state
// RETURN    :
//   (none)
//--------------------------------------------------------------------------------
#ifdef AODO_MODULE
void hw_set_led(LED_ID_E led_id, LED_STATE_E led_state)
{
    // set LED indicated by led_id to the desired "led_state".

    switch(led_id)
    {
        ioport_level_t port_status;
        case LED_RELAY1:
          {
            if (led_state == LED_ON)
                g_ioport.p_api->pinWrite(LED_PIN_RELAY1, IOPORT_LEVEL_HIGH);
            else if (led_state == LED_OFF)
                g_ioport.p_api->pinWrite(LED_PIN_RELAY1, IOPORT_LEVEL_LOW);
            else
            {
                g_ioport.p_api->pinRead(LED_PIN_RELAY1, &port_status);
                if (IOPORT_LEVEL_HIGH == port_status)
                    g_ioport.p_api->pinWrite(LED_PIN_RELAY1, IOPORT_LEVEL_LOW);
                else
                    g_ioport.p_api->pinWrite(LED_PIN_RELAY1, IOPORT_LEVEL_HIGH);
            }
          }
          break;

        case LED_RELAY2:
          {
            if (led_state == LED_ON)
                g_ioport.p_api->pinWrite(LED_PIN_RELAY2, IOPORT_LEVEL_HIGH);
            else if (led_state == LED_OFF)
                g_ioport.p_api->pinWrite(LED_PIN_RELAY2, IOPORT_LEVEL_LOW);
            else
            {
                g_ioport.p_api->pinRead(LED_PIN_RELAY2, &port_status);
                if (IOPORT_LEVEL_HIGH == port_status)
                    g_ioport.p_api->pinWrite(LED_PIN_RELAY2, IOPORT_LEVEL_LOW);
                else
                    g_ioport.p_api->pinWrite(LED_PIN_RELAY2, IOPORT_LEVEL_HIGH);
            }
          }
          break;

        case LED_RELAY3:
          {
            if (led_state == LED_ON)
                g_ioport.p_api->pinWrite(LED_PIN_RELAY3, IOPORT_LEVEL_HIGH);
            else if (led_state == LED_OFF)
                g_ioport.p_api->pinWrite(LED_PIN_RELAY3, IOPORT_LEVEL_LOW);
            else
            {
                g_ioport.p_api->pinRead(LED_PIN_RELAY3, &port_status);
                if (IOPORT_LEVEL_HIGH == port_status)
                    g_ioport.p_api->pinWrite(LED_PIN_RELAY3, IOPORT_LEVEL_LOW);
                else
                    g_ioport.p_api->pinWrite(LED_PIN_RELAY3, IOPORT_LEVEL_HIGH);
            }
          }
          break;

        case LED_RELAY4:
          {
            if (led_state == LED_ON)
                g_ioport.p_api->pinWrite(LED_PIN_RELAY4, IOPORT_LEVEL_HIGH);
            else if (led_state == LED_OFF)
                g_ioport.p_api->pinWrite(LED_PIN_RELAY4, IOPORT_LEVEL_LOW);
            else
            {
                g_ioport.p_api->pinRead(LED_PIN_RELAY4, &port_status);
                if (IOPORT_LEVEL_HIGH == port_status)
                    g_ioport.p_api->pinWrite(LED_PIN_RELAY4, IOPORT_LEVEL_LOW);
                else
                    g_ioport.p_api->pinWrite(LED_PIN_RELAY4, IOPORT_LEVEL_HIGH);
            }
          }
          break;

        case LED_RS485:
          // RS485 Traffic indication LED
          break;

        case LED_UNUSED:
          // This LED is current not designated for any purpose.
          break;

        default:
          break;

    }
    return;
}
#else
void hw_set_led(LED_ID_E led_id, LED_STATE_E led_state)
{
    // set LED indicated by led_id to the desired "led_state".

    switch(led_id)
    {
        ioport_level_t port_status;
        case LED_RELAY1:
          {
            if (led_state == LED_ON)
                g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_07, IOPORT_LEVEL_HIGH);
            else if (led_state == LED_OFF)
                g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_07, IOPORT_LEVEL_LOW);
            else
            {
                g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_07, &port_status);
                if (IOPORT_LEVEL_HIGH == port_status)
                    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_07, IOPORT_LEVEL_LOW);
                else
                    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_07, IOPORT_LEVEL_HIGH);
            }
          }
          break;

        case LED_RELAY2:
          {
            if (led_state == LED_ON)
                g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_13, IOPORT_LEVEL_HIGH);
            else if (led_state == LED_OFF)
                g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_13, IOPORT_LEVEL_LOW);
            else
            {
                g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_13, &port_status);
                if (IOPORT_LEVEL_HIGH == port_status)
                    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_13, IOPORT_LEVEL_LOW);
                else
                    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_13, IOPORT_LEVEL_HIGH);
            }
          }
          break;

        case LED_RELAY3:
          {
            if (led_state == LED_ON)
                g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_11, IOPORT_LEVEL_HIGH);
            else if (led_state == LED_OFF)
                g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_11, IOPORT_LEVEL_LOW);
            else
            {
                g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_11, &port_status);
                if (IOPORT_LEVEL_HIGH == port_status)
                    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_11, IOPORT_LEVEL_LOW);
                else
                    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_11, IOPORT_LEVEL_HIGH);
            }
          }
          break;

        case LED_RELAY4:
          {
            if (led_state == LED_ON)
                g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_10, IOPORT_LEVEL_HIGH);
            else if (led_state == LED_OFF)
                g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_10, IOPORT_LEVEL_LOW);
            else
            {
                g_ioport.p_api->pinRead(IOPORT_PORT_01_PIN_10, &port_status);
                if (IOPORT_LEVEL_HIGH == port_status)
                    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_10, IOPORT_LEVEL_LOW);
                else
                    g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_10, IOPORT_LEVEL_HIGH);
            }
          }
          break;

        default:
          break;

    }
    return;
}
#endif

//--------------------------------------------------------------------------------
// NAME      : hw_set_led
// ABSTRACT  : This API function set all LEDs by calling the set led api
//             individually.
// ARGUMENTS :
//   LED_STATE_E: led_state
// RETURN    :
//   (none)
//--------------------------------------------------------------------------------
void hw_set_all_leds(LED_STATE_E led_state)
{
    // set LEDs
    hw_set_led(LED_RELAY1, led_state);
    hw_set_led(LED_RELAY2, led_state);
    hw_set_led(LED_RELAY3, led_state);
    hw_set_led(LED_RELAY4, led_state);
#ifdef AODO_MODULE
    hw_set_led(LED_RS485, led_state);
    hw_set_led(LED_UNUSED, led_state);
#endif
}



