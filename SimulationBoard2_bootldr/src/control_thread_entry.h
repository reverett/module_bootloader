//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  control_thread_entry.h
//
//  Description: Defines the Inter-Thread Communication (ITC) type and data.
//
//-----------------------------------------------------------------------------

#ifndef __CONTROL_THREAD_ENTRY_H__
#define __CONTROL_THREAD_ENTRY_H__

#include "tx_api.h"
#include "common_def.h"


// Size of each message, in unit of 32-bit,
// or 4-byte
#define CONTROL_TX_MESSAGE_SIZE            (sizeof(ULONG))

// Max number of message the queue can hold
#define CONTROL_TX_MAX_MESSAGE_NUM          5

// Total space to carved out from "byte pool"
// for the message queue
#define CONTROL_TX_QUEUE_SIZE          (CONTROL_TX_MESSAGE_SIZE * CONTROL_TX_MAX_MESSAGE_NUM)



STATUS_E     control_create_msg_queue(void);
STATUS_E     control_send_msg_to_controller(void *msg_p);
#ifdef DOAO_MODULE
STATUS_E     control_relay_instr_completion_cb_fn(void);
#endif


// define a type for the call back functions defined
// in the control thread
typedef STATUS_E (*control_cb_func_t)(void);

#endif // __CONTROL_THREAD_ENTRY_H__
