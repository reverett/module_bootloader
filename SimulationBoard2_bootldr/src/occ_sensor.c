//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  relay_state.c
//
//  Description: Contains the relay state machines
//
//-----------------------------------------------------------------------------
#include "jellyfish_common.h"
#include "switch_state.h"
#include "led_state.h"
#include "occ_sensor.h"
#include "db_api.h"

static unsigned int occ_timer = 0;
unsigned int occ_expiration_time = 0; // is in units of seconds
static OCC_STATES_E current_occ_state = OCC_STATE_VACANT;

// get the current occ state
OCC_STATES_E getOccState(void)
{
    return(current_occ_state);
}

// set a new expiration time
void setOccExpirationTime(unsigned int expirationTime)
{
    unsigned int num_sec;
    //enter_critical_section();
    switch(expirationTime)
    {
        case VAC_TIMEOUT_5_SEC:
            num_sec = 5;
            break;
        case VAC_TIMEOUT_15_SEC:
            num_sec = 15;
            break;
        case VAC_TIMEOUT_30_SEC:
            num_sec = 30;
            break;
        case VAC_TIMEOUT_1_MIN:
            num_sec = 60;
            break;
        case VAC_TIMEOUT_5_MIN:
            num_sec = 5 * 60;
            break;
        case VAC_TIMEOUT_10_MIN:
            num_sec = 10 * 60;
            break;
        case VAC_TIMEOUT_15_MIN:
            num_sec = 15 * 60;
            break;
        case VAC_TIMEOUT_20_MIN:
            num_sec = 20 * 60;
            break;
        case VAC_TIMEOUT_30_MIN:
            num_sec = 30 * 60;
            break;
        case VAC_TIMEOUT_45_MIN:
            num_sec = 45 * 60;
            break;
        case VAC_TIMEOUT_60_MIN:
            num_sec = 60 * 60;
            break;
        default:
            num_sec = 5;
    }
    occ_expiration_time = num_sec;
    //exit_critical_section();
}

// called every time there is a rising edge transition
void reset_occ_timer(void)
{
    occ_timer = 0;
    current_occ_state = OCC_STATE_OCCUPIED;
    db_vt_set_occ_state(OCC_STATE_OCCUPIED);
    set_led_state(SWITCH_1, LED_STATE_ON);
}

// called at start up room is VACANT
void start_occ_timer(void)
{
    current_occ_state = OCC_STATE_VACANT;
    db_vt_set_occ_state(OCC_STATE_VACANT);
    set_led_state(SWITCH_1, LED_STATE_OFF);
}

// called every TBD timer interval
void incr_occ_timer(void)
{

    if (occ_timer < occ_expiration_time)
        occ_timer = occ_timer + 1;
    else
    {
        current_occ_state = OCC_STATE_VACANT;
        db_vt_set_occ_state(OCC_STATE_VACANT);
        set_led_state(SWITCH_1, LED_STATE_OFF);
    }

}
