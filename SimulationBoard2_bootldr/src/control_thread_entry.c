//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  control_thread_entry.c
//
//  Description: Thread implementation of the DO-AO Output Module's main
//               hardware control functions.
//
//-----------------------------------------------------------------------------

// Include files of standard C language
#include <string.h>


// Include files of SSP and ThreadX

#include "control_thread.h"


// Include files of Jellyfish DO-AO Module

#include "common_def.h"
#include "sc_command.h"
#include "switch_state.h"
//#include "relay_state.h"
#include "hw_io_api.h"
//#include "db_api.h"
#include "critical_section.h"
#include "sys_manager.h"
#include "control_thread_entry.h"
#include "rs485_driver.h"
//#include "relay_thread_entry.h"

#ifdef DOAO_MODULE
#include "relay_state.h"
#include "db_api.h"
#include "relay_thread_entry.h"
#endif
//---------------------------------------------------------------------
// Local Data Type Declarations
//---------------------------------------------------------------------

#define MAX_NUM_RELAY_SET_INSTR            16

typedef struct relay_set_instr_s {
    unsigned char       hw_id;
    unsigned char       param;
} relay_set_instr_t;

// This is a circular queue for the SET command

typedef struct relay_set_instr_q_s {
    relay_set_instr_t     instr[MAX_NUM_RELAY_SET_INSTR];
    unsigned short        head_idx;    // to write in next command
    unsigned short        tail_idx;    // to read out next command
} relay_set_instr_q_t;



//---------------------------------------------------------------------
// Local Function Declarations
//---------------------------------------------------------------------
static STATUS_E        _control_handle_set_request(sc_cmd_t *set_cmd_p);
#ifdef DOAO_MODULE
static void            _init_relay_set_instr_q(void);
static unsigned short  _inc_relay_set_instr_q_index(unsigned short index);
static BOOL            _relay_set_instr_q_empty(void);
static BOOL            _relay_set_instr_q_full(void);
static void            _get_next_set_instr_in_q(relay_set_instr_t *relay_set_instr);
static STATUS_E        _add_set_instr_to_q(unsigned char hw_id, unsigned char param);
#endif

//---------------------------------------------------------------------
// Static Data Definitions
//---------------------------------------------------------------------

static unsigned short   control_total_msg = 0;
static sc_cmd_t         cmd_rcvd;

TX_QUEUE                control_msg_queue;
unsigned long           rs485_message;
int                     msg_cnt=0;



BOOL                    all_relay_set_completed = TRUE;

char   clear_screen[7] = {0x1B, 0x5B, '2', 'J', 0x1B, 0x5B, 'H',};
char   *prompt_msg     = "Jellyfish DOAO Module Initialized, now starting Threads ....\r\n";
char   *test_msg2      = "2. handling cmd\n\r";
char   *test_msg3      = "3. processing cmd\n\r";

#ifdef DOAO_MODULE
relay_set_instr_q_t     relay_set_instr_q;
//------------------------------------------------------------------------------
// Name:                _init_relay_set_instr_q
// Abstract:            This function initialize the relay set instruction's
//                      command buffer queue.
//                      head of the queue is the slot to read out next instruction.
//                      tail of the queue is the slot to write in next instruction.
// Arguments:           (none)
// Return value(s):     (none)
//------------------------------------------------------------------------------
static void _init_relay_set_instr_q(void)
{
    relay_set_instr_q.head_idx = 0;
    relay_set_instr_q.tail_idx = 0;
}


//------------------------------------------------------------------------------
// Name:                _inc_relay_set_instr_q_index
//
// Abstract:            This function increase the head (read) or tail (write)
//                      index to SET instruction queue. If it reaches the end
//                      of the flat data array, it loops back so that the queue
//                      is circular.
// Arguments:
//     unsigned short index: the queue index to be incremented
// Return value(s):
//     unsigned short: the result index after the increment.
//------------------------------------------------------------------------------
static unsigned short _inc_relay_set_instr_q_index(unsigned short index)
{
    if (++index < MAX_NUM_RELAY_SET_INSTR)
    {
        return index;
    }
    else
    {
        return 0;
    }
}


//------------------------------------------------------------------------------
// Name:                _relay_set_instr_q_empty
// Abstract:            This API function returns TRUE if there are no instruction
//                      in the relay set instruction queue.
// Arguments:           (none)
// Return value(s):     (none)
//------------------------------------------------------------------------------
static BOOL _relay_set_instr_q_empty(void)
{

    if (relay_set_instr_q.head_idx == relay_set_instr_q.tail_idx)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}


//------------------------------------------------------------------------------
// Name:                _relay_set_instr_q_full
// Abstract:            This function determines if the relay SET instruction
//                      queue is full. Here the tail is intentionally kept
//                      a one slot away from the head, to avoid the ambiguity
//                      of head_idx == tail_idx situation  (can't tell full or empty).
// Arguments:           (none)
// Return value(s):     (none)
//------------------------------------------------------------------------------
static BOOL _relay_set_instr_q_full(void)
{
    if((relay_set_instr_q.head_idx==MAX_NUM_RELAY_SET_INSTR-1) &&
       (relay_set_instr_q.tail_idx==0))
    {
        return TRUE;
    }
    else
    {
        if (relay_set_instr_q.head_idx == relay_set_instr_q.tail_idx - 1)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}


//------------------------------------------------------------------------------
// NAME      : _get_next_set_instr_in_q
// ABSTRACT  : This function returns the next SET command's instruction from
//             instruction queue (i.e., the head).
//             Since function will not check the status of the queue, the caller
//             must proceed this call with a call to the other API function
//             _relay_set_instr_q_full() to make sure the queue is not empty.
// ARGUMENTS :
//   relay_set_instr_t *relay_set_instr_p - point to location to store the next
//                                          instruction.
// RETURN    : (void)
//------------------------------------------------------------------------------
static void _get_next_set_instr_in_q(relay_set_instr_t *relay_set_instr)
{
    relay_set_instr->hw_id = relay_set_instr_q.instr[relay_set_instr_q.tail_idx].hw_id;
    // clear the data for debugging purpose
    relay_set_instr_q.instr[relay_set_instr_q.tail_idx].hw_id = 0;

    relay_set_instr->param = relay_set_instr_q.instr[relay_set_instr_q.tail_idx].param;
    // clear the data for debugging purpose.
    relay_set_instr_q.instr[relay_set_instr_q.tail_idx].param=0;

    // Move the read index
    relay_set_instr_q.tail_idx = _inc_relay_set_instr_q_index(relay_set_instr_q.tail_idx);
}


//------------------------------------------------------------------------------
// NAME      : _add_set_instr_to_q
// ABSTRACT  : This function add a SET command's instruction to the end of the
//             instruction queue (i.e., the head).
// ARGUMENTS :
//     unsigned char relay_id - id of the relay to set
//     unsigned char relay_param - value to set relay to
// RETURN    :
//     STATUS_E - SUCCESS if successful,
//                ERR_SC_INSTR_Q_FULL if the queue is full
//------------------------------------------------------------------------------
STATUS_E _add_set_instr_to_q(unsigned char hw_id, unsigned char param)
{

    if (_relay_set_instr_q_full() == TRUE)
    {
        return (ERR_SC_INSTR_Q_FULL);
    }

    relay_set_instr_q.instr[relay_set_instr_q.head_idx].hw_id = hw_id;
    relay_set_instr_q.instr[relay_set_instr_q.head_idx].param = param;

    // Move the write index
    relay_set_instr_q.head_idx = _inc_relay_set_instr_q_index(relay_set_instr_q.head_idx);

    return (SUCCESS);
}
#endif
#if 1
//--------------------------------------------------------------------------------
// NAME      : control_create_msg_queue
// ABSTRACT  : This function creates the message queue used to communicate with
//             the control thread.
// ARGUMENTS : (none)
// RETURN    :
//     STATUS_E : SUCCESS         - successful;
//              ERR_THRD_MEM_POOL - if the memory pool is not created
//              ERR_MSGQ_CREATE   - otherwise
//--------------------------------------------------------------------------------
STATUS_E control_create_msg_queue(void)
{
TX_BYTE_POOL  *tx_byte_pool;
unsigned char *queue_start;

    if ((tx_byte_pool=sys_get_tx_byte_pool()) == (TX_BYTE_POOL *)NULL)
    {
        return (ERR_THRD_MEM_POOL);
    }

    if (tx_byte_allocate(tx_byte_pool,           // ThreadX managed memory pool
                         (VOID **)&queue_start,  // address of the allocated memory
                         CONTROL_TX_QUEUE_SIZE,  // requested size
                         TX_NO_WAIT)             // "Do not wait" untilm memory available
            != TX_SUCCESS)
    {
        return (ERR_MSGQ_CREATE);
    }

    if (tx_queue_create(&control_msg_queue,      // created msg queue (control block)
                        "control_msg_queue",     // name of the msg queue
                        CONTROL_TX_MESSAGE_SIZE, // per msg size: 1 to 16 word of 4-byte
                        queue_start,             // starting address to allocate queue
                        CONTROL_TX_QUEUE_SIZE)   //
            != TX_SUCCESS)
    {
        return (ERR_MSGQ_CREATE);
    }

    return (SUCCESS);

}
#endif

//--------------------------------------------------------------------------------
// NAME      : control_send_msg_to_controller
// ABSTRACT  : This function sends a message to the controlDO-AO module by queuing it
//             on the message queue.
// ARGUMENTS : (none)
// RETURN    :
//   STATUS_E : SUCCESS       - successful;
//              ERR_MSGQ_FULL - message queue is full
//              ERR_MSGQ_POST - unable to send the message
//--------------------------------------------------------------------------------
STATUS_E control_send_msg_to_controller(void *msg)
{

    // *****TBD***** (Jianbai)
    // This needs to be in mutex

    // TBD - MUTEX ON
    if (control_total_msg == CONTROL_TX_MAX_MESSAGE_NUM)
    {
        // TBD- MUTEX OFF
        return ERR_MSGQ_FULL;
    }

    if (tx_queue_send(&control_msg_queue, msg, TX_WAIT_FOREVER) != TX_SUCCESS)
    {
        // TBD- MUTEX OFF
        return ERR_MSGQ_POST;
    }

    ++control_total_msg;

    // TBD - MUTEX OFF
    return SUCCESS;
}

#ifdef DOAO_MODULE
//--------------------------------------------------------------------------------
// NAME      : control_instr_completion_cb_fn
// ABSTRACT  : This is a call-back function which will be called by they
//             relay thread when a relay state change is complete which
//             is resulted in from executing a relay set instruction.
//             When a set instruction is executed, the relay will transition
//             from one state to another. This will consume time. Only when
//             the state change is completed, the SET command is considered
//             completed, in which case, this function will be called.
//
//             This function will check the SET instruction queue and issue
//             another instruction (pointed by the header index) if it is not
//             empty.
//
//             Note, when this function is called, the execution is in the
//             relay thread's run-time space.
// ARGUMENTS : (none)
// RETURN    :
//   STATUS_E : SUCCESS       - successful;
//
//--------------------------------------------------------------------------------
STATUS_E control_relay_instr_completion_cb_fn(void)
{
    relay_set_instr_t relay_set_instr;

    STATUS_E instr_status = SUCCESS;

    // Check to see if there is any instructions in the queue.
    if (_relay_set_instr_q_empty() != TRUE)
    {
        // There are SET instructions in the queue. Get the
        // first from the queue (will be deleted afterward).
        _get_next_set_instr_in_q(&relay_set_instr);

        // Issue the instruction for execution.
        instr_status = hw_set_relay_state(relay_set_instr.hw_id, relay_set_instr.param);
    }
    else
    {
// *****TBD***** (Jianbai)
// This needs to be in mutex

        // There is no more instructions in the queue, then
        // indicate the execution of relay instructions are all
        // completed.
        all_relay_set_completed = TRUE;

// *****TBD***** (Jianbai)
// This needs to be in mutex
    }

    return (instr_status);
}
#endif

//------------------------------------------------------------------------------
// Name:        _control_handle_set_request
//
// Abstract:    This function processes the "SET" command received from
//              the System Controller via the RS485 network.
//
//              When "set relay instructions" are executed, they must be
//              "staggered". There must be a waiting for a relay to complete
//              the state change before the next instruction can be issued.
//              To secure this delay, all relay instructions are queued first.
//              After parsing the instructions, the relay state status is checked.
//              if all instructions have completed, the first in the queue will
//              be taken out for execution.
//              A call back function will be invoked when a relay completes
//              the state change. It will check the queue and issue the first
//              instruction if the queue is not empty, otherwise, it will mark
//              all instructions are completed.
// Arguments:
//     sc_cmd_t *set_cmd_p - pointer to the received command
// Return:
//     STATUS_E status code: SUCCESS - if successful,
//                           ERR_SC_CMD_NO_INSTR - no instructions in the
//                                     command, no err, but waste of time!
//                           ERR_SC_CMD_INVALID - invalid instruction for
//                                     SET command.
//                           ERR_SC_CMD_EXEC - error happened during execution
//                                     of one or more instruction in the SET
//                                     command.
//------------------------------------------------------------------------------
static STATUS_E _control_handle_set_request(sc_cmd_t *set_cmd_p)
{
    unsigned char         instr_n;
    unsigned char         instr_i;
    sc_cmd_set_instr_t    *instr_p;
    relay_set_instr_t      relay_set_instr;
    STATUS_E               cmd_status;
    STATUS_E               instr_status;


// rs485_tx_write_nbyte(test_msg2, (unsigned short)strlen(test_msg2));

    // Get the total count of instructions in this command
    instr_n = set_cmd_p->cmd.u.params.num_of_instrs;

    if (instr_n == 0)
    {
        return ERR_SC_CMD_NO_INSTR;
    }

    cmd_status = SUCCESS;

    // Traversal each instruction in the SET command
    for (instr_i=0; instr_i<instr_n; instr_i++)
    {

        instr_p = &(set_cmd_p->instrs.set[instr_i]);

        instr_status = SUCCESS;

        switch (instr_p->hw_name)
        {
#ifdef DOAO_MODULE
          case SC_CMD_HW_RELAY:
            {
                // Instruction to SET relay ON/OFF.
                // Always queue up such instructions and wait until finish
                // parsing all the instructions.

// hw_set_relay_state((RELAY_ID_E)instr_p->hw_id, (SWITCH_STATE_E)instr_p->param);
                instr_status = _add_set_instr_to_q((RELAY_ID_E)instr_p->hw_id,
                                                   (SWITCH_STATE_E)instr_p->param);
// rs485_tx_write_nbyte(test_msg3, (unsigned short)strlen(test_msg3));
// tx_thread_sleep(CONTROL_THREAD_DELAY);
                break;
            }

          case SC_CMD_HW_ZC_DISABLE:
            {
                // Instruction to SET Zero-Cross DISABLE/NOT-DISABLE
                instr_status = hw_set_zc_enable((RELAY_ID_E)instr_p->hw_id,
                                                (RELAY_ID_E)instr_p->param);
                break;
            }
#else
          case SC_CMD_HW_PORT:
          {
              hw_set_port((PORT_ID_E)instr_p->hw_id,
                      (PORT_STATE_E)instr_p->param);
                      break;
          }
#endif
          case SC_CMD_HW_ANALOG_PWM:
            {
                // Instruction to SET ANALOG OUTPUT
                instr_status = hw_set_relay_pwm_output((PWM_ID_E)instr_p->hw_id,
                                                       (unsigned)instr_p->param);
                break;
            }


          case SC_CMD_HW_LED:
            {
                // Instruction to SET LED ON/OFF
                hw_set_led((LED_ID_E)instr_p->hw_id,
                           (LED_STATE_E)instr_p->param);
                break;
            }

          case SC_CMD_HW_RS485_EOL:
            {
                // Instruction to SET EOL ENABLE/DISABLE
                hw_set_rs485bus_eol((RS485_EOL_ENABLE_E)instr_p->param);
                break;
            }

          default:
            {
                // unrecognized instruction in the SET command
                // return ERR_SC_CMD_INVALID;
                break;
            }
        } // switch

        if (instr_status != SUCCESS)
        {
            cmd_status = ERR_SC_CMD_EXEC;
        }
        // Do not return abort the execution of the command even
        // if an individual instructions failed.

    }  // for (instr_i=1; instr_i<instr_n; instr_i++)
#ifdef DOAO_MODULE
    // We have parsed all instructions in the SET command and have
    // put any "set relay" instructions into the queue.
    // Now check if the relay thread is busy. If it is not busy, take
    // the first relay instruction from the queue and issue it to the
    // relay thread.
    if (all_relay_set_completed == TRUE)
    {

        // Issue the next relay set instruction only when all
        // previous instructions are completed by the relay thread.
        if (_relay_set_instr_q_empty() != TRUE)
        {
            // There are SET instructions in the queue

// *****TBD***** (Jianbai)
// This needs to be in mutex

            // Indicate the relay state machine will be busy
            all_relay_set_completed = FALSE;

            // Get the head of the instruction queue.
            // (note, the function will remove it from the queue)
            _get_next_set_instr_in_q(&relay_set_instr);

            // Issue the instruction for execution.

            instr_status = hw_set_relay_state(relay_set_instr.hw_id, relay_set_instr.param);


// *****TBD***** (Jianbai)
// This needs to be in mutex

            if (instr_status != SUCCESS)
            {
                cmd_status = instr_status;
            }

        }

    }
#endif
    return cmd_status;

}


//------------------------------------------------------------------------------
// Name:        control_thread_entry
// Abstract:    This function implements the thread that handles I/O Module (DO-AO)
//              hardware provision and run-time operation, as well as, updating the
//              Module's database.
//              Only one instance of the thread will be created at run-time.
// Arguments:   (none)
// Return:      (none)
//------------------------------------------------------------------------------
void control_thread_entry(void)
{


    tx_thread_sleep(CONTROL_THREAD_DELAY);

#ifdef DOAO_UNIT_TEST
    // Print an ESC code to clear the screen
    rs485_tx_write_nbyte(clear_screen, (unsigned short)7);

    // Print a prompt to the RS485 line
    rs485_tx_write_nbyte(prompt_msg, (unsigned short)strlen(prompt_msg));
#endif

#ifdef DOAO_MODULE
    _init_relay_set_instr_q();

    all_relay_set_completed = TRUE;

    // Set the call back function into the relay thread

    // *****TBD***** (Jianbai)
    // This may need to be in the sys_manager module so that it is executed
    // before the relay thread starts.

    relay_set_control_relay_instr_complete_cb(control_relay_instr_completion_cb_fn);
#endif
    while (1)
    {
        // clear the receiving buffer of the SC command
        memset(&cmd_rcvd, 0, sizeof(sc_cmd_t));

        if (tx_queue_receive(&control_msg_queue, &cmd_rcvd, TX_WAIT_FOREVER) == TX_SUCCESS)
        {
            // TBD - MUTEX ON
            --control_total_msg;
            // TBD - MUTEX OFF

            // Parse the command tag and dispatch the command
            // to processing function.
            switch (cmd_rcvd.cmd.tag)
            {
              case SC_CMD_PRV:
                {
                    // TBD, ignore
                    break;
                }

              case SC_CMD_SET:
                {
                    _control_handle_set_request(&cmd_rcvd);
                    break;
                }

              case SC_CMD_GET:
                {
                    // Should be processed by the RX485 thread, ignore
                    break;
                }

              case SC_CMD_IMG:
                {
                    // TBD, ignore
                    break;
                }

              default:
                {
                    // Error, ignore
                    break;
                }

            } // switch (cmd_rcvd.cmd.tag)

            msg_cnt++;
        }

        tx_thread_sleep(CONTROL_THREAD_DELAY);
    }

}

