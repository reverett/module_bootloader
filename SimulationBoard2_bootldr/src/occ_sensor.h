//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  relay_state.h
//
//  Description:
//
//-----------------------------------------------------------------------------
#ifndef __OCC_SENSOR_H__
#define __OCC_SENSOR_H__


typedef enum {
    OCC_STATE_VACANT = 0,
    OCC_STATE_OCCUPIED = 1,
    OCC_STATE_DISABLED = 2
}OCC_STATES_E;

void setOccExpirationTime(unsigned int expirationTime);
void reset_occ_timer(void);
void incr_occ_timer(void);
void start_occ_timer(void);
OCC_STATES_E getOccState(void);

#endif // __OCC_SENSOR_H__
