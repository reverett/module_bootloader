/*
 * flash_data.h
 *
 *  Created on: Mar 28, 2017
 *      Author: reverett
 */

#ifndef FLASH_DATA_H_
#define FLASH_DATA_H_

#include <inttypes.h>
#include "int_prot_frame.h"
#include "jellyfish_common.h"
#define DATAFLASH_ADDRESS       0x40100000
#define CONFIGFLASH_ADDRESS     0x40100400

typedef enum{
    BL_ST_GET_NEW_FW   = 1,        // application sets this when SC has new image available to update device
    BL_ST_FW_IN_PROG   = 2,        // firmware update in progress
    BL_ST_FW_IN_FLASH  = 3,        // application image in flash
    BL_ST_FAIL = 0xFF              // firmware failed crc test
}fw_status_t;

typedef enum {
    PRC_REC_RCVD_OK = 0,            // payload received checksum good
    PRC_REC_FIRST_REC = 1,          // first record the S0 record received OK
    PRC_REC_LAST_REC = 2,           // last record
    PRC_REC_NUM_SREC  = 3,          // last record received but not all records were received
    PRC_REC_SREC_CKSUM_ERR = 4,     // payload checksum error
    PRC_REC_FRAME_CKSUM_ERR = 5,    // frame chksum error
    PRC_REC_SREC_ORDER_ERR = 6,     // frame addresses are not in order
    PRC_REC_SREC_LEN_ERR = 7,       // exceeds bytecount maximum
    PRC_REC_IMG_CRC_ERR = 8,        // crc of image does not agree with crc in s0 record
    PRC_REC_ERASE_FAIL = 9,         // failure to erase block of memory
    PRC_REC_BLANK_FAIL = 10,        // blank check failed
    PRC_REC_FLASH_FAIL = 11,        // failure to flash data
    PRC_REC_IMG_SUCCESS = 12,       // image flashed successfully
    PRC_REC_FAIL = 0xFF
}record_status_t;

typedef enum {
    VAC_TIMEOUT_5_SEC = 0,
    VAC_TIMEOUT_15_SEC = 1,
    VAC_TIMEOUT_30_SEC = 2,
    VAC_TIMEOUT_1_MIN = 3,
    VAC_TIMEOUT_5_MIN = 4,
    VAC_TIMEOUT_10_MIN = 5,
    VAC_TIMEOUT_15_MIN = 6,
    VAC_TIMEOUT_20_MIN = 7,
    VAC_TIMEOUT_30_MIN = 8,
    VAC_TIMEOUT_45_MIN = 9,
    VAC_TIMEOUT_60_MIN = 10
}vacancy_timeout_t;

typedef enum {
    PIR_SENSITIVITY_50 = 0,
    PIR_SENSITIVITY_100 = 1
}pir_sensitivity_t;

typedef enum {
    HF_SENSITIVITY_25 = 0,
    HF_SENSITIVITY_50 = 1,
    HF_SENSITIVITY_75 = 2,
    HF_SENSITIVITY_100 = 3
}hf_sensitivity_t;

typedef enum {
    TRIGGER_LOGIC_BOTH = 0,
    TRIGGER_LOGIC_PIR = 1,
    TRIGGER_LOGIC_HF = 2
}trigger_logic_t;

// data structure in firmware update
typedef struct{

    uint8_t productID[10];
    uint8_t skew[10];
    uint8_t version[7]; // XX.XX.XXX major.minor.build
    uint8_t date[6];   // date of update 31117 is Apr 11 2017
    uint8_t time[6];   // time of update 164801 is 4:48:01 PM
    uint8_t unused_arr[23]; // unused
} firm_ascii_data_t;

typedef struct{
    uint8_t start_address[4];
    uint8_t end_address[4];
    uint8_t record_count[4];
    uint8_t not_used[2];
}firm_numdata_t;

typedef struct{
    firm_ascii_data_t ascii_info;
    firm_numdata_t num_info;
}firm_update_header;

// for use when initializing a const struct see enum type defined in int_prot_frame.h
#define disc 0
#define DO_TYPE   1
#define AIDI_TYPE 3
typedef struct{
	uint8_t uid[UID_LENGTH];
	uint8_t cur_address;
	uint8_t type;
	uint8_t state;
}mod_info; // note there are 2 mod_info type of structures one with a version array and one without
           // the flash structure does not save the version array


// firmware info
typedef struct{
    uint8_t crc_loc[2];
    firm_update_header fw_info;
    uint8_t device_id;
    uint8_t fw_status;
}fw_update_flash_struct;

// application firmware info
typedef struct{
    uint8_t crc_loc[2];
    firm_update_header fw_info;
    uint8_t fw_status;
    mod_info mdle_info;
}fw_update_app_flash_struct;


typedef struct{
    fw_update_flash_struct btldr; // starts at 0x00
    uint8_t unused[176];
    fw_update_app_flash_struct app;   // starts at 0x100
}sw_info_struct;

#if 1
typedef struct{
    uint8_t vacancy_timeout;        // 0-4 {0x0=5s,15s,30s,1min,5min,10min,15min,20min,30min,45min,60min}
    uint8_t pir_sensitivity;        // 5   {0x0=50%, 100%}
    uint8_t hf_sensitivity;         // 6-7 {0x0=25%, 50%, 75%, 100%}
    uint8_t trigger_logic;          // 8-9 {0x00=Both, PIR, HF, Both}
    uint8_t walkthrough_mode;       // 10  {0=No, 1=yes}
    uint8_t wink_led_time;          // 0-255 sec
  }occ_config_struct;
#else
typedef struct{
    uint32_t vacancy_timeout:4;      // 0-4 {0x0=5s,15s,30s,1min,5min,10min,15min,20min,30min,45min,60min}
    uint32_t pir_sensitivity:1;      // 5   {0x0=50%, 100%}
    uint32_t hf_sensitivity:2;       // 6-7 {0x0=25%, 50%, 75%, 100%}
    uint32_t trigger_logic:2;        // 8-9 {0x00=Both, PIR, HF, Both}
    uint32_t walkthrough_mode:1;     // 10  {0=No, 1=yes}
    uint8_t wink_led_time;           // 0-255 sec
  }__attribute__((packed)) occ_config_struct;
#endif

BOOL write_info_to_flash(sw_info_struct *ptr_to_sw_struct);
BOOL write_info_to_config(occ_config_struct *ptr_to_config_struct);
record_status_t write_header_to_flash(uint8_t update_status);
void initialize_bootloader_info(void);
void init_data_memory(void);
void read_config_memory(occ_config_struct *occ_config_ptr);
#endif /* FLASH_DATA_H_ */
