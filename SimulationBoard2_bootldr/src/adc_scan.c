//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  adc_scan.c
//
//  Description: Functions that scan the ADC and PWM output.
//
//-----------------------------------------------------------------------------

// #include "adc_thread.h"
//#include "relay_state.h"
#include "hw_io_api.h"
//#include "db_api.h"
#include "critical_section.h"
#include "adc_scan.h"
#include "sys_config.h"
//#include "relay_thread_entry.h"
//#include "relay_thread.h"
#define TIMER_VALUE            1 // value passed as arg to callback
#define RELAY_TIMEOUT         20 // 20 * 10 msec timeout timer ticks = 200 msec timeout value
#define ADC_TIMEOUT           50 // 50 * 10 msec timeout timer ticks = 500 msec timeout value
#define ONE_SHOT               0 // one shot timer

TX_TIMER        adc_timer;
unsigned int    timer_status;
static BOOL     adc_timeout = FALSE;


static void   _adc_timeout_callback(unsigned long int expire_input);
static void   _set_adc_timer_expired(BOOL new_value);

#if 0
static BOOL _get_adc_timer_expired(void);
static BOOL _get_adc_timer_expired(void)
{
    BOOL meas_timeout;

    // needs to be atomic operation
    enter_critical_section();
    meas_timeout = adc_timeout;
    exit_critical_section();

    return(meas_timeout);
}
#endif




static void _set_adc_timer_expired(BOOL new_value)
{
    adc_timeout = new_value;
}


void _adc_timeout_callback(unsigned long int expire_input)
{
    // This is to avoid compiler complaining about the parameter
    // not used.
    SSP_PARAMETER_NOT_USED(expire_input);

    // timer expired put data in data base
    _set_adc_timer_expired(TRUE);
}



// updates the database values for AN1, AN2, AN3, AN4, 24V_MON
// every scan
void adc_callback(adc_callback_args_t * p_args)
{
    SSP_PARAMETER_NOT_USED(p_args);
}


//--------------------------------------------------------------------------------
// NAME      : adc_test_rs485bus_eol
// ABSTRACT  : This API reads the designated pin on the relay board to determine
//             if this module is the end of line on the RS485 bus.
// ARGUMENTS : (none)
// RETURN    :
//   BOOL    : TRUE if it is the EOL, or FALSE otherwise
//--------------------------------------------------------------------------------
BOOL adc_test_rs485bus_eol(void)
{
    ioport_level_t port_status;

    g_ioport.p_api->pinRead(RS485_EOL_POWER_PIN, &port_status);

    if (port_status != IOPORT_LEVEL_HIGH)
    {
        return (FALSE);
    }

    return (TRUE);
}

//------------------------------------------------------------------------------
// Name:        adc_set_pwm_init_level
// Abstract:    This function sets the initial analog output value be setting
//              the PWM level for all analog channels.
// Arguments:   (none)
// Return:      (none)
//------------------------------------------------------------------------------
void adc_set_pwm_init_level(void)
{

    g_timer4_pwm.p_api->dutyCycleSet(g_timer4_pwm.p_ctrl,0,TIMER_PWM_UNIT_PERCENT,0); // PWM 1

}


//------------------------------------------------------------------------------
// Name:        adc_create_adc_thx_timer
// Abstract:    Create a ThreadX timer which will timeout when no pulses are
//              measured by the input capture hardware.
// Arguments:   (none)
// Return:
//   STATUS_E:  SUCCESS, if successful;
//              ERR_HW_ADC_TIMER_INIT, if error occurred.
//------------------------------------------------------------------------------
STATUS_E adc_create_adc_thx_timer(void)
{

    if (tx_timer_create(&adc_timer,
                        "adc_Timeout",
                        _adc_timeout_callback,
                        TIMER_VALUE,
                        ADC_TIMEOUT,
                        ONE_SHOT,
                        TX_NO_ACTIVATE) != SUCCESS)
    {
        return ERR_HW_ADC_TIMER_INIT;
    }

    return SUCCESS;
}


//------------------------------------------------------------------------------
// Name:        adc_init_timer
// Abstract:    This function initializes SSP timer_1 and timer_4 for PWM
//              cycle counting.
// Arguments:   (none)
// Return:
//   STATUS_E:  SUCCESS, if successful;
//              ERR_HW_ADC_TIMER_INIT, if error occurred.
//------------------------------------------------------------------------------
STATUS_E adc_init_hw_timer(void)
{

    // gpt 4
    if (g_timer4_pwm.p_api->open(g_timer4_pwm.p_ctrl, g_timer4_pwm.p_cfg) != SSP_SUCCESS)
    {
        return ERR_HW_ADC_TIMER_INIT;
    }

    return SUCCESS;
}


//------------------------------------------------------------------------------
// Name:        adc_init_adc_channel
// Abstract:    This function initializes ADC channel for analog output
//              feedback scanning.
// Arguments:   (none)
// Return:
//   STATUS_E:  SUCCESS, if successful;
//              ERR_HW_ADC_CHNNL_INIT, if error occurred.
//------------------------------------------------------------------------------
STATUS_E adc_init_adc_channel(void)
{
    if (g_adc0.p_api->open(g_adc0.p_ctrl, g_adc0.p_cfg) != SSP_SUCCESS)
    {
        return ERR_HW_ADC_CHNNL_INIT;
    }

    if (g_adc0.p_api->scanCfg(g_adc0.p_ctrl,g_adc0.p_channel_cfg) != SSP_SUCCESS)
    {
        return ERR_HW_ADC_CHNNL_INIT;
    }

    return SUCCESS;

}
