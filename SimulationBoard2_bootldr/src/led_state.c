//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  relay_state.c
//
//  Description: Contains the relay state machines
//
//-----------------------------------------------------------------------------
#include "jellyfish_common.h"
#include "common_def.h"
#include "led_state.h"
#include "switch_state.h"
#include "hal_data.h"
#include "critical_section.h"

static led_context_t led_1 = {LED_STATE_OFF, LED_STATE_OFF};
static led_context_t led_2 = {LED_STATE_OFF, LED_STATE_OFF};
static led_context_t led_3 = {LED_STATE_OFF, LED_STATE_OFF};
static led_context_t led_4 = {LED_STATE_OFF, LED_STATE_OFF};
static led_context_t led_5 = {LED_STATE_OFF, LED_STATE_OFF};
static led_context_t led_6 = {LED_STATE_OFF, LED_STATE_OFF};
static led_context_t led_7 = {LED_STATE_OFF, LED_STATE_OFF};
static led_context_t led_8 = {LED_STATE_OFF, LED_STATE_OFF};

LED_STATES_E getLEDState(LED_IDS_E led_id)
{
    LED_STATES_E curr_led_state = LED_STATE_OFF;
    led_context_t *led_context = NULL;

    switch (led_id)
    {
    case LED_1:
        led_context = &led_1;
        break;
    case LED_2:
        led_context = &led_2;
        break;
    case LED_3:
        led_context = &led_3;
        break;
    case LED_4:
        led_context = &led_4;
        break;
    case LED_5:
        led_context = &led_5;
        break;
    case LED_6:
        led_context = &led_6;
        break;
    case LED_7:
        led_context = &led_7;
        break;
    case LED_8:
        led_context = &led_8;
        break;
    default:
        // no such relay
        // todo add error case
        break;
    }
    if (led_context != NULL)
    {
      // atomic operation - need exclusive access here while copying the state OK
      //enter_critical_section();
      curr_led_state = led_context->led_old_state;
      //exit_critical_section();
    }
    return(curr_led_state);
}
// toggle LED state
// OFF -> ON, or ON -> OFF
//
LED_STATES_E toggleLED(MANUAL_SWITCH_ID_E switch_id)
{
    LED_STATES_E current_led_state;
    // toggle the current LED state
    current_led_state = getLEDState(switch_id);
    //printf("switch toggle\n\r");

    if (current_led_state == LED_STATE_OFF)
    {
        // current LED state OFF toggle to ON
        //printf("current LED state OFF toggle to ON\r\n");
        set_led_state(switch_id, LED_STATE_ON);
    }
    else if (current_led_state == LED_STATE_ON)
    {
        //printf("current LED state ON toggle to OFF\n\r");
        set_led_state(switch_id, LED_STATE_OFF);
    }
    else
    {
        //printf("ignore manual switch override command\n\r");
    }
    return(current_led_state);
}

// this is our api function
STATUS_E set_led_state(LED_IDS_E led_id, LED_STATES_E led_state)
{
    led_context_t *led_context = NULL;
    ioport_level_t led_port_level;

    if (led_state == LED_STATE_ON)
        led_port_level = IOPORT_LEVEL_HIGH;
    else
        led_port_level = IOPORT_LEVEL_LOW;

    switch (led_id)
    {
    case LED_1:
        led_context = &led_1;
        g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_07, led_port_level); // LED 0
        break;
    case LED_2:
        led_context = &led_2;
        g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_13, led_port_level); // LED 1
        break;
    case LED_3:
        led_context = &led_3;
        g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_11, led_port_level); // LED 2
        break;
    case LED_4:
        led_context = &led_4;
        g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_10, led_port_level); // LED 3
        break;
    case LED_5:
        led_context = &led_5;
        g_ioport.p_api->pinWrite(IOPORT_PORT_01_PIN_09, led_port_level); // LED 4
        break;
    case LED_6:
        led_context = &led_6;
        g_ioport.p_api->pinWrite(IOPORT_PORT_03_PIN_01, led_port_level); // LED 5
        break;
    case LED_7:
        led_context = &led_7;
        g_ioport.p_api->pinWrite(IOPORT_PORT_03_PIN_02, led_port_level); // LED 6
        break;
    case LED_8:
        led_context = &led_8;
        g_ioport.p_api->pinWrite(IOPORT_PORT_03_PIN_03, led_port_level); // LED 7
        break;
    default:
        // no such relay
        // todo add error case
        break;
    }

    led_context->led_old_state = led_state;

    return(SUCCESS);
}
