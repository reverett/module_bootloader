//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016-2017
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  version.h
//
//  Description: This file is used to document the version history.
//
//  VERSION HISTORY
//  ---------------
//   Version    Date       Changes and GIT Commits
//  ---------------------------------------------------------------------
//   00.01.001 2017.01.20  Initial release for HW Team Critical Test.
//             2017.01.26  Created initial BitBucket repository
//             2017.02.10  Use to create new repository,
//             2017.02.10  Commit 2c770ff
//
//   00.02.001 2017.01.22  Fine tune RX/TX timing.
//                         Added code to fetch database tables.
//             2017.02.02  Pushed into initial repository
//             2017.02.10  Merged into new repository
//             2017.02.10  Commit c6a8805
//
//   00.02.002 2017.02.10  Merge the ADC thread code into Relay thread.
//             2017.02.10  Commit 3d1c808
//
//   00.02.003 2017.02.15  Created version.h file to document releases.
//                         Removed the screen clearing in control thread
//                         starting code.
//                         Commit d0f20a9, 9cdd950, dae83da, ce6cf2c, 0ae0590
//
//   00.02.004 2007.02.16  Changed the pull-up resister associated with RS485
//                         RxD (S128 board pin 104) from default "None" to
//                         default "Pull-up". This is intended to help reduce
//                         noise. It is done via the BSP configuration .xml
//                         file.
//                         Added compiler option macro DOAO_CRITICAL_TEST
//                         to let builder select the default module address
//                         so that the source file does not have to modified
//                         during build for critical test and changed back
//                         when checking in code.
//
//  Note, the "GIT Commit" number is only available after the push-up to the
//        BitBucket. So it is added afterward.
//-----------------------------------------------------------------------------

#ifndef __VERSION_H__
#define __VERSION_H__

#define FIRMWARE_VERSION_MAJOR      "00"
#define FIRMWARE_VERSION_MINOR      "02"
#define FIRMWARE_VERSION_BUILD      "004"
#define FIRMWARE_BUILT_YEAR         "2017"  // 02/15/2017
#define FIRMWARE_BUILT_MONTH        "02"
#define FIRMWARE_BUILT_DAY          "16"
#define FIRMWARE_BUILT_HOUR         "09"    // 9:59:00 am
#define FIRMWARE_BUILT_MINUTE       "59"    //
#define FIRMWARE_BUILT_SECOND       "00"




#endif // __VERSION_H__
