//-----------------------------------------------------------------------------
//
//  JellyFish Project
//
//  Copyright (c) Intermatic, Inc. 2016
//  All rights reserved. No part of this software may be disclosed or
//  distributed in any form or by any means without the prior written
//  consent of Intermatic.
//
//-----------------------------------------------------------------------------
//
//  switch_state.c
//
//  Description: Contains the switch state machines.
//
//-----------------------------------------------------------------------------

//#include "hal_data.h"
#include "jellyfish_common.h"
#include "common_def.h"
#include "switch_state.h"
#include "critical_section.h"
#include "led_state.h"
#include "hw_thread.h"
#include "occ_sensor.h"
#include "db_api.h"

#define GPT_DELAY 5 // milliseconds
#define DEBOUNCE_MAKE_DELAY 200 //10 // 10 milliseconds
#define DEBOUNCE_BREAK_DELAY 200 //20 // 20 milliseconds

#define MAKE_COUNT DEBOUNCE_MAKE_DELAY/GPT_DELAY
#define BREAK_COUNT DEBOUNCE_BREAK_DELAY/GPT_DELAY

#define OCC_TIME_INTERVAL 1000 // 1000 milliseconds = 1 sec
#define OCC_COUNT OCC_TIME_INTERVAL/GPT_DELAY

static pb_switch_context_t switch_1 = {0, STATE_BREAK, STATE_BREAK};
static pb_switch_context_t switch_2 = {0, STATE_BREAK, STATE_BREAK};
static pb_switch_context_t switch_3 = {0, STATE_BREAK, STATE_BREAK};
static pb_switch_context_t switch_4 = {0, STATE_BREAK, STATE_BREAK};
static pb_switch_context_t switch_5 = {0, STATE_BREAK, STATE_BREAK};
static pb_switch_context_t switch_6 = {0, STATE_BREAK, STATE_BREAK};
static pb_switch_context_t switch_7 = {0, STATE_BREAK, STATE_BREAK};
static pb_switch_context_t switch_8 = {0, STATE_BREAK, STATE_BREAK};


void switch_callback(timer_callback_args_t * p_args)
{
    static unsigned int timer_interval;

    SSP_PARAMETER_NOT_USED(p_args);

    if (timer_interval < OCC_COUNT)
    {
        timer_interval = timer_interval + 1;
    }
    else
    {
        incr_occ_timer();
        timer_interval = 0;
    }

    stateSwitch(SWITCH_1);
    stateSwitch(SWITCH_2);
    stateSwitch(SWITCH_3);
    stateSwitch(SWITCH_4);
    stateSwitch(SWITCH_5);
    stateSwitch(SWITCH_6);
    stateSwitch(SWITCH_7);
    stateSwitch(SWITCH_8);
}

// call this to get the current state of the switch
SW_DB_STATE_E getSwitchState(MANUAL_SWITCH_ID_E switch_id)
{
    SW_DB_STATE_E curr_state;
    pb_switch_context_t *switch_context = NULL;

    switch (switch_id)
    {
        case SWITCH_1:
            switch_context = &switch_1;
            break;
        case SWITCH_2:
            switch_context = &switch_2;
            break;
        case SWITCH_3:
            switch_context = &switch_3;
            break;
        case SWITCH_4:
            switch_context = &switch_4;
            break;
        case SWITCH_5:
            switch_context = &switch_5;
            break;
        case SWITCH_6:
            switch_context = &switch_6;
            break;
        case SWITCH_7:
            switch_context = &switch_7;
            break;
        case SWITCH_8:
            switch_context = &switch_8;
            break;
        default:
            break;
    }

    curr_state = STATE_BREAK;
    if (switch_context != NULL)
    {
        // atomic need to have exclusive access here while copying
        enter_critical_section();
        curr_state = switch_context->old_sw_state;
        exit_critical_section();
    }
    // todo add error condition for NULL case
    return(curr_state);
}

// run this routine every 5 msec for each switch from GPT interrupt
void stateSwitch(MANUAL_SWITCH_ID_E switch_id)
{
    SW_DB_STATE_E switch_new;
    ioport_level_t sw_status = 0;
    pb_switch_context_t *switch_context = NULL;

    switch (switch_id)
    {
        case SWITCH_1:
        {
            g_ioport.p_api->pinRead(IOPORT_PORT_00_PIN_04, &sw_status);
            switch_context = &switch_1;
        }
            break;
        case SWITCH_2:
        {
            g_ioport.p_api->pinRead(IOPORT_PORT_00_PIN_12, &sw_status);
            switch_context = &switch_2;
        }
            break;
        case SWITCH_3:
        {
            g_ioport.p_api->pinRead(IOPORT_PORT_00_PIN_13, &sw_status);
            switch_context = &switch_3;
        }
            break;
        case SWITCH_4:
        {
            g_ioport.p_api->pinRead(IOPORT_PORT_00_PIN_14, &sw_status);
            switch_context = &switch_4;
        }
            break;
        case SWITCH_5:
        {
            g_ioport.p_api->pinRead(IOPORT_PORT_05_PIN_00, &sw_status);
            switch_context = &switch_5;
        }
            break;
        case SWITCH_6:
        {
            g_ioport.p_api->pinRead(IOPORT_PORT_05_PIN_01, &sw_status);
            switch_context = &switch_6;
        }
            break;
        case SWITCH_7:
        {
            g_ioport.p_api->pinRead(IOPORT_PORT_05_PIN_02, &sw_status);
            switch_context = &switch_7;
        }
            break;
        case SWITCH_8:
        {
            g_ioport.p_api->pinRead(IOPORT_PORT_00_PIN_15, &sw_status);
            switch_context = &switch_8;
        }
            break;
        default:
            // todo add error condition
            break;
    }
    if (sw_status)
        switch_new = STATE_BREAK; // pullup on input 1 = switch open, input 0 = switch closed
    else
        switch_new = STATE_MAKE;

    if (switch_context != NULL)
    {
        switch (switch_context->old_sw_state)
        {
            case STATE_MAKE:
            {
                if (switch_new == switch_context->old_sw_state)
                {
                    switch_context->sw_count = BREAK_COUNT; // reset the counter
                }
                else
                {
                    switch_context->sw_count = switch_context->sw_count - 1;
                    if (switch_context->sw_count <= 0) // switch is stable now
                    {
                        switch_context->old_sw_state = STATE_BREAK;
                        switch_context->sw_count = MAKE_COUNT;
                    }
                }
            }
            break;
            case STATE_BREAK:
            {
                if (switch_new == switch_context->old_sw_state)
                {
                    switch_context->sw_count = MAKE_COUNT;
                }
                else
                {
                    switch_context->sw_count = switch_context->sw_count - 1;
                    if (switch_context->sw_count <= 0)
                    {
                        switch_context->old_sw_state = STATE_MAKE;
                        switch_context->sw_count = BREAK_COUNT;
                    }
                }
            }
            break;
            default:
            {
                switch_context->sw_count = MAKE_COUNT;
                switch_context->old_sw_state = STATE_BREAK;
            }
            break;
        }

        // detect rising edge from BREAK to MAKE state
        {
            SW_DB_STATE_E curr_switch_state;

            curr_switch_state = switch_context->old_sw_state;

            // detect change in state rising edge (BREAK to MAKE) and store into data base
            if ((switch_context->prev_sw_state == STATE_BREAK)&&(curr_switch_state != switch_context->prev_sw_state))
            {
                switch (switch_id)
                {
                    case SWITCH_1:
                    {
                        reset_occ_timer(); // room is occupied
                    }
                    break;
                    case SWITCH_2:
                    case SWITCH_3:
                    case SWITCH_4:
                    case SWITCH_5:
                    case SWITCH_6:
                    case SWITCH_7:
                    {
                        toggleLED((MANUAL_SWITCH_ID_E)switch_id);
                    }
                    break;
                    case SWITCH_8:
                    {
                        // set event in data base
                        toggleLED((MANUAL_SWITCH_ID_E)switch_id);
                        db_vt_set_provsion_switch_state(ON);
                    }
                    break;
                    default:
                    break;
                 }

             }
            switch_context->prev_sw_state = curr_switch_state;
        }
    }

}
